<?php

$idioma = array();

$idioma['ver_en_mapa'] = 'Ver en mapa';
$idioma['agenda_de_eventos'] = 'Agenda de eventos';
$idioma['rutas_de_vino_mallorca'] = 'Rutas del vino';
$idioma['mensaje_evento_pasado'] = 'Este evento ya se celebró. Si buscas actividades relacionadas con vino te recomendamos los siguientes planes: ';

return $idioma;