<?php

namespace App\Http\Controllers\Users;

use App\User;
use App\UsersLangs;
use App\UsersPhotos;
use Illuminate\Http\Request;
use App\Traits\Helpers;
use Intervention\Image\ImageManagerStatic as Image;
use Validator;

use Illuminate\Support\Facades\Hash;

class UsersController extends \App\Http\Controllers\Controller
{
    use Helpers;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function _index()
    {
        return User::all();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function _all()
    {
        return User::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function _store($request)
    {
        $validation = Validator::make($request, self::_get_rules($request), self::_get_messages());

        if ($validation->fails()) {
            return redirect()->route('back.users.create')->withErrors($validation)->withInput();
        }

        $iduser = parent::_insert_user($request, false);

        if($iduser != null){
            $tmp = User::find($iduser);

            self::_insert_langs($request, $tmp);

            if(isset($request['p_foto'])){
                self::_insert_images($request, $tmp);
            }

            if(isset($request['logo'])){
                self::_insert_logo($request, $tmp);
            }
            return $tmp;
        }
        return null;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _show(User $user)
    {
        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _update($request, User $user)
    {
        $validation = Validator::make($request, self::_get_rules($request), self::_get_messages());

        if ($validation->fails()) {
            return redirect()->route('back.users.show', $user->id)->withErrors($validation)->withInput();
        }

        if($user->name != $request['name']){
            $user->name = $request['name'];
            $user->slug = $this->getSlug($request['name'], 'slug', \App\User::getModel());
        }
        $user->email = $request['email'];

        if(isset($request['password'])){
            $user->password = Hash::make($request['password']);
        }
        $user->role = isset($request['role']) ? $request['role'] : 'user';
        $user->locations_id = $request['locations_id'];
        $user->districts_id = parent::_insert_district($request, $user);
        $user->address = $request['address'];
        $user->postal_code = $request['postal_code'];
        $user->phone = $request['phone'];
        $user->website = $request['website'];
        $user->active = $request['active'];
        $user->facebook_url = isset($params['facebook_url']) ? $params['facebook_url'] : null;
        $user->instagram_url = isset($params['instagram_url']) ? $params['instagram_url'] : null;
        $user->latitude = isset($request['latitude']) ? $request['latitude'] : null;
        $user->longitude = isset($request['longitude']) ? $request['longitude'] : null;
        if(isset($request['role']) && $request['role'] == 'company'){
            $user->company_name = $request['name'];
        }

        $user->save();

        self::_edit_langs($request, $user);

        if(isset($request['p_foto'])){
            self::_insert_images($request, $user);
        }

        if(isset($request['logo'])){
            self::_insert_logo($request, $user);
        }


        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _destroy(User $user)
    {
        $ruta = 'upload/users/'.$user->id.'/';
        if(is_dir($ruta)){
            $success = \File::deleteDirectory($ruta, true);
            rmdir($ruta);
            if(!$success){
                throw new \Exception('El directorio de imagenes de '.$user->id.' no existe');
            }
        }

        $user->delete();

        return true;
    }

    private function _insert_images($request, $user){
        $ruta = 'upload/users/'.$user->id.'/';
        \File::makeDirectory($ruta, 0775, true, true);

        foreach($request['p_foto'] as $foto){
            $tmp = new UsersPhotos;
            $tmp->order = 100;
            $tmp->users_id = $user->id;
            $tmp->save();

            $tmp->url = $ruta . $user->slug . "-" . $tmp->id . ".webp";
            $tmp->save();

            $img = Image::make($foto)->encode('webp', 90);
            $img->resize(1024, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($tmp->url);
            unlink($foto);
            $this->set_72dpi_image($tmp->url);
        }
    }

    private function _insert_logo($request, $user){
        $ruta = 'upload/users/'.$user->id.'/';
        \File::makeDirectory($ruta, 0775, true, true);

        $user->logo = $ruta . $user->slug . "_logo.webp";
        $user->save();

        $img = Image::make($request['logo'])->encode('webp', 90);
        $img->resize(400, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($user->logo);
        $this->set_72dpi_image($user->logo);
    }

    private function _insert_langs($request, $tmp){
        foreach(config('translatable.locales') as $keylocale => $locale){
            $lang = new UsersLangs;
            $lang->locale = $keylocale;
            $lang->users_id = $tmp->id;

            $lang->seo_description = isset($request['seo_description'][$keylocale]) ? 
                $request['seo_description'][$keylocale] : null;
            $lang->seo_keywords =  isset($request['seo_keywords'][$keylocale]) ? 
                $request['seo_keywords'][$keylocale] : null;
            $lang->description =  isset($request['description'][$keylocale]) ? 
                $request['description'][$keylocale] : null;
            $lang->save();
        }
    }

    private function _edit_langs($request, $tmp){
        foreach($tmp->langs as $lang){
            if(isset($request['title'][$lang->locale])){
                $lang->title = $request['title'][$lang->locale];
            }
            if(isset($request['seo_description'][$lang->locale])){
                $lang->seo_description = $request['seo_description'][$lang->locale];
            }
            if(isset($request['seo_keywords'][$lang->locale])){
                $lang->seo_keywords = $request['seo_keywords'][$lang->locale];
            }
            if(isset($request['description'][$lang->locale])){
                $lang->description = $request['description'][$lang->locale];
            }
            $lang->save();
        }
    }

    private function _get_rules($request){
        $rules = array();
        if(isset($request['users_id'])){
            $rules['users_id'] = 'required|max:250';
        }else{
            $rules['name'] = 'required|max:250';
        }

        $rules['locations_id'] = 'required|numeric';

        if(isset($request['role']) && $request['role'] == 'company'){
            $rules['role'] = 'required';
            //$rules['description.es'] = 'required|max:2000';
        }
        $rules['districts_id'] = 'required';

        $rules['address'] = 'required|max:250';
        $rules['postal_code'] = 'required|numeric|digits_between:4,5';
        $rules['phone'] = 'nullable|numeric|digits_between:6,15';
        $rules['email'] = 'required|email|max:250';

        return $rules;
    }
    
    private function _get_messages(){
        $messages = array();
        $messages['users_id.required'] = 'Se debe rellenar el nombre';
        $messages['users_id.max'] = 'El nombre es demasiado largo';
        $messages['name.required'] = trans('item.validation_name_required');
        $messages['name.max'] = trans('item.validation_name_max');

        $messages['locations_id.numeric'] = 'Hay un error con la provincia';
        $messages['role.required'] = 'El rol es obligatorio';
        $messages['districts_id.required'] = 'La población es obligatoria';
        $messages['districts_id.numeric'] = 'Hay un error con la población';
        $messages['address.required'] = trans('order.validation_address_required');
        $messages['address.max'] = trans('order.validation_address_max');
        $messages['postal_code.required'] = trans('order.validation_postal_code_required');
        $messages['postal_code.numeric'] = trans('order.validation_postal_code_numeric');
        $messages['postal_code.max'] = trans('order.validation_postal_code_max');
        $messages['phone.numeric'] = trans('order.validation_phone_numeric');
        $messages['phone.max'] = trans('order.validation_phone_max');
        $messages['email.required'] = trans('order.validation_email_required');
        $messages['email.email'] = trans('order.validation_email_email');
        $messages['email.max'] = trans('order.validation_email_max');

        $messages['description.es.required'] = __('item.validation_description_required', [], 'es');
        $messages['description.es.max'] = __('item.validation_description_max', [], 'es');
        return $messages;
    }
}
