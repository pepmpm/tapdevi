@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section ('body')
@include('plans._breadcrumbs')

<div class="content container">
    <div class="row">
        <div class="col-md-12">
            <div class="row margin-bottom-5">
                <div class="col-sm-8 result-category">
                    <h2>{{ trans('inicio.enotourism-mallorca') }}</h2>
                </div>
            </div>
            <div class="row news-v1 margin-bottom-20">
                <div class="col-md-12">
                    <div class="alert alert-info fade in">{{ trans('plan.mensaje_comentario') }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section ('assets_end')
    <link rel="stylesheet" href="{{ asset('assets/css/pages/timeline2.css') }}">
    
    <script type="text/javascript" src="{{ asset('js/plans.js') }}"></script>
@stop
