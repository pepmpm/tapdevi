@php($company = $item->user)
<div class="content">
    <table style="width: 100%">
        <tr>
            <td style="background-color: white; height: 460px; width: 50%; padding: 20px; vertical-align:top">
                <h5 style="margin-bottom: 30px">{!! trans('mail.mensaje_qr', [], null, $locale) !!}</h5>
                <img src="{{ \Image::make(QrCode::size(351)->format('png')
                    ->generate(route('orders.confirm_visit', 
                        ['order_encrypted' => base64_encode($order->id)])))->fit(250, 250)
                        ->encode('png', 90)->encode('data-url') }}">
            </td>
            <td style="background-color: #474747; height: 460px; width: 50%; vertical-align:top; padding: 10px; color: white;">
                <p>
                    <img src="{{ \Image::make('assets/img/logo_200.png')->encode('data-url') }}"/>
                </p>
                <h3>{{ trans('order._CODIGO_', [], null, $locale) }}</h3>
                <p style="font-size: 18px">{{ $order->code }}</p>

                @if($order->is_gift == 'N')
                    <h3>{{ trans('order._ACTIVIDAD_', [], null, $locale) }}</h3>
                @else
                    <h3>{{ trans('order._VALE_POR_', [], null, $locale) }}</h3>
                @endif
                <ul class="list-unstyled">
                    <li>{{ $item->name }}</li>
                    <li>{{ $order->quantity . " " . $item->get_unit_name() }}</li>
                    @if($order->is_gift == 'N')
                        <li><strong>{{ trans('mail.precio_total', [], null, $locale) }}:</strong> {{ $order->price . " €" }}</li>
                        @if($order->coupons_id != null)
                            @php($coupon = $order->coupon)
                            <li><strong>{{ trans('order.descuento_aplicado', [], null, $locale) }}:</strong> {{ "- " . $coupon->discount . "%" }}</li>
                        @endif
                        @if($order->pay_the_day > 0)
                            <li><strong>{{ trans('mail.a_pagar_el_dia', [], null, $locale) }}:</strong> {{ $order->pay_the_day }}</li>
                        @endif
                    @else
                        @if($order->gift_recipient != null)
                            <h3>{{ trans('order._PARA_QUIEN_', [], null, $locale) }}</h3>
                            <ul class="list-unstyled">
                                <li><strong>{{ trans('mail.nombre', [], null, $locale) }}:</strong> {{ $order->gift_recipient }}</li>
                            </ul>
                        @endif

                        @if($order->gift_remarks != null)
                            <li style="margin-top: 20px"><strong>{{ $order->gift_remarks }}</strong></li>
                        @else
                            <li style="margin-top: 20px"><strong>Molts d'anys!!</strong></li>
                        @endif
                    @endif
                    <li style="font-size: 12px; text-align: right">{{ trans('mail.valido_hasta_el_', ['day' => $helpers->fec_mysql_to_str($helpers->sumardias($order->created_at, 365))], null, $locale) }}</li>
                </ul>
            </td>
        </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td style="background-color: #474747; height: 460px; width: 50%; vertical-align:top; padding: 10px; color: white;">
                <h3>{{ trans('order._DE_QUIEN_', [], null, $locale) }}</h3>
                <ul class="list-unstyled">
                    @if($order->gift_name != null)
                        <li><strong>{{ trans('mail.nombre', [], null, $locale) }}:</strong> {{ $order->gift_name }}</li>
                    @else
                        <li><strong>{{ trans('mail.nombre', [], null, $locale) }}:</strong> {{ $order->name }}</li>
                    @endif
                    <li><strong>{{ trans('mail.email', [], null, $locale) }}:</strong> {{ $order->email }}</li>
                    @if($order->phone != null)
                        <li><strong>{{ trans('mail.telefono', [], null, $locale) }}:</strong> {{ $order->phone }}</li>
                    @endif
                    @if($order->remarks != null)
                        <tr>
                            <td colspan="2">
                                <strong>{{ trans('mail.comentarios', [], null, $locale) . ": " }}</strong>{{ $order->remarks }}
                            </td>
                        </tr>
                    @endif
                </ul>
                <h3>{{ trans('order._DONDE_', [], null, $locale) }}</h3>
                <ul class="list-unstyled">
                    <li><strong>{{ trans('mail.lugar', [], null, $locale) . ": " }}</strong>{{ $company->name }}</li>
                    <li><strong>{{ trans('mail.email', [], null, $locale) . ": " }}</strong>{{ $company->email }}</li>
                    @if($company->phone != null)
                        <li><strong>{{ trans('mail.telefono', [], null, $locale) . ": " }}</strong>{{ $company->phone }}</li>
                    @endif
                    @if($company->districts_id != null && $company->locations_id != null)
                        <li><strong>{{ trans('mail.poblacion', [], null, $locale) . ": " }}</strong>{{ $company->district->name . " (" . $company->location->name . ")" }}</li>
                    @endif
                    @if($company->address != null)
                        <li><strong>{{ trans('mail.direccion', [], null, $locale) . ": " }}</strong>{{ $company->address . ", " . $company->postal_code }}</li>
                    @endif
                </ul>
            </td>
            <td style="background-color: white; height: 460px; width: 50%; vertical-align:top">
                <img src="https://maps.googleapis.com/maps/api/staticmap?center={{ $company->latitude }},{{ $company->longitude }}&zoom=13
                    &markers=size:mid%7Ccolor:0x54062E%7Clabel:%7C{{ $company->latitude }},{{ $company->longitude }}&size=351x505&key=AIzaSyBw_spE5_xC8pjOBJVWs5LHLBKn0EC-cfQ"
                    alt="{{ $company->districts_id != null ? $company->district->name : '' }}"
                    style="margin-top: 0px">
            </td>
        </tr>
    </table>
</div>