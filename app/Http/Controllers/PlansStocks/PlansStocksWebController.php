<?php

namespace App\Http\Controllers\PlansStocks;

use App\ItemsStocks;
use Illuminate\Http\Request;
use Cache;
use App\Traits\Helpers;

class PlansStocksWebController extends PlansStocksController
{

    use Helpers;

    public function get_time(Request $request){
        $stock = \App\ItemsStocks::where('items_id', '=', $request->get('items_id'))
            ->where('date', '=', $this->fec_str_to_mysql($request->get('date')))->first();

        if($stock && $stock->times != null){
            $times = array_filter(explode("#", $stock->times));

            $cadena = '<ul class="list-group">';
            $primero = true;
            foreach($times as $time){
                $cadena .= '<li class="list-group-item"><div class="radio">';
                $cadena .= '<label>';
                $cadena .= '<input type="radio" name="time" value="'.$time.'" '.($primero ? 'checked' : '').'>';
                $cadena .= $time;
                $cadena .= '</label>';
                $cadena .= '</div></li>';

                $primero = false;
            }

            return $cadena . '</ul>';
        }

        return null;
    }
}