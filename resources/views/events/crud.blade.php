@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section('title', $query->name)
@section ('meta')
    <meta name="description" content="{{ $query->seo_description }}"/>
    <meta name="keywords" content="{{ $query->seo_keywords }}" />
    <meta name="Author" content="{{ trans('meta.author') }}" />
    <meta property="og:title" content="{{ $query->name }}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:image" content="{{ url('/') . '/' . $query->one_photo('upload') }}"/>
    <meta property="og:description" content="{{ $query->seo_description }}"/>
    @parent
@stop

@section ('body')
@php($tmpdistrict = $query->district)
<div class="shop-product">
    <div class="container">
        <ul class="breadcrumb-v5">
            <li><a href="{{ route('/') }}"><i class="fa fa-home"></i></a></li>
            <li><a href="{{ route('enotourism-mallorca.cat') }}">{{ trans('inicio.enotourism-mallorca') }}</a></li>
            <li><a href="{{ route('enotourism-mallorca.dis', ['district' => $tmpdistrict->slug]) }}">{{ $tmpdistrict->name }}</a></li>
            <li class="active">
                {{ $query->name }}
            </li>
        </ul>
    </div>
    <div class="container content profile">
        <div class="row">
            <div class="col-md-9">
                @if(isset($is_past) && $is_past)
                    <div class="alert alert-warning fade in">
                        {{ trans('events.mensaje_evento_pasado') }}<a href="{{ route('enotourism-mallorca.cat') }}">CLICK</a>
                    </div>
                @endif
                <div class="profile-body">
                    <div class="profile-bio">
                        <div class="row">
                            <div class="col-md-5">
                                @if($query->photos->count() > 0)
                                    <a class="fancybox" data-rel="galeria_fotos"
                                        title="{{ $query->name }}"
                                        href="{{ url('/') . '/' . $query->photos->first()->url }}">
                                        @if($agent->isMobile())
                                            <img class="img-responsive margin-bottom-10" alt="{{ $query->name }}"
                                                src="{{ Imgfly::imgPublic(str_replace("upload/", "", $query->photos->first()->url) . 
                                                    '?w=344&h=350&fit=crop-center', 'upload') }}">
                                        @else
                                            <img class="img-responsive margin-bottom-10" alt="{{ $query->name }}"
                                                src="{{ Imgfly::imgPublic(str_replace("upload/", "", $query->photos->first()->url) . 
                                                    '?w=420&h=430&fit=crop-center', 'upload') }}">
                                        @endif
                                    </a>
                                @else
                                    <img src="{{ asset('assets/img/no-image.jpg') }}"/>
                                @endif
                            </div>
                            <div class="col-md-7">
                                <h2>{{ $query->name }}</h2>
                                <div class="row">
                                    <div class="col-md-12">
                                        <ul class="list-unstyled social-contacts-v2">
                                            <li><i class="rounded-x fa fa-map-marker"></i> {{ $query->district->name }}</li>
                                            <li><i class="rounded-x fa fa fa-eur"></i> {{ $query->eventprice }}</li>
                                        </ul>
                                        <hr>
                                        <p>{!! $query->description !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-7">
                            <div class="panel panel-profile margin-bottom-40">
                                <div class="panel-heading overflow-h">
                                    <h2 class="panel-title heading-sm pull-left"><i class="fa fa-calendar"></i> {{ trans('order._CUANDO_') }}</h2>
                                </div>
                                <div class="panel-body">
                                    <ul class="timeline-v2">
                                        @foreach($query->allstocks as $stock)
                                            <li class="equal-height-columns">
                                                <div class="cbp_tmtime equal-height-column">
                                                    <span>{{ $helpers->fec_mysql_to_dm($stock->date) }}</span>
                                                    <span>{{ trans('base.day_' . date("N", strtotime($stock->date))) }}</span>
                                                </div>
                                                <i class="cbp_tmicon rounded-x hidden-xs"></i>
                                                <div class="cbp_tmlabel equal-height-column">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h2 class="width100_">
                                                                @foreach(explode("#", $stock->times) as $time)
                                                                    {{ ($loop->first ? '' : ' | ') . $time }}
                                                                @endforeach
                                                            </h2>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="panel panel-profile margin-bottom-40">
                                <div class="panel-heading overflow-h">
                                    <h2 class="panel-title heading-sm pull-left"><i class="fa fa-map-marker"></i> {{ trans('order._DONDE_') }}</h2>
                                </div>
                                <div class="panel-body">
                                    <ul class="list-unstyled social-contacts-v2">
                                        <li><i class="rounded-x fa fa-map-marker"></i> {{ $query->address }}</li>
                                        <li><i class="rounded-x fa fa fa-envelope"></i> {{ $query->district->name . ' (' . $query->postal_code . ')' }}</li>
                                    </ul>
                                    <div id="map" class="height350"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                @if(count($index_events) > 0)
                    <div class="panel-group" id="accordion-v0">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" style="float: none">
                                    <a data-toggle="collapse" data-parent="#accordion-v1" href="#collapseOne">
                                        {{ trans('inicio.proximos_eventos') }}
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                </h2>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body padding-lr-10">
                                    @include('events._timeline', ['show_link_map' => false])
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="content container" id="comentarios">
        <div class="row">
            <div class="col-md-12">
                <div class="product-comment margin-bottom-40">
                    <div id="disqus_thread"></div>
                    <script>
                    var disqus_config = function () {
                        this.page.url = '{{ LaravelLocalization::getLocalizedURL('es', Request::url()) }}';
                        this.page.identifier = '{{ Route::currentRouteName() }}';
                    };
                    (function() {
                    var d = document, s = d.createElement('script');
                    s.src = 'https://https-www-tapdevi-com.disqus.com/embed.js';
                    s.setAttribute('data-timestamp', +new Date());
                    (d.head || d.body).appendChild(s);
                    })();
                    </script>
                    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>                            
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section ('assets_end')
    <link rel="stylesheet" href="{{ asset('assets/css/pages/profile.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pages/timeline2.css') }}">

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBw_spE5_xC8pjOBJVWs5LHLBKn0EC-cfQ"></script>
    <script src="{{ asset('assets/plugins/gmap/gmap.js') }}"></script>

    <script src="{{ asset('assets/plugins/fancybox/source/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/plugins/fancybox/source/jquery.fancybox.css') }}">

    <script type="text/javascript">
        $(function(){
            if($(".fancybox").length){
                jQuery(".fancybox").fancybox({
                    groupAttr: 'data-rel',
                    prevEffect: 'fade',
                    nextEffect: 'fade',
                    openEffect  : 'elastic',
                    closeEffect  : 'fade',
                    closeBtn: true,
                    helpers: {
                        title: {
                            type: 'float'
                        }
                    }
                });
            }
        });
        var map;
        var marker;
        $(document).ready(function() {
            if($('#map').length){
                map = new GMaps({
                    div: '#map',
                    scrollwheel: false,             
                    lat: 40.416775,
                    lng: -3.703790,
                    zoom: 5
                });
                postGeocoords(map, '{{ $query->longitude }}', '{{ $query->latitude }}');
            }
        });
        function postGeocoords(mapa, longitud, latitud){
            mapa.setCenter(latitud, longitud);
            mapa.setZoom(15);
            if(marker == null){
                marker = mapa.addMarker({
                    lat: latitud,
                    lng: longitud,
                    draggable: true
                });
            }else{
                marker.setPosition( new google.maps.LatLng( latitud, longitud ) );
                mapa.panTo( new google.maps.LatLng( latitud, longitud ) );
            }
        }

    </script>
    @include('google_events', ['query' => $query])
@stop