<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    /*
UPDATE bikes_brands SET logo = REPLACE(logo, 'bici_marca', 'brands');
UPDATE bikes_models SET photo = REPLACE(photo, 'bici_modelo', 'models');
UPDATE routes SET url_gpx = REPLACE(url_gpx, 'upload/ruta/', 'upload/routes/');
UPDATE routes_photos SET url = REPLACE(url, 'ruta/', 'routes/');
UPDATE users SET logo = REPLACE(logo, 'usuario/', 'users/');
UPDATE users_photos SET url = REPLACE(url, 'usuario/', 'users/');
    */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 250);
            $table->string('slug', 250);
            $table->string('latitude', 30);
            $table->string('longitude', 30);
            $table->string('start_postal_code', 3)->nullable();
            $table->timestamps();

            $table->index('slug');
            $table->index(['latitude', 'longitude']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
