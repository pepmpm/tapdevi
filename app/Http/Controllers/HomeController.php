<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(\Auth::check() && \Auth::user()->role == 'user'){
            return redirect()->route('back.orders.index');
        }
        $this->data['chart_plans'] = \App\ItemsVisits::with('item')->where('year', '=', date('Y'))
            ->where('month', '=', date('m'))->whereHas('item', function ($query) {
                $query->where('status', '=', 'active');
                $query->where('type', '=', 'plan');
            })->get();
        $this->data['chart_users'] = \App\UsersVisits::with('user')->where('year', '=', date('Y'))
            ->where('month', '=', date('m'))->whereHas('user', function ($query) {
                $query->where('role', '=', 'company');
            })->get();
        return view('_back.home', $this->data);
    }
}
