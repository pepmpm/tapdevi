<?php 

namespace App\Http\Controllers;

use Sitemap;
use App;

class SitemapController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $sitemap = App::make("sitemap");

        self::plans();
        $sitemap->addSitemap(route('sitemap-plans'));

        self::users();
        $sitemap->addSitemap(route('sitemap-users'));

        self::extras();
        $sitemap->addSitemap(route('sitemap-extras'));

        // Return the sitemap to the client.
        $sitemap->store('sitemapindex', 'sitemap');
        return $sitemap->render('xml');
    }

    public function plans(){
        $sm = App::make("sitemap");

        $results = \App\Items::with('label', 'user')->where('type', '=', 'plan')->where('status', '=', 'active')->get();

        foreach ($results as $result) {
            $datetime = new \DateTime($result->updated_at);
            $fecha = $datetime->format('Y-m-d\TH:i:s+00:00');

            $sm->add(
                route('enotourism-mallorca.item', 
                ['district' => $result->user->district->slug, 'user' => $result->user->slug, 'item' => $result->slug]),
                $fecha, 
                'monthly', 
                '0.8'
            );
        }
        return $sm->store('xml', 'sitemap-plans');
    }

    public function users(){
        $sm = App::make("sitemap");

        $results = \App\User::where('role', '=', 'company')->whereNotNull('company_name')->get();

        foreach ($results as $result) {
            $datetime = new \DateTime($result->updated_at);
            $fecha = $datetime->format('Y-m-d\TH:i:s+00:00');

            $sm->add(
                route('enotourism-mallorca.user', ['district' => $result->district->slug, 'user' => $result->slug]),
                $fecha, 
                'monthly', 
                '0.8'
            );
        }
        return $sm->store('xml', 'sitemap-users');
    }

    public function extras(){
        $sm = App::make("sitemap");
        
        /*$sm->add(route('enotourism-mallorca.events.index'), date('c',time()), 'monthly', '0.8');
        $sm->add(route('enotourism-mallorca.events.map'), date('c',time()), 'monthly', '0.8');
        $sm->add(route('enotourism-mallorca.day.weekend'), date('c',time()), 'monthly', '0.8');*/

        return $sm->store('xml', 'sitemap-extras');
    }

}