@extends('_layouts/inicio')

@section ('body')

<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left">Cupones</h1>
    </div>
</div>
<div class="container content profile">
    <div class="row">
        <div class="col-md-12">
            <a href="{{route('back.coupons.create')}}" 
               class="btn btn-u btn-u-lg" role="button">
                Crear cupón
            </a>
        </div>     
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <table class="table" id="tabla">
                <thead>
                    <th>#</th>
                    <th>Código</th>
                    <th>Descripción</th>
                    <th>Activo</th>
                    <th>Descuento</th>
                    <th>Acciones</th>
                </thead>
                <tbody>
                    @foreach($query as $row)
                        <tr>
                            <td>{{ $row->id }}</td>
                            <td>{{ $row->code }}</td>
                            <td>{{ $row->description }}</td>
                            <td>{{ $row->active }}</td>
                            <td>{{ $row->discount }}</td>
                            <td>
                                <a href="{{ route('back.coupons.show', array('id' => $row->id)) }}" 
                                   class="btn btn-primary btn-sm pull-left" role="button">
                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                </a>
                                <form action="{{ route('back.coupons.destroy', [$row->id]) }}" method="POST" 
                                    onsubmit="if(!confirm('¿Estás seguro de querer borrar la categoría?')) return false"
                                    class="col-md-4">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-danger btn-sm" type="submit">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@stop

@section ('assets_end')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/datatables.min.css') }}">
    <script src="{{ asset('assets/plugins/datatables/datatables.min.js') }}"></script>
    
    <script type="text/javascript">
        $(document).ready(function() {
            $('#tabla').DataTable({
                "language": {
                    "url": ARNY.baseUrl + "/assets/plugins/datatables/spanish.lang"
                },
                stateSave: true
            });
        });
    </script>

@stop
