<div class="row">
    <div class="col-md-12">
        @include('_layouts.messages', array('errors' => $errors))
    </div>
</div>
<div class="panel panel-tdv">
    <div class="panel-heading">
        <h3 class="panel-title">{{ trans('plan.reservar') }}</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <ul class="list-inline shop-product-prices margin-bottom-30">
                    <li class="shop-red">{{ $query->trueprice . "€" }}</li>
                    @if($query->new_price < $query->price && $query->new_price > 0)
                        <li class="line-through">{{ $query->price . "€" }}</li>
                    @endif
                    <li class="font13">{{ trans('plan.por_persona') }}</li>
                </ul>
            </div>
            <div class="col-md-6">
                @if($query->confirmation == 'direct')
                    <strong><i class="fa fa-flash"></i>{!! " " . trans('plan.confirmacion_direct') !!}</strong>
                @endif
            </div>
        </div>
        <form role="form" method="post" action="{{ route('orders.index') }}" autocomplete="off">
            <input type="hidden" name="id" value="{{ $query->id }}">
            @csrf
            @if($query->confirmation == 'direct' || $query->confirmation == 'fee' || $query->confirmation == 'groupon')
                <h4>{{ trans('plan.mensaje_panel_reservar') }}</h4>
                <div class="row margin-bottom-25">
                    <div class="col-md-6" id="div_quantity">
                        <select name="quantity" class="form-control">
                            @for($x = $query->min_pax; $x <= ($query->max_pax != null ? $query->max_pax : 40); $x++)
                                <option value="{{ $x }}">{{ $x }}</option>
                            @endfor
                        </select>
                    </div>
                    <!--div class="col-md-4">
                        <div class="form-group has-feedback margin-bottom-0">
                            <input type="text" name="date_event" id="date_event" class="form-control">
                            <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
                            <span id="inputSuccess2Status" class="sr-only">(success)</span>
                        </div>
                    </div-->
                    <div class="col-md-6" id="div_language">
                        {!! Form::select('language', $languages, '', array('class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="div_time" class="text-center">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        @if($query->is_giftable == 'S')
                            <button type="submit" class="btn-u btn-brd btn-brd-hover btn-block color-tdv" name="submit" value="gift">
                                <i class="fa fa-gift"></i>&nbsp;{{ trans('plan.comprar_como_regalo') }}
                            </button>
                            <div class="note">
                                {{ "* " . trans('plan.mensaje_note_boton_gift') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-md-7">
                        <button type="submit" class="btn-u btn-block color-blanco" name="submit" value="book">
                            <i class="fa fa-cart-plus" aria-hidden="true"></i>&nbsp;{{ trans('plan.comprar') }}
                        </button>
                    </div>
                </div>
            @else
                <h4>{{ trans('plan.mensaje_panel_reservar_email') }}</h4>
                <div class="row margin-bottom-25">
                    <div class="col-md-6 col-xs-6">
                        <a href="https://api.whatsapp.com/send?phone={{ config('constants.PHONEWEB') }}" target="_blank" 
                            class="btn btn-success btn-block color-blanco">
                            <i class="fa fa-whatsapp color-blanco" aria-hidden="true"></i>&nbsp;Whatsapp
                        </a>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <a class="btn-u btn-block" href="tel:+{{ config('constants.PHONEWEB') }}">
                            <i class="fa fa-phone color-blanco" aria-hidden="true"></i>&nbsp;{{ trans('inicio.llamar') }}
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        @if($query->is_giftable == 'S')
                            <button type="submit" class="btn-u btn-brd btn-brd-hover btn-block color-tdv" name="submit" value="gift">
                                <i class="fa fa-gift"></i>&nbsp;{{ trans('plan.comprar_como_regalo') }}
                            </button>
                        @endif
                    </div>
                </div>
            @endif
        </form>
    </div>
</div>