<h2>Traducciones</h2>
<div class="row">
    <div class="col-md-12">
        <form class="margin-bottom-20" role="form" method="post"
            action="{{ route('back.admin.locations.langs.update', [$query->id]) }}">
            @method('PUT')
            @csrf
            <div class="row">
                <div class="col-md-6 margin-top-15 col-md-offset-3">
                    <button type="submit" class="btn-u btn-block">Guardar traducciones</button>
                </div>
            </div>
            <div class="tab-v2 margin-top-40">
                <ul class="nav nav-tabs">
                    @foreach($query->langs as $lang)
                        <li class="{{ Request::get('tab') == 'langs' && Request::get('idioma') == $lang->locale ? 'active' : (!Request::has('idioma') && $loop->first ? 'active' : '')}}">
                            <a href="#{{ $lang->locale }}" data-toggle="tab">{{ config('translatable.locales')[$lang->locale] }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content bg-color-white">
                    @foreach($query->langs as $lang)
                        <div class="tab-pane fade in {{ Request::get('tab') == 'langs' && Request::get('idioma') == $lang->locale ? 'active' : (!Request::has('idioma') && $loop->first ? 'active' : '') }}"
                            id="{{ $lang->locale }}">
                            <input type="hidden" name="id[{{ $lang->locale }}]" value="{{ $lang->id }}"/>
                            <input type="hidden" name="locale[{{ $lang->locale }}]" value="{{ $lang->locale }}"/>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Descripción</label>
                                                <textarea class="redactor" 
                                                    name="description[{{ $lang->locale }}]">{{ old('description.' . $lang->locale, $lang->description) }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Seo tags</label>
                                                <textarea class="form-control" name="seo_keywords[{{ $lang->locale }}]" 
                                                    rows="5">{{ old('seo_keywords.'.$lang->locale, $lang->seo_keywords) }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Seo descripción</label>
                                                <textarea class="form-control" name="seo_description[{{ $lang->locale }}]" 
                                                    rows="8">{{ old('seo_description.'.$lang->locale, $lang->seo_description) }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 margin-top-15 col-md-offset-3">
                    <button type="submit" class="btn-u btn-block">Guardar traducciones</button>
                </div>
            </div>
        </form>
    </div>
</div>