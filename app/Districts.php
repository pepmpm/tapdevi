<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class Districts extends Model
{

    use Translatable;
    public $translatedAttributes = ['title', 'description', 'seo_description', 'seo_keywords'];
	protected $appends = array('custom');

    protected $fillable = ['slug'];

	public function getCustomAttribute(){
		return $this->name . " (" . $this->location->name . ")";
	}

    public function langs(){
        return $this->hasMany('\App\DistrictsLangs', 'districts_id');
    }

    public function location(){
        return $this->belongsTo('\App\Locations', 'locations_id');
    }

    public function users(){
        return $this->hasMany('\App\User', 'districts_id');
    }

    public function items(){
        return $this->hasManyThrough('App\Items', 'App\User', 'districts_id', 'users_id')->where('type', '=', 'wine');
    }

    public function wines(){
        return $this->hasManyThrough('App\Items', 'App\User', 'districts_id', 'users_id')->where('type', '=', 'wine');
    }

    public function plans(){
        return $this->hasManyThrough('App\Items', 'App\User', 'districts_id', 'users_id')
            ->where('status', '=', 'active')->where('type', '=', 'plan');
    }
}
