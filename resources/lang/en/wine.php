<?php

$idioma = array();

$idioma['validation_stock_required'] = 'Stock is mandatory';
$idioma['validation_stock_numeric'] = 'Stock must be a number';
$idioma['validation_cost_numeric'] = 'Cost price must be a number';
$idioma['validation_price_required'] = 'Product price is mandatory';
$idioma['validation_price_numeric'] = 'Product price must be a number';
$idioma['validation_new_price_numeric'] = 'Discount price must be a number';
$idioma['validation_shipping_numeric'] = 'Shipping cost added must be a number';
$idioma['validation_labels_id_required'] = 'You must select a category';
$idioma['validation_labels_id_numeric'] = 'The category selected is wrong';
$idioma['validation_name_required'] = 'Product name is mandatory';
$idioma['validation_name_max'] = 'Product name is too long';
$idioma['validation_description_required'] = 'Product description is mandatory';
$idioma['validation_description_max'] = 'Product description is too long';
$idioma['validation_new_price_lt'] = 'Discount price must be lower than current price';

$idioma['otros_vinos'] = 'Other wines you must try';
$idioma['mensaje_cross_selling'] = 'Here we present other selected wines to delve into the wine culture of the Balearic Islands.';

$idioma['selecciona_cantidad'] = 'Quantity';
$idioma['crear_una_caja_personalizada'] = 'Create your custom box';
$idioma['wines'] = 'Wines';
$idioma['experiences'] = 'Experiences';
$idioma['comprar_caja_personalizada_con'] = 'Buy a custom wine box with :user';
$idioma['tu_caja_personalizada'] = 'Your custom wine box';
$idioma['elige_vinos_para_anyadirlos_a_tu_caja'] = 'Select wines to add to your custom wine box';

$idioma['vino'] = 'Wine';
$idioma['precio'] = 'Price';
$idioma['bodega'] = 'Winery';

$idioma['message_custom_box'] = 'Click on a wine from the list to add it to your custom wine box. When you have chosen the 6 bottles, the Add to Cart button will appear.';

$idioma['msg_transferencia_bancaria'] = 'Remember to write the order code :code in the concept field of your bank transfer';
$idioma['por_botella'] = 'unit';
$idioma['por_variedad'] = 'By variety';

$idioma['elige_una_bodega_para_vino_personalizado'] = 'Choose a winery to create your varied wine box';

$idioma['mensaje_botellas_envio_gratuito'] = 'Shipping of wine by boxes. You must add :num more bottles to the cart';
$idioma['envio_gratuito'] = 'Congratulations!! We can now proceed to finalize the order';

$idioma['novedad'] = 'New';

return $idioma;