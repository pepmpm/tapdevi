<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order')->default(0);

            $table->unsignedInteger('users_id')->nullable();
            $table->foreign('users_id')->references('id')->on('users');

            $table->unsignedInteger('labels_id')->nullable();
            $table->foreign('labels_id')->references('id')->on('labels');

            $table->unsignedInteger('stock')->nullable();
            $table->unsignedInteger('fake_stock')->nullable();

            $table->float('cost', 5, 2)->default(0);
            $table->float('price', 5, 2);
			$table->float('new_price', 5, 2)->nullable();

            $table->unsignedInteger('year')->nullable();
            $table->float('graduation', 5, 2)->nullable();

			$table->enum('type', ['item', 'plan', 'wine'])->default('item');
            $table->enum('status', ['active', 'outofstock', 'disabled'])->default('active');

            $table->unsignedInteger('min_pax')->nullable();
            $table->unsignedInteger('max_pax')->nullable();

            $table->enum('confirmation', ['email', 'instant', 'groupon'])->nullable();
			
            $table->index('graduation');
            $table->index('year');
            $table->index('type');
            $table->index('status');
            $table->index(['type', 'stock', 'status']);
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
