@if(Auth::check() && Auth::user()->isAdmin())
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('back.plans.promoted.toggle', ['item' => $plan->id]) }}">
                <i class="fa fa-star color-tdv"></i>
                @if($plan->is_promoted == 'S')
                    Despromo
                @else
                    Promocionar
                @endif
            </a><br/>
            <a href="{{ route('back.plans.show', ['item' => $plan->id]) }}" class="margin-lr-5">
                <i class="fa fa-pencil color-tdv"></i>
                Editar
            </a>
        </div>
    </div>
@endif