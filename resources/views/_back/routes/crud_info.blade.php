<div class="panel panel-default">
    <div class="panel-heading">Datos</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Nombre</label>
                    <input type="text" class="form-control {{ $errors->has('name') ? 'border-rojo' : "" }}" 
                        name="{{ isset($query) ? '' : 'name' }}" {{ isset($query) ? 'disabled' : '' }} placeholder="Nombre"
                        value="{{ old('name.' . config('app.locale'), isset($query) ? $query->name : '') }}"/>
                    @if(isset($query))
                        <div class="note">
                            <strong>Nota:</strong>
                            Después podrás modificar y traducir el nombre del producto en la sección de textos 
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>