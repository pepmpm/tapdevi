@if(isset($show_events) && $show_events && count($index_events) > 0)
    <div id="accordion-v0" class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion-v1" href="#collapseOne">
                        {{ trans('inicio.proximos_eventos') }}
                        <i class="fa fa-angle-down"></i>
                    </a>
                </h2>
            </div>
            <div id="collapseOne" class="panel-collapse collapse {{ $agent->isDesktop() ? 'in' : '' }}">
                <div class="panel-body padding-lr-10">
                    @include('events._timeline', ['show_link_map' => true])
                </div>
            </div>
        </div>
    </div>
@endif

<div id="accordion-v1" class="panel-group">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion-v1" href="#collapseOne">
                    {{ trans('inicio.enoturismo') }}
                    <i class="fa fa-angle-down"></i>
                </a>
            </h2>
        </div>
        <div id="collapseOne" class="panel-collapse collapse {{ $agent->isDesktop() ? 'in' : '' }}">
            <div class="panel-body">
                <ul class="list-unstyled checkbox-list">
                    @foreach($labels_sidebar_plans as $sec)
                    <li>
                        <label class="checkbox">
                            <a href="{{ route('enotourism-mallorca.cat', ['cat' => $sec->slug]) }}">
                                <input type="checkbox" name="checkbox" {{ $params['cat'] == $sec->slug ? 'checked' : '' }} />
                                <i></i>
                                {{ $sec->name }}
                                <small><a href="#">({{ $sec->plans_count }})</a></small>
                            </a>
                        </label>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="accordion-v3" class="panel-group">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion-v3" href="#collapseThree">
                    {{ trans('plan.por_zona') }}
                    <i class="fa fa-angle-down"></i>
                </a>
            </h2>
        </div>
        <div id="collapseThree" class="panel-collapse collapse {{ $agent->isDesktop() ? 'in' : '' }}">
            <div class="panel-body">
                <ul class="list-unstyled checkbox-list">
                    @foreach($districts_sidebar_plans as $district)
                    <li>
                        <label class="checkbox">
                            <a href="{{ route('enotourism-mallorca.dis', ['district' => $district->slug]) }}">
                                <input type="checkbox" name="checkbox" {{ $params['district'] == $district->slug ? 'checked' : '' }} />
                                <i></i>
                                {{ $district->name }}
                                <small><a href="#">({{ $district->plans_count }})</a></small>
                            </a>
                        </label>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="accordion-v2" class="panel-group">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion-v2" href="#collapseTwo">
                    {{ trans('plan.por_bodega') }}
                    <i class="fa fa-angle-down"></i>
                </a>
            </h2>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse {{ $agent->isDesktop() ? 'in' : '' }}">
            <div class="panel-body">
                <ul class="list-unstyled checkbox-list">
                    @foreach($users_sidebar_plans as $user)
                    <li>
                        <label class="checkbox">
                            <a href="{{ route('enotourism-mallorca.user', ['district' => $user->district->slug, 'user' => $user->slug]) }}">
                                <input type="checkbox" name="checkbox" {{ $params['user'] == $user->slug ? 'checked' : '' }} />
                                <i></i>
                                {{ $user->name }}
                                <small><a href="#">({{ $user->plans_count }})</a></small>
                            </a>
                        </label>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>

<a href="{{ route('enotourism-mallorca.cat') }}" class="btn-u btn-brd btn-brd-hover btn-u-lg btn-u-sea-shop btn-block">Reset</a>
