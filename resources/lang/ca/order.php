<?php

$idioma = array();

$idioma['validation_users_id_required'] = 'Falta el nom des client';
$idioma['validation_users_id_max'] = 'Es nom del client es massa llarg';

$idioma['validation_name_required'] = 'Falta es teu nom';
$idioma['validation_name_max'] = 'Es nom es massa llarg';

$idioma['validation_phone_numeric'] = "Es telefon ha de ser numèric";
$idioma['validation_phone_max'] = 'Es telefon es massa llarg';
$idioma['validation_email_required'] = "S'email es obligatori";
$idioma['validation_email_email'] = "S'email no es correcte";
$idioma['validation_email_max'] = "S'email es massa llarg";
$idioma['validation_items_id_required'] = 'No has seleccionat un producte';
$idioma['validation_items_id_numeric'] = 'Hi ha un error amb algún producte';
$idioma['validation_quantity_required'] = 'Sa quantitat es obligatoria';
$idioma['validation_quantity_numeric'] = 'Hi ha un error amb alguna quantitat';
$idioma['validation_discount_numeric'] = 'Es descompte ha de ser numèric';
$idioma['validation_credit_card_card_invalid'] = 'El número de targeta no es correcte';
$idioma['validation_credit_card_card_cvc_invalid'] = 'El codi CVC no es correcte';
$idioma['validation_credit_card_card_card_expiration_month_invalid'] = 'El mes de la targeta que has introduit no es correcte';
$idioma['validation_credit_card_card_expiration_year_invalid'] = "L'any de la targeta que has introduit no es correcte";
$idioma['validation_credit_card_required'] = 'Falta informació de la targeta de crèdit';
$idioma['botellas'] = 'botelles';
$idioma['unidades'] = 'unitats';
$idioma['personas'] = 'persones';

$idioma['carrito'] = 'Cistella';
$idioma['cantidad'] = 'Quantitat';
$idioma['informacion_envio'] = "Informació de la reserva";
$idioma['facturacion'] = "Facturació";
$idioma['nombre'] = 'Nom';
$idioma['apellidos'] = 'Llinatges';
$idioma['email'] = 'Email';
$idioma['telefono'] = 'Telèfon';
$idioma['direccion'] = 'Direcció';
$idioma['provincia'] = 'Provincia';
$idioma['poblacion'] = 'Població';
$idioma['codigo_postal'] = 'Codi Postal';
$idioma['numero_de_cuenta'] = 'Número de tarjeta';
$idioma['mes'] = 'Mes';
$idioma['ano'] = 'Any';
$idioma['cvv'] = 'CVV';
$idioma['note'] = 'Nota';
$idioma['mensaje_tarjetas_bbdd'] = config('constants.NAMEWEB') . " no guardarà cap dada de facturació.";
$idioma['enviar_direccion_diferente'] = "Necessit factura (pel cumpliment d'obligacions tributaries)";

$idioma['codigo_descuento_aplicado'] = 'Codi de descompte :code aplicat';
$idioma['descuento_aplicado'] = 'Descompte aplicat';
$idioma['descuento_en_el_envio'] = 'Descompte en el enviament';

$idioma['caja'] = 'Caixa|Caixes';
$idioma['persona'] = 'Persona|Persones';
$idioma['articulo'] = 'Article|Articles';
$idioma['lote'] = 'Lot|Lots';

$idioma['gratuito_islas_baleares'] = 'Enviament gratuit a Illes Balears';
$idioma['_gratuito_islas_baleares'] = 'gratuit a Illes Balears';

$idioma['elige_un_metodo_de_pago'] = 'Tria un métode de pagament';
$idioma['tarjeta_de_credito_o_debito'] = 'Targeta de credit o debit';
$idioma['pago_por_transferencia_bancaria'] = 'Pagament per transferència bancaria';

$idioma['finalizar_pedido'] = 'Acabar comanda';
$idioma['pedido'] = 'Comanda';

$idioma['mensaje_cabecera_login'] = 'Inserta es teu email i sa contrasenya que vares rebre per email amb sa teva primera comanda';

$idioma['mensaje_factura_direct'] = "El día de l'experiència es realitzarà el pagament";
$idioma['mensaje_factura_fee'] = "El día de l'experiència es pagarà la resta";
$idioma['mensaje_factura_groupon'] = "Ja s'ha fet el pagament de l'experiència, ara t'has de posar d'acord amb s'empresa per acordar el día de la visita";

$idioma['usar_este_usuario'] = 'Usar aquest email';
$idioma['producto_anyadido_al_carrito'] = 'Producte afegit a la cistella';

$idioma['_DE_QUIEN_'] = 'DE QUI?';
$idioma['_PARA_QUIEN_'] = 'PER A QUI?';
$idioma['FUTUROS_EVENTOS'] = 'FUTURS EVENTS';
$idioma['_ACTIVIDAD_'] = 'ACTIVITAT';
$idioma['_VALE_POR_'] = 'VAL PER';
$idioma['_CUANDO_'] = 'QUAN?';
$idioma['_DONDE_'] = 'A ON?';
$idioma['_CODIGO_'] = 'CODI RESERVA';
$idioma['_CUANTO_'] = 'QUANT?';

$idioma['proponer_fecha'] = 'Proposar data';
$idioma['contactar'] = 'Contacta amb noltros';


return $idioma;