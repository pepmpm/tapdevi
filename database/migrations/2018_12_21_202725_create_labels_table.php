<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labels', function (Blueprint $table) {
            $table->increments('id');

            $table->string('url', 250)->nullable();
            $table->string('icon', 250)->nullable();
            $table->unsignedInteger('order')->nullable();

            $table->enum('category', ['enotourism', 'wines', 'gourmet', 'liqueur']);

            $table->unsignedInteger('labels_id')->nullable();
            $table->foreign('labels_id')->references('id')->on('labels');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('labels');
    }
}
