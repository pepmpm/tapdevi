<div class="panel panel-default">
    <div class="panel-heading">Datos</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" class="form-control {{ $errors->has('name') ? 'border-rojo' : "" }}" 
                                name="{{ isset($query) ? '' : 'name' }}" {{ isset($query) ? 'disabled' : '' }} placeholder="Nombre"
                                value="{{ old('name.' . config('app.locale'), isset($query) ? $query->name : '') }}"/>
                            @if(isset($query))
                                <div class="note">
                                    <strong>Nota:</strong>
                                    Después podrás modificar y traducir el nombre del producto en la sección de textos 
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row hide">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Categoría</label>
                            {!! Form::select('labels_id', $labels_lov, 
                                old('labels_id', isset($query) ? $query->labels_id: ''), 
                                array('class' => 'form-control ' . ($errors->has('labels_id') ? 'border-rojo' : ""), 
                                'id' => 'labels_lov')) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Precio p.p</label>
                            <input type="text" class="form-control {{ $errors->has('price') ? 'border-rojo' : "" }}" 
                                name="price" placeholder="Precio por persona"
                                value="{{ old('price', isset($query) ? $query->price: '') }}"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Población</label>
                            {!! Form::select('districts_id', array('' => '') + (isset($districts) ? $districts : []), 
                                old('districts_id', isset($query) ? $query->districts_id : ''), 
                                array('class' => 'form-control districts_id', 'id' => 'districts_id' )) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>C. postal</label>
                            <input type="text" class="form-control" name="postal_code" id="postal_code" placeholder="C.P."
                                value="{{ old('postal_code', isset($query) ? $query->postal_code : '') }}"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Dirección</label>
                            <input type="text" class="form-control" name="address" id="address" placeholder="Dirección"
                                value="{{ old('address', isset($query) ? $query->address : '') }}"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Latitud</label>
                            <input type="text" class="form-control" name="latitude" id="latitud" placeholder="Latitud"
                                    value="{{ old('latitude', isset($query) ? $query->latitude: '') }}"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Longitud</label>
                            <input type="text" class="form-control" name="longitude" id="longitud" placeholder="Longitud"
                                    value="{{ old('longitude', isset($query) ? $query->longitude: '') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <label>Localización</label>
                <button type="button" id="btn_refresh_map" class="btn btn-u margin-bottom-5">
                    <i class="fa fa-refresh"></i>
                    Actualizar mapa
                </button>
                <div id="map" class="height350"></div>
            </div>
        </div>
    </div>
</div>