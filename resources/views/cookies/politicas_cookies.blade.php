@extends('_layouts/inicio')

@section('title', trans('meta.title_index'))
@section ('meta')
    <meta name="description" content="{{ trans('meta.description_index') }}"/>
    <meta name="keywords" content="{{ trans('meta.keywords_index') }}" />
    <meta name="Author" content="{{ trans('meta.author') }}" />
    <meta property="og:title" content="{{ trans('meta.title_index') }}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:image" content="{{ url('assets/img/logo3.png') }}"/>
    <meta property="og:description" content="{{ trans('meta.description_index') }}" />
    @parent
@stop

@section ('body')
	<div class="breadcrumbs">
	    <div class="container">
	        <h1 class="pull-left">Política de cookies</h1>
	    </div>
	</div>
	<div class="content-md">
    	<div class="container">
        	<div class="row">
        		<div class="col-md-12">
					<h1>POLÍTICA DE COOKIES</h1>
					<p><i>Esta política de cookies ha sido actualizada por última vez el 27 enero 2021 y se aplica a los ciudadanos del Espacio Económico Europeo.</i></p>

					<ul>
						<li>1. Introducción</li>
						<p>Nuestra web, {{ config('constants.NAMEWEB') }} (en adelante: «la web») utiliza cookies y otras tecnologías relacionadas 
						(para mayor comodidad, todas las tecnologías se denominan «cookies»). Las cookies también son colocadas por terceros a 
						los que hemos contratado. En el siguiente documento te informamos sobre el uso de cookies en nuestra web.</p>

						<li>2. ¿Qué son las cookies?</li>
						<p>Una cookie es un pequeño archivo simple que se envía junto con las páginas de esta web y que tu navegador almacena en el disco duro de tu ordenador o de otro dispositivo. La información almacenada puede ser devuelta a nuestros servidores o a los servidores de terceros relevantes durante una visita posterior.</p>

						<li>3. ¿Qué son los scripts?</li>
						<p>Un script es un fragmento de código de programa que se utiliza para hacer que nuestra web funcione correctamente y de forma interactiva. Este código se ejecuta en nuestro servidor o en tu dispositivo.</p>

						<li>4. ¿Qué es una baliza web?</li>
						<p>Una baliza web (o una etiqueta de píxel) es una pequeña e invisible pieza de texto o imagen en una web que se utiliza para hacer seguimiento del tráfico en una web. Para ello, se almacenan varios datos sobre ti mediante balizas web.</p>

						<li>5. Cookies</li>
						<ul>
							<li>5.1 Cookies técnicas o funcionales</li>
							<p>Algunas cookies aseguran que ciertas partes de la web funcionen correctamente y que tus preferencias de usuario sigan recordándose. Al colocar cookies funcionales, te facilitamos la visita a nuestra web. De esta manera, no necesitas introducir repetidamente la misma información cuando visitas nuestra web y, por ejemplo, los artículos permanecen en tu cesta de la compra hasta que hayas pagado. Podemos colocar estas cookies sin tu consentimiento.</p>

							<li>5.2 Cookies analíticas</li>
							<p>Usamos cookies analíticas para optimizar la experiencia en el sitio web para nuestros usuarios. Con estas cookies analíticas obtenemos conocimientos del uso de nuestro sitio web. Te pedimos tu permiso para insertar cookies analíticas.</p>

							<li>5.3 Cookies de marketing/seguimiento</li>
							<p>Las cookies de marketing/seguimiento son cookies, o cualquier otra forma de almacenamiento local, usadas para crear perfiles de usuario para mostrar publicidad o para hacer el seguimiento del usuario en esta web o en varias webs con fines de marketing similares.</p>

							<li>5.4 Botones de medios sociales</li>
							<p>En nuestra web hemos incluidos botones para Facebook, Twitter, WhatsApp y Instagram promocionar páginas (p. ej. «me gusta», «pin») o compartirlas (p. ej. «tuitear») en redes sociales como Facebook, Twitter, WhatsApp y Instagram. Estos botones funcionan usando código de Facebook, Twitter, WhatsApp y Instagram ellos mismos. Este código incrusta cookies. Estos botones de redes sociales pueden almacenar y procesar cierta información, de esta manera puede ser mostrado un anuncio personalizado.</p>

							<p>Por favor lee la política de privacidad de estas redes sociales (que puede cambiar frecuentemente) para saber que hacen con tus datos (personales) que procesan usando estas cookies. Los datos que reciben son anonimizados el máximo posible. Facebook, Twitter, WhatsApp y Instagram están ubicados en los Estados Unidos.</p>
						</ul>

						<li>6. Consentimiento</li>
						<p>Cuando visites nuestra web por primera vez, te mostraremos una ventana emergente con una explicación sobre las cookies. 
						Tan pronto como hagas clic en «Aceptar», aceptas que usemos cookies con tu visita, tal y como se describe en esta política 
						de cookies. Puedes desactivar el uso de cookies a través de tu navegador, pero, por favor, ten en cuenta que nuestra web puede 
						dejar de funcionar correctamente.</p>

						<li>7. Detalles de contacto</li>
						<p>Para preguntas y/o comentarios sobre nuestra política de cookies y esta declaración, por favor, contacta con 
						nosotros usando los siguientes datos de contacto:</p>
						<p>Tapdevi.com</p>
						<p>Sant Cosme 3, baixos, 07220, Pina (Illes Balears)</p>
						<p>{{ config('constants.FULLWEB') }}</p>
						<p>{{ config('constants.MAILWEB') }}</p>
						<p>{{ '+' . config('constants.PHONEWEB') }}</p>
					</ul>
        		</div>
        	</div>
        </div>
    </div>

@stop

@section ('assets_end')

@stop
