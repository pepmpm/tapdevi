<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersVisits extends Model
{
    protected $fillable = ['users_id', 'year', 'month'];

    public function user(){
        return $this->belongsTo('\App\User', 'users_id');
    }
}
