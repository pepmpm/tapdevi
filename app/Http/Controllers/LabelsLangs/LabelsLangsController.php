<?php

namespace App\Http\Controllers\LabelsLangs;

use App\Labels;
use App\LabelsLangs;
use Illuminate\Http\Request;
use App\Traits\Helpers;

class LabelsLangsController extends \App\Http\Controllers\Controller
{
	use Helpers;
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Labels $lang)
    {
		$tmp = $lang;
        if($request->get('name')['es'] == null || 
			$request->get('seo_keywords')['es'] == null || 
			$request->get('seo_description')['es'] == null)
		{
            return redirect()
                ->route('back.labels.show', array('labels' => $tmp->id, 'tab' => 'langs'))
                ->withInput()
                ->with('message_error', 'Las descripciones en español son obligatorias');
        }

        $spanish = [
			'name' => $request->get('name')['es'],
            'title' => $request->get('title')['es'],
            'description' => $request->get('description')['es'],
            'seo_keywords' => $request->get('seo_keywords')['es'], 
            'seo_description' => $request->get('seo_description')['es']
        ];

        foreach($tmp->langs as $lang){
			if($lang->name != $request->get('name')[$lang->locale]){
				$lang->name = $request->get('name')[$lang->locale];
				$lang->slug = $this->getSlug($lang->name, 'slug', LabelsLangs::getModel());
			}
            $lang->title = strip_tags($request->get('title')[$lang->locale]);
            $lang->description = strip_tags($request->get('description')[$lang->locale]);
            $lang->seo_keywords = str_limit($request->get('seo_keywords')[$lang->locale], 255);
            $lang->seo_description = str_limit($request->get('seo_description')[$lang->locale], 255);
            $lang->save();
        }

        return redirect()
            ->route('back.labels.show', array('labels' => $tmp->id, 'tab' => 'langs'))
            ->with('message_success', 'Cambios realizados correctamente');
    }

}
