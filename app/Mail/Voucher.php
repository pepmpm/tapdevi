<?php

namespace App\Mail;

use App\Orders;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Voucher extends Mailable
{
    use Queueable, SerializesModels;
    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Orders $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $params = array();
        $params['order'] = $this->order;
        $params['logo'] = \Imgfly::imgPublic('img/logo.png' . '?w=170', 'assets');

        if (\Auth::check() && \Auth::user()->isAdmin()){
            $params['order']->email = \Auth::user()->email;
            $params['order']->name = \Auth::user()->name;
        }

        // $pdf = \PDF::loadView('emails.pdf.pdf_voucher', $params);
        return $this->from(config('constants.MAILWEB'), config('constants.NAMEWEB'))
            ->to($params['order']->email, $params['order']->name)
            ->subject(config('constants.NAMEWEB') . " - " . trans('order.pedido') . " " . $this->order->code)
            /*->attachData(
                $pdf->stream("dompdf_out.pdf"), trans('order.pedido') . '_' . $this->order->code . '.pdf', ['mime' => 'application/pdf']
            )*/
            ->view('emails.voucher')
            ->with($params);
    }
}
