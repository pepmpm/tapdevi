<?php

namespace App\Http\Controllers\Plans;

use App\Items;
use App\ItemsLangs;
use App\ItemsDetails;
use App\ItemsPhotos;
use Illuminate\Http\Request;
use App\Traits\Helpers;
use App\Traits\Plans;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use Validator;
use Cache;

class PlansController extends \App\Http\Controllers\Controller
{
    use Helpers;
    use Plans;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function _search($params)
    {
        return $this->getPlans($params);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function _index()
    {
        return Items::where('type', '=', 'plan')->orderBy('id', 'desc')->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function _all()
    {
        return Items::where('type', '=', 'plan')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function _store($request)
    {
        $validation = Validator::make($request, self::_get_rules($request), self::_get_messages());

        if ($validation->fails()) {
            return redirect()->route('back.plans.create')->withErrors($validation)->withInput();
        }

        $tmp = new Items;
        $tmp->cost = isset($request['cost']) ? $request['cost'] : null;
        $tmp->order = 100;
        $tmp->labels_id = $request['labels_id'];
        $tmp->users_id = $request['users_id'];
        $tmp->price = $request['price'];
        $tmp->new_price = $request['new_price'];
        $tmp->min_pax = $request['min_pax'];
        $tmp->max_pax = $request['max_pax'];
        $tmp->confirmation = $request['confirmation'];
        $tmp->is_apto_pets = $request['is_apto_pets'];
        $tmp->is_apto_children = $request['is_apto_children'];
        $tmp->is_apto_disabled = $request['is_apto_disabled'];
        $tmp->is_has_parking = $request['is_has_parking'];
        $tmp->is_giftable = $request['is_giftable'];
        $tmp->advance_days = $request['advance_days'];
        $tmp->cancellation_days = $request['cancellation_days'];
        $tmp->duration = $request['duration'];
        $tmp->languages = implode("#", $request['languages']);
        $tmp->type = 'plan';
        $tmp->status = $request['status'];
        $tmp->save();

        self::_insert_langs($request, $tmp);

        if(isset($request['p_foto'])){
            self::_insert_images($request, $tmp);
        }

        self::_clean();
        //self::_create_json_search_home();

        return $tmp;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _show(Items $item)
    {
        return $item;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _update($request, Items $tmp)
    {
        $validation = Validator::make($request, self::_get_rules($request), self::_get_messages());

        if ($validation->fails()) {
            return redirect()->route('back.plans.show', $tmp->id)->withErrors($validation)->withInput();
        }

        $tmp->cost = isset($request['cost']) ? $request['cost'] : null;
        $tmp->order = 100;
        $tmp->labels_id = $request['labels_id'];
        $tmp->status = $request['status'];
        $tmp->languages = implode("#", $request['languages']);
        $tmp->price = $request['price'];
        $tmp->new_price = $request['new_price'];
        $tmp->min_pax = $request['min_pax'];
        $tmp->max_pax = $request['max_pax'];
        $tmp->confirmation = $request['confirmation'];
        $tmp->is_apto_pets = $request['is_apto_pets'];
        $tmp->is_apto_children = $request['is_apto_children'];
        $tmp->is_apto_disabled = $request['is_apto_disabled'];
        $tmp->is_has_parking = $request['is_has_parking'];
        $tmp->is_giftable = $request['is_giftable'];
        $tmp->advance_days = $request['advance_days'];
        $tmp->cancellation_days = $request['cancellation_days'];
        $tmp->duration = $request['duration'];
        $tmp->save();

        self::_insert_langs($request, $tmp);

        if(isset($request['p_foto'])){
            self::_insert_images($request, $tmp);
        }

        self::_clean();
        //self::_create_json_search_home();

        return $tmp;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _destroy(Items $item)
    {
        $ruta = 'upload/plans/'.$item->id.'/';
        if(is_dir($ruta)){
            $success = \File::deleteDirectory($ruta, true);
            rmdir($ruta);
            if(!$success){
                throw new \Exception('El directorio de imagenes de '.$item->id.' no existe');
            }
        }

        $item->delete();

        self::_clean();
        //self::_create_json_search_home();

        return true;
    }

    private function _insert_images($request, $item){
        $ruta = 'upload/plans/'.$item->id.'/';
        \File::makeDirectory($ruta, 0775, true, true);

        foreach($request['p_foto'] as $foto){
            $tmp = new ItemsPhotos;
            $tmp->order = 100;
            $tmp->items_id = $item->id;
            $tmp->save();

            $tmp->url = $ruta . $item->slug . "-" . $tmp->id . ".webp";
            $tmp->save();

            $img = Image::make($foto)->encode('webp', 90);
            $img->resize(1024, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($tmp->url);
            unlink($foto);
            $this->set_72dpi_image($tmp->url);
        }
    }

    private function _insert_langs($request, $tmp){
        $spanish = [
            'name' => $request['name']['es'],
            'description' => $request['description']['es'],
            'short_description' => $request['short_description']['es'],
            'timetable' => $request['timetable']['es'],
            'seo_keywords' => $request['seo_keywords']['es'], 
            'seo_description' => $request['seo_description']['es']
        ];

        foreach(config('translatable.locales') as $keylocale => $locale){
            $lang = ItemsLangs::firstOrNew(['locale' => $keylocale, 'items_id' => $tmp->id]);
            $lang->locale = $keylocale;
            $lang->items_id = $tmp->id;
            $lang->name = parent::_translate_langs($spanish['name'], isset($request['name'][$keylocale]) ? $request['name'][$keylocale] : null, $keylocale, 255);
            $lang->slug = $this->getSlug($lang->name, 'slug', ItemsLangs::getModel());


            $lang->seo_description = parent::_translate_langs(
                $spanish['seo_description'], isset($request['seo_description'][$keylocale]) ? $request['seo_description'][$keylocale] : null, $keylocale, 255);
            $lang->seo_keywords = parent::_translate_langs(
                $spanish['seo_keywords'], isset($request['seo_keywords'][$keylocale]) ? $request['seo_keywords'][$keylocale] : null, $keylocale, 255);
            $lang->description = parent::_translate_langs(
                $spanish['description'], isset($request['description'][$keylocale]) ? $request['description'][$keylocale] : null, $keylocale, null);
            $lang->timetable = parent::_translate_langs(
                $spanish['timetable'], isset($request['timetable'][$keylocale]) ? $request['timetable'][$keylocale] : null, $keylocale, null, true);
            $lang->short_description = parent::_translate_langs(
                $spanish['short_description'], isset($request['short_description'][$keylocale]) ? $request['short_description'][$keylocale] : null, $keylocale, null);

            $lang->save();
        }
    }

    private function _edit_langs($request, $tmp){
        $spanish = [
            'name' => $request['name']['es'],
            'description' => $request['description']['es'],
            'short_description' => $request['short_description']['es'],
            'timetable' => $request['timetable']['es'],
            'seo_keywords' => $request['seo_keywords']['es'], 
            'seo_description' => $request['seo_description']['es']
        ];

        foreach($tmp->langs as $lang){
            $tmpname = parent::_translate_langs(
                $spanish['name'], $request['name'][$lang->locale], $lang->locale, 255);
            if($tmpname != $lang->name){
                $lang->name = $tmpname;
                $lang->slug = $this->getSlug($tmpname, 'slug', ItemsLangs::getModel());
            }

            $lang->seo_description = parent::_translate_langs(
                $spanish['seo_description'], $request['seo_description'][$lang->locale], $lang->locale, 255);
            $lang->seo_keywords = parent::_translate_langs(
                $spanish['seo_keywords'], $request['seo_keywords'][$lang->locale], $lang->locale, 255);
            $lang->description = parent::_translate_langs(
                $spanish['description'], $request['description'][$lang->locale], $lang->locale, null);
            $lang->timetable = parent::_translate_langs(
                $spanish['timetable'], $request['timetable'][$lang->locale], $lang->locale, null, true);
            $lang->short_description = parent::_translate_langs(
                $spanish['short_description'], $request['short_description'][$lang->locale], $lang->locale, null);
            $lang->save();
        }
    }

    private function _get_rules($request){
        $rules = array();
        $rules['cost'] = 'nullable|numeric';
        $rules['labels_id'] = 'required|numeric';
        $rules['min_pax'] = 'required|numeric';
        $rules['max_pax'] = 'required|numeric';

        $rules['name'] = 'required|max:250';
        $rules['description.es'] = 'required|max:2000';

        $rules['users_id'] = 'required|max:250';

        return $rules;
    }
    
    private function _get_messages(){
        $messages = array();
        $messages['min_pax.required'] = trans('item.validation_stock_required');
        $messages['min_pax.numeric'] = trans('item.validation_stock_numeric');
        $messages['max_pax.required'] = trans('item.validation_max_pax_required');
        $messages['max_pax.numeric'] = trans('item.validation_max_pax_numeric');
        $messages['cost.numeric'] = trans('item.validation_cost_numeric');
/*        $messages['price.required'] = trans('item.validation_price_required');
        $messages['price.numeric'] = trans('item.validation_price_numeric');
        $messages['new_price.numeric'] = trans('item.validation_new_price_numeric');
        $messages['new_price.lt'] = trans('item.validation_new_price_lt');
        $messages['shipping.numeric'] = trans('item.validation_shipping_numeric');*/
        $messages['labels_id.required'] = trans('item.validation_labels_id_required');
        $messages['labels_id.numeric'] = trans('item.validation_labels_id_numeric');

        $messages['name.required'] = trans('item.validation_name_required');
        $messages['name.max'] = trans('item.validation_name_max');
        $messages['description.es.required'] = __('item.validation_description_required', [], 'es');
        $messages['description.es.max'] = __('item.validation_description_max', [], 'es');
        return $messages;
    }

    private function _filter_by_top($items, $params){
        $tmp = Cache::remember('filter_top_enotourism-mallorca', 1440, function () use ($params) {
            return \App\Labels::where('category', '=', 'enotourism-mallorca')->first();
        });

        return $items->where('labels_id', '=', $tmp->id);
    }

    private function _filter_by_category($items, $params){
        if($params['cat'] != null){
            $tmp = Cache::remember('filter_cat_' . $params['cat'] . "_" . $params['locale'], 1440, function () use ($params) {
                return \App\Labels::whereHas('langs', function ($query) use ($params) {
                        $query->where('slug', '=', $params['cat']);
                        $query->where('locale', '=', $params['locale']);
                    })->first();
            });

            return $items->where('labels_id', '=', $tmp->id);
        }

        return $items;
    }

    private function _filter_by_district($items, $params){
        if($params['district'] != null){
            $tmp = Cache::remember('eno_district_' . $params['district'], 1440, function () use ($params) {
                $dis = \App\Districts::where('slug', '=', $params['district'])->pluck('id')->all();
                return \App\User::whereIn('districts_id', $dis)->pluck('id')->all();
            });
            return $items->whereIn('users_id', $tmp);
        }

        return $items;
    }

    private function _filter_by_user($items, $params){
        if($params['user'] != null){
            return $items->whereHas('user', function ($query) use ($params) {
                $query->where('slug', '=', $params['user']);
            });
        }

        return $items;        
    }

    private function _clean(){
        \Cache::flush();
    }

    private function _create_json_search_home(){
        $langs = array('es', 'en', 'ca');
        $json = array();

        $plans = Items::where('type', '=', 'plan')->where('status', '=', 'active')->get();

        foreach($plans as $plan){
            foreach($langs as $lang){
                $json[$lang][] = [
                    'id' => 'plan_' . $plan->id,
                    'name' => $plan->name,
                    'user' => ($plan->users_id != null ? $plan->user->name : '')
                ];
            }
        }

        $users = \App\User::whereHas('plans', function ($query) {
                $query->where('status', '=', 'active');
                $query->where('type', '=', 'plan');
            })->orderBy('name', 'asc')->get();

        foreach($users as $user){
            foreach($langs as $lang){
                $json[$lang][] = [
                    'id' => 'user_' . $user->id,
                    'name' => $user->name,
                    'user' => ''
                ];
            }
        }

        $labels = \App\Labels::whereHas('plans', function ($query) {
                $query->where('status', '=', 'active');
            })->get();

        foreach($labels as $label){
            foreach($langs as $lang){
                $json[$lang][] = [
                    'id' => 'label_' . $label->id,
                    'name' => $label->name,
                    'user' => ''
                ];
            }
        }

        $districts = \App\Districts::whereHas('plans', function ($query) {
                $query->where('status', '=', 'active');
            })->orderBy('name', 'asc')->get();

        foreach($districts as $district){
            foreach($langs as $lang){
                $json[$lang][] = [
                    'id' => 'district_' . $district->id,
                    'name' => $district->name,
                    'user' => ''
                ];
            }
        }

        //$json = json_encode($json, JSON_UNESCAPED_UNICODE);

        foreach($langs as $lang){
            Storage::disk('public')->put('search/home_search_' . $lang . '.json', json_encode($json[$lang], JSON_UNESCAPED_UNICODE));
        }
    }
}
