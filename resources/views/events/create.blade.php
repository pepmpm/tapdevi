@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section ('body')
<div class="breadcrumbs-v4 breadcrumbs-crearevento">
    <div class="container">
        <span class="page-name">{{ trans('plan.eventos') }}</span>
        <h1>{{ trans('plan.nueva_experiencia_de_enoturismo') }}</h1>
        <ul class="breadcrumb-v4-in">
            <li><a href="{{ route('/') }}">{{ trans('inicio.inicio') }}</a></li>
            <li class="active">{{ trans('plan.nueva_experiencia_de_enoturismo') }}</li>
        </ul>
    </div>
</div>
<form class="margin-bottom-20" role="form" method="post" enctype="multipart/form-data" id="form_item"
      action="{{ route('enotourism-mallorca.events.store') }}">
    @csrf
    <div class="content container">
        <div class="row">
            <div class="col-md-9">
                @include('_layouts.messages', array('errors' => $errors))
                <h3>{{ trans('plan.tus_datos') }}</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ trans('plan.tu_nombre') }}</label>
                            @if(Auth::check() && Auth::user()->isAdmin())
                                <p class="font18">{{ Auth::user()->name }}</p>
                            @else
                                <input type="text" class="form-control input-lg {{ $errors->has('advisor_name') ? 'border-rojo' : "" }}"
                                    name="advisor_name" value="{{ old('advisor_name') }}"/>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ trans('plan.tu_email') }}</label>
                            @if(Auth::check() && Auth::user()->isAdmin())
                                <p class="font18">{{ Auth::user()->email }}</p>
                            @else
                                <input type="text" class="form-control input-lg {{ $errors->has('advisor_email') ? 'border-rojo' : "" }}"
                                    name="advisor_email" value="{{ old('advisor_email') }}"/>
                            @endif
                        </div>
                    </div>
                </div>

                <h3>{{ trans('plan.datos_del_evento') }}</h3>
                <div class="row">
                    <div class="col-md-7">
                        <div class="form-group">
                            <label>{{ trans('plan.nombre_del_evento') }}</label>
                            <input type="text" class="form-control input-lg {{ $errors->has('name') ? 'border-rojo' : "" }}"
                                name="name" value="{{ old('name') }}"/>
                            <div class="note">
                                {{ trans('plan.por_ejemplo') }}
                            </div>
                            {!! Form::select('labels_id', $labels_lov, '', array('class' => 'hide')) !!}
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>{{ trans('plan.poblacion') }}</label>
                            {!! Form::select('districts_id', isset($districts) ? $districts : [], 
                                old('districts_id', isset($query) ? $query->districts_id : ''), 
                                array('class' => 'form-control input-lg' . ($errors->has('districts_id') ? 'border-rojo' : "") )) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label>{{ trans('plan.direccion') }}</label>
                            <input type="text" class="form-control input-lg {{ $errors->has('address') ? 'border-rojo' : "" }}"
                                name="address" value="{{ old('address') }}"/>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>{{ trans('plan.codigo_postal') }}</label>
                            <input type="text" class="form-control input-lg {{ $errors->has('postal_code') ? 'border-rojo' : "" }}"
                                name="postal_code" value="{{ old('postal_code') }}"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{ trans('plan.cuando_empieza') }}</label>
                            <input type="text" class="form-control input-lg {{ $errors->has('dates') ? 'border-rojo' : "" }}"
                                name="dates" value="{{ old('dates') }}"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{ trans('plan.precio_por_persona') }}</label>
                            <input type="text" class="form-control input-lg {{ $errors->has('price') ? 'border-rojo' : "" }}"
                                name="price" value="{{ old('price') }}"/>
                            <div class="note">
                                {{ trans('plan.note_precio') }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{ trans('plan.horarios') }}</label>
                            {!! Form::select('times[]', isset($times) ? $times : [], 
                                old('times'), 
                                array(
                                    'class' => 'form-control ' . ($errors->has('times') ? 'border-rojo' : ""),
                                    'multiple' => 'multiple',
                                    'id' => 'times'
                                )) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>{{ trans('plan.detalles_del_evento') }}</label>
                            <textarea name="description[es]" class="form-control input-lg redactor" 
                                rows="4">{{ old('description.es') }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ trans('plan.imagen_del_evento') }}</label>
                            <div id="fileuploader">{{ trans('plan.subir') }}</div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ trans('plan.enlace_foto_instagram') }}</label>
                            <input type="text" class="form-control input-lg {{ $errors->has('instagram_url') ? 'border-rojo' : "" }}"
                                name="instagram_url" value="{{ old('instagram_url') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn-u btn-u-lg btn-block">{{ trans('plan.enviar_evento') }}</button>
                <div class="note">
                    {{ trans('plan.tu_propuesta_se_mostrara') }}
                </div>
            </div>
        </div>
    </div>
</form>

@stop

@section ('assets_end')
    @if($errors->has('description.es'))
        <style>
            .redactor-editor{border-color: red !important}
        </style>
    @endif
    <script type="text/javascript" src="{{ asset('assets/plugins/daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/daterangepicker/daterangepicker.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css') }}">

    <script src="{{ asset('assets/plugins/select2/dist/js/select2.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/dist/css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/dist/css/select2-input-lg.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/plugins/jquery-upload-file/uploadfile.css') }}">
    <script src="{{ asset('assets/plugins/jquery-upload-file/jquery.uploadfile.min.js') }}"></script>

    <script src="{{ asset('assets/plugins/redactor/redactor.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/plugins/redactor/redactor.css') }}">
    <script src="{{ asset('assets/plugins/redactor/lang/es.js') }}"></script>
    <script src="{{ asset('assets/plugins/redactor/plugins/read-more-plugin.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#times").select2({ selectOnClose: true });
            $('input[name="dates"]').daterangepicker({
                opens: 'left',
                autoApply: true,
                locale: {
                    format: 'DD/MM/YYYY'
                }
            });
            $("#fileuploader").uploadFile({
                url:"{{ route('events.photos.store') }}",
                fileName:"p_file",
                onSuccess:function(files,data,xhr,pd){
                    $('#form_item').append('<input type="hidden" name="p_foto[]" value="'+data+'" />');
                }
            });
            if($('.redactor').length){
                $('.redactor').redactor({
                    minHeight: 250,
                    lang: 'es',
                    plugins: ['leermas']
                });
            }
        });
    </script>
@stop
