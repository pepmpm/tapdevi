@extends('_layouts/inicio')

@section('title', trans('meta.title_index'))
@section ('meta')
    <meta name="description" content="{{ trans('meta.description_index') }}"/>
    <meta name="keywords" content="{{ trans('meta.keywords_index') }}" />
    <meta name="Author" content="{{ trans('meta.author') }}" />
    <meta property="og:title" content="{{ trans('meta.title_index') }}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:image" content="{{ url('assets/img/logo3.png') }}"/>
    <meta property="og:description" content="{{ trans('meta.description_index') }}" />
    @parent
@stop

@section ('body')
<div class="breadcrumbs-v4 breadcrumbs-register">
    <div class="container">
        <span class="page-name">{{ trans('inicio.registrarse') }}</span>
        <h1>{{ trans('inicio.registrarse_en') }}</h1>
        <ul class="breadcrumb-v4-in">
            <li><a href="{{ route('/') }}">{{ trans('inicio.inicio') }}</a></li>
            <li class="active">{{ trans('inicio.registrarse') }}</li>
        </ul>
    </div>
</div>
<div class="log-reg-v3 content-md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 md-margin-bottom-50">
                <h2 class="welcome-title">{{ trans('inicio.bienvenido_a') }}</h2>
                <div class="info-block-v2">
                    <i class="icon icon-settings"></i>
                    <div class="info-block-in">
                        <h3>{{ trans('inicio.titulo_login_1') }}</h3>
                        <p>{{ trans('inicio.mensaje_login_1') }}</p>
                    </div>
                </div>
                <div class="info-block-v2">
                    <i class="icon icon-eyeglasses"></i>
                    <div class="info-block-in">
                        <h3>{{ trans('inicio.titulo_login_2') }}</h3>
                        <p>{{ trans('inicio.mensaje_login_2') }}</p>
                    </div>
                </div>
                <div class="info-block-v2">
                    <i class="icon icon-paper-plane"></i>
                    <div class="info-block-in">
                        <h3>{{ trans('inicio.titulo_login_3') }}</h3>
                        <p>{{ trans('inicio.mensaje_login_3') }}</p>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <form id="sky-form1" method="POST" action="{{ route('register') }}" class="log-reg-block sky-form">
                    @csrf
                    <h2>{{ trans('inicio.registrarse_en') }}</h2>
                    <section>
                        <label class="input login-input">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" placeholder="{{ trans('inicio.nombre_empresa') }}" name="name" 
                                    class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}">
                            </div>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </label>
                    </section>
                    <section>
                        <label class="input login-input">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="email" placeholder="{{ trans('inicio.email') }}" name="email" 
                                    class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}">
                            </div>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </label>
                    </section>
                    <section>
                        <label class="input login-input no-border-top">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                        <input type="password" placeholder="{{ trans('inicio.password') }}" name="password"
                                            class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required>
                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                                            placeholder="{{ trans('inicio.confirm_password') }}" required>
                                    </div>
                                </div>
                            </div>
                        </label>
                    </section>

                    <button class="btn-u btn-u-sea-shop btn-block margin-bottom-20" type="submit">{{ trans('inicio.registrarse') }}</button>

                </form>

                <div class="margin-bottom-20"></div>
                <p class="text-center"><a href="{{ route('login') }}">{{ trans('inicio.login') }}</a></p>
            </div>
        </div>
    </div>
</div>

@stop

@section ('assets_end')

    <link rel="stylesheet" href="{{ asset('assets/css/pages/log-reg-v3.css') }}">

@stop
