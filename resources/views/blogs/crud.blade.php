@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section('title', trans('meta.title_index_event'))
@section ('meta')
    <meta name="description" content="{{ trans('meta.desc_index_event') }}"/>
    <meta name="keywords" content="{{ trans('meta.keys_index_event') }}" />
    <meta name="Author" content="{{ trans('meta.author') }}" />
    <meta property="og:title" content="{{ trans('meta.title_index_event') }}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:image" content="{{ url('assets/img/home/enoturismo-en-mallorca.webp') }}"/>
    <meta property="og:description" content="{{ trans('meta.desc_index_event') }}"/>
    @parent
@stop

@section ('body')
<div class="bg-color-light">
	<div class="container content-sm">
		<div class="row">
			<!-- Blog All Posts -->
			<div class="col-md-9">
				<!-- News v3 -->
				<div class="news-v3 bg-color-white margin-bottom-30">
					<img class="img-responsive full-width" src="assets/img/main/img12.jpg" alt="">
					<div class="news-v3-in">
						<h2>{{ $query->name }}</h2>
						<p>{!! $query->description !!}</p>
						<ul class="list-inline posted-info">
							@foreach(explode('#', $query->tags) as $tmptag)
								<li>{{ $tmptag }}</li>
							@endforeach
						</ul>
					</div>
				</div>
			</div>
			<!-- End Blog All Posts -->

			<!-- Blog Sidebar -->
			<div class="col-md-3">
				<div class="headline-v2"><h2>Tags</h2></div>
				<!-- Tags v2 -->
				<ul class="list-inline tags-v2 margin-bottom-50">
					@foreach($tags as $tag)
						<li><a href="#">{{ $tag }}</a></li>
					@endforeach
				</ul>
			</div>
			<!-- End Blog Sidebar -->
		</div>
	</div><!--/end container-->
</div>
@stop

@section ('assets_end')

@stop
