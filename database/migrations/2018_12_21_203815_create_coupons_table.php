<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
			$table->string('code', 80);
			$table->string('description', 250)->nullable();
            $table->enum('active', ['S', 'N'])->default('N');

            $table->unsignedInteger('discount')->nullable();
            $table->unsignedInteger('discount_shipping')->nullable();

            $table->timestamps();
			
            $table->index('code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
