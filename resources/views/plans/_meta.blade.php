@if(isset($params['date']))

    @if($params['date'] == 'weekend')

        @section('title', trans('meta.title_index_plan_date_' . $params['date']))
        @section ('meta')
            <meta name="description" content="{{ trans('meta.desc_index_plan_date_' . $params['date']) }}"/>
            <meta name="keywords" content="{{ trans('meta.keys_index_plan_date_' . $params['date']) }}" />
            <meta property="og:title" content="{{ trans('meta.title_index_plan_date_' . $params['date']) }}"/>
            <meta property="og:description" content="{{ trans('meta.desc_index_plan_date_' . $params['date']) }}"/>
            <meta name="Author" content="{{ trans('meta.author') }}" />
            <meta property="og:type" content="article"/>

            @parent
        @stop

    @else

        @section('title', trans('meta.title_index_plan_date_day', ['day' => $params['date']]))
        @section ('meta')
            <meta name="description" content="{{ trans('meta.desc_index_plan_date_day', ['day' => $params['date']]) }}"/>
            <meta name="keywords" content="{{ trans('meta.keys_index_plan_date_day', ['day' => $params['date']]) }}" />
            <meta property="og:title" content="{{ trans('meta.title_index_plan_date_day', ['day' => $params['date']]) }}"/>
            <meta property="og:description" content="{{ trans('meta.desc_index_plan_date_day', ['day' => $params['date']]) }}"/>
            <meta name="Author" content="{{ trans('meta.author') }}" />
            <meta property="og:type" content="article"/>

            @parent
        @stop

    @endif

    
@elseif($params['district'] != null)

    @php($tmpmeta = $district)

    @section('title', $tmpmeta->title)
    @section ('meta')
        <meta name="description" content="{{ $tmpmeta->seo_description }}"/>
        <meta name="keywords" content="{{ $tmpmeta->seo_keywords }}" />
        <meta property="og:title" content="{{ $tmpmeta->title }}"/>
        <meta property="og:image" content="{{ url('/') . '/' . $tmpmeta->url }}"/>
        <meta property="og:description" content="{{ $tmpmeta->seo_description }}"/>
        <meta name="Author" content="{{ trans('meta.author') }}" />
        <meta property="og:type" content="article"/>

        @parent
    @stop


@elseif($params['cat'] != null)

    @php($tmpmeta = $category)

    @section('title', $tmpmeta->title)
    @section ('meta')
        <meta name="description" content="{{ $tmpmeta->seo_description }}"/>
        <meta name="keywords" content="{{ $tmpmeta->seo_keywords }}" />
        <meta property="og:title" content="{{ $tmpmeta->title }}"/>
        <meta property="og:image" content="{{ url('/') . '/' . $tmpmeta->url }}"/>
        <meta property="og:description" content="{{ $tmpmeta->seo_description }}"/>
        <meta name="Author" content="{{ trans('meta.author') }}" />
        <meta property="og:type" content="article"/>

        @parent
    @stop

@else

    @section('title', trans('meta.title_index_plan'))
    @section ('meta')
        <meta name="description" content="{{ trans('meta.desc_index_plan') }}"/>
        <meta name="keywords" content="{{ trans('meta.keys_index_plan') }}" />
        <meta property="og:title" content="{{ trans('meta.title_index_plan') }}"/>
        <meta property="og:description" content="{{ trans('meta.desc_index_plan') }}"/>
        <meta name="Author" content="{{ trans('meta.author') }}" />
        <meta property="og:type" content="article"/>

        @parent
    @stop

@endif