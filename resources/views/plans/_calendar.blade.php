@for($x = 0; $x < 3; $x++)
    @php($current_month = strtotime("+" . $x . " month", strtotime(date('Y-m') . '-1')))
    <div class="row {{ $x > 0 ? 'hide' : '' }} table_month table_month_{{ $x }}">
        <div class="col-md-12">
            <table class="table table-bordered table-condensed text-center bgcolor-blanco color-negro" style="table-layout: fixed;">
                <thead>
                    <tr class="active">
                        <td>
                            <button class="btn btn-link color-tdv previous_month padding-lr-7" index="{{ $x }}">
                                <i class="fa fa-chevron-left" aria-hidden="true"></i>
                            </button>
                        </td>
                        <td colspan="5" class="text-center">
                            {{ trans('base.' . date("F", $current_month)) . ' ' . date("Y", $current_month) }}
                        </td>
                        <td>
                            <button class="btn btn-link color-tdv next_month padding-lr-7" index="{{ $x }}">
                                <i class="fa fa-chevron-right" aria-hidden="true"></i>
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>L</td>
                        <td>M</td>
                        <td>X</td>
                        <td>J</td>
                        <td>V</td>
                        <td>S</td>
                        <td>D</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        @php($first_day_week = date('N', $current_month))
                        @for($colspan_td = 1; $colspan_td < $first_day_week; $colspan_td++)
                            <td></td>
                        @endfor

                        @for($dia = 0; $dia < date('t', $current_month); $dia ++)

                            @php($tmpdate = date_create_from_format('Y-m-d', date("Y-m-d", strtotime("+" . $dia . " day", $current_month))))
                            <td class="text-center {{ in_array($tmpdate->format('Y-m-d'), $day_stocks) ? 'bgcolor-tdv color-blanco' : '' }}">
                                @if(in_array($tmpdate->format('Y-m-d'), $day_stocks))
                                    <label>
                                        <strong>
                                            <a class="color-blanco" 
                                                href="{{ route('enotourism-mallorca.day', [
                                                'day' => $tmpdate->format('d') . "-" . 
                                                strtolower(trans('base.' . $tmpdate->format('F'))) . "-" . 
                                                $tmpdate->format('Y')]) }}">
                                                {{ $tmpdate->format('d') }}
                                            </a>
                                        </strong>
                                    </label>
                                @else
                                    <label><strong>{{ $tmpdate->format('d') }}</strong></label>
                                @endif
                            </td>

                            @if($tmpdate->format('N') % 7 == 0)
                                </tr><tr>
                            @endif

                        @endfor
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endfor