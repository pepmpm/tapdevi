@if($query->plans->count() > 0)
    <h2>{{ trans('panel.listado_plans') }}</h2>
    <div class="row news-v1 margin-bottom-20">
        @foreach($query->plans as $plan)
            @include('plans.element', ['plan' => $plan])
            @if($loop->iteration % 4 == 0)
                </div><div class="row news-v1 margin-bottom-20"> 
            @endif
        @endforeach
    </div>
@endif