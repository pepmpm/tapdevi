<div class="navbar navbar-default mega-menu" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ isset($helpers) && $helpers->show_menu() ? route('/') : route('orders.index') }}">
                <img id="logo-header" src="{{ Imgfly::imgPublic('img/logo.png' . '?w=200', 'assets') }}" alt="Logo" class="img-responsive"/>
            </a>
        </div>
        
        <!-- Shopping Cart -->
        <div class="shop-badge badge-icons pull-right">
            <a href="#"><i class="fa fa-shopping-cart"></i></a>
            <span class="badge badge-sea rounded-x">{{ session()->has('cart') ? session('cart')['quantity'] : 0 }}</span>
            <div class="badge-open">
                <ul class="list-unstyled mCustomScrollbar" data-mcs-theme="minimal-dark">
                    @if(session()->has('cart'))
                        <li>
                            <img src="{{ Imgfly::imgPublic(str_replace("upload/", "", session('cart')['item']->one_photo()) . '?w=100', 'upload') }}" 
                                alt="{{ session('cart')['item']->name }}">
                            <a class="close" href="{{ route('orders.delete', ['id' => session('cart')['item']->id]) }}">x</a>
                            <div class="overflow-h">
                                <span>{{ session('cart')['item']->name }}</span>
                                <small>{{ session('cart')['quantity'] . " x " . session('cart')['item']->trueprice . "€" }}</small>
                            </div>
                        </li>
                    @endif
                </ul>
                <div class="subtotal">
                    <div class="overflow-h margin-bottom-10">
                        <span>Subtotal</span>
                        <span class="pull-right subtotal-cost">{{ (session()->has('cart') ? session('cart')['price'] : 0) . "€" }}</span>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <a href="{{ route('orders.index') }}" class="btn-u btn-brd btn-brd-hover btn-u-sea-shop btn-block">
                                {{ trans('base.ver_carrito') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if(isset($helpers) && $helpers->show_menu())
            @include('_layouts.menu')
        @endif
    </div>
</div>