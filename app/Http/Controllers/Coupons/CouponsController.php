<?php

namespace App\Http\Controllers\Coupons;

use App\Coupons;
use Illuminate\Http\Request;
use App\Traits\Helpers;
use Intervention\Image\ImageManagerStatic as Image;

class CouponsController extends \App\Http\Controllers\Controller
{
    use Helpers;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function _index()
    {
        return Coupons::all();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function _all()
    {
        return Coupons::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function _store($request)
    {
        $tmp = new Coupons;
        $tmp->code = mb_strtoupper($request['code'], 'UTF-8');
        $tmp->description = $request['description'];
		$tmp->active = $request['active'];
		$tmp->discount = $request['discount'];
        $tmp->save();

        return $tmp;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _show(Coupons $coupon)
    {
        return $coupon;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _update($request, Coupons $tmp)
    {
        $tmp->code = mb_strtoupper($request['code'], 'UTF-8');
        $tmp->description = $request['description'];
		$tmp->active = $request['active'];
		$tmp->discount = $request['discount'];
		$tmp->save();
        
        return $tmp;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _destroy(Coupons $coupon)
    {
        $coupon->delete();

        return true;
    }
}
