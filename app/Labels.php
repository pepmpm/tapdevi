<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use App\Traits\Helpers;

class Labels extends Model
{
    use Helpers;
    use Translatable;
    public $translatedAttributes = ['name', 'title', 'slug', 'description', 'seo_description', 'seo_keywords'];

    public function langs(){
        return $this->hasMany('\App\LabelsLangs', 'labels_id')->orderBy('id', 'asc');
    }

    public function plans(){
        return $this->hasMany('\App\Items', 'labels_id')->where('type', '=', 'plan')->orderBy('order', 'asc');
    }

    public function labels(){
        return $this->hasMany('\App\Labels', 'labels_id')->orderBy('id', 'asc');
    }

    public function label(){
        return $this->belongsTo('\App\Labels', 'labels_id');
    }

    public function labels_by_cat(){
        return $this->hasMany('\App\Labels', 'category', 'category')->orderBy('id', 'asc');
    }

}
