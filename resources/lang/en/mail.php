<?php

$idioma = array();

$idioma['gracias_por_comprar_en_'] = 'Thank you for shopping at ' . config('constants.NAMEWEB');
$idioma['codigo_del_pedido'] = 'Order code';
$idioma['tus_datos'] = 'Your info';
$idioma['nombre'] = 'Name';
$idioma['email'] = 'Email';
$idioma['telefono'] = 'Phone';
$idioma['poblacion'] = 'City';
$idioma['direccion'] = 'Address';

$idioma['para_ver_la_ubicacion_en_google_maps'] = 'To see the location on Google Maps press';
$idioma['aqui'] = 'here';
$idioma['comentarios'] = 'Remarks';
$idioma['pedido'] = 'Order';

$idioma['cantidad'] = 'Quantity';
$idioma['precio'] = 'Price';
$idioma['precio_total'] = 'Total price';

$idioma['de'] = 'of';
$idioma['fecha'] = 'Date';
$idioma['informacion'] = 'Information';
$idioma['detalle'] = 'Detail';
$idioma['a_pagar_ahora'] = 'To pay now';
$idioma['ya_pagado'] = 'Already paid';
$idioma['a_pagar_el_dia'] = 'To pay the day';
$idioma['es_un_regalo'] = "This activity is already paid. Contratulations!";
$idioma['precio_envio'] = 'Shipping price';
$idioma['lugar'] = 'Winery';

$idioma['reservado_el_dia'] = 'Booked the day';
$idioma['fecha_propuesta'] = 'Proposed date';
$idioma['valido_hasta_el_'] = 'Valid until :day';

$idioma['mensaje_qr'] = 'You must show or print this voucher. <br/>QR code to scan the day of the visit to mark it as done';
$idioma['mensaje_pdf_pack'] = 'To see the plans included in this pack, click on the following link: ';

return $idioma;