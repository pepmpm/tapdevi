<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Stichoza\GoogleTranslate\GoogleTranslate;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $data = array();
    protected $carts_id = null;
    
    public function __construct() {
        \App::setLocale(\LaravelLocalization::getCurrentLocale());

        $this->data['menu_enoturismo_2'] = \Cache::remember('menu_enoturismo_2', 1440, function () {
            return \App\Labels::with('plans')->withCount('plans')->whereHas('plans', function ($query) {
                    $query->where('status', '=', 'active');
                })->where('category', '=', 'enotourism-mallorca')->get();
        });
        $this->data['menu_routes'] = \Cache::remember('menu_routes', 1440, function () {
            return \App\Blogs::where('category', '=', 'route')->where('status', '=', 'active')
                ->orderBy('created_at', 'desc')->take(7)->get();
        });
        $this->data['contact_bar'] = 'full';
        $this->data['header'] = 'header';

        $this->data['idiomaweb'] = \LaravelLocalization::getCurrentLocale();

    }

    protected function _insert_user($params, $langs = false){
        if(isset($params['users_id']) && !is_numeric($params['users_id'])){
            $user = \App\User::firstOrNew(['email' => $params['email']]);

            //si no es un nuevo usuario no creamos los idiomas
            if($user->id != null){
                $langs = false;
            }

            $user->name = $params['users_id'];
            $user->email = $params['email'];
            $user->slug = $this->getSlug($params['users_id'], 'slug', \App\User::getModel());
            $user->password = Hash::make('Hola1234');
            $user->role = isset($params['role']) ? $params['role'] : 'user';
            $user->locations_id = isset($params['locations_id']) ? $params['locations_id'] : null;
            if(isset($params['town'])){
                $user->town = $params['town'];
            }else{
                $user->districts_id = self::_insert_district($params, $user);
            }
            
            $user->address = isset($params['address']) ? $params['address'] : null;
            $user->postal_code = isset($params['postal_code']) ? $params['postal_code'] : null;
            $user->phone = $params['phone'];
            $user->website = isset($params['website']) ? $params['website'] : null;
            $user->facebook_url = isset($params['facebook_url']) ? $params['facebook_url'] : null;
            $user->instagram_url = isset($params['instagram_url']) ? $params['instagram_url'] : null;

            $user->save();

            if(isset($params['role']) && $params['role'] == 'company'){
                $user->company_name = $params['users_id'];
                $result = self::_get_geoposition($params, $user);
                if($result != null){
                    $user->latitude = $result['latitude'];
                    $user->longitude = $result['longitude'];
                }
            }
            $user->save();

            if($langs){
                foreach(config('translatable.locales') as $keylocale => $locale){
                    $lang = new \App\UsersLangs;
                    $lang->locale = $keylocale;
                    $lang->users_id = $user->id;
                    $lang->save();
                }
            }

            return $user->id;
        }
        return $params['users_id'];
    }

    public function _insert_district($params, $user, $suffix = ''){
        if(isset($params['districts_id' . $suffix]) && !is_numeric($params['districts_id' . $suffix])){
            $dis = \App\Districts::firstOrNew(['slug' => str_slug($params['districts_id' . $suffix])]);
            $dis->name = $params['districts_id' . $suffix];
            $dis->slug = $this->getSlug($params['districts_id' . $suffix], 'slug', \App\Districts::getModel());
            $dis->locations_id = $params['locations_id'];
            $dis->save();

            return $dis->id;
        }
        if(isset($params['districts_id' . $suffix]) && is_numeric($params['districts_id' . $suffix])){
            return $params['districts_id' . $suffix];
        }
        
        return null;
    }

    private function _get_geoposition($params, $user){
        try{
            $address = urlencode($params['address'] . " " . $user->district->name . " " . $params['postal_code']);

            $url = "https://maps.google.com/maps/api/geocode/json?key=AIzaSyBw_spE5_xC8pjOBJVWs5LHLBKn0EC-cfQ&address=".$address."&region=Illes+Balears";
            logger('BUSCAMOS GEOPOSICIONAMIENTO: ' . $url);

            // convert response
            $r = json_decode(file_get_contents($url), true);

            if($r['status'] == 'OK') {
                $latitude = isset($r['results'][0]['geometry']['location']['lat']) ? $r['results'][0]['geometry']['location']['lat'] : "";
                $longitude = isset($r['results'][0]['geometry']['location']['lng']) ? $r['results'][0]['geometry']['location']['lng'] : "";
                if($latitude && $longitude) {
                    return array(
                        'latitude' => $latitude,
                        'longitude' => $longitude
                    );                         
                }         
            }
        } catch (Exception $e) {
            logger('ERROR: No se ha encontrado la geoposición');
        }

        return null;
    }

    protected function _translate_langs($spanish, $text, $locale, $limit, $strip_tags = null){
        if($text == null){
            if($spanish == null){
                return null;
            }
            try {
                $tr = new GoogleTranslate();
                $text = $tr->setSource('es')->setTarget($locale)->translate($spanish);
            } catch (ErrorException $e1) {
                return null;
            } catch (UnexpectedValueException $e2) {
                return null;
            }
        }

        if($limit != null){
            $text = str_limit($text, $limit);
        }

        if($strip_tags != null){
            $text = strip_tags($text);
        }

        return $text;
    }
}
