@extends('_layouts/inicio')

@section('title', trans('meta.title_index'))
@section ('meta')
    <meta name="description" content="{{ trans('meta.description_index') }}"/>
    <meta name="keywords" content="{{ trans('meta.keywords_index') }}" />
    <meta name="Author" content="{{ trans('meta.author') }}" />
    <meta property="og:title" content="{{ trans('meta.title_index') }}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:image" content="{{ url('assets/img/logo3.png') }}"/>
    <meta property="og:description" content="{{ trans('meta.description_index') }}" />
    @parent
@stop

@section ('body')

<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left">Home</h1>
        <ul class="pull-right breadcrumb">
            <li class="active">Home</li>
        </ul>
    </div>
</div>

<div class="container content profile">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-dark">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-tasks"></i> Visitas a vinos, planes y bodegas en {{ trans('base.' . date('F')) }}</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-2">
                            <span class="label" style="background-color: #f05b4f">&nbsp;&nbsp;</span> Planes
                        </div>
                        <div class="col-md-2">
                            <span class="label" style="background-color: #f4c63d">&nbsp;&nbsp;</span> Bodegas
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="full-chart">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section ('assets_end')
    <link rel="stylesheet" href="{{ asset('assets/plugins/chartist/chartist.min.css') }}">
    <script src="{{ asset('assets/plugins/chartist/chartist.min.js') }}"></script>

    <script type="text/javascript">
        @php($arrplans = [])
        @php($arrusers = [])
        @foreach($chart_plans as $chplan)
            @for($x = 1; $x <= 31; $x++)
                @php($arrplans[$x] = (isset($arrplans[$x]) ? $arrplans[$x] : 0) + $chplan->{'day_' . $x})
            @endfor
        @endforeach
        @foreach($chart_users as $chuser)
            @for($x = 1; $x <= 31; $x++)
                @php($arrusers[$x] = (isset($arrusers[$x]) ? $arrusers[$x] : 0) + $chuser->{'day_' . $x})
            @endfor
        @endforeach
        new Chartist.Line('.full-chart', {
            labels: [
            @for($x = 1; $x <= 31; $x++)
                '{{ $x }}',
            @endfor
            ],
            series: [
                {{ '[' }}
                @foreach($arrplans as $arrplan)
                    {{ $arrplan . ", " }}
                @endforeach
                {{ '],' }}
                {{ '[' }}
                @foreach($arrusers as $arruser)
                    {{ $arruser . ", " }}
                @endforeach
                {{ '],' }}
            ]
        }, {
            fullWidth: true,
            axisY: {
                onlyInteger: true
            },
            height: '300px'
        });
    </script>

@stop
