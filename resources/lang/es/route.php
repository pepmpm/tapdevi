<?php

$idioma = array();

$idioma['enotourism-mallorca-create'] = trans('inicio.enotourism-mallorca-slug', [], 'es').'/nuevo-evento';
$idioma['enotourism-mallorca-store'] = trans('inicio.enotourism-mallorca-slug', [], 'es').'/nuevo-evento/guardar';
$idioma['enotourism-mallorca-show_event'] = trans('inicio.enotourism-mallorca-slug', [], 'es').'/eventos/{item}';
$idioma['enotourism-mallorca-index'] = trans('inicio.enotourism-mallorca-slug', [], 'es').'/eventos/info/{year?}/{month?}/{day?}';
$idioma['enotourism-mallorca-map'] = trans('inicio.enotourism-mallorca-slug', [], 'es').'/eventos/mapa';


$idioma['enotourism-mallorca-cat'] = trans('inicio.enotourism-mallorca-slug', [], 'es').'/{cat?}';
$idioma['enotourism-mallorca-dis'] = trans('inicio.enotourism-mallorca-slug', [], 'es').'/en-{district?}';
$idioma['enotourism-mallorca-user'] = trans('inicio.enotourism-mallorca-slug', [], 'es').'/en-{district?}/{user}';
$idioma['enotourism-mallorca-item'] = trans('inicio.enotourism-mallorca-slug', [], 'es').'/en-{district?}/{user}/{item}';
$idioma['enotourism-mallorca-gift'] = trans('inicio.enotourism-mallorca-slug', [], 'es').'/regala-catas-de-vino-en-mallorca/{item}';

$idioma['route_index'] = 'rutas-del-vino-en-mallorca';
$idioma['route_show'] = 'rutas-del-vino-en-mallorca/{route}';

$idioma['enotourism-mallorca-day'] = trans('inicio.enotourism-mallorca-slug', [], 'es').'/para-{day}';
$idioma['enotourism-mallorca-day-weekend'] = trans('inicio.enotourism-mallorca-slug', [], 'es').'/para-el-fin-de-semana';

// temporal hasta que Google deje de indexar los vinos
$idioma['wines'] = trans('inicio.vinos_de_mallorca-slug', [], 'es') . '/{url?}/{url2?}/{url3?}/{url4?}';

$idioma['lots_index'] = 'lotes-de-vinos-y-productos-mallorquines';
$idioma['lots_show'] = 'lotes-de-vinos-y-productos-mallorquines/{cat}/{item}';

$idioma['wines_show'] = trans('inicio.vinos_de_mallorca-slug', [], 'es') . '/{cat}/{user}/{item}';
$idioma['search'] = 'buscar';

return $idioma;