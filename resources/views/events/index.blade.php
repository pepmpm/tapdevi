@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section('title', trans('meta.title_index_event'))
@section ('meta')
    <meta name="description" content="{{ trans('meta.desc_index_event') }}"/>
    <meta name="keywords" content="{{ trans('meta.keys_index_event') }}" />
    <meta name="Author" content="{{ trans('meta.author') }}" />
    <meta property="og:title" content="{{ trans('meta.title_index_event') }}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:image" content="{{ url('assets/img/home/enoturismo-en-mallorca.webp') }}"/>
    <meta property="og:description" content="{{ trans('meta.desc_index_event') }}"/>
    @parent
@stop

@section ('body')
<div class="breadcrumbs-v4 breadcrumbs-crearevento">
    <div class="container">
        <span class="page-name">{{ trans('plan.eventos') }}</span>
        <h1>{{ trans('events.agenda_de_eventos') }}</h1>
        <ul class="breadcrumb-v4-in">
            <li><a href="{{ route('/') }}">{{ trans('inicio.inicio') }}</a></li>
            <li class="active">{{ trans('events.agenda_de_eventos') }}</li>
        </ul>
    </div>
</div>
<div class="container content calendario">
    <div class="calendar-container">
        @php($previous_month = strtotime("-1 month", strtotime($year . '-' . $month . '-1')))
        @php($current_month = strtotime($year . '-' . $month . '-1'))
        @php($next_month = strtotime("+1 month", strtotime($year . '-' . $month . '-1')))

        <header class="margin-bottom-40">
            <div class="row">
                <div class="col-md-4">
                    @if($agent->isMobile())
                        <h3 class="text-center color-blanco">{{ ($day != null ? $day : $year) . " " . trans('base.' . date("F", $current_month)) }}</h3>
                    @else
                        @if($day != null)
                            <div class="day">{{ $day }}</div>
                            <div class="month">{{ trans('base.' . date("F", $current_month)) }}</div>
                        @else
                            <div class="year">{{ $year }}</div>
                            <div class="month padding-bottom-45">{{ trans('base.' . date("F", $current_month)) }}</div>
                        @endif
                    @endif
                </div>
                <div class="col-md-8">
                    <div id="scrollbar" class="panel-body no-padding height165">
                        @foreach($index_events as $event)
                            <h3 class="heading-xs">
                                <a href="{{ route('enotourism-mallorca.events.show', ['item' => $event->item->slug]) }}"
                                    class="color-blanco">
                                    <span class="label label-dark">{{ $helpers->fec_mysql_to_dm($event->date) }}</span>
                                    {{ ' ' . $event->item->name }}
                                </a>
                            </h3>
                        @endforeach
                    </div>
                </div>
            </div>
        </header>

        <table class="calendar table">
            <thead>
                <tr class="active">
                    <td>
                        <a class="btn btn-link color-tdv padding-lr-7"
                            href="{{ route('enotourism-mallorca.events.index', 
                                ['month' => strtolower(trans('base.' . date("F", $previous_month))), 'year' => date("Y", $previous_month)]) }}">
                            <i class="fa fa-chevron-left" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td colspan="5" class="text-center">
                    </td>
                    <td>
                        <a class="btn btn-link color-tdv padding-lr-7"
                            href="{{ route('enotourism-mallorca.events.index', 
                                ['month' => strtolower(trans('base.' . date("F", $next_month))), 'year' => date("Y", $next_month)]) }}">
                            <i class="fa fa-chevron-right" aria-hidden="true"></i>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>{{ trans('base.day_1') }}</td>
                    <td>{{ trans('base.day_2') }}</td>
                    <td>{{ trans('base.day_3') }}</td>
                    <td>{{ trans('base.day_4') }}</td>
                    <td>{{ trans('base.day_5') }}</td>
                    <td>{{ trans('base.day_6') }}</td>
                    <td>{{ trans('base.day_7') }}</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    @php($first_day_week = date('N', $current_month))
                    @for($colspan_td = 1; $colspan_td < $first_day_week; $colspan_td++)
                        <td></td>
                    @endfor

                    @for($dia = 0; $dia < date('t', $current_month); $dia ++)

                        @php($tmpdate = date_create_from_format('Y-m-d', date("Y-m-d", strtotime("+" . $dia . " day", $current_month))))
                        
                        @if(in_array($tmpdate->format('Y-m-d'), $day_stocks))
                            <td class="text-center">
                                <a href="{{ route('enotourism-mallorca.events.index', 
                                    ['month' => strtolower(trans('base.' . $tmpdate->format('F'))), 'year' => $tmpdate->format('Y'), 'day' => $tmpdate->format('d')]) }}"
                                    class="btn btn-u margin-bottom-4 padding-lr-2-6" style="white-space: normal;">
                                    <strong>{{ $tmpdate->format('d') }}</strong>
                                </a>
                            </td>
                        @else
                            <td class="text-center"><label><strong>{{ $tmpdate->format('d') }}</strong></label></td>
                        @endif

                        @if($tmpdate->format('N') % 7 == 0)
                            </tr><tr>
                        @endif

                    @endfor
                </tr>

            </tbody>

        </table>

        <div class="ring-left"></div>
        <div class="ring-right"></div>
    </div>
</div>
@stop

@section ('assets_end')
    <link rel="stylesheet" href="{{ asset('assets/css/pages/calendar.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/plugins/scrollbar/css/jquery.mCustomScrollbar.css') }}">
    <script type="text/javascript" src="{{ asset('assets/plugins/scrollbar/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/timeline.js') }}"></script>
@stop
