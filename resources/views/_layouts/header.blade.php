<div class="header-v5 header-static">
    <!-- Topbar v3 -->
    <div class="topbar-v3">
        <div class="search-open">
            <div class="container">
                <input type="text" class="form-control" placeholder="Search">
                <div class="search-close"><i class="icon-close"></i></div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <!-- Topbar Navigation -->
                    <ul class="left-topbar">
                        <li>
                            <a>Idioma ({{ LaravelLocalization::getCurrentLocale() }})</a>
                            <ul class="language">
                                <li class="active">
                                    <a href="{{ url('/') }}">
                                        Español (ES)&nbsp;
                                        @if(LaravelLocalization::getCurrentLocale() == 'es')
                                            <i class="fa fa-check"></i>
                                        @endif
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ LaravelLocalization::getLocalizedURL('ca') }}">
                                        Català (CAT)&nbsp;
                                        @if(LaravelLocalization::getCurrentLocale() == 'ca')
                                            <i class="fa fa-check"></i>
                                        @endif
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ LaravelLocalization::getLocalizedURL('en') }}">
                                        English (EN)&nbsp;
                                        @if(LaravelLocalization::getCurrentLocale() == 'en')
                                            <i class="fa fa-check"></i>
                                        @endif
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul><!--/end left-topbar-->
                </div>
                <div class="col-sm-6">
                    <ul class="list-inline right-topbar pull-right">
                        @if(\Auth::check())
                            <li><a href="{{ route('back') }}">{{ trans('base.hola') . " " . \Auth::user()->name }}</a></li>
                        @else
                            <li><a href="{{ route('login') }}">{{ trans('inicio.login') }}</a> | <a href="{{ route('register') }}">{{ trans('inicio.registrarse') }}</a></li>
                        @endif
                        <li><i class="search fa fa-search search-button"></i></li>
                    </ul>
                </div>
            </div>
        </div><!--/container-->
    </div>

    @include('_layouts.nav')
</div>