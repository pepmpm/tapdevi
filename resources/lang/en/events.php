<?php

$idioma = array();

$idioma['ver_en_mapa'] = 'See in map';
$idioma['agenda_de_eventos'] = 'Events calendar';
$idioma['rutas_de_vino_mallorca'] = 'Wine routes';
$idioma['mensaje_evento_pasado'] = 'This event has already taken place. If you are looking for activities related to wine, we recommend the following plans: ';

return $idioma;