<?php

$idioma = array();

$idioma['por_zona'] = 'Search by location';
$idioma['por_tipo'] = 'Search by type';
$idioma['por_experiencia'] = 'Search by experience';
$idioma['por_bodega'] = 'Search by place';

$idioma['anadir_al_carrito'] = 'Add to cart';

$idioma['categorias'] = 'Categories';
$idioma['comentarios'] = 'Review';
$idioma['comentar'] = 'Add review';
$idioma['cantidad'] = 'Quantity';
$idioma['seleccionar_fecha'] = 'Select date';
$idioma['mensaje_confirmacion_email'] = '+Info in WhatsApp or Email';
$idioma['mensaje_whatsapp_plan'] = "I'm interested in: :plan";
$idioma['idiomas_disponibles'] = 'Languages';

$idioma['horario'] = 'Timetable';
$idioma['llamar'] = 'Call';
$idioma['ajustar_busqueda'] = 'Adjust search';
$idioma['remarks_placeholder'] = 'Comments, allergens, details...';
$idioma['gift_remarks_placeholder'] = 'Write your dedication. It will appear on the voucher.';
$idioma['tu_mensaje'] = 'Your message';
$idioma['tu_nombre'] = 'Your name';
$idioma['quien_lo_regala'] = 'Who is the giver?';
$idioma['nombre_destinatario'] = "Recipient's name";
$idioma['anyade_mensaje_de_regalo'] = 'Add a gift message';
$idioma['reserva_con_antelacion'] = 'Book in advance';
$idioma['cancela_con_antelacion'] = 'Cancel in advance';
$idioma['duracion'] = 'Duration';
$idioma['confirmacion_direct'] = 'Payment at place<br/>(except in case of gift)';
$idioma['comprar_como_regalo'] = 'Give this as a gift';
$idioma['comprar'] = 'Buy';
$idioma['guia'] = 'Guide';
$idioma['dias'] = 'days';
$idioma['accesible_silla_ruedas'] = 'Wheelchair accessible';
$idioma['apto_mascotas'] = 'Pet friendly';
$idioma['apto_ninos'] = 'Suitable for children';
$idioma['parking_propio'] = 'Own parking';
$idioma['la_experiencia'] = 'The experience';
$idioma['detallado'] = 'Detailed';
$idioma['informacion_del_horario'] = 'Schedule information';
$idioma['por_persona'] = 'per person';
$idioma['reservar'] = 'Book now';
$idioma['mensaje_panel_reservar'] = 'Select participants, date and language';
$idioma['mensaje_note_boton_gift'] = "Or if you don't know the date";
$idioma['mensaje_panel_reservar_email'] = 'For more information contact us by Whatsapp or call';
$idioma['contacta_establecimiento_reservar'] = 'Booking date pending';


$idioma['nueva_experiencia_de_enoturismo'] = 'New wine tourism experience in Mallorca';
$idioma['eventos'] = 'Events';
$idioma['tus_datos'] = 'Your info';
$idioma['tu_nombre'] = 'Your name';
$idioma['tu_email'] = 'Your email';
$idioma['datos_del_evento'] = 'Event details';
$idioma['nombre_del_evento'] = 'Event name';
$idioma['precio_por_persona'] = 'Price per person';
$idioma['poblacion'] = 'Location';
$idioma['direccion'] = 'Address';
$idioma['horarios'] = 'Timetable';
$idioma['codigo_postal'] = 'Postal code';
$idioma['note_precio'] = "If you don't know don't fill it in, and if it is free put a 0";
$idioma['por_ejemplo'] = 'For example: Mallorcan wine tasting accompanied by sushi';
$idioma['cuando_empieza'] = 'When does it start and end?';
$idioma['detalles_del_evento'] = 'Event details';
$idioma['imagen_del_evento'] = 'Event image';
$idioma['enlace_foto_instagram'] = 'Link to the photo in Instagram';
$idioma['subir'] = 'Upload';
$idioma['enviar_evento'] = 'Send event';
$idioma['tu_propuesta_se_mostrara'] = 'Your proposal will be shown to the public after it has been reviewed';
$idioma['more_info'] = '+Info';

$idioma['gracias_por_enviar_el_evento'] = 'Thank you for sending us the information';
$idioma['enseguida_que_lo_revisemos'] = 'As soon as we review and complete the event information, we will publish it';
$idioma['selecciona_una_categoria'] = 'Select a category';
$idioma['mensaje_comentario'] = 'At ' . config('constants.NAMEWEB') . ' we are interested in your opinion about the visit you made. And also it will help other wine lovers to better understand the experience.';

return $idioma;