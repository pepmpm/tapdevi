<?php

$idioma = array();

$idioma['enotourism-mallorca-create'] = trans('inicio.enotourism-mallorca-slug', [], 'en').'/new-wine-event';
$idioma['enotourism-mallorca-store'] = trans('inicio.enotourism-mallorca-slug', [], 'en').'/new-wine-event/save';
$idioma['enotourism-mallorca-show_event'] = trans('inicio.enotourism-mallorca-slug', [], 'en').'/events/{item}';
$idioma['enotourism-mallorca-index'] = trans('inicio.enotourism-mallorca-slug', [], 'en').'/events/info/{year?}/{month?}/{day?}';
$idioma['enotourism-mallorca-map'] = trans('inicio.enotourism-mallorca-slug', [], 'es').'/eventos/map';

$idioma['enotourism-mallorca-cat'] = trans('inicio.enotourism-mallorca-slug', [], 'en').'/{cat?}';
$idioma['enotourism-mallorca-dis'] = trans('inicio.enotourism-mallorca-slug', [], 'en').'/in-{district?}';
$idioma['enotourism-mallorca-user'] = trans('inicio.enotourism-mallorca-slug', [], 'en').'/in-{district?}/{user}';
$idioma['enotourism-mallorca-item'] = trans('inicio.enotourism-mallorca-slug', [], 'en').'/in-{district?}/{user}/{item}';
$idioma['enotourism-mallorca-gift'] = trans('inicio.enotourism-mallorca-slug', [], 'es').'/give-wine-tastings-in-majorca/{item}';

$idioma['enotourism-mallorca-day'] = trans('inicio.enotourism-mallorca-slug', [], 'en').'/for-{day}';
$idioma['enotourism-mallorca-day-weekend'] = trans('inicio.enotourism-mallorca-slug', [], 'en').'/for-the-weekend';

$idioma['route_index'] = 'wine-routes-in-mallorca';
$idioma['route_show'] = 'wine-routes-in-mallorca/{route}';

// temporal hasta que Google deje de indexar los vinos
$idioma['wines'] = trans('inicio.vinos_de_mallorca-slug', [], 'en') . '/{url?}/{url2?}/{url3?}/{url4?}';

$idioma['lots_index'] = 'majorcan-wine-lots-and-products';
$idioma['lots_show'] = 'majorcan-wine-lots-and-products/{cat}/{item}';

$idioma['wines_show'] = trans('inicio.vinos_de_mallorca-slug', [], 'en') . '/{cat}/{user}/{item}';
$idioma['search'] = 'search';

return $idioma;