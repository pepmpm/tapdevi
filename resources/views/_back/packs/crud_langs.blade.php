<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-9">
                Textos
            </div>
            @if(isset($query))
                <div class="col-md-3 text-right">
                    <a href="{{ route('back.events.translate', ['item' => $query->id]) }}" id="btn_translate">
                        Traducir a CA y EN
                    </a>
                </div>
            @endif
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                @php($looplangs = isset($query) ? $query->langs : config('translatable.locales'))
                <div class="tab-v2">
                    <ul class="nav nav-tabs">
                        @foreach($looplangs as $keylocale => $lang)
                            @php($keylang = isset($query) ? $lang->locale : $keylocale)
                            @php($valuelang = isset($query) ? config('translatable.locales')[$lang->locale] : $lang)
                            <li class="{{ Request::get('idioma') == $keylang ? 'active' : (!Request::has('idioma') && $loop->first ? 'active' : '') }}">
                                <a href="#tapdevi{{ $keylang }}" data-toggle="tab">{{ $valuelang }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content bg-color-white">
                        @foreach($looplangs as $keylocale => $lang)
                            @php($keylang = isset($query) ? $lang->locale : $keylocale)
                            @php($valuelang = isset($query) ? config('translatable.locales')[$lang->locale] : $lang)
                            <div class="tab-pane fade in {{ Request::get('idioma') == $keylang ? 'active' : (!Request::has('idioma') && $loop->first ? 'active' : '') }}"
                                id="tapdevi{{ $keylang }}">
                                <input type="hidden" name="id[{{ $keylang }}]" value="{{ isset($query) ? $lang->id : '' }}"/>
                                <input type="hidden" name="locale[{{ $keylang }}]" value="{{ $keylang }}"/>
                                <div class="row">
                                    <div class="col-md-9">
                                        @if(isset($query))
                                            <div class="form-group">
                                                <label>Nombre</label>
                                                <input type="text" class="form-control {{ $errors->has('name.' . $keylang) ? 'border-rojo' : "" }}"
                                                    name="name[{{ $keylang }}]" placeholder="Nombre"
                                                    value="{{ old('name.' . $keylang, (isset($query) ? $lang->name : '')) }}"/>
                                            </div>
                                        @endif
                                        <div class="form-group">
                                            <label>Descripción</label>
                                            <textarea class="redactor {{ $errors->has('description.' . $keylang) ? 'border-rojo' : "" }}" 
                                                name="description[{{ $keylang }}]">{{ old('description.' . $keylang, (isset($query) ? $lang->description : '')) }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Seo tags</label>
                                                    <textarea class="form-control {{ $errors->has('seo_keywords.'.$keylang) ? 'border-rojo' : "" }}" 
                                                        name="seo_keywords[{{ $keylang }}]" 
                                                        rows="5">{{ old('seo_keywords.'.$keylang, (isset($query) ? $lang->seo_keywords : '')) }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Seo descripción</label>
                                                    <textarea class="form-control {{ $errors->has('seo_description.'.$keylang) ? 'border-rojo' : "" }}" 
                                                        name="seo_description[{{ $keylang }}]" 
                                                        rows="8">{{ old('seo_description.'.$keylang, (isset($query) ? $lang->seo_description : '')) }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>