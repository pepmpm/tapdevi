@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section ('body')

<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left">Pedidos</h1>
    </div>
</div>
<div class="container content profile">
    <div class="row">
        <div class="col-md-12">
            <table class="table display" id="tabla">
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Fecha</th>
                        <th>Precio total</th>
                        <th>Estado</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($query as $order)
                        <tr>
                            <td>{{ $order->code }}</td>
                            <td>{{ $helpers->fechora_mysql_to_str($order->created_at) }}</td>
                            <td>{{ $order->price . "€" }}</td>
                            <td>{{ $order->status }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@stop

@section ('assets_end')
@stop
