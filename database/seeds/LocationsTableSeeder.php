<?php

use Illuminate\Database\Seeder;
use \App\Locations;

class LocationsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        \DB::table('locations')->truncate();

        Locations::create(['start_postal_code' => '01', 'name' => 'Álava', 'slug' => 'alava', 'latitude' => '42.84998', 'longitude' => '-2.67268']);
        Locations::create(['start_postal_code' => '02', 'name' => 'Albacete', 'slug' => 'albacete', 'latitude' => '38.99424', 'longitude' => '-1.85643']);
        Locations::create(['start_postal_code' => '03', 'name' => 'Alicante', 'slug' => 'alicante', 'latitude' => '38.34517', 'longitude' => '-0.48149']);
        Locations::create(['start_postal_code' => '04', 'name' => 'Almería', 'slug' => 'almeria', 'latitude' => '36.83814', 'longitude' => '-2.45974']);
        Locations::create(['start_postal_code' => '33', 'name' => 'Asturias', 'slug' => 'asturias', 'latitude' => '43.36029', 'longitude' => '-5.84476']);
        Locations::create(['start_postal_code' => '05', 'name' => 'Ávila', 'slug' => 'avila', 'latitude' => '40.65', 'longitude' => '-4.7']);
        Locations::create(['start_postal_code' => '06', 'name' => 'Badajoz', 'slug' => 'badajoz', 'latitude' => '38.87789', 'longitude' => '-6.97061']);
        Locations::create(['start_postal_code' => '07', 'name' => 'Illes Balears', 'slug' => 'illes-balears', 'latitude' => '39.56939', 'longitude' => '2.65024']);
        Locations::create(['start_postal_code' => '08', 'name' => 'Barcelona', 'slug' => 'barcelona', 'latitude' => '41.38879', 'longitude' => '2.15899']);
        Locations::create(['start_postal_code' => '09', 'name' => 'Burgos', 'slug' => 'burgos', 'latitude' => '42.35', 'longitude' => '-3.7']);
        Locations::create(['start_postal_code' => '10', 'name' => 'Cáceres', 'slug' => 'caceres', 'latitude' => '39.47649', 'longitude' => '-6.37224']);
        Locations::create(['start_postal_code' => '11', 'name' => 'Cádiz', 'slug' => 'cadiz', 'latitude' => '36.53361', 'longitude' => '-6.29944']);
        Locations::create(['start_postal_code' => '39', 'name' => 'Cantabria', 'slug' => 'cantabria', 'latitude' => '43.46472', 'longitude' => '-3.80444']);
        Locations::create(['start_postal_code' => '12', 'name' => 'Castellón', 'slug' => 'castellon', 'latitude' => '39.98333', 'longitude' => '-0.03333']);
        Locations::create(['start_postal_code' => '51', 'name' => 'Ceuta', 'slug' => 'ceuta', 'latitude' => '35.8902800', 'longitude' => '-5.3075000']);
        Locations::create(['start_postal_code' => '13', 'name' => 'Ciudad Real', 'slug' => 'ciudad-real', 'latitude' => '38.98333', 'longitude' => '-3.93333']);
        Locations::create(['start_postal_code' => '14', 'name' => 'Córdoba', 'slug' => 'cordoba', 'latitude' => '37.88333', 'longitude' => '-4.76667']);
        Locations::create(['start_postal_code' => '16', 'name' => 'Cuenca', 'slug' => 'cuenca', 'latitude' => '40.07492', 'longitude' => '-2.13615']);
        Locations::create(['start_postal_code' => '17', 'name' => 'Girona', 'slug' => 'girona', 'latitude' => '41.98311', 'longitude' => '2.82493']);
        Locations::create(['start_postal_code' => '18', 'name' => 'Granada', 'slug' => 'granada', 'latitude' => '37.18817', 'longitude' => '-3.60667']);
        Locations::create(['start_postal_code' => '19', 'name' => 'Guadalajara', 'slug' => 'guadalajara', 'latitude' => '40.63333', 'longitude' => '-3.16667']);
        Locations::create(['start_postal_code' => '20', 'name' => 'Guipúzcoa', 'slug' => 'guipuzcoa', 'latitude' => '43.1752', 'longitude' => '-2.18666']);
        Locations::create(['start_postal_code' => '21', 'name' => 'Huelva', 'slug' => 'huelva', 'latitude' => '37.25833', 'longitude' => '-6.95083']);
        Locations::create(['start_postal_code' => '22', 'name' => 'Huesca', 'slug' => 'huesca', 'latitude' => '42.13615', 'longitude' => '-0.4087']);
        Locations::create(['start_postal_code' => '23', 'name' => 'Jaén', 'slug' => 'jaen', 'latitude' => '37.76922', 'longitude' => '-3.79028']);
        Locations::create(['start_postal_code' => '15', 'name' => 'La Coruña', 'slug' => 'la-coruna', 'latitude' => '43.37135', 'longitude' => '-8.396']);
        Locations::create(['start_postal_code' => '26', 'name' => 'La Rioja', 'slug' => 'la-rioja', 'latitude' => '42.46667', 'longitude' => '-2.45']);
        Locations::create(['start_postal_code' => '35', 'name' => 'Las Palmas', 'slug' => 'las-palmas', 'latitude' => '28.09973', 'longitude' => '-15.41343']);
        Locations::create(['start_postal_code' => '24', 'name' => 'León', 'slug' => 'leon', 'latitude' => '42.6', 'longitude' => '-5.56667']);
        Locations::create(['start_postal_code' => '25', 'name' => 'Lleida', 'slug' => 'lleida', 'latitude' => '41.61667', 'longitude' => '0.61667']);
        Locations::create(['start_postal_code' => '27', 'name' => 'Lugo', 'slug' => 'lugo', 'latitude' => '43.00992', 'longitude' => '-7.55602']);
        Locations::create(['start_postal_code' => '28', 'name' => 'Madrid', 'slug' => 'madrid', 'latitude' => '40.4165', 'longitude' => '-3.70256']);
        Locations::create(['start_postal_code' => '29', 'name' => 'Málaga', 'slug' => 'malaga', 'latitude' => '36.72016', 'longitude' => '-4.42034']);
        Locations::create(['start_postal_code' => '52', 'name' => 'Melilla', 'slug' => 'melilla', 'latitude' => '35.29369', 'longitude' => '-2.93833']);
        Locations::create(['start_postal_code' => '30', 'name' => 'Murcia', 'slug' => 'murcia', 'latitude' => '37.98704', 'longitude' => '-1.13004']);
        Locations::create(['start_postal_code' => '31', 'name' => 'Navarra', 'slug' => 'navarra', 'latitude' => '42.81687', 'longitude' => '-1.64323']);
        Locations::create(['start_postal_code' => '32', 'name' => 'Orense', 'slug' => 'orense', 'latitude' => '42.33669', 'longitude' => '-7.86407']);
        Locations::create(['start_postal_code' => '34', 'name' => 'Palencia', 'slug' => 'palencia', 'latitude' => '42.01667', 'longitude' => '-4.53333']);
        Locations::create(['start_postal_code' => '36', 'name' => 'Pontevedra', 'slug' => 'pontevedra', 'latitude' => '42.431', 'longitude' => '-8.64435']);
        Locations::create(['start_postal_code' => '37', 'name' => 'Salamanca', 'slug' => 'salamanca', 'latitude' => '40.96882', 'longitude' => '-5.66388']);
        Locations::create(['start_postal_code' => '38', 'name' => 'Santa Cruz de Tenerife', 'slug' => 'santa-cruz-de-tenerife', 'latitude' => '28.46824', 'longitude' => '-16.25462']);
        Locations::create(['start_postal_code' => '40', 'name' => 'Segovia', 'slug' => 'segovia', 'latitude' => '40.95', 'longitude' => '-4.11667']);
        Locations::create(['start_postal_code' => '41', 'name' => 'Sevilla', 'slug' => 'sevilla', 'latitude' => '37.38241', 'longitude' => '-5.97613']);
        Locations::create(['start_postal_code' => '42', 'name' => 'Soria', 'slug' => 'soria', 'latitude' => '41.66667', 'longitude' => '-2.66667']);
        Locations::create(['start_postal_code' => '43', 'name' => 'Tarragona', 'slug' => 'tarragona', 'latitude' => '41.11667', 'longitude' => '1.25']);
        Locations::create(['start_postal_code' => '44', 'name' => 'Teruel', 'slug' => 'teruel', 'latitude' => '40.3456', 'longitude' => '-1.10646']);
        Locations::create(['start_postal_code' => '45', 'name' => 'Toledo', 'slug' => 'toledo', 'latitude' => '39.8581', 'longitude' => '-4.02263']);
        Locations::create(['start_postal_code' => '46', 'name' => 'Valencia', 'slug' => 'valencia', 'latitude' => '39.46975', 'longitude' => '-0.37739']);
        Locations::create(['start_postal_code' => '47', 'name' => 'Valladolid', 'slug' => 'valladolid', 'latitude' => '41.65518', 'longitude' => '-4.72372']);
        Locations::create(['start_postal_code' => '48', 'name' => 'Vizcaya', 'slug' => 'vizcaya', 'latitude' => '43.26271', 'longitude' => '-2.92528']);
        Locations::create(['start_postal_code' => '49', 'name' => 'Zamora', 'slug' => 'zamora', 'latitude' => '41.50633', 'longitude' => '-5.74456']);
        Locations::create(['start_postal_code' => '50', 'name' => 'Zaragoza', 'slug' => 'zaragoza', 'latitude' => '41.65606', 'longitude' => '-0.87734']);
    }
}
