<div class="row">
    <div class="col-md-5">
        @if(isset($show_link_map) && $show_link_map)
            <div class="pull-left">
                <a class="font13" href="{{ route('enotourism-mallorca.events.map') }}">{{ trans('events.ver_en_mapa') }}</a>
            </div>
        @endif
    </div>
    <div class="col-md-7">
        <div class="pull-right">
            <a class="font13" href="{{ route('enotourism-mallorca.events.index') }}">{{ trans('events.agenda_de_eventos') }}</a>
        </div>
    </div>
</div>

<div class="row margin-top-15 {{ isset($fullheight) ? 'min-height-100vh' : 'mCustomScrollbar450' }} no-padding" id="scrollbar" data-mcs-theme="minimal-dark">
    <div class="col-md-12">
        <ul class="timeline-v2">
            @foreach($index_events as $event)
                @if($event->next_stock()->first() != null)
                    @php($tmpdate = $event->next_stock()->first()->date)
                    <li class="equal-height-columns">
                        <div class="cbp_tmtime equal-height-column">
                            <strong>
                                <span class="font13">
                                    {{ date("d", strtotime($tmpdate)) }}<br/><br/>
                                </span>
                                <span class="font11">
                                    {{ strtoupper(trans('base.mes' . date("m", strtotime($tmpdate)))) }}
                                </span>
                            </strong>
                        </div>
                        <i class="cbp_tmicon rounded-x hidden-xs"></i>
                        <div class="cbp_tmlabel equal-height-column">
                            <a href="{{ route('enotourism-mallorca.events.show', ['item' => $event->slug]) }}">
                                <p class="font17 border-bottom-gris">{{ $event->name }}</p>
                            </a>
                            <p>
                                {{ 
                                    (is_numeric($event->eventprice) ? $event->eventprice . "€" : $event->eventprice) .
                                    (isset($event->districts_id) ? ' | ' . $event->district->name : '') . ' | ' 
                                }}
                                @if(isset($show_link_map) && !$show_link_map)
                                    <a href="#" class="pan-to-marker" data-marker-index="{{ $loop->index }}"><i class="fa fa-search" aria-hidden="true"></i></a>
                                @endif
                            </p>
                        </div>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
</div>