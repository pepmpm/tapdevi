<?php

$idioma = array();

$idioma['gracias_por_comprar_en_'] = 'Gracias por comprar en ' . config('constants.NAMEWEB');
$idioma['codigo_del_pedido'] = 'Código del pedido';
$idioma['tus_datos'] = 'Tus datos';
$idioma['nombre'] = 'Nombre';
$idioma['email'] = 'Email';
$idioma['telefono'] = 'Teléfono';
$idioma['poblacion'] = 'Población';
$idioma['direccion'] = 'Dirección';

$idioma['para_ver_la_ubicacion_en_google_maps'] = 'Para ver la ubicación en Google Maps pulsa';
$idioma['aqui'] = 'aquí';
$idioma['comentarios'] = 'Comentarios';
$idioma['pedido'] = 'Pedido';

$idioma['cantidad'] = 'Cantidad';
$idioma['precio'] = 'Precio';
$idioma['precio_total'] = 'Precio total';

$idioma['de'] = 'de';
$idioma['fecha'] = 'Fecha';
$idioma['informacion'] = 'Información';
$idioma['detalle'] = 'Detalle';
$idioma['a_pagar_ahora'] = 'A pagar ahora';
$idioma['ya_pagado'] = 'Ya pagado';
$idioma['a_pagar_el_dia'] = 'Pendiente pago';
$idioma['es_un_regalo'] = 'Esta actividad ya está pagada. ¡Enhorabuena!';
$idioma['precio_envio'] = 'Precio envío';
$idioma['lugar'] = 'Bodega';

$idioma['reservado_el_dia'] = 'Reservado el día';
$idioma['fecha_propuesta'] = 'Fecha propuesta';
$idioma['valido_hasta_el_'] = 'Válido hasta el :day';

$idioma['mensaje_qr'] = '<span style="color: red">Debes imprimir o llevar en el móvil este bono.</span><br/>Código QR a escanear el día de la visita para marcarla como realizada.';
$idioma['mensaje_pdf_pack'] = 'Para ver los planes incluidos en este pack pulsa en el siguiente enlace: ';
$idioma['descargar_bono'] = 'Descargar bono en PDF';
$idioma['ver_planes_incluidos'] = 'Ver planes incluidos en el pack';

return $idioma;