<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url', 250)->nullable();
            $table->unsignedInteger('order')->default(0);

            $table->unsignedInteger('items_id');
            $table->foreign('items_id')->references('id')->on('items')->onDelete('cascade');
            $table->timestamps();

            $table->index('items_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items_photos');
    }
}
