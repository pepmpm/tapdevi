@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section ('body')

<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left">Actividad: {{ $query->name }}</h1>
    </div>
</div>
<div class="container content">
    <div class="row margin-bottom-15">
        <div class="col-md-12">
            @include('_layouts.messages', array('errors' => $errors))
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('back.packs.index') }}" 
               class="btn-u btn-u-lg" role="button">
                Volver a la lista
            </a>
            <a href="{{ route('back.packs.create') }}" 
               class="btn-u btn-u-lg" role="button">
                Crear otra actividad
            </a>
            <a href="{{ route('back.packs.show', [$query->id]) }}" 
               class="btn-u btn-u-green btn-u-lg" role="button">
                Modificar información
            </a>
        </div>
    </div>
    <hr/>

    <form method="post" id="form_additems" action="{{ route('back.plans.additems', ['items_id' => $query->id]) }}">
        @csrf

        <div class="row margin-bottom-15">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary btn-block btn-sm" role="button">
                    <i class="fa fa-save" aria-hidden="true"></i>&nbsp;Guardar
                </button>
            </div>
        </div>
        <h2>Planes seleccionados</h2>
        <table class="table" id="tabla">
            <thead>
                <th>#</th>
                <th>Nombre</th>
                <th>Categoría</th>
                <th>Proveedor</th>
                <th>Precio</th>
            </thead>
            <tbody>
                @foreach($items_selected as $item_selected)
                    <tr>
                        <td>
                            <label class="checkbox">
                                <input type="checkbox" class="chk_unselect" value="{{ $item_selected->id }}" {{ old('remember') ? 'checked' : '' }}/>
                            </label>
                        </td>
                        <td>{{ $item_selected->name }}</td>
                        <td>{{ $item_selected->labels_id != null ? $item_selected->label->name : '' }}</td>
                        <td>{{ $item_selected->user->name }}</td>
                        <td>{{ $item_selected->trueprice }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <h2>Planes disponibles</h2>
        <table class="table" id="tabla2">
            <thead>
                <th>#</th>
                <th>Nombre</th>
                <th>Categoría</th>
                <th>Proveedor</th>
                <th>Precio</th>
            </thead>
            <tbody>
                @foreach($items as $item)
                    <tr>
                        <td>
                            <label class="checkbox">
                                <input type="checkbox" class="chk_select" value="{{ $item->id }}" {{ old('remember') ? 'checked' : '' }}/>
                            </label>
                        </td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->labels_id != null ? $item->label->name : '' }}</td>
                        <td>{{ $item->user->name }}</td>
                        <td>{{ $item->trueprice }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </form>
</div>
@stop

@section ('assets_end')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/datatables.min.css') }}">
    <script src="{{ asset('assets/plugins/datatables/datatables.min.js') }}"></script>

    <script type="text/javascript">
        $(function(){
            $(document).ready(function() {
                $('#tabla').DataTable({
                    "language": {
                        "url": ARNY.baseUrl + "/assets/plugins/datatables/spanish.lang"
                    },
                    stateSave: true
                });

                $('#tabla2').DataTable({
                    "language": {
                        "url": ARNY.baseUrl + "/assets/plugins/datatables/spanish.lang"
                    },
                    stateSave: true
                });

                $(".chk_select").change(function(){
                    $('#form_additems').append('<input type="hidden" name="item_selected[]" value="'+$(this).val()+'" />');
                });
                $(".chk_unselect").change(function(){
                    $('#form_additems').append('<input type="hidden" name="item_unselected[]" value="'+$(this).val()+'" />');
                });
            });
        });
    </script>
@stop