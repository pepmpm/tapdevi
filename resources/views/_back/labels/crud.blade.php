@extends('_layouts/inicio')

@section ('body')

<!--=== Breadcrumbs ===-->
<div class="breadcrumbs">
    <div class="container">
        @if(isset($query))
            <h1 class="pull-left">Categoría: {{ $query->name }}</h1>
        @else
            <h1 class="pull-left">Nueva categoría</h1>
        @endif
    </div><!--/container-->
</div><!--/breadcrumbs-->
<div class="container content">
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('back.labels.index') }}" 
               class="btn-u btn-u-sea-shop btn-u-lg" role="button">
                Volver a la lista
            </a>
            <a href="{{ route('back.labels.create') }}" 
               class="btn-u btn-u-sea-shop btn-u-lg" role="button">
                Crear otra categoria
            </a>
        </div>     
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            @include('_layouts.messages', array('errors' => $errors))
            <div class="panel panel-u margin-bottom-40 bg-grey">
                <div class="panel-body">
                    <form class="margin-bottom-20" role="form" method="post" enctype="multipart/form-data" 
                          action="{{ isset($query) ? route('back.labels.update', $query->id) : route('back.labels.store') }}">
                        @if(isset($query))
                            @method('PUT')
                        @endif
                        @csrf
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Status</label>
                                    {!! Form::select('status', $status, 
                                        old('status', isset($query) ? $query->status : ''), 
                                        array('class' => 'form-control', 'id' => 'status')) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Categoría padre</label>
                                    {!! Form::select('labels_id', array('' => '') + $labels, 
                                        old('labels_id', isset($query) ? $query->labels_id : ''), 
                                        array('class' => 'form-control', 'id' => 'labels_id')) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Subir imagen de cabecera</label>
                                    <input id="p_foto" type="hidden" name="p_foto">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Subir imagen pequeña de menú</label>
                                    <input id="p_menu" type="hidden" name="p_menu">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Tipo</label>
                                    {!! Form::select('category', $categories, 
                                        old('category', isset($query) ? $query->category : ''), 
                                        array('class' => 'form-control', 'id' => 'category')) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    @if(isset($query) && $query->url != null)
                                        <label>Imagen de cabecera</label>
                                        <a class="fancybox" data-rel="galeria_fotos"
                                            title="{{ $query->name }}"
                                            href="{{ url('/') . '/' . $query->url }}">
                                            <img src="{{ url('/') . '/' . $query->url }}" alt="{{ $query->name }}" class="img-responsive" />
                                        </a>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    @if(isset($query) && $query->icon != null)
                                        <label>Imagen de pequeña</label>
                                        <a class="fancybox" data-rel="galeria_fotos" title="{{ $query->name }}"
                                            href="{{ url('/') . '/' . $query->icon }}">
                                            <img src="{{ url('/') . '/' . $query->icon }}" alt="{{ $query->name }}" class="img-responsive" />
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <button type="submit" class="btn-u btn-u-sea-shop btn-block" style="margin-top: 25px">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-md-12">
                            @if(isset($query))
                                @include('_back.labels.crud_langs', array('query' => $query))
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section ('assets_end')
    <link rel="stylesheet" href="{{ asset('assets/plugins/crop-image/components/imgareaselect/css/imgareaselect-default.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/crop-image/css/jquery.awesome-cropper.css') }}">
    <script src="{{ asset('assets/plugins/crop-image/components/imgareaselect/scripts/jquery.imgareaselect.js') }}"></script>
    <script src="{{ asset('assets/plugins/crop-image/build/jquery.awesome-cropper.js') }}"></script>

    <link rel="stylesheet" href="{{ asset('assets/plugins/fancybox/source/jquery.fancybox.css') }}">
    <script src="{{ asset('assets/plugins/fancybox/source/jquery.fancybox.pack.js') }}"></script>

    <script src="{{ asset('assets/plugins/redactor/redactor.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/plugins/redactor/redactor.css') }}">
    <script src="{{ asset('assets/plugins/redactor/lang/es.js') }}"></script>
    <script src="{{ asset('assets/plugins/redactor/plugins/read-more-plugin.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            if($('.redactor').length){
                $('.redactor').redactor({
                    minHeight: 400,
                    lang: 'es',
                    plugins: ['leermas']
                });
            }

            $('#p_foto').awesomeCropper({
                width: 1600,
                height: 215
            });
            $('#p_menu').awesomeCropper({
                width: 250,
                height: 250
            });
        });
        $(function(){
            if($().fancybox){
                $(".fancybox").fancybox({
                    groupAttr: 'data-rel',
                    prevEffect: 'fade',
                    nextEffect: 'fade',
                    openEffect  : 'elastic',
                    closeEffect  : 'fade',
                    closeBtn: true,
                    helpers: {
                        title: {
                            type: 'float'
                        }
                    }
                });
            }
        });
    </script>
@stop
