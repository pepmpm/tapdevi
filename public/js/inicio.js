$(document).ready(function() {
  //$("#labels_id").select2({theme: "bootstrap"});
    jQuery('.search').live("click", function() {
      if(jQuery('.search-btn').hasClass('fa-search')){
        jQuery('.search-open').fadeIn(500);
        jQuery('.search-btn').removeClass('fa-search');
        jQuery('.search-btn').addClass('fa-times');
      } else {
        jQuery('.search-open').fadeOut(500);
        jQuery('.search-btn').addClass('fa-search');
        jQuery('.search-btn').removeClass('fa-times');
      }
    });
    var owl = jQuery(".owl-categories-home");
    owl.owlCarousel({
        items: [5],
        itemsDesktop : [1000,5],
        itemsDesktopSmall : [900,4],
        itemsTablet: [600,3],
        itemsMobile : [479,2],
        slideSpeed: 1000
    });

    // Custom Navigation Events
    jQuery(".next-v2").click(function(){
        owl.trigger('owl.next');
    });
    jQuery(".prev-v2").click(function(){
        owl.trigger('owl.prev');
    });
});
$(window).bind("pageshow", function() {
    $('#home_form')[0].reset();
});