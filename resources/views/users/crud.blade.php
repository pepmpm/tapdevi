@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section('title', $query->title)
@section ('meta')
    <meta name="description" content="{{ $query->seo_description }}"/>
    <meta name="keywords" content="{{ $query->seo_keywords }}" />
    <meta name="Author" content="{{ trans('meta.author') }}" />
    <meta property="og:title" content="{{ $query->name }}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:image" content="{{ url('/') . '/' . $query->one_photo('upload') }}"/>
    <meta property="og:description" content="{{ $query->seo_description }}"/>
    @parent
@stop

@section ('body')
@php($tmpdistrict = $query->district)
<div class="shop-product">
    <div class="container">
        <ul class="breadcrumb-v5">
            <li><a href="{{ route('/') }}"><i class="fa fa-home"></i></a></li>
            <li><a href="{{ route('enotourism-mallorca.cat') }}">{{ trans('inicio.enotourism-mallorca') }}</a></li>
            <li><a href="{{ route('enotourism-mallorca.dis', ['district' => $tmpdistrict->slug]) }}">{{ $tmpdistrict->name }}</a></li>
            <li class="active">{{ $query->name }}</li>
        </ul>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-6 md-margin-bottom-50">
                <div class="shop-product-heading">
                    <h2>
                        {{ $query->name }}
                    </h2>
                    <ul class="list-inline shop-product-social">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                </div>
                <ul class="list-inline product-ratings margin-bottom-30">
                    <!--li><i class="rating-selected fa fa-star"></i></li>
                    <li><i class="rating-selected fa fa-star"></i></li>
                    <li><i class="rating-selected fa fa-star"></i></li>
                    <li><i class="rating fa fa-star"></i></li>
                    <li><i class="rating fa fa-star"></i></li-->
                    <li class="product-review-list">
                        <span>
                            (<span class="disqus-comment-count" data-disqus-identifier="{{ Route::currentRouteName() }}"></span>)&nbsp;
                            <a href="#comentarios">{{ trans('plan.comentarios') }}</a> | <a href="#"> {{ trans('plan.comentar') }}</a>
                        </span>
                    </li>
                </ul>
                <div class="row">
                    <div class="col-md-12">
                        <div class="ms-showcase2-template">
                            <div class="master-slider ms-skin-default" id="masterslider">
                                @foreach($query->photos as $photo)
                                <div class="ms-slide">
                                    <img class="ms-brd" src="{{ Imgfly::imgPublic(str_replace("upload/", "", $photo->url) . '?w=550', 'upload') }}" 
                                        alt="{{ $query->name }}" class="img-responsive"/>
                                    <img class="ms-thumb" src="{{ Imgfly::imgPublic(str_replace("upload/", "", $photo->url) . '?w=180', 'upload') }}" 
                                        alt="{{ $query->name }} thumb">
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                @if($query->logo != null)
                    <div class="row">
                        <div class="col-md-12 margin-bottom-30">
                            <img src="{{ Imgfly::imgPublic(str_replace("upload/", "", $query->logo) . '?w=200', 'upload') }}" class="img-responsive" />
                        </div>
                    </div>
                @endif

                {!! $query->description !!}

                <ul class="list-inline add-to-wishlist add-to-wishlist-brd">
                    <li class="compare-in">
                        <i class="fa fa-map-pin"></i>
                        <a href="{{ route('enotourism-mallorca.dis', ['district' => $tmpdistrict->slug]) }}">
                            {{ $tmpdistrict->name }}
                        </a>
                    </li>
                </ul>
                <p class="wishlist-category"><strong>{{ trans('user.direccion') }}:</strong> {{ $query->address }}</p>
                <p class="wishlist-category">
                    <strong>{{ trans('user.poblacion') }}:</strong> {{ $query->district->name . " (" . $query->location->name . ")" }}&nbsp;
                    <strong>{{ trans('user.codigo_postal') }}:</strong> {{ $query->postal_code }}
                </p>
                <p class="wishlist-category">
                    @if($query->phone != null && false)
                        <strong>{{ trans('user.phone') }}:</strong> {{ $query->phone }}&nbsp;
                    @endif
                    @if($query->website != null)
                        <strong>{{ trans('user.pagina_web') }}:</strong> {{ $query->website }}
                    @endif
                </p>
                @if($query->longitude != null || $query->latitude != null)
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" id="longitud" value="{{ $query->longitude }} "/>
                            <input type="hidden" id="latitud" value="{{ $query->latitude }} "/>
                            <div id="map" class="height250"></div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        @include('users.crud_plans')
    </div>
</div>



<div class="content container" id="comentarios">
    <div class="row">
        <div class="col-md-12">
            <div class="product-comment margin-bottom-40">
                <div id="disqus_thread"></div>
                <script>
                var disqus_config = function () {
                    this.page.url = '{{ LaravelLocalization::getLocalizedURL('es', Request::url()) }}';
                    this.page.identifier = '{{ Route::currentRouteName() }}';
                };
                (function() {
                var d = document, s = d.createElement('script');
                s.src = 'https://https-www-tapdevi-com.disqus.com/embed.js';
                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
                })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>                            
            </div>
        </div>
    </div>
</div>
@stop

@section ('assets_end')
    <link rel="stylesheet" href="{{ asset('assets/plugins/master-slider/masterslider/style/masterslider.css') }}">
    <link rel='stylesheet' href="{{ asset('assets/plugins/master-slider/masterslider/skins/default/style.css') }}">

    <script src="{{ asset('assets/plugins/master-slider/masterslider/masterslider.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/master-slider/masterslider/jquery.easing.min.js') }}"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBw_spE5_xC8pjOBJVWs5LHLBKn0EC-cfQ"></script>
    <script src="{{ asset('assets/plugins/gmap/gmap.js') }}"></script>

    <script type="text/javascript">
        var map;
        $(document).ready(function(){
            if($("#map").length){
                map = new GMaps({
                    div: '#map',
                    scrollwheel: false,             
                    lat: $("#latitud").val(),
                    lng: $("#longitud").val(),
                    zoom: 15
                });

                map.addMarker({
                    lat: $("#latitud").val(),
                    lng: $("#longitud").val()
                });
            }

        });
        $(function(){
            var slider = new MasterSlider();
             
            slider.control('arrows');  
            slider.control('thumblist' , {autohide:false ,dir:'h',arrows:false, align:'bottom', width:180, height:170, margin:5, space:5});
         
            slider.setup('masterslider' , {
                width:550,
                height:450,
                space:5,
                view:'fade'
            });
        });
    </script>

    <script id="dsq-count-scr" src="//https-www-tapdevi-com.disqus.com/count.js" async></script>
@stop
