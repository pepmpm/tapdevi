@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section ('body')

<!--=== Breadcrumbs ===-->
<div class="breadcrumbs">
    <div class="container">
        @if(isset($query))
            <h1 class="pull-left">Actividad: {{$query->name}}</h1>
        @else
            <h1 class="pull-left">Nueva actividad</h1>
        @endif
    </div><!--/container-->
</div><!--/breadcrumbs-->
<div class="container content">
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('back.plans.index') }}" 
               class="btn-u" role="button">
                Volver a la lista
            </a>
            <a href="{{ route('back.plans.create') }}" 
               class="btn-u" role="button">
                Crear otra actividad
            </a>
            @if(isset($query))
                <a href="{{ route('back.plans.stocks.show', ['items_id' => $query->id]) }}" 
                   class="btn-u btn-u-green" role="button">
                    Modificar disponibilidad
                </a>
            @endif
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <form class="margin-bottom-20" id="form_item" role="form" method="post" enctype="multipart/form-data" 
                action="{{ isset($query) ? route('back.plans.update', $query->id) : route('back.plans.store') }}">
                @if(isset($query))
                    @method('PUT')
                @endif
                @csrf
                <div class="row">
                    <div class="col-md-9">
                        @include('_layouts.messages', array('errors' => $errors))
                        <div class="row">
                            <div class="col-md-12">
                                @include('_back.plans.crud_info', array('query' => isset($query) ? $query : null))
                            </div>
                        </div>
                        
                        @include('_back.plans.crud_langs', array('query' => isset($query) ? $query : null))
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">Acciones</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn-u btn-block margin-bottom-25">
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Status</label>
                                            {!! Form::select('status', $status_lov, 
                                                old('status', isset($query) ? $query->status: ''), 
                                                array('class' => 'form-control', 'id' => 'status')) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Confirmación</label>
                                            {!! Form::select('confirmation', $confirmation_lov, 
                                                old('confirmation', isset($query) ? $query->confirmation: ''), 
                                                array('class' => 'form-control', 'id' => 'confirmation')) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Mostrar botón para regalar</label>
                                            {!! Form::select('is_giftable', $yesno_lov, 
                                                old('is_giftable', isset($query) ? $query->is_giftable: 'S'), 
                                                array('class' => 'form-control', 'id' => 'is_giftable')) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Idiomas disponibles</label>
                                            {!! Form::select('languages[]', $languages_lov, 
                                                old('languages[]', isset($query) ? explode("#", $query->languages) : ['ES']), 
                                                array('class' => 'form-control', 'id' => 'languages', 
                                                    'multiple' => 'multiple', 'style' => 'height: 180px')) !!}
                                        </div>
                                    </div>
                                </div>
                                @include('_back.plans.crud_photos', array('query' => isset($query) ? $query : null))
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section ('assets_end')
    <script src="{{ asset('assets/plugins/redactor/redactor.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/plugins/redactor/redactor.css') }}">
    <script src="{{ asset('assets/plugins/redactor/lang/es.js') }}"></script>
    <script src="{{ asset('assets/plugins/redactor/plugins/read-more-plugin.js') }}"></script>

    <script src="{{ asset('assets/plugins/fancybox/source/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/plugins/fancybox/source/jquery.fancybox.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/plugins/jquery-upload-file/uploadfile.css') }}">
    <script src="{{ asset('assets/plugins/jquery-upload-file/jquery.uploadfile.min.js') }}"></script>

    <script src="{{ asset('assets/plugins/select2/dist/js/select2.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/dist/css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/dist/css/select2-bootstrap.css') }}">

    <script src="{{ asset('assets/plugins/datetimepicker/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/datetimepicker/locales/bootstrap-datetimepicker.es.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/plugins/datetimepicker/datetimepicker.min.css') }}">
    <script src="{{ asset('assets/plugins/datetimepicker/datetimepicker.min.js') }}"></script>


    <script type="text/javascript">
        $(document).ready(function() {
            $("#fileuploader").uploadFile({
                url:"{{ route('back.plans.photos.store') }}",
                fileName:"p_file",
                onSuccess:function(files,data,xhr,pd){
                    $('#form_item').append('<input type="hidden" name="p_foto[]" value="'+data+'" />');
                }
            });
            if($('.redactor').length){
                $('.redactor').redactor({
                    minHeight: 400,
                    lang: 'es',
                    plugins: ['leermas']
                });
            }
        });
    </script>

    <script type="text/javascript">
        $(function(){
            if (jQuery().datetimepicker) {
                $('.fecha_inicio').datetimepicker({
                    language: 'es',
                    minDate: new Date(),
                    pick12HourFormat: false,
                    format: 'DD/MM/YYYY',
                    pickTime: false
                });
            }

            $(".locations_id").select2({theme: "bootstrap", selectOnClose: true});
            $(".users_id").select2({theme: "bootstrap", selectOnClose: true});
            $(document).on('focus', '.users_id', function() {
                $(this).siblings('select').select2('open');
            });
            $(document).on('focus', '.locations_id', function() {
                $(this).siblings('select').select2('open');
            });
            //$("#languages").select2({theme: "bootstrap", multiple: true});

            if($(".fancybox").length){
                jQuery(".fancybox").fancybox({
                    groupAttr: 'data-rel',
                    prevEffect: 'fade',
                    nextEffect: 'fade',
                    openEffect  : 'elastic',
                    closeEffect  : 'fade',
                    closeBtn: true,
                    helpers: {
                        title: {
                            type: 'float'
                        }
                    }
                });
            }
        });
    </script>
    <style type="text/css">
        .btn-u-sm, a.btn-u-sm {
            padding: 5px 12px;
        }
    </style>
@stop
