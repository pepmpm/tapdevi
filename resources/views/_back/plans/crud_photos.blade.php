<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label>Fotos</label>
            <div id="fileuploader">Upload</div>
        </div>
    </div>
</div>
@if(old('p_foto', null) != null)
    <div class="row margin-bottom-20">
        @foreach(old('p_foto') as $tmpphoto)
            <div class="col-md-6">
                <input type="hidden" name="p_foto[]" value="{{ $tmpphoto }}">
                <a class="fancybox" data-rel="galeria_fotos"
                    href="{{ url('/') . '/' . $tmpphoto }}">
                    <img src="{{ url('/') . '/' . $tmpphoto }}" class="img-responsive">
                </a>
                
            </div>
            @if($loop->iteration % 2 == 0)
                </div><div class="row"> 
            @endif
        @endforeach
    </div>
@endif
@if(isset($query))
    <div class="row margin-bottom-20">
        @foreach($query->photos as $tmpphoto)
            <div class="col-md-6">
                <a class="fancybox" data-rel="galeria_fotos"
                    title="{{ $query->name }}"
                    href="{{ url('/') . '/' . $tmpphoto->url }}">
                    <img src="{{ url('/') . '/' . $tmpphoto->url }}" class="img-responsive">
                </a>
            </div>
            @if($loop->iteration % 2 == 0)
                </div><div class="row"> 
            @endif
        @endforeach
    </div>
@endif