<div class="panel panel-default">
    <div class="panel-heading">Datos</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label>Nombre</label>
                    <input type="text" class="form-control {{ $errors->has('name') ? 'border-rojo' : "" }}" 
                        name="{{ isset($query) ? '' : 'name' }}" {{ isset($query) ? 'disabled' : '' }} placeholder="Nombre"
                        value="{{ old('name.' . config('app.locale'), isset($query) ? $query->name : '') }}"/>
                    @if(isset($query))
                        <div class="note">
                            <strong>Nota:</strong>
                            Después podrás modificar y traducir el nombre del producto en la sección de textos 
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Precio p.p</label>
                    <input type="text" class="form-control {{ $errors->has('price') ? 'border-rojo' : "" }}" 
                        name="price" placeholder="Precio por persona"
                        value="{{ old('price', isset($query) ? $query->price: '') }}"/>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Precio p.p. rebajado</label>
                    <input type="text" class="form-control {{ $errors->has('new_price') ? 'border-rojo' : "" }}" 
                        name="new_price" placeholder="Precio rebajado"
                        value="{{ old('new_price', isset($query) ? $query->new_price: '') }}"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Duración (H:M h)</label>
                    <input type="text" class="form-control {{ $errors->has('duration') ? 'border-rojo' : "" }}" 
                        name="duration" placeholder="Duración"
                        value="{{ old('duration', isset($query) ? $query->duration: '') }}"/>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Mínimo personas</label>
                    <input type="text" class="form-control {{ $errors->has('min_pax') ? 'border-rojo' : "" }}" 
                        name="min_pax" placeholder="Mínimo personas"
                        value="{{ old('min_pax', isset($query) ? $query->min_pax: '') }}"/>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Máximo personas</label>
                    <input type="text" class="form-control {{ $errors->has('max_pax') ? 'border-rojo' : "" }}" 
                        name="max_pax" placeholder="Máximo personas"
                        value="{{ old('max_pax', isset($query) ? $query->max_pax: '') }}"/>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Días de antelación para pedido</label>
                    {!! Form::select('advance_days', $numbers_lov, 
                        old('advance_days', isset($query) ? $query->advance_days: ''), 
                        array('class' => 'form-control', 'id' => 'advance_days')) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Días de antelación para cancelar</label>
                    {!! Form::select('cancellation_days', $numbers_lov, 
                        old('cancellation_days', isset($query) ? $query->cancellation_days: ''), 
                        array('class' => 'form-control', 'id' => 'cancellation_days')) !!}
                </div>
            </div>
        </div>
    </div>
</div>