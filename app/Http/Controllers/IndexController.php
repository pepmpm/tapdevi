<?php 

namespace App\Http\Controllers;

use Cache;
use Image;
use App\Items;
use Illuminate\Http\Request;
use Validator;
use App\Traits\Plans;
use App\Traits\Helpers;
use Stichoza\GoogleTranslate\GoogleTranslate;

class IndexController extends Controller {

    use Plans;
    use Helpers;

    public function __construct() {
        parent::__construct();
    }

    public function index(Request $request) {
        if($request->has('where') && $request->has('category')){
            return redirect()->route('search', ['cat' => $request->get('category'), 'dis' => $request->get('where')]);
        }else{
            if($request->has('where') && $request->get('where') != null){
                return redirect()->route('enotourism-mallorca.dis', ['cat' => $request->get('where')]);
            }
            if($request->has('category') && $request->get('category') != null){
                return redirect()->route('enotourism-mallorca.cat', ['district' => $request->get('category')]);
            }
        }
        
        $this->data['header'] = 'header_home';
        $this->data['params'] = $this->_get_params($request);

        $this->data['districts_home'] = $this->_get_districts_array();
        $this->data['labels_home'] = $this->_get_labels_array();
        $this->data['categories'] = $this->_get_labels_home();

        $this->data['index_items_packs'] = Cache::remember('index_items_packs', 1440, function () {
            return \App\Items::has('packs')->where('status', '=', 'active')
                ->where('type', '=', 'pack')->orderBy('price', 'asc')->get();
        });

        $this->data['index_plans_promoteds'] = Cache::remember('index_plans_promoteds', 1440, function () {
            return $this->getPlans(['take' => 4, 'promoted' => 'S']);
        });
        $this->data['index_plans'] = Cache::remember('index_plans', 1440, function () {
            return $this->getPlans(['take' => 8, 'promoted' => 'N']);
        });
        $this->data['index_events'] = Cache::remember('index_events', 1440, function () {
            return $this->getEvents(['limit' => '15']);
        });

        $this->data['day_stocks'] = \App\ItemsStocks::select('date')->distinct()
            ->where('date', '>=', date('Y-m-d'))->pluck('date')->all();

        return view('index', $this->data);
    }

    public function search(Request $request){
        $this->data['params'] = ['district' => $request->get('dis'), 'cat' => $request->get('cat')];
        $this->data['districts_sidebar_plans'] = $this->_get_districts();
        $this->data['labels_sidebar_plans'] = $this->_get_labels();
        $this->data['users_sidebar_plans'] = $this->_get_users();
        $this->data['categories'] = $this->_get_labels_home();

        $this->data['query'] = $this->getPlans(array_merge($request->all(), ['locale' => \App::getLocale()]));
        $this->data['day_stocks'] = \App\ItemsStocks::select('date')->distinct()
            ->where('date', '>=', date('Y-m-d'))->pluck('date')->all();

        return view('plans.index', $this->data);
    }

    public function politicas_cookies(){
        return view('cookies.politicas_cookies', $this->data);
    }

    public function politicas_privacidad(){
        return view('cookies.politicas_privacidad', $this->data);
    }

    /**
     * @deprecated
     */
    public function temp_image_png(Request $request){
        return \Image::make($request->get('url'))->resize(80, null, function ($constraint) {
            $constraint->aspectRatio();
        })->response('png');
    }

    public function generate_image_events(Request $request){
        if($request->has('generate') && $request->get('generate') == 'true'){
            $captura = str_replace("data:image/png;base64,", "", urldecode($request->get('captura')));
            Image::make(base64_decode($captura))->save('events.png');
            return url('events.png');
        }
        
        $this->data['future_events'] = $this->getEvents(['limit' => '12']);
        return view('generate_image_events', $this->data);
    }

    public function pep_action(){
        $users = \App\User::has('orders')->orWhereIn('role', ['admin', 'company'])->pluck('id')->all();

        $spammers = \App\User::whereNotIn('id', $users)->get();
        foreach($spammers as $spam){
            echo 'deleting: ' . $spam->id . "<br/>";
            $spam->langs()->delete();
            $spam->delete();
        }

        $orders = \App\Orders::all();
        foreach($orders as $order){
            echo 'fixing order: ' . $order->id . "<br/>";
            if($order->users_id != null){
                $order->name = $order->user->name;
                $order->email = $order->user->email;
                $order->phone = $order->user->phone;
                $order->date_visited = date('Y-m-d H:i:s');
                $order->date_paid = date('Y-m-d H:i:s');
                $order->save();
            }
        }
        /*$plans = \App\Items::where('type', '=', 'plan')->orderBy('id', 'asc')->get();
        foreach($plans as $plan){
            for($x = 1; $x <= 12; $x++){
                $tmpmonth = date_create_from_format('Y-m-d', '2020-' . ($x < 10 ? '0' . $x : $x) . '-01');
    
                $tmp = new \App\ItemsStocks;
                $tmp->items_id = $plan->id;
                $tmp->month = date_format($tmpmonth, 'm');
                $tmp->save();
            }
        }*/
    }

    protected function _get_districts_array(){
        return Cache::remember('districts_array', 1440, function () {
            return \App\Districts::whereHas('plans', function ($query) {
                    $query->where('status', '=', 'active');
                    $query->where('type', '=', 'plan');
                })->orderBy('name', 'asc')->pluck('name', 'slug')->all();
        });
    }

    protected function _get_labels_array(){
        return \Cache::remember('labels_array', 1440, function () {
            $labels = \App\Labels::with('plans')->whereHas('plans', function ($query) {
                    $query->where('status', '=', 'active');
                })->where('category', '=', 'enotourism-mallorca')->pluck('id')->all();

            return \App\LabelsLangs::whereIn('labels_id', $labels)->where('locale', '=', $this->data['idiomaweb'])
                ->pluck('name', 'slug')->all();
        });
    }

    protected function _get_labels_home(){
        return \Cache::remember('labels_home', 1440, function () {
            return \App\Labels::with('plans')->whereHas('plans', function ($query) {
                    $query->where('status', '=', 'active');
                })->where('category', '=', 'enotourism-mallorca')->get();
        });
    }

}