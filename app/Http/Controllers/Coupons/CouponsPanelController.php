<?php

namespace App\Http\Controllers\Coupons;

use App\Coupons;
use Illuminate\Http\Request;

class CouponsPanelController extends CouponsController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['query'] = parent::_index();
        return view('_back.coupons.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['active_lov'] = Coupons::getPossibleEnumValues('active');
        return view('_back.coupons.crud', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->data['query'] = parent::_store($request->all());

        if($this->data['query'] == null){
            return redirect()->back()->with('message_error', 'Debes informar el nombre');
        }

        return redirect()
            ->route('back.coupons.show', array('coupon' => $this->data['query']->id))
            ->with('message_success', 'Cambios guardados correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function show(Coupons $coupon)
    {
        $this->data['active_lov'] = Coupons::getPossibleEnumValues('active');
        $this->data['query'] = parent::_show($coupon);
        return view('_back.coupons.crud', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupons $coupon)
    {
        $this->data['query'] = parent::_update($request->all(), $coupon);

        if($this->data['query'] == null){
            return redirect()->back()->with('message_error', 'Debes informar el nombre y subir una foto');
        }

        return redirect()
            ->route('back.coupons.show', array('coupon' => $this->data['query']->id))
            ->with('message_success', 'Cambios guardados correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupons $coupon)
    {
        parent::_destroy($coupon);
        return redirect()
            ->route('back.coupons.index')
            ->with('message_success', 'Cambios guardados correctamente');
    }
}
