<?php

$idioma = array();

$idioma['hola'] = 'Hola';
$idioma['cargando'] = 'Carregant';
$idioma['en'] = 'a';
$idioma['guardar'] = 'Desar';
$idioma['cerrar'] = 'Tancar';
$idioma['anyadir'] = 'Afegir';
$idioma['buscar'] = 'Cercar';
$idioma['gratuito'] = 'Gratuit';
$idioma['pendiente'] = 'Pendent';

$idioma['aceptar'] = 'Acceptar';
$idioma['denegar'] = 'Denegar';

$idioma['ver_carrito'] = 'Veure cistella';
$idioma['realizar_compra'] = 'Finalitzar compra';
$idioma['calendario'] = 'Calendari';

$idioma['minimo'] = 'Mínim';
$idioma['maximo'] = 'Màxim';

$idioma['January'] = 'Gener';
$idioma['February'] = 'Febrer';
$idioma['March'] = 'Març';
$idioma['April'] = 'Abril';
$idioma['May'] = 'Maig';
$idioma['June'] = 'Juny';
$idioma['July'] = 'Juliol';
$idioma['August'] = 'Agost';
$idioma['September'] = 'Setembre';
$idioma['October'] = 'Octubre';
$idioma['November'] = 'Novembre';
$idioma['December'] = 'Decembre';

$idioma['num_gener'] = '01';
$idioma['num_febrer'] = '02';
$idioma['num_març'] = '03';
$idioma['num_abril'] = '04';
$idioma['num_maig'] = '05';
$idioma['num_juny'] = '06';
$idioma['num_juliol'] = '07';
$idioma['num_agost'] = '08';
$idioma['num_setembre'] = '09';
$idioma['num_octubre'] = '10';
$idioma['num_novembre'] = '11';
$idioma['num_decembre'] = '12';

$idioma['mes1'] = 'Gen';
$idioma['mes2'] = 'Feb';
$idioma['mes3'] = 'Mar';
$idioma['mes4'] = 'Abr';
$idioma['mes5'] = 'Mai';
$idioma['mes6'] = 'Jun';
$idioma['mes7'] = 'Jul';
$idioma['mes8'] = 'Ago';
$idioma['mes9'] = 'Set';
$idioma['mes10'] = 'Oct';
$idioma['mes11'] = 'Nov';
$idioma['mes12'] = 'Dec';

$idioma['mes01'] = 'Gen';
$idioma['mes02'] = 'Feb';
$idioma['mes03'] = 'Mar';
$idioma['mes04'] = 'Abr';
$idioma['mes05'] = 'Mai';
$idioma['mes06'] = 'Jun';
$idioma['mes07'] = 'Jul';
$idioma['mes08'] = 'Ago';
$idioma['mes09'] = 'Set';
$idioma['mes10'] = 'Oct';
$idioma['mes11'] = 'Nov';
$idioma['mes12'] = 'Dec';

$idioma['day1'] = 'Dl';
$idioma['day2'] = 'Dm';
$idioma['day3'] = 'Dc';
$idioma['day4'] = 'Dj';
$idioma['day5'] = 'Dv';
$idioma['day6'] = 'Ds';
$idioma['day7'] = 'Dg';

$idioma['day_1'] = 'Dl';
$idioma['day_2'] = 'Dm';
$idioma['day_3'] = 'Dc';
$idioma['day_4'] = 'Dj';
$idioma['day_5'] = 'Dv';
$idioma['day_6'] = 'Ds';
$idioma['day_7'] = 'Dg';

$idioma['day_1_'] = 'Dilluns';
$idioma['day_2_'] = 'Dimarts';
$idioma['day_3_'] = 'Dimecres';
$idioma['day_4_'] = 'Dijous';
$idioma['day_5_'] = 'Divendres';
$idioma['day_6_'] = 'Dissabte';
$idioma['day_7_'] = 'Diumenge';

return $idioma;