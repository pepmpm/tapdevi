<?php

$idioma = array();

$idioma['hola'] = 'Hi';
$idioma['cargando'] = 'Loading';
$idioma['en'] = 'in';
$idioma['guardar'] = 'Save';
$idioma['cerrar'] = 'Close';
$idioma['anyadir'] = 'Add';
$idioma['buscar'] = 'Search';
$idioma['gratuito'] = 'Free';
$idioma['pendiente'] = 'Pending';

$idioma['aceptar'] = 'Accept';
$idioma['denegar'] = 'Deny';

$idioma['ver_carrito'] = 'Go to cart';
$idioma['realizar_compra'] = 'Finish purchase';
$idioma['calendario'] = 'Calendar';

$idioma['minimo'] = 'Minimum';
$idioma['maximo'] = 'Maximum';

$idioma['January'] = 'January';
$idioma['February'] = 'February';
$idioma['March'] = 'March';
$idioma['April'] = 'April';
$idioma['May'] = 'May';
$idioma['June'] = 'June';
$idioma['July'] = 'July';
$idioma['August'] = 'August';
$idioma['September'] = 'Setember';
$idioma['October'] = 'October';
$idioma['November'] = 'November';
$idioma['December'] = 'December';

$idioma['num_january'] = '01';
$idioma['num_february'] = '02';
$idioma['num_march'] = '03';
$idioma['num_april'] = '04';
$idioma['num_may'] = '05';
$idioma['num_june'] = '06';
$idioma['num_july'] = '07';
$idioma['num_august'] = '08';
$idioma['num_september'] = '09';
$idioma['num_october'] = '10';
$idioma['num_november'] = '11';
$idioma['num_december'] = '12';

$idioma['mes1'] = 'Jan';
$idioma['mes2'] = 'Feb';
$idioma['mes3'] = 'Mar';
$idioma['mes4'] = 'Apr';
$idioma['mes5'] = 'May';
$idioma['mes6'] = 'Jun';
$idioma['mes7'] = 'Jul';
$idioma['mes8'] = 'Aug';
$idioma['mes9'] = 'Set';
$idioma['mes10'] = 'Oct';
$idioma['mes11'] = 'Nov';
$idioma['mes12'] = 'Dec';

$idioma['mes01'] = 'Jan';
$idioma['mes02'] = 'Feb';
$idioma['mes03'] = 'Mar';
$idioma['mes04'] = 'Apr';
$idioma['mes05'] = 'May';
$idioma['mes06'] = 'Jun';
$idioma['mes07'] = 'Jul';
$idioma['mes08'] = 'Aug';
$idioma['mes09'] = 'Set';
$idioma['mes10'] = 'Oct';
$idioma['mes11'] = 'Nov';
$idioma['mes12'] = 'Dec';

$idioma['day1'] = 'M';
$idioma['day2'] = 'T';
$idioma['day3'] = 'W';
$idioma['day4'] = 'T';
$idioma['day5'] = 'F';
$idioma['day6'] = 'Sa';
$idioma['day7'] = 'Su';

$idioma['day_1'] = 'Mon';
$idioma['day_2'] = 'Tue';
$idioma['day_3'] = 'Wed';
$idioma['day_4'] = 'Thu';
$idioma['day_5'] = 'Fri';
$idioma['day_6'] = 'Sat';
$idioma['day_7'] = 'Sun';

$idioma['day_1_'] = 'Monday';
$idioma['day_2_'] = 'Tuesday';
$idioma['day_3_'] = 'Wednesday';
$idioma['day_4_'] = 'Thursday';
$idioma['day_5_'] = 'Friday';
$idioma['day_6_'] = 'Saturday';
$idioma['day_7_'] = 'Sunday';

return $idioma;