<?php

namespace App\Http\Controllers\Events;

use App\Items;
use App\ItemsLangs;
use App\ItemsStocks;
use App\ItemsPhotos;
use Illuminate\Http\Request;
use App\Traits\Helpers;
use App\Traits\Plans;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use Validator;
use Cache;

class EventsController extends \App\Http\Controllers\Controller
{
    private $iva = 21;
    private $default_keywords = 'eventos vino mallorca, catas vino mallorca, ferias vino mallorca, agenda vinos mallorca, que hacer mallorca, bodegas mallorca';
    use Helpers;
    use Plans;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function _index($params)
    {
        $items = Items::where('type', '=', 'event')->orderBy('id', 'desc');
        if(isset($params['month']) && isset($params['year'])){
            $items = $items->whereHas('allstocks', function ($query) use ($params) {
                $query->whereMonth('date', '=', $params['month']);
                $query->whereYear('date', '=', $params['year']);
            });
        }
        return $items->orderBy('id', 'desc')->get();
    }

    public function _get_plans($params){
        return $this->getPlans($params);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function _all()
    {
        return Items::where('type', '=', 'event')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function _store($request)
    {
        $validation = Validator::make($request, self::_get_rules($request), self::_get_messages());

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }

        $tmp = new Items;
        $tmp->order = 100;
        $tmp->labels_id = isset($request['labels_id']) ? $request['labels_id'] : null;
        $tmp->users_id = 1; //siempre es Tapdevi
        $tmp->price = isset($request['price']) ? $request['price'] : 999;
        $tmp->confirmation = 'none';
        $tmp->type = 'event';
        $tmp->districts_id = isset($request['districts_id']) ?  $request['districts_id'] : null;
        $tmp->address = isset($request['address']) ? $request['address'] : null;
        $tmp->postal_code = isset($request['postal_code']) ? $request['postal_code'] : null;
        $tmp->advisor_name = isset($request['advisor_name']) ? $request['advisor_name'] : null;
        $tmp->advisor_email = isset($request['advisor_email']) ? $request['advisor_email'] : null;
        $tmp->longitude = isset($request['longitude']) ? $request['longitude'] : null;
        $tmp->latitude = isset($request['latitude']) ? $request['latitude'] : null;
        $tmp->status = isset($request['status']) ? $request['status'] : 'outofstock';
        $tmp->instagram_url = isset($request['instagram_url']) ? $request['instagram_url'] : null;
        $tmp->save();

        self::_insert_langs($request, $tmp);

        if(isset($request['p_foto'])){
            self::_insert_images($request, $tmp);
        }

        self::_clean();

        return $tmp;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _show(Items $item)
    {
        return $item;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _update($request, Items $tmp)
    {
        $validation = Validator::make($request, self::_get_rules($request), self::_get_messages());

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }

        $tmp->order = 100;
        $tmp->labels_id = isset($request['labels_id']) ? $request['labels_id'] : null;
        $tmp->users_id = 1; //siempre es Tapdevi
        $tmp->price = isset($request['price']) ? $request['price'] : 0;
        $tmp->confirmation = 'none';
        $tmp->type = 'event';
        $tmp->districts_id = isset($request['districts_id']) ?  $request['districts_id'] : null;
        $tmp->address = isset($request['address']) ? $request['address'] : null;
        $tmp->postal_code = isset($request['postal_code']) ? $request['postal_code'] : null;
        $tmp->longitude = isset($request['longitude']) ? $request['longitude'] : null;
        $tmp->latitude = isset($request['latitude']) ? $request['latitude'] : null;
        $tmp->status = isset($request['status']) ? $request['status'] : 'outofstock';
        $tmp->instagram_url = isset($request['instagram_url']) ? $request['instagram_url'] : null;
        $tmp->save();

        self::_edit_langs($request, $tmp);

        if(isset($request['p_foto'])){
            self::_insert_images($request, $tmp);
        }

        self::_clean();

        return $tmp;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _destroy(Items $item)
    {
        $ruta = 'upload/events/'.$item->id.'/';
        if(is_dir($ruta)){
            $success = \File::deleteDirectory($ruta, true);
            rmdir($ruta);
            if(!$success){
                throw new \Exception('El directorio de imagenes de '.$item->id.' no existe');
            }
        }

        $item->delete();

        self::_clean();

        return true;
    }

    private function _insert_images($request, $item){
        $ruta = 'upload/events/'.$item->id.'/';
        \File::makeDirectory($ruta, 0775, true, true);

        foreach($request['p_foto'] as $foto){
            $tmp = new ItemsPhotos;
            $tmp->order = 100;
            $tmp->items_id = $item->id;
            $tmp->save();

            $tmp->url = $ruta . $item->slug . "-" . $tmp->id . ".webp";
            $tmp->save();

            $img = Image::make($foto)->encode('webp', 90);
            $img->resize(1024, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($tmp->url);
            unlink($foto);
            $this->set_72dpi_image($tmp->url);
        }
    }

    public function _insert_langs($request, $tmp){
        $spanish = [
            'name' => $request['name'],
            'description' => $request['description']['es'],
            'seo_keywords' => $this->default_keywords
        ];
        foreach(config('translatable.locales') as $keylocale => $locale){
            if(isset($request['name']) && isset($request['description'][$keylocale])){
                $lang = ItemsLangs::firstOrNew(['locale' => $keylocale, 'items_id' => $tmp->id]);
                
                $lang->locale = $keylocale;
                $lang->items_id = $tmp->id;
                $lang->name = parent::_translate_langs($spanish['name'], $request['name'], $keylocale, 255);
                $lang->slug = $this->getSlug($lang->name, 'slug', ItemsLangs::getModel());

                $lang->description = parent::_translate_langs(
                    $spanish['description'], $request['description'][$keylocale], $keylocale, null);

                $lang->seo_description = str_limit($lang->description, 250);

                $lang->seo_keywords = parent::_translate_langs(
                    $spanish['seo_keywords'] . " " . $lang->name, null, $keylocale, 255
                );

                $lang->save();
            }
        }
    }

    private function _edit_langs($request, $tmp){
        $spanish = [
            'name' => $request['name']['es'],
            'description' => $request['description']['es'],
            'seo_keywords' => $this->default_keywords
        ];

        foreach($tmp->langs as $lang){
            $tmpname = parent::_translate_langs(
                $spanish['name'], $request['name'][$lang->locale], $lang->locale, 255);
            if($tmpname != $lang->name){
                $lang->name = $tmpname;
                $lang->slug = $this->getSlug($tmpname, 'slug', ItemsLangs::getModel());
            }

            $lang->description = parent::_translate_langs(
                $spanish['description'], $request['description'][$lang->locale], $lang->locale, null);
            $lang->seo_description = str_limit($lang->description, 250);

            $lang->seo_keywords = parent::_translate_langs(
                $spanish['seo_keywords'] . " " . $lang->name, null, $lang->locale, 255
            );

            $lang->save();
        }
    }

    private function _get_rules($request){
        $rules = array();

        $rules['name'] = 'required|max:250';
        $rules['description.es'] = 'required|max:2000';

        return $rules;
    }
    
    private function _get_messages(){
        $messages = array();
        $messages['labels_id.required'] = trans('item.validation_labels_id_required');
        $messages['labels_id.numeric'] = trans('item.validation_labels_id_numeric');

        $messages['name.required'] = trans('item.validation_name_required');
        $messages['name.max'] = trans('item.validation_name_max');
        $messages['description.es.required'] = __('item.validation_description_required', [], 'es');
        $messages['description.es.max'] = __('item.validation_description_max', [], 'es');
        return $messages;
    }

    private function _clean(){
        \Cache::flush();
    }
}
