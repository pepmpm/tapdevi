<?php

$idioma = array();

$idioma['author'] = 'Pep Manel Palmer';
$idioma['title_default'] = 'Buy majorcan wine • Enotourism • Gourmet products ✔️ Buy wine from Balearic Islands';

//index
$idioma['title_index'] = 'Mallorcan Enotourism ✔️ Wine tours ✔️ Enjoy wine experiences in Majorca';
$idioma['desc_index'] = 'Visits to wineries, activities related to Majorcan wine, events, workshops, courses... Get to know and enjoy Majorcan wine. More than 100 wineries to discover.';
$idioma['keys_index'] = 'Majorca wine tourism, Majorca wine experiences, Majorca winery visits, Majorca wine tasting, Santa Maria wine tasting, Binissalem wine tasting';

//plan.index
$idioma['title_index_plan'] = 'Enotourism in Majorca ✔️ Wine Tours in Majorca ✔️ Enjoy experiences with wine from Majorca';
$idioma['desc_index_plan'] = 'Do not miss the wine tourism tours that will take you to family wineries and to the restaurants where residents of Majorca go. You will enjoy our excellent wines in natural settings that will make you fall in love';
$idioma['keys_index_plan'] = 'Majorca wine, enotourism Majorca, wine experiences Majorca, wine tourism Majorca, Majorca wine tours';

//plan.date.today
$idioma['title_index_plan_date_day'] = 'Wine tourism plans for day :day ✔️ Wine tourism in Mallorca ✔️ Wine tours in Mallorca ✔️ Enjoy experiences with wine from Mallorca';
$idioma['desc_index_plan_date_day'] = "If you don't have plans for day :day and you like wine, go in and choose the plan that you like the most to take advantage of the day. Do not miss the wine tourism tours that will take you through family wineries and restaurants where residents of Mallorca go";
$idioma['keys_index_plan_date_day'] = 'Mallorca wine, plans for :day, Enotourism Mallorca, Wine experiences mallorca for :day, tourism wine Mallorca, tours wine Mallorca :day';

//plan.date.weekend
$idioma['title_index_plan_date_weekend'] = 'Wine tourism plans for the weekend ✔️ Wine tourism in Mallorca ✔️ Wine tours in Mallorca ✔️ Enjoy experiences with Mallorca wine';
$idioma['desc_index_plan_date_weekend'] = 'Plans for the weekend from 12€ in wineries, restaurants, fairs, boats, in the countryside, in rustic farms ... wine does not understand places, but moments. If you do not have plans for the weekend we will give you ideas.';
$idioma['keys_index_plan_date_weekend'] = 'Mallorca wine weekend, weekend plans, Mallorca wine tourism, Mallorca wine weekend experiences, Mallorca wine tourism, Mallorca weekend wine tours';


$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';

return $idioma;