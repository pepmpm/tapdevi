<?php

namespace App\Http\Controllers\BlogsPhotos;

use App\Blogs;
use App\BlogsPhotos;
use Illuminate\Http\Request;
use Image;
use App\Traits\Helpers;

class BlogsPhotosController extends \App\Http\Controllers\Controller
{
    use Helpers;

    public function __construct(array $attributes = array()) {
        parent::__construct($attributes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function _store($request)
    {
        $ruta = 'upload/blogs/pending/';
        \File::makeDirectory($ruta, 0775, true, true);

        $file = $ruta . time() . str_random(6) . '.webp';

        $img = Image::make(file_get_contents($request['p_file']))->encode('webp', 90);
        $img->resize(1024, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($file);

        $this->set_72dpi_image($file);

		return $file;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Plans  $plans
     * @return \Illuminate\Http\Response
     */
    protected function _update($request, BlogsPhotos $blog)
    {
        //pasamos todas las imagenes a orden 100
        BlogsPhotos::where('blogs_id', '=', $request['id'])
                ->update(array('order' => 100));

        //pasamos la imagen seleccionada a orden 1
        $blog->order = 1;
        $blog->save();

        return $blog;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Plans  $plans
     * @return \Illuminate\Http\Response
     */
    protected function _destroy(BlogsPhotos $blog)
    {
        $blogs_id = $blog->blogs_id;

        $ruta = 'upload/blogs/' . $blogs_id . '/';
        if(is_dir($ruta)){
            $success = \File::deleteDirectory($ruta, true);
            rmdir($ruta);
            if(!$success){
                throw new \Exception('El directorio de imagenes de ' . $blog->name . ' no existe');
            }
        }

        $blog->delete();

        return $blogs_id;
    }
}
