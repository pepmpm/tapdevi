<?php

$idioma = array();

$idioma['validation_stock_required'] = 'El stock es obligatori';
$idioma['validation_stock_numeric'] = 'El stock ha de ser numèric';
$idioma['validation_cost_numeric'] = 'Es preu de cost del producte ha de ser numèric';
$idioma['validation_price_required'] = 'El preu de venta es obligatori';
$idioma['validation_price_numeric'] = 'El preu ha de ser numèric';
$idioma['validation_new_price_numeric'] = 'El preu amb descompte ha de ser numèric';
$idioma['validation_labels_id_required'] = "S'ha de seleccionar una categoría";
$idioma['validation_labels_id_numeric'] = 'Sa categoría seleccionada es incorrecta';
$idioma['validation_name_required'] = "S'ha d'emplenar el nom del producte";
$idioma['validation_name_max'] = 'Es nom des producte no pot ser tan llarg';
$idioma['validation_description_required'] = "S'ha d'emplenar la descripció del producte";
$idioma['validation_description_max'] = 'Sa descripció del producte es massa llarga';
$idioma['validation_new_price_lt'] = 'El preu amb descompte ha de ser menor que el preu habitual';

$idioma['otros_vinos'] = 'Altres vins que has de provar';
$idioma['mensaje_cross_selling'] = 'A continuació et mostram altres vins seleccionats para endinsar-te a la cultira vitivinicola de les Illes Balears.';

$idioma['selecciona_cantidad'] = 'Selecciona la quantitat';
$idioma['crear_una_caja_personalizada'] = 'Crear sa teva caixa personalitzada';
$idioma['wines'] = 'Vins';
$idioma['experiences'] = 'Experiències';
$idioma['comprar_caja_personalizada_con'] = 'Comprar una caixa de vi personalitzada de :user';
$idioma['tu_caja_personalizada'] = 'Sa teva caixa personalitzada';
$idioma['elige_vinos_para_anyadirlos_a_tu_caja'] = 'Tria vins per afegir-los a sa teva caixa personalitzada';

$idioma['vino'] = 'Vi';
$idioma['precio'] = 'Preu';
$idioma['bodega'] = 'Bodega';

$idioma['message_custom_box'] = "Pitja damunt un vi del llistat per afegir-lo a sa teva caisa personalitzada. Quan hagis triat ses 6 botelles es mostrarà el botó d'afegir a sa cistella.";

$idioma['msg_transferencia_bancaria'] = 'Recorda escriure el codi de comanda :code al concepte de la transferència';
$idioma['por_botella'] = 'per botella';
$idioma['por_variedad'] = 'Per varietat';

$idioma['elige_una_bodega_para_vino_personalizado'] = 'Tria una bodega per crear sa teva caixa de vi variada';

$idioma['mensaje_botellas_envio_gratuito'] = "Enviament de vi per caixes. Has d'afegir :num botelles més a la cistella";
$idioma['envio_gratuito'] = 'Enhorabona! Ja pots finalitzar sa comanda';

$idioma['novedad'] = 'Novetat';

return $idioma;