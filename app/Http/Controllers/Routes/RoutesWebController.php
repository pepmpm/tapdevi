<?php

namespace App\Http\Controllers\Routes;

use App\Blogs;
use Illuminate\Http\Request;
use Cache;
use App\Traits\Helpers;
use App\Traits\Plans;

class RoutesWebController extends RoutesController
{

    use Helpers;
    use Plans;


    public function __construct()
    {
        parent::__construct();
        $this->data['contact_bar'] = 'none';
        $this->data['blog'] = 'route';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $this->data['query'] = parent::_index(['status' => 'active', 'paginate' => 20]);
        $this->data['tags'] = parent::get_tags();
        return view('blogs.index', $this->data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $route = null)
    {
        if($route == null){
            return redirect()->route('routes.index');
        }
        
        $query = Blogs::with('langs', 'photos')->whereHas('langs', function ($query) use ($request, $route) {
            $query->where('slug', '=', $route);
            $query->where('locale', '=', \App::getLocale());
        })->where('category', '=', 'route')->where('status', '=', 'active')->first();

        if($query){
            $this->data['tags'] = parent::get_tags();
            $this->data['query'] = parent::_show($query);
            self::_save_visits($request, $query->id);
            return view('blogs.crud', $this->data);
        }

        return redirect()->back();
    }

    public function map(Request $request){
        $items_id = \App\ItemsStocks::where('date', '>=', date('Y-m-d'))->orderBy('date', 'asc')->pluck('items_id')->all();

        $this->data['index_events'] = Items::with('photos', 'stocks', 'langs')->where('type', '=', 'event')
            ->where('status', '=', 'active')->whereIn('id', $items_id)->take(12)->get()->sortBy(function($event, $key) use($items_id) {
                return array_search($event->id, $items_id);
            });

        $this->data['index_plans'] = parent::_get_plans(['limit' => 100]);

        return view('events.map', $this->data);
    }

    private function _save_visits($request, $routeId){
        $cookie = config('constants.NAMELITE') . '.blog.' . $routeId;
        if(!isset($_COOKIE[str_replace('.', '_', $cookie)])){
            $tmp = \App\BlogsVisits::firstOrNew(['blogs_id' => $routeId, 'year' => date('Y'), 'month' => date('m')]);
            $tmp->blogs_id = $routeId;
            $tmp->year = date('Y');
            $tmp->month = date('m');
            $tmp->{'day_' . date('j')} = $tmp->{'day_' . date('j')} + 1;
            $tmp->save();

            setcookie(config('constants.NAMELITE') . '.blog.' . $routeId, 'X', time() + 86400);
        }
    }

}
