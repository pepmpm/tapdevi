$(document).ready(function() {
    if(jQuery('#scrollbar').length){
        jQuery('#scrollbar').mCustomScrollbar({
            theme:"minimal",
            scrollInertia: 200,
            scrollEasing: "linear"
        });
    }
});
