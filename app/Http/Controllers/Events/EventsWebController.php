<?php

namespace App\Http\Controllers\Events;

use App\Items;
use Illuminate\Http\Request;
use Cache;
use App\Traits\Helpers;
use App\Traits\Plans;

class EventsWebController extends EventsController
{

    use Helpers;
    use Plans;


    public function __construct()
    {
        parent::__construct();
        $this->data['contact_bar'] = 'none';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($year = null, $month = null, $day = null){
        $this->data['day'] = $day;
        $this->data['month'] = $month != null ? trans('base.num_'.$month) : date('m');
        $this->data['year'] = $year != null ? $year : date('Y');

        /*$this->data['query'] = parent::_index(
            ['month' => date('m', $this->data['currenttime']), 'year' => date('Y', $this->data['currenttime'])]
        );*/

        $this->data['day_stocks'] = \App\ItemsStocks::select('date')->distinct()
            ->whereIn('items_id', Items::where('type', '=', 'event')->where('status', '=', 'active')->pluck('id')->all())
            ->whereMonth('date', $this->data['month'])
            ->whereYear('date', $this->data['year'])->pluck('date')->all();

        $this->data['index_events'] = $this->getEvents(
            [
                'day' => $this->data['day'],
                'month' => $this->data['month'],
                'year' => $this->data['year'],
                'get' => 'stocks'
            ]
        );

        return view('events.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['districts'] = \App\Districts::where('island', '=', 'mallorca')
            ->orderBy('name')->pluck('name', 'id')->all();
        $this->data['labels_lov'] = \App\Labels::listsTranslations('title')->where('category', '=', 'event')
            ->pluck('title', 'id')->all();
        $this->data['times'] = self::_get_times();
        return view('events.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $params = $request->all();
        $query = parent::_store($params);

        if(!($query instanceof Items)){
            return $query;
        }

        $params['times'] = isset($params['times']) ? implode('#', $params['times']) : null;
        $params['dates'] = explode(' - ', $params['dates']);

        $datefrom = new \DateTime($this->fec_str_to_mysql($params['dates'][0]));
        $dateto = new \DateTime($this->fec_str_to_mysql($params['dates'][1]));

        for($i = $datefrom; $i <= $dateto; $i->modify('+1 day')){

            $tmp = \App\ItemsStocks::where('items_id', '=', $query->id)
                ->whereMonth('date', $i->format('m'))
                ->whereDay('date', $i->format('d'))->first();

            if(!$tmp){
                $tmp = new \App\ItemsStocks;
            }

            $tmp->items_id = $query->id;
            $tmp->date = $i->format('Y-m-d');
            $tmp->times = $params['times'];
            $tmp->updated_at = date('Y-m-d H:i:s');
            $tmp->save();
        }

        return redirect()->route('enotourism-mallorca.events.show', array('item' => $query->slug, 'pending' => 'S'))
            ->with('message_success', 'Cambios guardados correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $item)
    {
        if($request->has('pending')){
            return view('events.pending', $this->data);
        }
        
        $event = Items::with('langs', 'photos')->whereHas('langs', function ($query) use ($request, $item) {
            $query->where('slug', '=', $item);
            $query->where('locale', '=', \App::getLocale());
        })->where('type', '=', 'event')->where('status', '=', 'active')->first();

        if($event){
            $this->data['index_events'] = $this->getEvents(['limit' => '15', 'not_in' => $event->id]);
            $this->data['query'] = parent::_show($event);
            $this->data['is_past'] = \App\ItemsStocks::where('items_id', '=', $event->id)
                ->where('date', '>', date('Y-m-d'))->count() == 0;

            self::_save_visits($request, $event->id);
            return view('events.crud', $this->data);
        }

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function edit(Bikes $bikes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bikes $bikes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bikes $bikes)
    {
        //
    }

    public function map(Request $request){
        $items_id = \App\ItemsStocks::where('date', '>=', date('Y-m-d'))->orderBy('date', 'asc')->pluck('items_id')->all();

        $this->data['index_events'] = Items::with('photos', 'stocks', 'langs')->where('type', '=', 'event')
            ->where('status', '=', 'active')->whereIn('id', $items_id)->take(12)->get()->sortBy(function($event, $key) use($items_id) {
                return array_search($event->id, $items_id);
            });

        $this->data['index_plans'] = parent::_get_plans(['limit' => 100]);

        return view('events.map', $this->data);
    }

    private function _save_visits($request, $plan){
        $cookie = config('constants.NAMELITE') . '.event.' . $plan;
        if(!isset($_COOKIE[str_replace('.', '_', $cookie)])){
            $tmp = \App\ItemsVisits::firstOrNew(['items_id' => $plan, 'year' => date('Y'), 'month' => date('m')]);
            $tmp->items_id = $plan;
            $tmp->year = date('Y');
            $tmp->month = date('m');
            $tmp->{'day_' . date('j')} = $tmp->{'day_' . date('j')} + 1;
            $tmp->save();

            setcookie(config('constants.NAMELITE') . '.plan.' . $plan, 'X', time() + 86400);
        }
    }

    private function _get_cross_selling($event){
        $events = Items::with('langs', 'photos')->has('stocks')->where('id', '!=', $event->id)
            ->where('type', '=', 'event')->where('status', '=', 'active')->take(8)->get();

        return $events;
    }

    private function _get_times(){
        $array = array();

        for($x = 9; $x < 24; $x++){
            $array[str_pad($x, 2, "0", STR_PAD_LEFT) . ':00'] = str_pad($x, 2, "0", STR_PAD_LEFT) . ':00';
            $array[str_pad($x, 2, "0", STR_PAD_LEFT) . ':30'] = str_pad($x, 2, "0", STR_PAD_LEFT) . ':30';
        }

        return $array;
    }

}
