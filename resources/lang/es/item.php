<?php

$idioma = array();

$idioma['validation_stock_required'] = 'Debes rellenar el stock que tendrá el producto';
$idioma['validation_stock_numeric'] = 'El stock debe ser un número';
$idioma['validation_min_pax_required'] = 'Debes rellenar el número mínimo de personas para esta actividad';
$idioma['validation_min_pax_numeric'] = 'El mínimo de personas debe ser un número';
$idioma['validation_max_pax_required'] = 'Debes rellenar el número máximo de personas para esta actividad';
$idioma['validation_max_pax_numeric'] = 'El máximo de personas debe ser un número';
$idioma['validation_cost_numeric'] = 'El precio de coste del producto debe ser un número';
$idioma['validation_price_required'] = 'Debes rellenar el precio de venta del producto';
$idioma['validation_price_numeric'] = 'El precio debe ser un número';
$idioma['validation_new_price_numeric'] = 'El precio con descuento debe ser numérico';
$idioma['validation_shipping_numeric'] = 'El precio a añadir a los portes base debe ser numérico';
$idioma['validation_labels_id_required'] = 'Se debe seleccionar una categoría';
$idioma['validation_labels_id_numeric'] = 'La categoría seleccionada es incorrecta';
$idioma['validation_users_id_required'] = 'Se debe seleccionar una bodega';
$idioma['validation_users_id_numeric'] = 'La bodega seleccionada es incorrecta';
$idioma['validation_name_required'] = 'Se debe rellenar el nombre del evento';
$idioma['validation_name_max'] = 'El nombre del producto no puede ser tan largo';
$idioma['validation_description_required'] = 'Se debe rellenar una descripción para el evento';
$idioma['validation_description_max'] = 'La descripción del evento es demasiado larga';
$idioma['validation_new_price_lt'] = 'El precio con descuendo debe ser menor que el precio habitual';

$idioma['custom_name'] = 'Caja personalizada de :user';
$idioma['custom_description'] = 'Si quieres probar varios vinos de la bodega :user puedes seleccionar los vinos que quieras hasta completar una caja. El precio de la caja se irá actualizando cada vez que añadas o elimines un vino.';
$idioma['custom_seo_description'] = '¿Quieres vino de :user pero una caja entera es mucho? Con '. config('constants.NAMEWEB') .' puedes elegir los vinos que quieres que lleve tu caja. ¿Dos blancos, un rosado y tres tintos? Tu eliges.';
$idioma['custom_seo_keywords'] = 'caja personalizada :user, caja custom :user, caja vino variada :user, caja variada :user, elegir vinos caja :user';


$idioma['cada_lote'] = 'cada lote';
$idioma['mensaje_elegir_plan_regalo'] = 'Tienes :planes plan con vino para elegir|Tienes :planes planes con vino para elegir';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';

return $idioma;