<?php

$idioma = array();

$idioma['inicio'] = 'Home';
$idioma['login'] = 'Login';
$idioma['registrarse'] = 'Register';
$idioma['registrarse_en'] = 'Register in ' . config('constants.NAMEWEB');
$idioma['nombre_empresa'] = 'Company name';
$idioma['email'] = 'Email';
$idioma['password'] = 'Password';
$idioma['confirm_password'] = 'Confirm password';
$idioma['olvidaste_password'] = 'Forgot your password?';
$idioma['recordarme'] = 'Remember me';
$idioma['acceder'] = 'Log in';
$idioma['acceder_a_tu_cuenta'] = 'Log into your account';

$idioma['bienvenido_a'] = 'Welcome to ' . config('constants.NAMEWEB');

$idioma['titulo_login_1'] = 'Stock control';
$idioma['mensaje_login_1'] = 'With ' . config('constants.NAMEWEB') . ' you will be able to control and manage your stock and the orders that you will receive.';
$idioma['titulo_login_2'] = 'Increase your visibility';
$idioma['mensaje_login_2'] = 'Diversify your offer and receive orders from our website.';
$idioma['titulo_login_3'] = 'Majorcan product of km0';
$idioma['mensaje_login_3'] = "We are committed to local trade and we demand the quality of Mallorca's products.";

$idioma['enoturismo'] = 'Enotourism';
$idioma['turismo_de_vino'] = 'Wine tourism';

$idioma['enotourism-mallorca'] = 'Enotourism in Majorca';
$idioma['enotourism-mallorca-slug'] = 'enotourism-in-majorca';
$idioma['vinos_de_mallorca'] = 'Majorcan wines';
$idioma['msg_h5_inicio'] = 'Many options to give away in wineries, hotels and restaurants';
$idioma['msg_h1_inicio'] = 'Discover Majorcan wine through tastings, events and pairings';

$idioma['ver_todos'] = 'See all';

$idioma['regalos_y_lotes'] = 'Gifts and lots';

$idioma['destacados'] = 'Popular';
$idioma['destacado'] = 'Top';
$idioma['tipo'] = 'Types';
$idioma['precio'] = 'Prices';
$idioma['bodegas'] = 'Wineries';
$idioma['packs'] = 'Packs';
$idioma['planes'] = 'plans';
$idioma['recomendaciones'] = 'recommendations';
$idioma['mas_vendidos'] = 'Best sellers';
$idioma['regala_cata_de_vino'] = 'Give wine tastings';
$idioma['por_tipo'] = 'By type';
$idioma['por_precio'] = 'By price';

$idioma['packs_slug'] = 'packs';
$idioma['recomendaciones_slug'] = 'recommendations';
$idioma['mas_vendidos_slug'] = 'best-sellers';

$idioma['carrito'] = 'Cart';
$idioma['carrito_numero'] = 'Your cart has :num items';
$idioma['revisa_tu_pedido'] = 'Review and edit your products';
$idioma['informacion_de_facturacion'] = 'Billing info';
$idioma['pago'] = 'Payment';
$idioma['informacion_pago'] = 'Select Payment method';

$idioma['articulo'] = 'Item';
$idioma['cuando'] = 'When';
$idioma['cantidad'] = 'Quantity';
$idioma['total'] = 'Total';
$idioma['subtotal'] = 'Subtotal';
$idioma['anterior'] = 'Previous';
$idioma['siguiente'] = 'Next';
$idioma['codigo_descuento'] = 'Discount code';
$idioma['introduce_codigo_descuento'] = 'Enter your coupon code';
$idioma['aplicar_cupon'] = 'Apply coupon';
$idioma['calcular_envio'] = 'Calculate shipping';
$idioma['introduce_codigo_postal'] = 'Insert your postal code';
$idioma['actualizar_totales'] = 'Refresh total';
$idioma['actualizar_carrito'] = 'Refresh';

$idioma['lotes_de_vinos_y_productos_mallorquines'] = 'Lots of majorcan wines and products';
$idioma['msg_vi_mallorqui'] = 'Do you want to taste Mallorcan wine at home?';
$idioma['msg_vi_mallorqui2'] = 'Selection of Mallorcan wines from family wineries and with limited productions directly at your home with free shipping if you live in the Balearic Islands';


$idioma['cookies_message'] = 'This website uses its own and third-party cookies to collect information in order to improve our services and show you advertising related to your preferences, based on a profile drawn up from your browsing habits.';
$idioma['aceptar'] = 'Accept';
$idioma['llamar'] = 'Call';

$idioma['contenido_carrito'] = 'Cart items';

$idioma['plans_for_day'] = 'Plans for :day'; 
$idioma['plans_for_weekend'] = 'Weekend plans';

$idioma['politica_cookies'] = 'Cookies policy';
$idioma['politica_privacidad'] = 'Privacy policy';

$idioma['msg_search'] = 'Search for plans, wineries, places...';
$idioma['comprar_vino_mallorquin'] = 'Buy majorcan wine';
$idioma['tienda_de_vinos'] = 'Wine shop';
$idioma['vinos_de_mallorca-slug'] = 'majorca-wines';

$idioma['msg_publica_evento'] = 'Publish your wine event in Mallorca for free';
$idioma['msg_publica_evento_btn'] = 'Publish event';

$idioma['proximos_eventos'] = 'Upcoming events';
$idioma['eventos_con_vino_en_mallorca'] = 'Events with wine in Mallorca';
$idioma['routes_mensaje_guias_vino'] = 'Information and guides about wine in Mallorca';
$idioma['routes_mensaje_titulo'] = 'Guides and wine routes';

return $idioma;