<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogsPhotos extends _BaseModel{

	protected $fillable = array('order', 'url', 'origin', 'blogs_id');

    public function __construct(array $attributes = array()) {
        parent::__construct($attributes);
    }

    public function blog(){
        return $this->belongsTo('\App\Blogs', 'blogs_id');
    }

}
