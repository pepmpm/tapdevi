<?php

namespace App\Http\Controllers\UsersPhotos;

use App\User;
use App\UsersPhotos;
use Illuminate\Http\Request;

class UsersPhotosPanelController extends UsersPhotosController
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $return = parent::_store($request->all());
		echo $return;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Plans  $plans
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UsersPhotos $user_photo)
    {
        $this->data['query'] = parent::_update($request->all(), $user_photo);
        return redirect()
            ->route('back.users.show', array('user' => $request->get('id')))
            ->with('message_success', 'Cambios realizados correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Plans  $plans
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsersPhotos $photo)
    {
        $users_id = parent::_destroy($photo);
        return redirect()
            ->route('back.users.show', array('user' => $users_id))
            ->with('message_success', 'Cambios realizados correctamente');
    }
}
