@extends('_layouts/inicio')

@section('title', trans('meta.title_index'))
@section ('meta')
    <meta name="description" content="{{ trans('meta.description_index') }}"/>
    <meta name="keywords" content="{{ trans('meta.keywords_index') }}" />
    <meta name="Author" content="{{ trans('meta.author') }}" />
    <meta property="og:title" content="{{ trans('meta.title_index') }}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:image" content="{{ url('assets/img/logo3.png') }}"/>
    <meta property="og:description" content="{{ trans('meta.description_index') }}" />
    @parent
@stop

@section ('body')
	<div class="breadcrumbs">
	    <div class="container">
	        <h1 class="pull-left">Política de privacidad</h1>
	    </div>
	</div>
	<div class="content-md">
    	<div class="container">
        	<div class="row">
        		<div class="col-md-12">
        			<h1>POLÍTICA DE PRIVACIDAD</h1>
					<ul>
						<li>1. INFORMACIÓN AL USUARIO</li>
						<p>{{ config('constants.NAMEWEB') }}, en adelante RESPONSABLE, es el Responsable del tratamiento de los datos 
						personales el Usuario y le informa que estos datos serán tratados de conformidad con lo dispuesto en el 
						Reglamento (UE) 2016/679 de 27 de abril (GDPR) y la Ley Orgánica 3/2018 de 5 de diciembre (LOPDGDD), por lo 
						que se le facilita a siguiente información del tratamiento:</p>

						<p>Fin del tratamiento: mantener una relación comercial con el Usuario. Las operaciones previstas para realizar 
						el tratamiento son:</p>

						<p>Remisión de comunicaciones comerciales publicitarias por email, SMS, MMS, comunidades sociales o 
						cualquier otro medio electrónico o físico, presente o futuro, que posibilite realizar comunicaciones 
						comerciales. Estas comunicaciones serán realizadas por el RESPONSABLE y relacionadas sobre sus productos 
						y servicios, o de sus colaboradores o proveedores con los que éste haya alcanzado algún acuerdo de promoción. 
						En este caso, los terceros nunca tendrán acceso a los datos personales.</p>

						<p>Realizar estudios estadísticos.</p>
						<p>Tramitar encargos, solicitudes o cualquier tipo de petición que sea realizada por el usuario a través de 
						cualquiera de las formas de contacto que se ponen a su disposición.</p>

						<p>Remitir el boletín de noticias de la página web.</p>
						<p>Base jurídica del tratamiento: consentimiento del interesado.</p>

						<p>Criterios de conservación de los datos: se conservarán durante no más tiempo del necesario para mantener el 
						fin del tratamiento y cuando ya no sea necesario para tal fin, se suprimirán con medidas de seguridad 
						adecuadas para garantizar la seudonimización de los datos o la destrucción total de los mismos.</p>

						<p>Comunicación de los datos: no se comunicarán los datos a terceros, salvo obligación legal.</p>

						<p>Derechos que asisten al Usuario:</p>
						<p>Derecho a retirar el consentimiento en cualquier momento.</p>
						<p>Derecho de acceso, rectificación, portabilidad y supresión de sus datos y a la limitación u oposición al 
						su tratamiento.</p>
						<p>Derecho a presentar una reclamación ante la autoridad de control (www.aepd.es) si considera que el tratamiento 
						no se ajusta a la normativa vigente.</p>
						<p>Datos de contacto para ejercer sus derechos:</p>

						<p>{{ config('constants.NAMEWEB') }}. {{ config('constants.ADDRESSWEB') }}, Email: {{ config('constants.MAILWEB') }}</p>

						<li>2. CARÁCTER OBLIGATORIO O FACULTATIVO DE LA INFORMACIÓN FACILITADA POR EL USUARIO</li>
						<p>Los Usuarios, mediante la marcación de las casillas correspondientes y entrada de datos en los campos, 
						marcados con un asterisco (*) en el formulario de contacto o presentados en formularios de descarga, 
						aceptan expresamente y de forma libre e inequívoca, que sus datos son necesarios para atender su petición, 
						por parte del prestador, siendo voluntaria la inclusión de datos en los campos restantes. El Usuario 
						garantiza que los datos personales facilitados al RESPONSABLE son veraces y se hace responsable de 
						comunicar cualquier modificación de los mismos. El RESPONSABLE informa y garantiza expresamente a los 
						usuarios que sus datos personales no serán cedidos en ningún caso a terceros, y que siempre que realizara 
						algún tipo de cesión de datos personales, se pedirá previamente el consentimiento expreso, informado e 
						inequívoco por parte los Usuarios. Todos los datos solicitados a través del sitio web son obligatorios, 
						ya que son necesarios para la prestación de un servicio óptimo al Usuario. En caso de que no sean 
						facilitados todos los datos, no se garantiza que la información y servicios facilitados sean completamente 
						ajustados a sus necesidades.</p>

						<li>3. MEDIDAS DE SEGURIDAD</li>
						<p>Que de conformidad con lo dispuesto en las normativas vigentes en protección de datos personales, el 
						RESPONSABLE está cumpliendo con todas las disposiciones de las normativas GDPR para el tratamiento de los 
						datos personales de su responsabilidad, y manifiestamente con los principios descritos en el artículo 5 del 
						GDPR, por los cuales son tratados de manera lícita, leal y transparente en relación con el interesado y 
						adecuados, pertinentes y limitados a lo necesario en relación con los fines para los que son tratados. 
						El RESPONSABLE garantiza que ha implementado políticas técnicas y organizativas apropiadas para aplicar 
						las medidas de seguridad que establecen el GDPR con el fin de proteger los derechos y libertades de los 
						Usuarios y les ha comunicado la información adecuada para que puedan ejercerlos.</p>
					</ul>
        		</div>
        	</div>
        </div>
    </div>

@stop

@section ('assets_end')

@stop
