<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use App\Traits\Helpers;

class Items extends Model
{

    // Eliminar lo de publicar tu evento, no tendrá exito.
    // Sección blog
    // contactar hoteles
    // posicionar en inglés
    // revisar descripciones en inglés y catalán
    // modificar el elements.blade.php de los planes, para que salgan las bodegas con cada uno de sus planes


	use Helpers;
    use Translatable;
    public $translatedAttributes = ['name', 'title', 'slug', 'short_description', 'description', 'seo_description', 'seo_keywords', 'timetable'];
    protected $with = ['langs'];
    protected $appends = array('trueprice', 'fullname', 'eventprice');

    public function getTruepriceAttribute(){
        if($this->new_price != null && $this->new_price < $this->price){
            return $this->new_price;
        }

        return $this->price;
    }

    public function getEventpriceAttribute(){
        if($this->price == 0){
            return trans('base.gratuito');
        }
        if($this->price == 999){
            return trans('base.pendiente');
        }

        return $this->price;
    }

    public function getFullNameAttribute(){
        if($this->users_id != null){
            return $this->name . " (" . $this->user->name . ")";
        }

        return $this->name;
    }

    public function user(){
        return $this->belongsTo('\App\User', 'users_id');
    }

    public function label(){
        return $this->belongsTo('\App\Labels', 'labels_id');
    }

    public function district(){
        return $this->belongsTo('\App\Districts', 'districts_id');
    }
    
    public function langs(){
        return $this->hasMany('\App\ItemsLangs', 'items_id')->orderBy('id', 'asc');
    }

    public function packs(){
        return $this->hasMany('\App\ItemsPacks', 'items_id');
    }

    public function packsitems(){
        return $this->belongsToMany('\App\Items', 'items_packs', 'items_id', 'items_id_related');
    }

    public function visits(){
        return $this->hasMany('\App\ItemsVisits', 'items_id');
    }

    public function allstocks(){
        return $this->hasMany('\App\ItemsStocks', 'items_id')->orderBy('date', 'asc');
    }

    public function stocks(){
        return $this->hasMany('\App\ItemsStocks', 'items_id')->where('date', '>=', date('Y-m-d'))->orderBy('date', 'asc');
    }

    public function stock($date){
        return $this->hasMany('\App\ItemsStocks', 'items_id')->where('date', '=', $date)->first();
    }

    public function next_stock($sumdays = 0){
        return $this->hasMany('\App\ItemsStocks', 'items_id')
            ->where('date', '>=', now()->addDays($sumdays)->setTime(0, 0, 0)->toDateTimeString())
            ->orderBy('date', 'asc');
    }

    public function stocks_array(){
        return $this->hasMany('\App\ItemsStocks', 'items_id')
            ->where('date', '>=', now()->addDays(3)->setTime(0, 0, 0)->toDateTimeString())
            ->pluck('items_id', 'date')->all();
    }

    public function times_array(){
        return $this->hasMany('\App\ItemsStocks', 'items_id')->where('date', '>=', date('Y-m-d'))
                ->pluck('times', 'date')->all();
    }

    public function enabled_dates(){
        $enabled_dates = [];
        foreach($this->stocks_array() as $stock_key => $stock_array){
            $enabled_dates[] = '"' . $this->fec_mysql_to_mdy($stock_key) . '"';
        }
        return $enabled_dates;
    }

    public function photos(){
        return $this->hasMany('\App\ItemsPhotos', 'items_id')->orderBy('order', 'asc');
    }

    public function one_photo($default = ''){
        $photos = $this->hasMany('\App\ItemsPhotos', 'items_id')->orderBy('order', 'asc')->orderBy('id', 'asc');
        if($photos->count() > 0){
            return $photos->first()->url;
        }
        return $default . '/' . 'default.png';
    }

    public function two_photo($default = ''){
        $photos = $this->hasMany('\App\ItemsPhotos', 'items_id')->orderBy('order', 'asc')->orderBy('id', 'asc');
        if($photos->count() > 0){
            return $photos->skip(1)->first()->url;
        }
        return $default . '/' . 'default.png';
    }

    public function get_a_pagar_ahora($quantity){
        $price = 0;
        if($this->type == 'plan'){
            switch ($this->confirmation) {
                case 'groupon':
                    $price = $quantity * $this->trueprice;
                    break;
                case 'direct':
                    $price = 0;
                    break;
                case 'fee':
                    $price = $quantity * ($this->trueprice - $this->cost);
                    break;
                default:
                    $price = $quantity * $this->trueprice;
                    break;
            }
        }else{
            $price = $quantity * $this->trueprice;
        }

        return $price;
    }

    public function get_unit_name(){
        return trans('order.personas');
    }

    public function get_unit_name2($num){
        return trans_choice('order.persona', $num);
    }
    
}
