$(function(){
    $('.btn_wine').click(function () {
        var botellas = $('#tabla > tbody > tr').length + 1;
        if(botellas > 6){
            return;
        }else if(botellas == 6){
            $('#btn_submit').show();
        }
        /* Quitamos stock */
        $( this ).children( '.stock' ).val(parseInt($( this ).children( '.stock' ).val()) - 1);

        /* cambiamos la imagen de las botellas */
        get_box_image(botellas);

        /* añadimos un registro a la tabla personalizada */
        var price = $(this).attr('price');
        var winery = $(this).attr('winery');
        var wine = $(this).attr('wine');
        var idwine = $(this).attr('idwine');
        $('#tabla > tbody:last-child').append( get_tr( idwine, wine, price, winery ) );

        /* Borramos el propio botón si hemos acabado el stock */
        if($( this ).children( '.stock' ).val() < 1){
            $(this).hide();
        }

        calc_price_tr();
    });

    $('.btn_delete_bottle').live("click", function(event) {
        /* Escondemos el botón submit */
        $('#btn_submit').hide();

        /* Borramos el TR */
        $(this).closest("tr").remove();

        /* Anyadimos stock */
        $( '#wine_' + $(this).attr('idwine') ).children( '.stock' ).val(parseInt($( '#wine_' + $(this).attr('idwine') ).children( '.stock' ).val()) + 1);

        /* Borramos el propio botón si hemos acabado el stock */
        if($( '#wine_' + $(this).attr('idwine') ).children( '.stock' ).val() > 0){
            $( '#wine_' + $(this).attr('idwine') ).show();
        }

        /* cambiamos la imagen de las botellas */
        get_box_image($('#tabla > tbody > tr').length);
        event.preventDefault();
    });
});

function calc_price_tr(){
    var price = 0;
    $('#tabla > tbody > tr').each(function(index, tr) {
        price += parseFloat($(this).attr('price'));
    });
    $('.total').html(price + "€");
}

function get_tr(idwine, wine, price, winery){
    var cadena = "<tr price='"+ price +"'>";
    cadena += "<td><input type='hidden' name='id[]' value='"+ idwine +"'/>" + wine + "</td>";
    cadena += "<td>" + price + "</td>";
    cadena += "<td>" + winery + "</td>";
    cadena += '<td><a href="#" idwine="'+ idwine +'" class="btn_delete_bottle"><i class="fa fa-times" aria-hidden="true"></i></a></td>';
    cadena += "</tr>";

    return cadena;
}