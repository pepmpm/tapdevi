<?php

namespace App\Http\Controllers\Users;

use App\Bikes;
use Illuminate\Http\Request;

class UsersWebController extends UsersController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $params = self::_get_params($request);
        $user = \App\User::where('slug', '=', $params['user'])->first();
        
        $this->data['params'] = $params;
        $this->data['query'] = parent::_show($user);

        self::_save_visits($request, $user->id);

        return view('users.crud', $this->data);
    }

    private function _get_params($request){
        $params = array();
        $params['district'] = $request->route('district');
        $params['cat'] = $request->route('cat');
        $params['user'] = $request->route('user');
        $params['locale'] = \App::getLocale();

        return $params;
    }

    private function _save_visits($request, $user){
        $cookie = config('constants.NAMELITE') . '.user.' . $user;
        if(!isset($_COOKIE[str_replace('.', '_', $cookie)])){
            $tmp = \App\UsersVisits::firstOrNew(['users_id' => $user, 'year' => date('Y'), 'month' => date('m')]);
            $tmp->users_id = $user;
            $tmp->year = date('Y');
            $tmp->month = date('m');
            $tmp->{'day_' . date('j')} = $tmp->id != null ? $tmp->{'day_' . date('j')} + 1 : 1;
            $tmp->save();

            setcookie(config('constants.NAMELITE') . '.user.' . $user, 'X', time() + 86400);
        }
    }

}
