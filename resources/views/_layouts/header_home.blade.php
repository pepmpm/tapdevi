<div class="header-v6">
    <div class="navbar mega-menu" role="navigation">
        <div class="container">
            <div class="menu-container">
                <div class="navbar-brand">
                    <a href="{{ route('/') }}">
                        <img class="default-logo" src="{{ asset('assets/img/logo_200.png') }}" alt="Logo"/>
                        <img class="shrink-logo" src="{{ Imgfly::imgPublic('img/logo.png' . '?w=200', 'assets') }}" alt="Logo"/>
                    </a>
                </div>
                <div class="header-inner-right">
                    <ul class="menu-icons-list">
                        <li class="menu-icons shopping-cart">
                            <i class="menu-icons-style radius-x fa fa-2x fa-shopping-cart"></i>
                            <span class="badge badge-sea">{{ session()->has('cart') ? session('cart')['quantity'] : 0 }}</span>
                            <div class="shopping-cart-open">
                                <ul class="list-unstyled mCustomScrollbar" data-mcs-theme="minimal-dark">
                                    @if(session()->has('cart'))
                                        <li>
                                            <img src="{{ Imgfly::imgPublic(str_replace("upload/", "", session('cart')['item']->one_photo()) . '?w=100', 'upload') }}" 
                                                alt="{{ session('cart')['item']->name }}">
                                            <a class="close" href="{{ route('orders.delete', ['id' => session('cart')['item']->id]) }}">x</a>
                                            <div class="overflow-h">
                                                <span>{{ session('cart')['item']->name }}</span>
                                                <small>{{ session('cart')['quantity'] . " x " . session('cart')['item']->trueprice . "€" }}</small>
                                            </div>
                                        </li>
                                    @endif
                                </ul>
                                <div class="subtotal">
                                    <div class="overflow-h margin-bottom-10">
                                        <span>Subtotal</span>
                                        <span class="pull-right subtotal-cost">{{ (session()->has('cart') ? session('cart')['price'] : 0) . "€" }}</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <a href="{{ route('orders.index') }}" class="btn-u btn-brd btn-brd-hover btn-u-sea-shop btn-block">
                                                {{ trans('base.ver_carrito') }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="menu-icons">
                            <i class="menu-icons-style search search-close search-btn fa fa-2x fa-search"></i>
                            <div class="search-open">
                                <input type="text" class="animated fadeIn form-control" placeholder="Start searching ..."/>
                            </div>
                        </li>
                        <li class="menu-icons">
                            <button type="button" class="navbar-toggle margin0 padding-tb-2-10" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </li>
                    </ul>

                </div>
            </div>
            @include('_layouts.menu')
        </div>
    </div>
</div>
<div class="interactive-slider-v2 breadcrumbs-home2">
    <div class="container bg-opacity-75">
        <h1 class="font25">{{ trans('inicio.msg_h1_inicio') }}</h1>
        <p>{{ trans('inicio.msg_h5_inicio') }}</p>
        <form id="home_form" role="form" method="get" enctype="multipart/form-data" autocomplete="off" action="{{ route('/') }}">
            <div class="row margin-bottom-20">
                <div class="col-md-5">
                    {!! Form::select('where', ['' => trans('plan.por_zona')] + $districts_home, '', 
                        array('class' => 'form-control input-lg margin-bottom-15')) !!}
                </div>
                <div class="col-md-5">
                    {!! Form::select('category', ['' => trans('plan.selecciona_una_categoria')] + $labels_home, '', 
                        array('class' => 'form-control input-lg margin-bottom-15')) !!}
                </div>
                <div class="col-md-2">
                    <button class="btn-u btn-u-lg btn-block" role="button" type="submit">
                        {{ trans('base.buscar') }}
                    </button>
                </div>
            </div>
        </form>
        <p><a href="{{ route('enotourism-mallorca.events.index') }}" class="padding0">{{ trans('inicio.eventos_con_vino_en_mallorca') }}</a></p>
    </div>
</div>