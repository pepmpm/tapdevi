<?php

namespace App\Http\Controllers\Labels;

use App\Labels;
use Illuminate\Http\Request;

class LabelsPanelController extends LabelsController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['query'] = parent::_index();
        $this->data['all'] = parent::_all();
        return view('_back.labels.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['status'] = Labels::getPossibleEnumValues('status');
        $this->data['labels'] = Labels::listsTranslations('name')->pluck('name', 'id')->all();
        $this->data['categories'] = Labels::getPossibleEnumValues('category');
        return view('_back.labels.crud', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->data['query'] = parent::_store($request->all());

        if($this->data['query'] == null){
            return redirect()->back()->with('message_error', 'Debes informar el nombre');
        }

        return redirect()
            ->route('back.labels.show', array('label' => $this->data['query']->id))
            ->with('message_success', 'Cambios guardados correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function show(Labels $label)
    {
        $this->data['status'] = Labels::getPossibleEnumValues('status');
        $this->data['labels'] = Labels::listsTranslations('name')->where('labels.id', '!=', $label->id)
            ->pluck('name', 'id')->all();
        $this->data['categories'] = Labels::getPossibleEnumValues('category');
        $this->data['query'] = parent::_show($label);
        return view('_back.labels.crud', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Labels $label)
    {
        $this->data['query'] = parent::_update($request->all(), $label);

        if($this->data['query'] == null){
            return redirect()->back()->with('message_error', 'Debes informar el nombre y subir una foto');
        }

        return redirect()
            ->route('back.labels.show', array('label' => $this->data['query']->id))
            ->with('message_success', 'Cambios guardados correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Labels $label)
    {
        parent::_destroy($label);
        return redirect()
            ->route('back.labels.index')
            ->with('message_success', 'Cambios guardados correctamente');
    }
}
