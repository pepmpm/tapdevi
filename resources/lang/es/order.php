<?php

$idioma = array();

$idioma['validation_users_id_required'] = 'Falta el nombre del cliente';
$idioma['validation_users_id_max'] = 'El nombre del cliente es demasiado largo';

$idioma['validation_name_required'] = 'Falta tu nombre';
$idioma['validation_name_max'] = 'El nombre es demasiado largo';
$idioma['validation_phone_numeric'] = "El teléfono debe ser numérico";
$idioma['validation_phone_max'] = 'El teléfono es demasiado largo';
$idioma['validation_email_required'] = "El email es obligatorio";
$idioma['validation_email_email'] = 'El email no es correcto';
$idioma['validation_email_max'] = 'El email es demasiado largo';
$idioma['validation_items_id_required'] = 'No has seleccionado un producto';
$idioma['validation_items_id_numeric'] = 'Hay un error con algún producto';
$idioma['validation_quantity_required'] = 'La cantidad es obligatoria';
$idioma['validation_quantity_numeric'] = 'Hay un error con alguna cantidad';
$idioma['validation_discount_numeric'] = 'El descuento ha de ser un número';
$idioma['validation_credit_card_card_invalid'] = 'El número de tarjeta no es correcto';
$idioma['validation_credit_card_card_cvc_invalid'] = 'El código CVC no es correcto';
$idioma['validation_credit_card_card_card_expiration_month_invalid'] = 'El mes de la tarjeta que has introducido no es correcto';
$idioma['validation_credit_card_card_expiration_year_invalid'] = 'El año de la tarjeta que has introducido no es correcto';
$idioma['validation_credit_card_required'] = 'Falta información de la tarjeta de crédito';
$idioma['botellas'] = 'botellas';
$idioma['unidades'] = 'unidades';
$idioma['personas'] = 'personas';


$idioma['carrito'] = 'Carrito';
$idioma['cantidad'] = 'Cantidad';
$idioma['informacion_envio'] = 'Información de la reserva';
$idioma['facturacion'] = "Facturación";
$idioma['nombre'] = 'Nombre';
$idioma['apellidos'] = 'Apellidos';
$idioma['email'] = 'Email';
$idioma['telefono'] = 'Teléfono';
$idioma['direccion'] = 'Dirección';
$idioma['provincia'] = 'Provincia';
$idioma['poblacion'] = 'Población';
$idioma['codigo_postal'] = 'Código Postal';
$idioma['numero_de_cuenta'] = 'Número de tarjeta';
$idioma['mes'] = 'Mes';
$idioma['ano'] = 'Año';
$idioma['cvv'] = 'CVV';
$idioma['note'] = 'Nota';
$idioma['mensaje_tarjetas_bbdd'] = config('constants.NAMEWEB') . ' no guardará ningun dato de facturación.';
$idioma['enviar_direccion_diferente'] = 'Necesito factura (para el cumplimiento de obligaciones tributarias)';

$idioma['codigo_descuento_aplicado'] = 'Código de descuento :code aplicado';
$idioma['descuento_aplicado'] = 'Descuento aplicado';
$idioma['descuento_en_el_envio'] = 'Descuento en el envío';

$idioma['caja'] = 'Caja|Cajas';
$idioma['persona'] = 'Persona|Personas';
$idioma['articulo'] = 'Artículo|Artículos';
$idioma['lote'] = 'Lote|Lotes';

$idioma['gratuito_islas_baleares'] = 'Envío gratis a Islas Baleares';
$idioma['_gratuito_islas_baleares'] = 'gratis a Islas Baleares';

$idioma['elige_un_metodo_de_pago'] = 'Elige un método de pago';
$idioma['tarjeta_de_credito_o_debito'] = 'Tarjeta de crédito o débito';
$idioma['pago_por_transferencia_bancaria'] = 'Pago por transferencia bancaria';

$idioma['finalizar_pedido'] = 'Finalizar pedido';
$idioma['pedido'] = 'Pedido';

$idioma['mensaje_cabecera_login'] = 'Introduce tu email y la contraseña que recibiste por email con tu primer pedido';

$idioma['pedido_finalizado'] = 'Pedido finalizado';
$idioma['factura'] = 'Factura';
$idioma['tu_informacion'] = 'Tu información';
$idioma['estado'] = 'Estado';
$idioma['fecha'] = 'Fecha';
$idioma['codigo_de_seguimiento'] = 'Código de seguimiento';
$idioma['datos_del_pedido'] = 'Datos del pedido';

$idioma['mensaje_factura_direct'] = 'El día de la experiencia se realizará el pago';
$idioma['mensaje_factura_fee'] = 'El día de la experiencia se realizará el resto del pago';
$idioma['mensaje_factura_groupon'] = 'Ya se ha realizado el pago de la experiencia, ahora debes ponerte en contacto con la empresa para acordar el día';
$idioma['usar_este_usuario'] = 'Usar este email';

$idioma['producto_anyadido_al_carrito'] = 'Producto añadido al carrito';

$idioma['FUTUROS_EVENTOS'] = 'FUTUROS EVENTOS';
$idioma['_DE_QUIEN_'] = '¿DE QUIEN?';
$idioma['_PARA_QUIEN_'] = '¿PARA QUIEN?';
$idioma['_ACTIVIDAD_'] = 'ACTIVIDAD';
$idioma['_VALE_POR_'] = 'VALE POR';
$idioma['_CUANDO_'] = '¿CUÁNDO?';
$idioma['_DONDE_'] = '¿DÓNDE?';
$idioma['_CODIGO_'] = 'CÓDIGO RESERVA';
$idioma['_CUANTO_'] = '¿CUÁNTO?';

$idioma['proponer_fecha'] = 'Proponer fecha';
$idioma['contactar'] = 'Contactar';


return $idioma;