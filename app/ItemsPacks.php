<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemsPacks extends _BaseModel
{
	protected $fillable = ['items_id', 'items_id_related'];
    public $timestamps = true;

    public function item(){
        return $this->belongsTo('\App\Items', 'items_id');
    }


	public function related_item(){
        return $this->belongsTo('\App\Items', 'items_id_related');
    }
}
