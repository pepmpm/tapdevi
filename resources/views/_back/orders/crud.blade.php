@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section ('body')

<!--=== Breadcrumbs ===-->
<div class="breadcrumbs">
    <div class="container">
        @if(isset($query))
            <h1 class="pull-left">Pedido: {{ $query->code }}</h1>
        @else
            <h1 class="pull-left">Nuevo pedido</h1>
        @endif
    </div>
</div>
<div class="container content">
    <div class="row">
        <div class="col-md-8">
            <a href="{{ route('back.orders.index') }}" 
               class="btn-u btn-u-lg" role="button">
                Volver a la lista
            </a>
            <a href="{{ route('back.orders.create') }}" 
               class="btn-u btn-u-lg" role="button">
                Crear otro pedido
            </a>
            @if(isset($query))
                <a href="{{ route('back.orders.download_voucher', ['orders_id' => $query->id, 'email' => $query->email]) }}" 
                   class="btn-u btn-u-green btn-u-lg" role="button">
                    Descargar bono
                </a>
            @endif
        </div>
        @if(isset($query) && $query->price != null)
            <div class="col-md-2">
                <span class="label label-yellow font40">
                    {{ $query->price . "€" }}
                </span>
            </div>
        @endif
        @if(isset($query))
            <form action="{{ route('back.orders.destroy', [$query->id]) }}" method="POST">
                @method('DELETE')
                @csrf
                <button class="btn-u" type="submit">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                </button>
            </form>
        @endif
    </div>
    <hr/>
    @include('_layouts.messages', array('errors' => $errors))
    <form class="margin-bottom-20" role="form" method="post" enctype="multipart/form-data" autocomplete="off" 
        action="{{ isset($query) ? route('back.orders.update', $query->id) : route('back.orders.store') }}">
        @csrf
        @if(isset($query))
            @method('PUT')
            @php($tmpitem = $query->item)
        @endif
        <div class="row">
            <div class="col-md-5">
                <div class="panel panel-default">
                    <div class="panel-heading">Información</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" class="btn-u btn-block">Guardar datos</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Status</label>
                                    {!! Form::select('status', $status_lov, 
                                        old('status', isset($query) ? $query->status: ''), 
                                        array('class' => 'form-control', 'id' => 'status')) !!}
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Nombre del cliente"
                                        value="{{ old('name', isset($query) ? $query->name : '') }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Teléfono</label>
                                    <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone"
                                        value="{{ old('phone', isset($query) ? $query->phone : '') }}"/>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" name="email" id="email" placeholder="Email"
                                        value="{{ old('email', isset($query) ? $query->email : '') }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-md-6">
                                @if(isset($query))
                                    @if($query->date_visited != null)
                                        <label>Visited</label>
                                        <p>{{ $helpers->fechora_mysql_to_str($query->date_visited, 'd/m/Y H:i') }}</p>
                                    @else
                                        <a href="{{ route('back.orders.mark_booking', ['order' => $query->id, 'action' => 'visited']) }}">
                                            Marcar como visitado
                                        </a>
                                    @endif
                                @endif
                            </div>
                            <div class="col-md-6">
                                @if(isset($query))
                                    @if($query->date_paid != null)
                                        <label>Paid</label>
                                        <p>{{ $helpers->fechora_mysql_to_str($query->date_paid, 'd/m/Y H:i') }}</p>
                                    @else
                                        <a href="{{ route('back.orders.mark_booking', ['order' => $query->id, 'action' => 'paid']) }}">
                                            Marcar como pagado
                                        </a>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="panel panel-default">
                    <div class="panel-heading">Productos</div>
                    <div class="panel-body">
                        <div class="row margin-bottom-15">
                            <div class="col-md-11">
                                <label>Producto</label>
                                <select class="form-control items_id input-sm" name="items_id">
                                    @foreach($items as $item)
                                        <option value="{{ $item->id }}" price="{{ $item->trueprice }}"
                                            {{ old('items_id', isset($query) ? $tmpitem->id : null) == $item->id ? 'selected=""' : '' }}>
                                            {{ $item->fullname }}
                                        </option>
                                    @endforeach
                                </select>
                                <input type="hidden" id="tmpprice" value="{{ isset($query) ? $tmpitem->trueprice : '' }}"/>
                            </div>
                            @if(isset($tmpitem))
                                <div class="col-md-1">
                                    <label>&nbsp;</label>
                                    <a href="{{ route('back.plans.show', ['item' => $tmpitem->id]) }}" class="margin-lr-5">
                                        <i class="fa fa-pencil color-tdv"></i>
                                    </a>
                                </div>
                            @endif
                        </div>
                        <div class="row margin-bottom-15">
                            <div class="col-md-3">
                                <label>Fecha</label>
                                <input type="text" class="form-control input-sm date_event" name="date_event" 
                                    items_id="{{ isset($tmpitem) ? $tmpitem->id : null }}"
                                    value="{{ isset($query) ? $helpers->fec_mysql_to_str($query->date_event) : '' }}">
                            </div>
                            <div class="col-md-2">
                                <label>Hora</label>
                                {{ $time_lov }}
                            </div>
                            <div class="col-md-2">
                                @if(isset($tmpitem) && $tmpitem->type == 'plan')
                                    @php($for_min = $tmpitem->min_pax)
                                    @php($for_max = ($tmpitem->max_pax != null ? $tmpitem->max_pax : 40))
                                @else
                                    @php($for_min = 1)
                                    @php($for_max = 40)
                                @endif
                                <label>Cantidad</label>
                                <select name="quantity" id="quantity" class="form-control input-sm">
                                    @for($x = $for_min; $x <= $for_max; $x++)
                                        <option value="{{ $x }}" {{ isset($query) && $query->quantity == $x ? 'selected=selected' : '' }}>{{ $x }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>¿Regalo?</label>
                                {!! Form::select('is_gift', $yesno_lov, 
                                    old('is_gift', isset($query) ? $query->is_gift: 'N'), 
                                    array('class' => 'form-control input-sm', 'id' => 'is_gift')) !!}
                            </div>
                            <div class="col-md-2">
                                <label>Precio</label>
                                <input type="text" class="form-control tmpprice input-sm" id="price" name="price" 
                                    value="{{ isset($query) ? $query->price : '' }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@stop

@section ('assets_end')
    <script src="{{ asset('assets/plugins/select2/dist/js/select2.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/dist/css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/dist/css/select2-bootstrap.css') }}">

    <script type="text/javascript" src="{{ asset('assets/plugins/daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css') }}" />

    <script src="{{ asset('assets/plugins/datetimepicker/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/datetimepicker/locales/bootstrap-datetimepicker.es.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/plugins/datetimepicker/datetimepicker.min.css') }}">
    <script src="{{ asset('assets/plugins/datetimepicker/datetimepicker.min.js') }}"></script>

    <script type="text/javascript">
        $(function(){
            $(".items_id").select2({theme: "bootstrap", selectOnClose: true});
            $('.date_event').datetimepicker({
                language: 'es',
                pick12HourFormat: false,
                format: 'DD/MM/YYYY',
                useCurrent: true,
                pickTime: false,
                orientation: "left"
            }).on('dp.change', function (e) { 
                $.post(ARNY.baseUrl + '/orders/get_time', 
                    {date: $(this).val(), items_id: $(this).attr('items_id')},
                    function(data){
                        $('#div_time').html(data);
                    }
                );
            });
            $( ".items_id" ).change(function(event){
                var price = $('option:selected', this).attr('price');
                set_price(price, $('#quantity').val());
            });
            $( "#quantity" ).change(function(event){
                var price = $('#tmpprice').val();
                set_price(price, $(this).val());
            });

            function set_price(price, quantity){
                $('#price').val(price * quantity);
                $('#tmpprice').val(price);
            }
            $(document).on('focus', '.select2', function() {
                $(this).siblings('select').select2('open');
            });
            @if(old('items_id', null) != null)
                $('.items_id').trigger("change");
            @endif
        });
    </script>
    <style type="text/css">
        .td_items .select2-container--bootstrap .select2-selection--single{
            height: 30px !important;
        }
        .btn-u-sm, a.btn-u-sm {
            padding: 5px 12px;
        }
    </style>
@stop
