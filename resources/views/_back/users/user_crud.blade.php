@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section ('body')

<!--=== Breadcrumbs ===-->
<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left">{{ $query->name }}</h1>
    </div>
</div>
<div class="container content">
    <form class="margin-bottom-20" id="form_item" role="form" method="post" enctype="multipart/form-data" 
        action="{{ isset($query) ? route('back.users.update', $query->id) : route('back.users.store') }}">
        @if(isset($query))
            @method('PUT')
        @endif
        @csrf
        <div class="row">
            <div class="col-md-8">
                @include('_layouts.messages', array('errors' => $errors))
                <div class="panel panel-default">
                    <div class="panel-heading">Información</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" class="form-control" name="{{ isset($query) ? 'name' : 'users_id' }}" 
                                        placeholder="Nombre" id="name" 
                                        value="{{ old('name', isset($query) ? $query->name : '') }}"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Provincia</label>
                                    {!! Form::select('locations_id', array('' => '') + $locations, 
                                        old('locations_id', isset($query) ? $query->locations_id : '8'), 
                                        array('class' => 'form-control locations_id', 'id' => 'locations_id')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Población</label>
                                    {!! Form::select('districts_id', array('' => '') + (isset($districts) ? $districts : null), 
                                        old('districts_id', isset($query) ? $query->districts_id : ''), 
                                        array('class' => 'form-control districts_id', 'id' => 'districts_id' )) !!}
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label>Dirección</label>
                                    <input type="text" class="form-control" name="address" id="address" placeholder="Dirección"
                                        value="{{ old('address', isset($query) ? $query->address : '') }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>C. postal</label>
                                    <input type="text" class="form-control" name="postal_code" id="postal_code" placeholder="C.P."
                                        value="{{ old('postal_code', isset($query) ? $query->postal_code : '') }}"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Teléfono</label>
                                    <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone"
                                        value="{{ old('phone', isset($query) ? $query->phone : '') }}"/>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" name="email" id="email" placeholder="Email"
                                        value="{{ old('email', isset($query) ? $query->email : '') }}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Acciones</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn-u btn-block margin-bottom-25">
                                    Guardar
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nueva contraseña</label>
                                    <input type="password" name="password" class="form-control"/> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@stop

@section ('assets_end')
    <script src="{{ asset('assets/plugins/select2/dist/js/select2.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/dist/css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/dist/css/select2-bootstrap.css') }}">

    <script type="text/javascript">
        $(function(){
            $(".locations_id").select2({theme: "bootstrap", selectOnClose: true});
            $(".districts_id").select2({tags: true, theme: "bootstrap", selectOnClose: true});
            $(document).on('focus', '.select2', function() {
                $(this).siblings('select').select2('open');
            });

            $( ".locations_id" ).change(function(event){
                $.post(ARNY.baseUrl + '/back/locations/get_districts', 
                    {locations_id: $(this).val()},
                    function(data){
                        var options = $('.districts_id');
                        options.empty();
                        $.each(JSON.parse(data), function(i, it) {
                            options.append('<option value="' + it.id + '">'+ it.text +'<option/>');
                        });

                    }
                );
            });
        });
    </script>
@stop
