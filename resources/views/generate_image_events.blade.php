@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section ('body')

<div class="container content">
    <div class="row">
        <div class="col-md-12">
            <a href="#" class="btn_future_events" style="margin-right: 30px">Future events</a>
            <a download="events.png" href="{{ url('events.png') }}">Download image</a>
        </div>
    </div>
</div>
<div style="width: 1080px; background:linear-gradient(rgba(255,255,255,0.3), rgba(255,255,255,0.3)), url({{ url('/') . '/assets/img/fondo_eventos.png' }}); 
        min-height: 1920px" id="div_future_events">
    <div class="color-blanco" style="padding: 60px !important;">
        <div class="container-fluid text-center" style="padding-top: 40px; padding-bottom: 40px; background-color: #a31158; opacity: 0.75;">
            <p class="color-blanco" style="font-size: 24px; font-weight: 200; margin-bottom: 0; text-transform: uppercase;">
                <strong>Si vols sortir aquí contacta amb @tapdevi</strong>
            </p>
            <h1 class="color-blanco" style="font-size: 40px; line-height: 50px; text-transform: uppercase;">WINE EVENTS</h1>
        </div>
    </div>


    <div class="container" style="width: 960px">
        @foreach($future_events as $future)
            @php($stock = $future->next_stock()->first())
            <div class="row bg-opacity font20 color-blanco margin-bottom-15">
                <div class="col-md-2 font34">
                    <div class="row">
                        <div class="col-md-12 font34">
                            {{ $helpers->fec_mysql_to_dm($stock->date) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 font18">
                            @if($stock->times != null)
                                {{ str_replace("#", "h, ", $stock->times) . "h" }}
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-8 font30">
                    {{ $future->name }}<br/>
                    {{ $future->district->name }}
                </div>
                @if($future->trueprice == 0 || $future->trueprice == 999)
                    <div class="col-md-2 font30 margin-top-15">
                        {{ $future->eventprice }}
                    </div>
                @else
                    <div class="col-md-2 font40">
                        {{ $future->eventprice . "€" }}
                    </div>
                @endif
            </div>
        @endforeach
    </div>
</div>

@stop

@section ('assets_end')
    <link rel="stylesheet" href="{{ asset('assets/css/pages/timeline2.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/pages/inicio.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pages/page_search.css') }}">

    <script type="text/javascript" src="{{ asset('js/html2canvas.min.js') }}"></script>

    <script>
        const enviarCapturaAServidor = canvas => {
            let imagenComoBase64 = canvas.toDataURL();
            imagenComoBase64 = encodeURIComponent(imagenComoBase64);

            $.post(ARNY.baseUrl + '/generate_image_events?generate=true', 
                {captura: JSON.stringify(imagenComoBase64)},
                function(data){
                    window.open(data);
                }
            );
        };

        $('.btn_future_events').live("click", (event) => {
            html2canvas($('#div_future_events')[0]) // Llamar a html2canvas y pasarle el elemento
                .then(enviarCapturaAServidor); // Cuando se resuelva, enviarla al servidor
            event.preventDefault();
        });
    </script>
@stop