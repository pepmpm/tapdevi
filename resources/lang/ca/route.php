<?php

$idioma = array();

$idioma['enotourism-mallorca-create'] = trans('inicio.enotourism-mallorca-slug', [], 'ca').'/nou-event';
$idioma['enotourism-mallorca-store'] = trans('inicio.enotourism-mallorca-slug', [], 'ca').'/nou-event/desar';
$idioma['enotourism-mallorca-show_event'] = trans('inicio.enotourism-mallorca-slug', [], 'ca').'/events/{item}';
$idioma['enotourism-mallorca-index'] = trans('inicio.enotourism-mallorca-slug', [], 'ca').'/events/info/{year?}/{month?}/{day?}';
$idioma['enotourism-mallorca-map'] = trans('inicio.enotourism-mallorca-slug', [], 'es').'/eventos/mapa';

$idioma['enotourism-mallorca-cat'] = trans('inicio.enotourism-mallorca-slug', [], 'ca').'/{cat?}';
$idioma['enotourism-mallorca-dis'] = trans('inicio.enotourism-mallorca-slug', [], 'ca').'/a-{district?}';
$idioma['enotourism-mallorca-user'] = trans('inicio.enotourism-mallorca-slug', [], 'ca').'/a-{district?}/{user}';
$idioma['enotourism-mallorca-item'] = trans('inicio.enotourism-mallorca-slug', [], 'ca').'/a-{district?}/{user}/{item}';
$idioma['enotourism-mallorca-gift'] = trans('inicio.enotourism-mallorca-slug', [], 'es').'/regala-tasts-de-vi-a-mallorca/{item}';

$idioma['enotourism-mallorca-day'] = trans('inicio.enotourism-mallorca-slug', [], 'ca').'/per-{day}';
$idioma['enotourism-mallorca-day-weekend'] = trans('inicio.enotourism-mallorca-slug', [], 'ca').'/per-es-cap-de-setmana';

$idioma['route_index'] = 'rutes-del-vi-a-mallorca';
$idioma['route_show'] = 'rutes-del-vi-a-mallorca/{route}';

// temporal hasta que Google deje de indexar los vinos
$idioma['wines'] = trans('inicio.vinos_de_mallorca-slug', [], 'ca') . '/{url?}/{url2?}/{url3?}/{url4?}';

$idioma['lots_index'] = 'lots-de-vins-i-productes-mallorquins';
$idioma['lots_show'] = 'lots-de-vins-i-productes-mallorquins/{cat}/{item}';

$idioma['wines_show'] = trans('inicio.vinos_de_mallorca-slug', [], 'ca') . '/{cat}/{user}/{item}';
$idioma['search'] = 'cercar';
return $idioma;