<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test_mail', function () {
    //$order = App\Orders::find(51);
    //return new App\Mail\Voucher($order);
	//Mail::send(new App\Mail\Voucher($order));
    //return null;

    $params = array();
    $params['order'] = App\Orders::find(51);
	//return view('emails.pdf.pdf_voucher', $params);
    $pdf = \PDF::loadView('emails.pdf.pdf_voucher', $params);
    return $pdf->download('test.pdf');

});

Route::get('cron/reorder', array('as' => 'cron.reorder', 'uses' => 'CronController@reorder'));
Route::get('cron/send_emails', array('as' => 'cron.send_emails', 'uses' => 'CronController@send_emails'));
Route::any('generate_image_events', array('as' => 'generate_image_events', 'uses' => 'IndexController@generate_image_events'));
Route::get('pep_action', array('as' => 'pep.action', 'uses' => 'IndexController@pep_action'));


// temporal hasta que Google deje de indexar los vinos
/*Route::get(LaravelLocalization::transRoute('route.wines'), 
	array('as' => 'wines', 'uses' => 'IndexController@redirect_wines'));*/

Route::group(array('middleware' => 'minificar', 'prefix' => LaravelLocalization::setLocale()), function(){
//Route::group(array('prefix' => LaravelLocalization::setLocale()), function(){
	Route::get('/', array('as' => '/', 'uses' => 'IndexController@index'));
	Route::get('politicas_cookies', array('as' => 'politicas_cookies', 'uses' => 'IndexController@politicas_cookies'));
	Route::get('politicas_privacidad', array('as' => 'politicas_privacidad', 'uses' => 'IndexController@politicas_privacidad'));

	Route::get(LaravelLocalization::transRoute('route.route_index'), 
		array('as' => 'routes.index', 'uses' => 'Routes\RoutesWebController@index'));
	Route::get(LaravelLocalization::transRoute('route.route_show'), 
		array('as' => 'routes.show', 'uses' => 'Routes\RoutesWebController@show'));

	Route::get(LaravelLocalization::transRoute('route.search'), 
		array('as' => 'search', 'uses' => 'Plans\PlansWebController@search'));

	Route::get(LaravelLocalization::transRoute('route.enotourism-mallorca-index'), 
		array('as' => 'enotourism-mallorca.events.index', 'uses' => 'Events\EventsWebController@index'));
	Route::get(LaravelLocalization::transRoute('route.enotourism-mallorca-map'), 
		array('as' => 'enotourism-mallorca.events.map', 'uses' => 'Events\EventsWebController@map'));
	Route::get(LaravelLocalization::transRoute('route.enotourism-mallorca-create'), 
		array('as' => 'enotourism-mallorca.events.create', 'uses' => 'Events\EventsWebController@create'));
	Route::post(LaravelLocalization::transRoute('route.enotourism-mallorca-store'), 
		array('as' => 'enotourism-mallorca.events.store', 'uses' => 'Events\EventsWebController@store'));
	Route::get(LaravelLocalization::transRoute('route.enotourism-mallorca-show_event'), 
		array('as' => 'enotourism-mallorca.events.show', 'uses' => 'Events\EventsWebController@show'));


	Route::get(LaravelLocalization::transRoute('route.enotourism-mallorca-gift'), 
		array('as' => 'enotourism-mallorca.gift', 'uses' => 'Plans\PlansWebController@show'));
	Route::get(LaravelLocalization::transRoute('route.enotourism-mallorca-dis'), 
		array('as' => 'enotourism-mallorca.dis', 'uses' => 'Plans\PlansWebController@index_dis'));
	Route::get(LaravelLocalization::transRoute('route.enotourism-mallorca-user'), 
		array('as' => 'enotourism-mallorca.user', 'uses' => 'Users\UsersWebController@show'));
	Route::get(LaravelLocalization::transRoute('route.enotourism-mallorca-item'), 
		array('as' => 'enotourism-mallorca.item', 'uses' => 'Plans\PlansWebController@show'));
	Route::get(LaravelLocalization::transRoute('route.enotourism-mallorca-cat'), 
		array('as' => 'enotourism-mallorca.cat', 'uses' => 'Plans\PlansWebController@index_cat'));

	Route::any('orders/confirm_visit/{order_encrypted}/{set?}', array('as' => 'orders.confirm_visit', 
		'uses' => 'Orders\OrdersWebController@confirm_visit'));
	Route::any('orders/confirm_payment/{crypt}/{set?}', array('as' => 'orders.confirm_payment', 
		'uses' => 'Orders\OrdersWebController@confirm_payment'));
	Route::post('plans/stocks/get_time', array('as' => 'plans.stocks.get_time', 'uses' => 'PlansStocks\PlansStocksWebController@get_time'));
	Route::post('orders/get_time', array('as' => 'orders.get_time', 
		'uses' => 'Orders\OrdersPanelController@get_time'));
	Route::resource('events/photos', 'EventsPhotos\EventsPhotosPanelController', 
		['only' => ['store', 'update', 'destroy'], 'as' => 'events']);

	Route::any('plans/comment', array('as' => 'plans.comment', 'uses' => 'Plans\PlansWebController@comment'));

	Route::any('cart', array('as' => 'orders.index', 'uses' => 'Orders\OrdersWebController@index'));
	Route::post('cart/update', array('as' => 'orders.update', 'uses' => 'Orders\OrdersWebController@update'));
	Route::get('add_to_cart', array('as' => 'orders.add_to_cart', 'uses' => 'Orders\OrdersWebController@add_to_cart'));
	Route::get('orders/{code}/{email}', array('as' => 'orders.show', 'uses' => 'Orders\OrdersWebController@show'));
	Route::post('cart/save', array('as' => 'orders.save', 'uses' => 'Orders\OrdersWebController@save'));
	Route::get('cart/delete/{id}', array('as' => 'orders.delete', 'uses' => 'Orders\OrdersWebController@delete'));
	Route::get('cart/coupon/{coupons_id?}', array('as' => 'orders.coupon', 'uses' => 'Orders\OrdersWebController@coupon'));
	Route::get('cart/callback/{id}', array('as' => 'orders.callback', 'uses' => 'Orders\OrdersWebController@callback'));

	Route::get('temp_image_png', array('as' => 'index.temp_image_png', 'uses' => 'IndexController@temp_image_png'));

});

Auth::routes();
Imgfly::routes();

Route::get('back/orders/download_voucher/{orders_id}/{email}', 
	array('as' => 'back.orders.download_voucher', 'uses' => 'Orders\OrdersPanelController@download_voucher'));


Route::group(array('middleware' => 'auth'), function(){

	Route::get('back', 'HomeController@index')->name('back');

	Route::group(array('prefix' => 'back'), function(){

		Route::group(array('middleware' => 'admin'), function(){

			Route::resource('labels/langs', 'LabelsLangs\LabelsLangsController', 
				['only' => ['update'], 'as' => 'back.labels']);

			Route::resource('labels', 'Labels\LabelsPanelController', 
				['only' => ['index', 'create', 'store', 'show', 'update', 'destroy'], 'as' => 'back']);

		});

		Route::group(array('middleware' => 'notuser'), function(){

			Route::resource('plans/stocks', 'PlansStocks\PlansStocksPanelController', 
				['only' => ['store', 'update', 'destroy'], 'as' => 'back.plans']);
			Route::get('plans/stocks/{items_id}', 
				array('as' => 'back.plans.stocks.show', 'uses' => 'PlansStocks\PlansStocksPanelController@show'));
				
			Route::resource('plans/photos', 'PlansPhotos\PlansPhotosPanelController', 
				['only' => ['store', 'update', 'destroy'], 'as' => 'back.plans']);

			Route::resource('plans', 'Plans\PlansPanelController', 
				['only' => ['index', 'create', 'store', 'show', 'update', 'destroy'], 'as' => 'back']);
            Route::get('plans/action/promoted/{item}', 
                array('as' => 'back.plans.promoted.toggle', 'uses' => 'Plans\PlansPanelController@toggle_promoted'));

			Route::resource('events/stocks', 'EventsStocks\EventsStocksPanelController', 
				['only' => ['store', 'update', 'destroy'], 'as' => 'back.events']);
			Route::get('events/stocks/{items_id}', 
				array('as' => 'back.events.stocks.show', 'uses' => 'EventsStocks\EventsStocksPanelController@show'));

			Route::resource('events/photos', 'EventsPhotos\EventsPhotosPanelController', 
				['only' => ['store', 'update', 'destroy'], 'as' => 'back.events']);
			Route::get('events/photos/destroy/{photo}', 
				array('as' => 'back.events.photos.destroy', 'uses' => 'EventsPhotos\EventsPhotosPanelController@destroy'));

			Route::get('events/instagram/get/{item}', array('as' => 'back.events.instagram.get', 'uses' => 'Events\EventsPanelController@instagram_get'));
			Route::get('events/translate/{item}', array('as' => 'back.events.translate', 'uses' => 'Events\EventsPanelController@translate'));

			Route::resource('events', 'Events\EventsPanelController', 
				['only' => ['index', 'create', 'store', 'show', 'update', 'destroy'], 'as' => 'back']);


			Route::resource('packs/photos', 'PacksPhotos\PacksPhotosPanelController', 
				['only' => ['store', 'update', 'destroy'], 'as' => 'back.packs']);
			Route::get('packs/photos/destroy/{photo}', 
				array('as' => 'back.packs.photos.destroy', 'uses' => 'PacksPhotos\PacksPhotosPanelController@destroy'));

			Route::any('packs/additems/{items_id}', 
				array('as' => 'back.plans.additems', 'uses' => 'Packs\PacksPanelController@additems'));

			Route::get('packs/translate/{item}', array('as' => 'back.packs.translate', 'uses' => 'Packs\PacksPanelController@translate'));
			Route::resource('packs', 'Packs\PacksPanelController', 
				['only' => ['index', 'create', 'store', 'show', 'update', 'destroy'], 'as' => 'back']);


			Route::get('blogs/translate/{blog}', array('as' => 'back.blogs.translate', 'uses' => 'Routes\RoutesPanelController@translate'));
			Route::resource('blogs/photos', 'BlogsPhotos\BlogsPhotosPanelController', 
				['only' => ['store', 'update', 'destroy'], 'as' => 'back.blogs']);
			Route::get('blogs/photos/destroy/{photo}', 
				array('as' => 'back.blogs.photos.destroy', 'uses' => 'BlogsPhotos\BlogsPhotosPanelController@destroy'));
			Route::resource('routes', 'Routes\RoutesPanelController', 
				['only' => ['index', 'create', 'store', 'show', 'update', 'destroy'], 'as' => 'back']);

			Route::get('users/action/promoted/{item}', 
				array('as' => 'back.users.promoted.toggle', 'uses' => 'Users\UsersPanelController@toggle_promoted'));

		});


		Route::resource('coupons', 'Coupons\CouponsPanelController', 
			['only' => ['index', 'create', 'store', 'show', 'update', 'destroy'], 'as' => 'back']);

		Route::resource('users/photos', 'UsersPhotos\UsersPhotosPanelController', 
			['only' => ['store', 'update', 'destroy'], 'as' => 'back.users']);

		Route::resource('users', 'Users\UsersPanelController', 
			['only' => ['index', 'create', 'store', 'show', 'update', 'destroy'], 'as' => 'back']);


		Route::resource('orders', 'Orders\OrdersPanelController', 
			['only' => ['index', 'create', 'store', 'show', 'update', 'destroy'], 'as' => 'back']);
		Route::get('back/orders/mark_booking/{order}/{action}', 
			array('as' => 'back.orders.mark_booking', 'uses' => 'Orders\OrdersPanelController@mark_booking'));
		/*Route::post('locations/get_districts', array('as' => 'back.locations.get_districts', 'uses' => 'Locations\LocationsPanelController@get_districts'));*/

	});
	
});


Route::get('sitemap', array('as' => 'sitemap', 'uses' => 'SitemapController@index'));
Route::get('/sitemap-plans.xml', array('as' => 'sitemap-plans', 'uses' => 'SitemapController@plans'));
Route::get('/sitemap-users.xml', array('as' => 'sitemap-users', 'uses' => 'SitemapController@users'));
Route::get('/sitemap-extras.xml', array('as' => 'sitemap-extras', 'uses' => 'SitemapController@extras'));