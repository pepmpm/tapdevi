<?php

use Illuminate\Database\Seeder;
use \App\Districts;

class DistrictsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        \DB::table('districts')->truncate();

        Districts::create(['base' => 'S', 'island' => 'menorca', 'locations_id' => 8, 'name' => 'Alaior', 'slug' => 'alaior', 'latitude' => '39.93064', 'longitude' => '4.139987']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Alaró', 'slug' => 'alaro', 'latitude' => '39.70685', 'longitude' => '2.790548']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Alcúdia', 'slug' => 'alcudia', 'latitude' => '39.85231', 'longitude' => '3.122027']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Algaida', 'slug' => 'algaida', 'latitude' => '39.55969', 'longitude' => '2.892573']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Andratx', 'slug' => 'andratx', 'latitude' => '39.5708', 'longitude' => '2.421365']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Ariany', 'slug' => 'ariany', 'latitude' => '39.64943', 'longitude' => '3.11086']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Artà', 'slug' => 'arta', 'latitude' => '39.69215', 'longitude' => '3.349073']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Banyalbufar', 'slug' => 'banyalbufar', 'latitude' => '39.68718', 'longitude' => '2.513457']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Binissalem', 'slug' => 'binissalem', 'latitude' => '39.68446', 'longitude' => '2.847511']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Búger', 'slug' => 'buger', 'latitude' => '39.75927', 'longitude' => '2.985579']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Bunyola', 'slug' => 'bunyola', 'latitude' => '39.69663', 'longitude' => '2.699386']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Calvià', 'slug' => 'calvia', 'latitude' => '39.56524', 'longitude' => '2.504216']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Campanet', 'slug' => 'campanet', 'latitude' => '39.77475', 'longitude' => '2.965845']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Campos', 'slug' => 'campos', 'latitude' => '39.42727', 'longitude' => '3.021843']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Capdepera', 'slug' => 'capdepera', 'latitude' => '39.70133', 'longitude' => '3.435561']);
        Districts::create(['base' => 'S', 'island' => 'menorca', 'locations_id' => 8, 'name' => 'Es Castell', 'slug' => 'es-castell', 'latitude' => '39.8776', 'longitude' => '4.289603']);
        Districts::create(['base' => 'S', 'island' => 'menorca', 'locations_id' => 8, 'name' => 'Ciutadella', 'slug' => 'ciutadella', 'latitude' => '40.00232', 'longitude' => '3.839262']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Consell', 'slug' => 'consell', 'latitude' => '39.67012', 'longitude' => '2.816188']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Costitx', 'slug' => 'costitx', 'latitude' => '39.65622', 'longitude' => '2.949179']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Deià', 'slug' => 'deia', 'latitude' => '39.74844', 'longitude' => '2.649177']);
        Districts::create(['base' => 'S', 'island' => 'ibiza', 'locations_id' => 8, 'name' => 'Eivissa', 'slug' => 'eivissa', 'latitude' => '38.90886', 'longitude' => '1.432378']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Escorca', 'slug' => 'escorca', 'latitude' => '39.8211', 'longitude' => '2.869478']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Esporles', 'slug' => 'esporles', 'latitude' => '39.66946', 'longitude' => '2.57915']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Estellencs', 'slug' => 'estellencs', 'latitude' => '39.65333', 'longitude' => '2.481678']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Felanitx', 'slug' => 'felanitx', 'latitude' => '39.46876', 'longitude' => '3.148028']);
        Districts::create(['base' => 'S', 'island' => 'menorca', 'locations_id' => 8, 'name' => 'Ferreries', 'slug' => 'ferreries', 'latitude' => '39.98328', 'longitude' => '4.010968']);
        Districts::create(['base' => 'S', 'island' => 'formentera', 'locations_id' => 8, 'name' => 'Formentera', 'slug' => 'formentera', 'latitude' => '38.6964', 'longitude' => '1.453135']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Fornalutx', 'slug' => 'fornalutx', 'latitude' => '39.7826', 'longitude' => '2.741559']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Inca', 'slug' => 'inca', 'latitude' => '39.71875', 'longitude' => '2.915544']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Lloret de Vistalegre', 'slug' => 'lloret-de-vistalegre', 'latitude' => '39.61689', 'longitude' => '2.974723']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Lloseta', 'slug' => 'lloseta', 'latitude' => '39.71799', 'longitude' => '2.864577']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Llubí', 'slug' => 'llubi', 'latitude' => '39.70045', 'longitude' => '3.004657']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Llucmajor', 'slug' => 'llucmajor', 'latitude' => '39.48591', 'longitude' => '2.89485']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Manacor', 'slug' => 'manacor', 'latitude' => '39.57434', 'longitude' => '3.20142']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Mancor de la Vall', 'slug' => 'mancor-de-la-vall', 'latitude' => '39.75063', 'longitude' => '2.873395']);
        Districts::create(['base' => 'S', 'island' => 'menorca', 'locations_id' => 8, 'name' => 'Maó', 'slug' => 'mao', 'latitude' => '39.88961', 'longitude' => '4.264211']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'María de la Salut', 'slug' => 'maria-de-la-salut', 'latitude' => '39.67049', 'longitude' => '3.076207']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Marratxí', 'slug' => 'marratxi', 'latitude' => '39.64213', 'longitude' => '2.752193']);
        Districts::create(['base' => 'S', 'island' => 'menorca', 'locations_id' => 8, 'name' => 'Es Mercadal', 'slug' => 'es-mercadal', 'latitude' => '39.99089', 'longitude' => '4.093808']);
        Districts::create(['base' => 'S', 'island' => 'menorca', 'locations_id' => 8, 'name' => 'Es Migjorn Gran', 'slug' => 'es-migjorn-gran', 'latitude' => '39.94633', 'longitude' => '4.049105']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Montuïri', 'slug' => 'montuiri', 'latitude' => '39.57029', 'longitude' => '2.984617']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Muro', 'slug' => 'muro', 'latitude' => '39.73408', 'longitude' => '3.053606']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Palma', 'slug' => 'palma', 'latitude' => '39.56951', 'longitude' => '2.649966']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Petra', 'slug' => 'petra', 'latitude' => '39.6091', 'longitude' => '3.112162']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Sa Pobla', 'slug' => 'sa-pobla', 'latitude' => '39.77052', 'longitude' => '3.016888']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Pollença', 'slug' => 'pollensa', 'latitude' => '39.87775', 'longitude' => '3.017193']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Porreres', 'slug' => 'porreres', 'latitude' => '39.51733', 'longitude' => '3.023831']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Puigpunyent', 'slug' => 'puigpunyent', 'latitude' => '39.62405', 'longitude' => '2.527614']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Ses Salines', 'slug' => 'ses-salines', 'latitude' => '39.339', 'longitude' => '3.051014']);
        Districts::create(['base' => 'S', 'island' => 'ibiza', 'locations_id' => 8, 'name' => 'Sant Antoni de Portmany', 'slug' => 'sant-antoni-de-portmany', 'latitude' => '38.98012', 'longitude' => '1.306587']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Sant Joan', 'slug' => 'sant-joan', 'latitude' => '39.59594', 'longitude' => '3.039554']);
		Districts::create(['base' => 'S', 'island' => 'ibiza', 'locations_id' => 8, 'name' => 'Sant Joan de Labritja', 'slug' => 'sant-joan-de-labritja', 'latitude' => '39.07785', 'longitude' => '1.51227']);
        Districts::create(['base' => 'S', 'island' => 'ibiza', 'locations_id' => 8, 'name' => 'Sant Josep de sa Talaia', 'slug' => 'sant-josep-de-sa-talaia', 'latitude' => '38.92173', 'longitude' => '1.293443']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Sant Llorenç des Cardassar', 'slug' => 'sant-llorens-des-cardassar', 'latitude' => '39.51733', 'longitude' => '3.023831']);
        Districts::create(['base' => 'S', 'island' => 'menorca', 'locations_id' => 8, 'name' => 'Sant Lluís', 'slug' => 'sant-lluis', 'latitude' => '39.85266', 'longitude' => '4.259394']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Santa Eugènia', 'slug' => 'santa-eugenia', 'latitude' => '39.62394', 'longitude' => '2.839166']);
        Districts::create(['base' => 'S', 'island' => 'ibiza', 'locations_id' => 8, 'name' => 'Santa Eulàlia del Ríu', 'slug' => 'santa-eulalia-del-riu', 'latitude' => '38.98257', 'longitude' => '1.530198']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Santa Margalida', 'slug' => 'santa-margalida', 'latitude' => '39.70306', 'longitude' => '3.103551']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Santa María del Camí', 'slug' => 'santa-maria-del-cami', 'latitude' => '39.6468', 'longitude' => '2.776213']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Santanyí', 'slug' => 'santanyi', 'latitude' => '39.35489', 'longitude' => '3.127745']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Selva', 'slug' => 'selva', 'latitude' => '39.75464', 'longitude' => '2.90122']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Sencelles', 'slug' => 'sencelles', 'latitude' => '39.64624', 'longitude' => '2.896189']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Sineu', 'slug' => 'sineu', 'latitude' => '39.64284', 'longitude' => '3.010996']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Sóller', 'slug' => 'soller', 'latitude' => '39.76583', 'longitude' => '2.714238']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Son Servera', 'slug' => 'son-servera', 'latitude' => '39.62423', 'longitude' => '3.356735']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Valldemossa', 'slug' => 'valldemossa', 'latitude' => '39.71176', 'longitude' => '2.622538']);
        Districts::create(['base' => 'S', 'island' => 'mallorca', 'locations_id' => 8, 'name' => 'Vilafranca de Bonany', 'slug' => 'vilafranca-de-bonany', 'latitude' => '39.56957', 'longitude' => '3.084015']);
    }
}
