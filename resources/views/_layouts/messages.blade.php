@if(Session::has('message_success' . (isset($index) ? '_' . $index : '') ))
    <div class="alert alert-success in">
        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
        @if(is_array(Session::get('message_success' . (isset($index) ? '_' . $index : '') )))
            {!! implode("<br/>", Session::get('message_success' . (isset($index) ? '_' . $index : '') )) !!}
        @else
            {!! Session::get('message_success' . (isset($index) ? '_' . $index : '') ) !!}
        @endif
    </div>
@endif

@if(Session::has('message_info' . (isset($index) ? '_' . $index : '') ))
    <div class="alert alert-info in">
        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
        @if(is_array(Session::get('message_info' . (isset($index) ? '_' . $index : '') )))
            {!! implode("<br/>", Session::get('message_info' . (isset($index) ? '_' . $index : '') )) !!}
        @else
            {!! Session::get('message_info' . (isset($index) ? '_' . $index : '') ) !!}
        @endif
    </div>
@endif

@if(Session::has('message_warning' . (isset($index) ? '_' . $index : '') ))
    <div class="alert alert-warning in">
        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
        @if(is_array(Session::get('message_warning' . (isset($index) ? '_' . $index : '') )))
            {!! implode("<br/>", Session::get('message_warning' . (isset($index) ? '_' . $index : '') )) !!}
        @else
            {!! Session::get('message_warning' . (isset($index) ? '_' . $index : '') ) !!}
        @endif
    </div>
@endif

@if(Session::has('message_error' . (isset($index) ? '_' . $index : '') ))
    <div class="alert alert-danger in">
        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
        @if(is_array(Session::get('message_error' . (isset($index) ? '_' . $index : '') )))
            {!! implode("<br/>", Session::get('message_error' . (isset($index) ? '_' . $index : '') )) !!}
        @else
            {!! Session::get('message_error' . (isset($index) ? '_' . $index : '') ) !!}
        @endif
    </div>
@endif

@if(isset($errors) && count($errors) > 0)
    <div class="alert alert-danger in">
        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
        @if(is_array($errors))
            @foreach ($errors as $error)
                <p style="color: #333">{!! implode("<br/>", $error) !!}</p>
            @endforeach
        @else
            @foreach ($errors->all() as $message)
                <p style="color: #333">{!! $message !!}</p>
            @endforeach
        @endif
    </div>
@endif