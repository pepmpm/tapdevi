<nav class="navbar navbar-default margin-bottom-0">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="collapsed navbar-toggle" data-toggle="collapse" 
                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="{{ route('back') }}" class="navbar-brand">{{ \Auth::user()->name }}</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            @if(Auth::check() && Auth::user()->isAdmin())
             <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" 
                        aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-fire-extinguisher"></i> Admin
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('back.labels.index') }}">Categorias</a></li>
                        <li><a href="{{ route('back.users.index') }}">Usuarios/Empresas</a></li>
                        <li><a href="{{ route('back.coupons.index') }}">Cupones</a></li>
                    </ul>
                </li>
            </ul>
            @endif
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" 
                        aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user"></i> {{ trans('panel.perfil') }} 
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('back.users.show', [\Auth::id()]) }}">{{trans('panel.modificar_info')}}</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                @if(Auth::check() && Auth::user()->role != 'user')
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
                        aria-expanded="false">
                        <i class="fa fa-shopping-basket"></i> {{ trans('panel.items') }} 
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('back.plans.index') }}">{{trans('panel.listado_plans')}}</a></li>
                        <li><a href="{{ route('back.events.index') }}">{{trans('panel.listado_events')}}</a></li>
                        <li><a href="{{ route('back.routes.index') }}">{{trans('panel.listado_routes')}}</a></li>
                        <li><a href="{{ route('back.packs.index') }}">{{trans('panel.listado_packs')}}</a></li>
                    </ul>
                </li>
                @endif
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
                        aria-expanded="false">
                        <i class="fa fa-cc-visa"></i> {{ trans('panel.pedidos') }} 
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('back.orders.index') }}">{{trans('panel.listado_pedidos')}}</a></li>
                    </ul>
                </li>
                <li class="active">
                    <form id="logout-form" action="{{ route('logout') }}" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-link margin-top-8"><i class="fa fa-stop-circle"></i> Desconectar <span class="sr-only">(current)</span></button>
                    </form>
                </li>
            </ul>
        </div>
    </div>
</nav>