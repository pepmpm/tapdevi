<?php

$idioma = array();

$idioma['por_zona'] = 'Buscar por zona';
$idioma['por_tipo'] = 'Buscar por tipo';
$idioma['por_experiencia'] = 'Buscar por experiencia';
$idioma['por_bodega'] = 'Buscar por lugar';

$idioma['anadir_al_carrito'] = 'Añadir al carrito';
$idioma['categorias'] = 'Categorías';
$idioma['comentarios'] = 'Comentarios';
$idioma['comentar'] = 'Comentar';
$idioma['cantidad'] = 'Cantidad';
$idioma['seleccionar_fecha'] = 'Seleccionar fecha';
$idioma['mensaje_confirmacion_email'] = '+Info en Whatsapp o Email';
$idioma['mensaje_whatsapp_plan'] = 'Estoy interesado en: :plan';
$idioma['idiomas_disponibles'] = 'Idiomas';
$idioma['horario'] = 'Horario';
$idioma['llamar'] = 'Llamar';
$idioma['ajustar_busqueda'] = 'Ajustar búsqueda';
$idioma['remarks_placeholder'] = 'Comentarios, alérgenos, detalles...';
$idioma['gift_remarks_placeholder'] = 'Escribe tu dedicatoria. Aparecerá en el bono.';
$idioma['tu_mensaje'] = 'Tu mensaje';
$idioma['tu_nombre'] = 'Tu nombre';
$idioma['quien_lo_regala'] = '¿Quién lo regala?';
$idioma['nombre_destinatario'] = 'Nombre del destinatario';
$idioma['anyade_mensaje_de_regalo'] = 'Añade un mensaje regalo';
$idioma['reserva_con_antelacion'] = 'Reserva con antelación';
$idioma['cancela_con_antelacion'] = 'Cancela con antelación';
$idioma['duracion'] = 'Duración';
$idioma['confirmacion_direct'] = 'Pago el día de la actividad<br/>(excepto en caso de regalo)';
$idioma['comprar_como_regalo'] = 'Comprar como regalo';
$idioma['comprar'] = 'Comprar';
$idioma['guia'] = 'Guía';
$idioma['dias'] = 'días';
$idioma['accesible_silla_ruedas'] = 'Accesible silla de ruedas';
$idioma['apto_mascotas'] = 'Apto para mascotas';
$idioma['apto_ninos'] = 'Apto para niños';
$idioma['parking_propio'] = 'Parking propio';
$idioma['la_experiencia'] = 'La experiencia';
$idioma['detallado'] = 'Detallado';
$idioma['informacion_del_horario'] = 'Información del horario';
$idioma['por_persona'] = 'por persona';
$idioma['reservar'] = 'Reservar';
$idioma['mensaje_panel_reservar'] = 'Selecciona número de participantes, fecha e idioma';
$idioma['mensaje_note_boton_gift'] = 'O por si no sabes la fecha';
$idioma['mensaje_panel_reservar_email'] = 'Para más info contáctanos por whatsapp o por teléfono';
$idioma['contacta_establecimiento_reservar'] = 'Pendiente reservar fecha';

$idioma['nueva_experiencia_de_enoturismo'] = 'Nueva experiencia de enoturismo en Mallorca';
$idioma['eventos'] = 'Eventos';
$idioma['tus_datos'] = 'Tus datos';
$idioma['tu_nombre'] = 'Tu nombre';
$idioma['tu_email'] = 'Tu email';
$idioma['datos_del_evento'] = 'Datos del evento';
$idioma['nombre_del_evento'] = 'Nombre del evento';
$idioma['precio_por_persona'] = 'Precio por persona';
$idioma['poblacion'] = 'Población';
$idioma['direccion'] = 'Dirección';
$idioma['horarios'] = 'Horarios';
$idioma['codigo_postal'] = 'Código postal';
$idioma['note_precio'] = 'Si no lo sabes no lo rellenes y si es gratuito pon un 0';
$idioma['por_ejemplo'] = 'Por ejemplo: Cata de vinos mallorquines acompañados de sushi';
$idioma['cuando_empieza'] = '¿Cuando empieza y termina?';
$idioma['detalles_del_evento'] = 'Detalles del evento';
$idioma['imagen_del_evento'] = 'Imagen del evento';
$idioma['enlace_foto_instagram'] = 'Enlace a la foto en Instagram';
$idioma['subir'] = 'Subir';
$idioma['enviar_evento'] = 'Enviar evento';
$idioma['tu_propuesta_se_mostrara'] = 'Tu propuesta se mostrará al público después de haber sido revisada';
$idioma['more_info'] = '+Info';

$idioma['gracias_por_enviar_el_evento'] = 'Gracias por enviarnos la información';
$idioma['enseguida_que_lo_revisemos'] = 'Enseguida que revisemos y completemos la información del evento lo publicaremos';
$idioma['selecciona_una_categoria'] = 'Selecciona una categoría';

$idioma['mensaje_comentario'] = 'En ' . config('constants.NAMEWEB') . " nos interesa tu opinión sobre la visita que hiciste. Y de paso servirá a otros aficionados al vino a conocer mejor la experiencia.";

return $idioma;