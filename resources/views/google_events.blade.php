<script type="application/ld+json" charSet="UTF-8">
    [{
    "@context" : "https://schema.org/",
    "@type": [
        "TouristAttraction",
        "LocalBusiness",
        "Event"
    ],
    "name": "{{ $query->name }}",
    @if($query->latitude != null && $query->longitude != null)
    	"hasMap": "https://www.google.com/maps?ie=UTF8&hq&ll={{ $query->latitude }},{{ $query->longitude }}&z=13",
	    "geo": {
	        "@type": "GeoCoordinates",
	        "latitude": "{{ $query->latitude }}",
	        "longitude": "{{ $query->longitude }}"
	    },
    @endif
    "@id": "{{ Request::url() }}",
    "url": "{{ Request::url() }}",
    "image": "{{ url($query->one_photo()) }}",
    "description": "{{ strip_tags($query->description) }}",

    @if($query->next_stock() != null)
        @php($rowstock = $query->next_stock()->first())
        @php($tmptime = null)
        @if($rowstock != null && $rowstock->times != null)
            @php($tmptime = explode("#", $rowstock->times)[0])
            "startDate": "{{ $rowstock->date . ($tmptime != null ? 'T' . $tmptime . '+01:00' : '') }}",
        @endif
    @endif

    @if($query->districts_id !=null)
	    "location": {
	        "@type": "Place",
	        "address": {
	            "@type": "PostalAddress",
	            "addressLocality": "{{ $query->district->name }}",
	            "addressCountry": "ES",
	            "postalCode": "{{ $query->postal_code }}",
	            "streetAddress": "{{ $query->address }}"
	        }
	    },
    @endif

    "priceRange": "{{ $query->trueprice }}"

    }]
</script>