<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Dimsav\Translatable\Translatable;
use App\Traits\Helpers;

class User extends Authenticatable
{
    use Notifiable;
    use Helpers;
    use Translatable;
    public $translatedAttributes = ['title', 'description', 'seo_description', 'seo_keywords'];
    public $translationModel = 'App\UsersLangs';
    public $translationForeignKey = 'users_id';

    protected $with = ['langs', 'photos'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin() {
        return $this->role == 'admin';
    }
    
    public function isModerator() {
        return $this->role == 'admin' || $this->role == 'moderator';
    }

    public function all_items(){
        return $this->hasMany('\App\Items', 'users_id')->orderBy('order', 'asc');
    }

    public function plans(){
        return $this->hasMany('\App\Items', 'users_id')->where('type', '=', 'plan')->where('status', '=', 'active')->orderBy('order', 'asc');
    }

    public function packs(){
        return $this->hasMany('\App\Items', 'users_id')->where('type', '=', 'pack')->where('status', '=', 'active')->orderBy('order', 'asc');
    }

    public function langs(){
        return $this->hasMany('\App\UsersLangs', 'users_id')->orderBy('id', 'asc');
    }

    public function orders(){
        return $this->hasMany('\App\Orders', 'users_id')->orderBy('id', 'asc');
    }

    public function photos(){
        return $this->hasMany('\App\UsersPhotos', 'users_id');
    }

    public function location(){
        return $this->belongsTo('\App\Locations', 'locations_id');
    }

    public function district(){
        return $this->belongsTo('\App\Districts', 'districts_id');
    }

    public function district_billing(){
        return $this->belongsTo('\App\Districts', 'districts_id_billing');
    }

    public function one_photo($default = ''){
        $photos = $this->hasMany('\App\UsersPhotos', 'users_id')->orderBy('order', 'asc')->orderBy('id', 'asc');
        if($photos->count() > 0){
            return $photos->first()->url;
        }
        return $default . '/' . 'default.png';
    }
}
