@extends('_layouts/inicio')

@section ('body')

<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left">Categorías</h1>
    </div>
</div>
<div class="container content profile">
    <div class="row">
        <div class="col-md-12">
            <a href="{{route('back.labels.create')}}" 
               class="btn btn-warning btn-lg" role="button">
                Crear categoría
            </a>
        </div>     
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-8">
            <table class="table" id="tabla">
                <thead>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Padre</th>
                    <th>Categoria</th>
                    <th>¿Keywords?</th>
                    <th>¿Descripción?</th>
                    <th>Acciones</th>
                </thead>
                <tbody>
                    @foreach($all as $label)
                        <tr>
                            <td>{{ $label->id }}</td>
                            <td>{{ $label->name }}</td>
                            <td>{{ $label->labels_id != null ? $label->label->name : "" }}</td>
                            <td>{{ $label->category }}</td>
                            <td>{{ $label->seo_keywords != null ? 'Sí' : 'No' }}</td>
                            <td>{{ $label->seo_description != null ? 'Sí' : 'No' }}</td>
                            <td>
                                <a href="{{ route('back.labels.show', array('id' => $label->id)) }}" 
                                   class="btn btn-primary btn-sm pull-left" role="button">
                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                </a>
                                <form action="{{ route('back.labels.destroy', [$label->id]) }}" method="POST" 
                                    onsubmit="if(!confirm('¿Estás seguro de querer borrar la categoría?')) return false"
                                    class="col-md-4">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-danger btn-sm" type="submit">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            @include('_layouts.messages', array('errors' => $errors))
            <div id="jstree">
                <ul>
                    @foreach($query as $label1)
                        <li id="{{ $label1->id }}">
                            {{ $label1->name }} 
                            @foreach($label1->labels as $label2)
                                <ul>
                                    <li id="{{ $label2->id }}">
                                        {{ $label2->name }}
                                        @foreach($label2->labels as $label3)
                                            <ul>
                                                <li id="{{ $label3->id }}">
                                                    {{ $label3->name }}
                                                </li>
                                            </ul>
                                        @endforeach
                                    </li>
                                </ul>
                            @endforeach
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>     
    </div>
</div>

@stop

@section ('assets_end')
    <link rel="stylesheet" href="{{ asset('assets/plugins/jstree/themes/default/style.css') }}" />
    <script src="{{ asset('assets/plugins/jstree/jstree.js') }}"></script>

    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/datatables.min.css') }}">
    <script src="{{ asset('assets/plugins/datatables/datatables.min.js') }}"></script>
    
    <script type="text/javascript">
        $(document).ready(function() {
            $('#tabla').DataTable({
                "language": {
                    "url": ARNY.baseUrl + "/assets/plugins/datatables/spanish.lang"
                },
                stateSave: true
            });
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $('#jstree').on('changed.jstree', function (e, data) {
                for(var i = 0; i < data.selected.length; i++) {
                    $('#tabla').DataTable().search( data.instance.get_node(data.selected[i]).id ).draw();
                }
                
            }).jstree({
                "core" : {
                    "multiple" : false
                }
            }); 

            $('#jstree').on('ready.jstree', function() {
                $(this).jstree("open_all");          
            });
        });
    </script>
@stop
