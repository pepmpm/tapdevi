<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Helpers;

class Orders extends Model
{
	use Helpers;
    
/*    public function getRouteKeyName(){
        return 'code';
    }*/

    public function user(){
        return $this->belongsTo('\App\User', 'users_id');
    }

    public function coupon(){
        return $this->belongsTo('\App\Coupons', 'coupons_id');
    }

    public function item(){
        return $this->belongsTo('\App\Items', 'items_id');
    }
}
