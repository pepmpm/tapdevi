<?php

namespace App\Http\Controllers\Users;

use App\User;
use Illuminate\Http\Request;

class UsersPanelController extends UsersController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Auth::check() && \Auth::user()->role == 'user'){
            return redirect()->route('back');
        }

        $this->data['query'] = parent::_index();
        return view('_back.users.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(\Auth::check() && \Auth::user()->role == 'user'){
            return redirect()->route('back');
        }

        //$this->data['districts'] = \App\Districts::all()->pluck('custom', 'id')->all();
        $this->data['yesno_lov'] = \App\Items::getPossibleEnumValues('is_giftable');
        $this->data['locations'] = \App\Locations::orderBy('name', 'asc')->pluck('name', 'id')->all();
        $this->data['roles'] = User::getPossibleEnumValues('role');
        return view('_back.users.crud' . ($request->get('role') == 'company' ? '_company' : ''), $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(\Auth::check() && \Auth::user()->role == 'user'){
            return redirect()->route('back');
        }

        $query = parent::_store($request->all());

        if(!($query instanceof User)){
            return $query;
        }

        return redirect()->route('back.users.show', array('user' => $query->id))
            ->with('message_success', 'Cambios guardados correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user)
    {
        if(\Auth::check() && \Auth::user()->role == 'user' && \Auth::id() != $user->id){
            return redirect()->route('back');
        }

        $this->data['query'] = parent::_show($user);
        $this->data['yesno_lov'] = \App\Items::getPossibleEnumValues('is_giftable');
        $this->data['locations'] = \App\Locations::orderBy('name', 'asc')->pluck('name', 'id')->all();
        $this->data['districts'] = \App\Districts::where('locations_id', '=', $this->data['query']->locations_id)
            ->orderBy('name', 'asc')->pluck('name', 'id')->all();
        $this->data['roles'] = User::getPossibleEnumValues('role');
        if(\Auth::check() && \Auth::user()->role == 'user'){
            return view('_back.users.user_crud', $this->data);
        }

        return view('_back.users.crud' . ($this->data['query']->role == 'company' ? '_company' : ''), $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if(\Auth::check() && \Auth::user()->role == 'user' && \Auth::id() != $user->id){
            return redirect()->route('back');
        }

        $query = parent::_update($request->all(), $user);

        if(!($query instanceof User)){
            return $query;
        }

        return redirect()->route('back.users.show', ['id' => $query->id] + ($query->role == 'company' ? ['role' => 'company'] : []))
            ->with('message_success', 'Cambios guardados correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if(\Auth::check() && \Auth::user()->role == 'user'){
            return redirect()->route('back');
        }

        parent::_destroy($user);
        return redirect()
            ->route('back.users.index')
            ->with('message_success', 'Cambios guardados correctamente');
    }

}
