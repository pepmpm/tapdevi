<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemsStocks extends _BaseModel
{
    protected $fillable = ['items_id', 'date', 'updated_at'];
    public $timestamps = true;

    public function item(){
        return $this->belongsTo('\App\Items', 'items_id');
    }
}
