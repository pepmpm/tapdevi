<div class="col-md-4">
    <div class="funny-boxes funny-boxes-colored funny-boxes-tdv">
        <div class="row">
            <div class="col-md-12 funny-boxes-img">
                <a href="{{ route('routes.show', ['route' => $blog->slug]) }}">
                    @if($agent->isMobile())
                        <img class="img-responsive" alt="{{ $blog->name }}"
                            src="{{ Imgfly::imgPublic(str_replace("upload/", "", $blog->one_photo()) . '?w=370&h=260&fit=crop-center', 
                                'upload') }}">
                    @else
                        <img class="img-responsive" alt="{{ $blog->name }}"
                            src="{{ Imgfly::imgPublic(str_replace("upload/", "", $blog->one_photo()) . '?w=325&h=280&fit=crop-center', 
                                'upload') }}">
                    @endif
                    <div class="row margin-bottom-15">
                        <div class="col-md-12">
                            @foreach(explode("#", $blog->tags) as $tag)
                                @if($tag != null)
                                    <a class="btn-u btn-u-xs btn-u-default margin-bottom-5">#{{ $tag }}</a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h2><a href="{{ route('routes.show', ['route' => $blog->slug]) }}">{{ $blog->name }}</a></h2>
            </div>
        </div>
    </div>
</div>