@extends('_layouts/inicio')

@section ('body')

<!--=== Breadcrumbs ===-->
<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left">Localizaciones</h1>
    </div><!--/container-->
</div><!--/breadcrumbs-->
<div class="container content profile">
    <div class="row">
        <div class="col-md-12">
            <a href="{{route('back.admin.locations.create')}}" 
               class="btn btn-warning btn-lg" role="button">
                Crear localización
            </a>
        </div>     
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            @include('_layouts.messages', array('errors' => $errors))
            <table class="table" id="tabla">
                <thead>
                    <th>Nombre</th>
                    <th>Slug</th>
                    <th>Latitud</th>
                    <th>Longitud</th>
                    <th>Acciones</th>
                </thead>
                <tbody>
                    @foreach($query as $location)
                        <tr>
                            <td>{{ $location->name }}</td>
                            <td>{{ $location->slug }}</td>
                            <td>{{ $location->latitude }}</td>
                            <td>{{ $location->longitude }}</td>
                            <td>
                                <a href="{{ route('back.admin.locations.show', array('id' => $location->id)) }}" 
                                   class="btn btn-primary btn-sm col-md-3" role="button">
                                    Editar
                                </a>
                                <form action="{{ route('back.admin.locations.destroy', [$location->id]) }}" method="POST" 
                                    onsubmit="if(!confirm('¿Estás seguro de querer borrar la localización?')) return false"
                                    class="col-md-4">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-danger btn-sm" type="submit">
                                        Borrar
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>     
    </div>
</div><!--/container-->    
<!--=== End Profile ===-->

@stop

@section ('assets_end')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/datatables.min.css') }}">
    <script src="{{ asset('assets/plugins/datatables/datatables.min.js') }}"></script>
    
    <script type="text/javascript">
        $(document).ready(function() {
            $('#tabla').DataTable({
                "language": {
                    "url": ARNY.baseUrl + "/assets/plugins/datatables/spanish.lang"
                }
            });
        });
    </script>
@stop
