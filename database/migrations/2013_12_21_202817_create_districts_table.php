<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	 
    public function up()
    {
        Schema::create('districts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 250);
            $table->string('slug', 250);
            $table->string('latitude', 30)->nullable();
            $table->string('longitude', 30)->nullable();
            $table->enum('island', ['mallorca', 'menorca', 'ibiza', 'formentera'])->nullable();
            $table->enum('base', ['S', 'N'])->default('N');
            $table->timestamps();

            $table->unsignedInteger('locations_id')->nullable();
            $table->foreign('locations_id')->references('id')->on('locations');
			
            $table->index('slug');
            $table->index('island');
            $table->index(['latitude', 'longitude']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('districts');
    }
}
