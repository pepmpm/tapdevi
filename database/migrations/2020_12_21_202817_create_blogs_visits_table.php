<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs_visits', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('blogs_id');
            $table->foreign('blogs_id')->references('id')->on('blogs')->onDelete('cascade');

            $table->string('year', 4);
            $table->string('month', 2);

            $table->unsignedInteger('day_1')->default(0);
            $table->unsignedInteger('day_2')->default(0);
            $table->unsignedInteger('day_3')->default(0);
            $table->unsignedInteger('day_4')->default(0);
            $table->unsignedInteger('day_5')->default(0);
            $table->unsignedInteger('day_6')->default(0);
            $table->unsignedInteger('day_7')->default(0);
            $table->unsignedInteger('day_8')->default(0);
            $table->unsignedInteger('day_9')->default(0);
            $table->unsignedInteger('day_10')->default(0);
            $table->unsignedInteger('day_11')->default(0);
            $table->unsignedInteger('day_12')->default(0);
            $table->unsignedInteger('day_13')->default(0);
            $table->unsignedInteger('day_14')->default(0);
            $table->unsignedInteger('day_15')->default(0);
            $table->unsignedInteger('day_16')->default(0);
            $table->unsignedInteger('day_17')->default(0);
            $table->unsignedInteger('day_18')->default(0);
            $table->unsignedInteger('day_19')->default(0);
            $table->unsignedInteger('day_20')->default(0);
            $table->unsignedInteger('day_21')->default(0);
            $table->unsignedInteger('day_22')->default(0);
            $table->unsignedInteger('day_23')->default(0);
            $table->unsignedInteger('day_24')->default(0);
            $table->unsignedInteger('day_25')->default(0);
            $table->unsignedInteger('day_26')->default(0);
            $table->unsignedInteger('day_27')->default(0);
            $table->unsignedInteger('day_28')->default(0);
            $table->unsignedInteger('day_29')->default(0);
            $table->unsignedInteger('day_30')->default(0);
            $table->unsignedInteger('day_31')->default(0);

            $table->timestamps();

            $table->index('blogs_id');
            $table->index(['month', 'year']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs_visits');
    }
}
