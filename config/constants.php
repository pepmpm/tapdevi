<?php

return array(
    'NAMEWEB' => 'Tapdevi.com',
    'NAMELITE' => 'Tapdevi',
    'FULLWEB' => 'https://www.tapdevi.com',
    'MAILWEB' => 'info@tapdevi.com',
    'PHONEWEB' => '34635379675',
    'ADDRESSWEB' => 'Calle Sant Cosme 3, bajos. 07220 Pina. Illes Balears.',
    'FACEBOOKWEB' => 'https://www.facebook.com/Tapdevi',
    'precio_envio_gratuito' => 60,
    'peso_caja_vino_6' => 8000,
    'largo_caja_vino_6' => 24,
    'ancho_caja_vino_6' => 16,
    'alto_caja_vino_6' => 33,
    'peso_magnum' => 2000,
    'largo_magnum' => 12,
    'ancho_magnum' => 12,
    'alto_magnum' => 35,
    'venta_por_multiplos_de' => 6,
    'comision_distribucion' => 26,
    'web_vimallorqui' => 'https://www.vimallorqui.com'
);
?>
