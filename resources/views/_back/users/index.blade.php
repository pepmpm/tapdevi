@extends('_layouts/inicio')

@section ('body')

<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left">Usuarios/Empresas</h1>
    </div>
</div>
<div class="container content profile">
    <div class="row">
        <div class="col-md-12">
            <a href="{{route('back.users.create')}}" 
               class="btn-u btn-u-lg" role="button">
                Nuevo usuario
            </a>
            <a href="{{route('back.users.create', ['role' => 'company'])}}" 
               class="btn-u btn-u-lg" role="button">
                Nueva empresa
            </a>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Items</div>
                <div class="panel-body">
                    @include('_layouts.messages', array('errors' => $errors))
                    <table class="table" id="tabla">
                        <thead>
                            <th>Nombre</th>
                            <th>Rol</th>
                            <th>Email</th>
                            <th>Provincia</th>
                            <th>Población</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                            @foreach($query as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->role }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->location != null ? $user->location->name : '' }}</td>
                                    <td>{{ $user->district != null ? $user->district->name : '' }}</td>
                                    <td>
                                        <a href="{{ route('back.users.show', 
                                            ['id' => $user->id] + ($user->role == 'company' ? ['role' => 'company'] : [])) }}" 
                                           class="btn btn-primary btn-sm pull-left" role="button">
                                            <i class="fa fa-edit" aria-hidden="true"></i>
                                        </a>
                                        <form action="{{ route('back.users.destroy', [$user->id]) }}" method="POST" 
                                            onsubmit="if(!confirm('¿Estás seguro de querer borrar el usuario?')) return false"
                                            class="col-md-4">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-danger btn-sm" type="submit">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>     
    </div>
</div><!--/container-->    
<!--=== End Profile ===-->

@stop

@section ('assets_end')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/datatables.min.css') }}">
    <script src="{{ asset('assets/plugins/datatables/datatables.min.js') }}"></script>
    
    <script type="text/javascript">
        $(document).ready(function() {
            $('#tabla').DataTable({
                "language": {
                    "url": ARNY.baseUrl + "/assets/plugins/datatables/spanish.lang"
                },
                stateSave: true
            });
        });
    </script>
@stop
