@extends('_layouts/inicio')

@section ('body')

<div class="breadcrumbs">
    <div class="container">
        @if(isset($query))
            <h1 class="pull-left">Cupón: {{ $query->name }}</h1>
        @else
            <h1 class="pull-left">Nuevo cupón</h1>
        @endif
    </div>
</div>
<div class="container content">
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('back.coupons.index') }}" 
               class="btn-u btn-u-sea-shop btn-u-lg" role="button">
                Volver a la lista
            </a>
            <a href="{{ route('back.coupons.create') }}" 
               class="btn-u btn-u-sea-shop btn-u-lg" role="button">
                Crear otro cupón
            </a>
        </div>     
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            @include('_layouts.messages', array('errors' => $errors))
            <div class="panel panel-u margin-bottom-40 bg-grey">
                <div class="panel-body">
                    <form class="margin-bottom-20" role="form" method="post" enctype="multipart/form-data" 
                          action="{{ isset($query) ? route('back.coupons.update', $query->id) : route('back.coupons.store') }}">
                        @if(isset($query))
                            @method('PUT')
                        @endif
                        @csrf
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Código</label>
                                    <input type="text" class="form-control" name="code" placeholder="Código" 
                                        value="{{ old('code', isset($query) ? $query->code : '') }}"/>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Descripción</label>
                                    <input type="text" class="form-control" name="description" placeholder="Descripción" 
                                        value="{{ old('description', isset($query) ? $query->description : '') }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Activo</label>
                                    {!! Form::select('active', $active_lov, 
                                        old('active', isset($query) ? $query->active: ''), 
                                        array('class' => 'form-control', 'id' => 'active')) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Descuento</label>
                                    <input type="text" class="form-control" name="discount" placeholder="Descuento" 
                                        value="{{ old('discount', isset($query) ? $query->discount : '') }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <button type="submit" class="btn-u btn-u-sea-shop btn-block" style="margin-top: 25px">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section ('assets_end')

@stop
