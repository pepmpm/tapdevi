<div class="collapse navbar-collapse navbar-responsive-collapse">
    <ul class="nav navbar-nav">
        @if(isset($menu_routes) && $menu_routes->count() > 0)
            <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                    {{ trans('events.rutas_de_vino_mallorca') }}
                </a>
                <ul class="dropdown-menu">
                    <li class="padding-top-10 padding-bottom-15"><a href="{{ route('routes.index') }}" class="btn btn-u button margin-lr-15 color-blanco">{{ trans('blog.ver_todas_las_rutas') }}</a></li>
                    @foreach($menu_routes as $tmproute)
                        <li>
                            <a href="{{ route('routes.show', ['route' => $tmproute->slug]) }}">
                                {{ $tmproute->name }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </li>
        @endif
        <li class="dropdown">
            <a href="javascript:void(0);" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">
                {{ trans('inicio.enoturismo') }}
            </a>
            <ul class="dropdown-menu">
                <li class="padding-lr-15 padding-top-10"><strong>{{ trans('inicio.turismo_de_vino') }}</strong></li>
                <li><a href="{{ route('enotourism-mallorca.cat') }}"><i class="fa fa-glass"></i> {{ trans('inicio.enotourism-mallorca') }}</a></li>
                <li><a href="{{ route('enotourism-mallorca.events.index') }}"><i class="fa fa-calendar"></i> {{ trans('inicio.eventos_con_vino_en_mallorca') }}</a></li>
                <li><a href="{{ route('enotourism-mallorca.events.map') }}"><i class="fa fa-ellipsis-h"></i> {{ trans('events.rutas_de_vino_mallorca') }}</a></li>
                <hr class="margin-top-bottom-10" />
                <li class="padding-lr-15"><strong>{{ trans('plan.categorias') }}</strong></li>
                @foreach($menu_enoturismo_2 as $tmp)
                    <li><a href="{{ route($tmp->category . '.cat', ['cat' => $tmp->slug]) }}">{{ $tmp->name }}</a></li>
                @endforeach
            </ul>
        </li>
    </ul>
</div>