@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section('title', trans('meta.title_index'))
@section ('meta')
    <meta name="description" content="{{ trans('meta.desc_index') }}"/>
    <meta name="keywords" content="{{ trans('meta.keys_index') }}" />
    <meta name="Author" content="{{ trans('meta.author') }}" />
    <meta property="og:title" content="{{ trans('meta.title_index') }}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:image" content="{{ url('assets/img/logo3.png') }}"/>
    <meta property="og:description" content="{{ trans('meta.desc_index') }}" />
    @parent
@stop

@section ('body')
<div class="container margin-top-30">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <h2 class="title-v2">{{ trans('inicio.regala_cata_de_vino') }}</h2>
        </div>
    </div>
    <div class="row padding-lr-15">
        @php($rand = array(1,2,3,4,5,6,7))
        @php(shuffle($rand))
        @foreach($index_items_packs as $key => $pack)
            <a href="{{ route('enotourism-mallorca.gift', ['item' => $pack->slug]) }}">
                <div class="col-md-6 height250 container-image" 
                    style="background: url('{{ Imgfly::imgPublic(str_replace("upload/", "", $pack->one_photo()) . '?w=585&h=250&fit=crop-center', 
                            'upload') }}') no-repeat scroll center center / cover">
                    <div class="col-md-6 bg-opacity-75 text-lefted padding-top-bottom-7">
                        <div class="row">
                            <div class="col-md-12 font25">
                                {{ $pack->name }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 font20">
                                {{ $pack->price . ' €' }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 font15">
                                {{ '( ' . $pack->packs->count() . ' ' . trans('inicio.planes') . ' )' }}
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        @endforeach
    </div>
    <div class="owl-carousel-v1 owl-work-v1 margin-bottom-40 margin-top-30">
        <div class="row">
            <div class="col-md-10 col-xs-9">
                <h2 class="title-v2">{{ trans('plan.por_tipo') }}</h2>
            </div>
            <div class="col-md-2 col-xs-3">
                <div class="owl-navigation">
                    <div class="customNavigation">
                        <a class="owl-btn prev-v2"><i class="fa fa-angle-left"></i></a>
                        <a class="owl-btn next-v2"><i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="owl-categories-home">
            @foreach($categories as $category)
                <div class="item">
                    <a href="{{ route('enotourism-mallorca.cat', ['cat' => $category->slug]) }}">
                        <em class="overflow-hidden">
                            <img class="img-responsive" alt="{{ $category->name }}"
                                src="{{ Imgfly::imgPublic(str_replace("upload/", "", $category->icon) . '?w=' . 
                                ($agent->isMobile() ? '194' : '218') . '&fit=crop-center', 'upload') }}">
                        </em>
                        <span>
                            <strong>{{ $category->name }}</strong>
                        </span>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>

<div class="container margin-bottom-40" id="go_inicio">
    <div class="row">
        <div class="col-md-10 col-xs-8">
            <h2 class="title-v2">{{ trans('inicio.mas_vendidos') }}</h2>
        </div>
        <div class="col-md-2 col-xs-4">
            <div class="pull-right margin-top-20">
                <a href="{{ route('enotourism-mallorca.cat') }}">
                    <strong>Ver todos&nbsp;<i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></strong>
                </a>
            </div>
        </div>
    </div>
    <div class="row news-v1 margin-bottom-20">
        @foreach($index_plans_promoteds as $plan)
            @include('plans.element', ['plan' => $plan])
            @if($loop->iteration % 4 == 0)
                </div>
                <div class="row news-v1 margin-bottom-20"> 
            @endif
        @endforeach
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-10 col-xs-8">
            <h2 class="title-v2">{{ trans('inicio.recomendaciones') }}</h2>
        </div>
        <div class="col-md-2 col-xs-4">
            <div class="pull-right margin-top-20">
                <a href="{{ route('enotourism-mallorca.cat') }}">
                    <strong>Ver todos&nbsp;<i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></strong>
                </a>
            </div>
        </div>
    </div>

    <div class="row news-v1 margin-bottom-20">
        @foreach($index_plans as $plan)
            @include('plans.element', ['plan' => $plan])
            @if($loop->iteration % 4 == 0)
                </div>
                <div class="row news-v1 margin-bottom-20"> 
            @endif
        @endforeach
    </div>

    <div class="row margin-bottom-20">
        <div class="col-md-12 text-center">
            <a href="{{ route('enotourism-mallorca.cat') }}"
                class="btn-u btn-brd btn-brd-hover btn-u-lg btn-u-sea-shop btn-block">{{ trans('inicio.ver_todos') }}</a>
        </div>
    </div>

</div>

@stop

@section ('assets_end')
    <link rel="stylesheet" href="{{ asset('assets/css/pages/timeline2.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/pages/inicio.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pages/page_search.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/plugins/owl-carousel/owl-carousel/owl.carousel.css') }}">
    <script src="{{ asset('assets/plugins/owl-carousel/owl-carousel/owl.carousel.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/plans.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/inicio.js') }}"></script>
@stop