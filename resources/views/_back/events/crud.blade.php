@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section ('body')

<!--=== Breadcrumbs ===-->
<div class="breadcrumbs">
    <div class="container">
        @if(isset($query))
            <h1 class="pull-left">Evento: {{$query->name}}</h1>
        @else
            <h1 class="pull-left">Nuevo evento</h1>
        @endif
    </div><!--/container-->
</div><!--/breadcrumbs-->
<div class="container content">
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('back.events.index') }}" 
               class="btn-u btn-u-lg" role="button">
                Volver a la lista
            </a>
            <a href="{{ route('back.events.create') }}" 
               class="btn-u btn-u-lg" role="button">
                Crear otro evento
            </a>
            @if(isset($query))
                <a href="{{ route('back.events.stocks.show', ['items_id' => $query->id]) }}" 
                   class="btn-u btn-u-green btn-u-lg" role="button">
                    Modificar disponibilidad
                </a>
            @endif
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <form class="margin-bottom-20" id="form_item" role="form" method="post" enctype="multipart/form-data" 
                action="{{ isset($query) ? route('back.events.update', ['event' => $query->id]) : route('back.events.store') }}">
                @if(isset($query))
                    @method('PUT')
                @endif
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        @include('_layouts.messages', array('errors' => $errors))
                        @include('_back.events.crud_info', array('query' => isset($query) ? $query : null))
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9" id="div_crud_langs">
                        @include('_back.events.crud_langs', array('query' => isset($query) ? $query : null))
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">Acciones</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn-u btn-block margin-bottom-25">
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Status</label>
                                            {!! Form::select('status', $status_lov, 
                                                old('status', isset($query) ? $query->status: ''), 
                                                array('class' => 'form-control', 'id' => 'status')) !!}
                                        </div>
                                    </div>
                                </div>
                                <div id="div_crud_photos"> 
                                    @include('_back.events.crud_photos', array('query' => isset($query) ? $query : null))
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section ('assets_end')
    <script src="{{ asset('assets/plugins/redactor/redactor.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/plugins/redactor/redactor.css') }}">
    <script src="{{ asset('assets/plugins/redactor/lang/es.js') }}"></script>
    <script src="{{ asset('assets/plugins/redactor/plugins/read-more-plugin.js') }}"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBw_spE5_xC8pjOBJVWs5LHLBKn0EC-cfQ"></script>
    <script src="{{ asset('assets/plugins/gmap/gmap.js') }}"></script>

    <script src="{{ asset('assets/plugins/fancybox/source/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/plugins/fancybox/source/jquery.fancybox.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/plugins/jquery-upload-file/uploadfile.css') }}">
    <script src="{{ asset('assets/plugins/jquery-upload-file/jquery.uploadfile.min.js') }}"></script>

    <script src="{{ asset('assets/plugins/select2/dist/js/select2.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/dist/css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/dist/css/select2-bootstrap.css') }}">

    <script type="text/javascript">
        $(document).ready(function() {
            @if(isset($query))
                $('#btn_instagram').on('click', function(event) {
                    $.get(ARNY.baseUrl + '/back/events/instagram/get/{{ $query->id }}',
                        function(data){
                            $('#div_crud_photos').html(data);
                        }
                    );
                    event.preventDefault();
                });
                $('#btn_translate').on('click', function(event) {
                    $.get(ARNY.baseUrl + '/back/events/translate/{{ $query->id }}',
                        function(data){
                            $('#div_crud_langs').html(data);
                            if($('.redactor').length){
                                $('.redactor').redactor({
                                    minHeight: 400,
                                    lang: 'es',
                                    plugins: ['leermas']
                                });
                            }
                        }
                    );
                    event.preventDefault();
                });
            @endif
            $("#fileuploader").uploadFile({
                url:"{{ route('back.events.photos.store') }}",
                fileName:"p_file",
                onSuccess:function(files,data,xhr,pd){
                    $('#form_item').append('<input type="hidden" name="p_foto[]" value="'+data+'" />');
                }
            });
            if($('.redactor').length){
                $('.redactor').redactor({
                    minHeight: 400,
                    lang: 'es',
                    plugins: ['leermas']
                });
            }
        });
    </script>

    <script type="text/javascript">
        $(function(){
            $("#districts_id").select2({theme: "bootstrap", selectOnClose: true});

            if($(".fancybox").length){
                jQuery(".fancybox").fancybox({
                    groupAttr: 'data-rel',
                    prevEffect: 'fade',
                    nextEffect: 'fade',
                    openEffect  : 'elastic',
                    closeEffect  : 'fade',
                    closeBtn: true,
                    helpers: {
                        title: {
                            type: 'float'
                        }
                    }
                });
            }
        });
    </script>
    <script type="text/javascript">
        var map;
        var marker;
        $(document).ready(function() {
            if($('#map').length){
                map = new GMaps({
                    div: '#map',
                    scrollwheel: false,             
                    lat: 40.416775,
                    lng: -3.703790,
                    zoom: 5
                });
                if($("#latitud").val() && $("#longitud").val()){
                    postGeocoords(map, $("#longitud").val(), $("#latitud").val());
                }

                $("#btn_refresh_map").click(function () {
                    postGeocode(map);
                });
            }
        });
        function postGeocoords(mapa, longitud, latitud){
            mapa.setCenter(latitud, longitud);
            mapa.setZoom(15);
            if(marker == null){
                marker = mapa.addMarker({
                    lat: latitud,
                    lng: longitud,
                    draggable: true
                });
            }else{
                marker.setPosition( new google.maps.LatLng( latitud, longitud ) );
                mapa.panTo( new google.maps.LatLng( latitud, longitud ) );
            }

            $("#latitud").val(latitud);
            $("#longitud").val(longitud);

            google.maps.event.addListener(
                marker,
                'drag',
                function() {
                    $("#latitud").val(marker.position.lat());
                    $("#longitud").val(marker.position.lng());
                }
            );
        }

        function postGeocode(mapa){
            var direccion = $("#address").val();
            direccion += " "  + $("#postal_code").val();
            direccion += " "  + $("#districts_id option:selected").text();
            GMaps.geocode({
                address: direccion,
                callback: function(results, status) {
                    if (status == 'OK') {
                        var latlng = results[0].geometry.location;
                        mapa.setCenter(latlng.lat(), latlng.lng());
                        mapa.setZoom(15);
                        if(marker == null){
                            marker = mapa.addMarker({
                                lat: latlng.lat(),
                                lng: latlng.lng(),
                                draggable: true
                            });
                        }else{
                            marker.setPosition( new google.maps.LatLng( latlng.lat(), latlng.lng() ) );
                            mapa.panTo( new google.maps.LatLng( latlng.lat(), latlng.lng() ) );
                        }

                        $("#latitud").val(latlng.lat());
                        $("#longitud").val(latlng.lng());

                        google.maps.event.addListener(
                            marker,
                            'drag',
                            function() {
                                $("#latitud").val(marker.position.lat());
                                $("#longitud").val(marker.position.lng());
                            }
                        );
                    }
                }
            });
        }
    </script>
    <style type="text/css">
        .btn-u-sm, a.btn-u-sm {
            padding: 5px 12px;
        }
    </style>
@stop
