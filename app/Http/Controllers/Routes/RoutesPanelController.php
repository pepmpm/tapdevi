<?php

namespace App\Http\Controllers\Routes;

use App\Blogs;
use Image;
use Illuminate\Http\Request;

class RoutesPanelController extends RoutesController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['query'] = parent::_index([]);
        $this->data['status_lov'] = Blogs::getPossibleEnumValues('status');
        return view('_back.routes.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['status_lov'] = Blogs::getPossibleEnumValues('status');
        $this->data['tags'] = parent::get_tags();
        return view('_back.routes.crud', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query = parent::_store($request->all());

        if(!($query instanceof Blogs)){
            return $query;
        }

        return redirect()->route('back.routes.show', array('blog' => $query->id))
            ->with('message_success', 'Cambios guardados correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function show(Blogs $route)
    {
        $this->data['tags'] = parent::get_tags();
        $this->data['status_lov'] = Blogs::getPossibleEnumValues('status');
        $this->data['query'] = parent::_show($route);
        return view('_back.routes.crud', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blogs $route)
    {
        $query = parent::_update($request->all(), $route);

        if(!($query instanceof Blogs)){
            return $query;
        }

        return redirect()->route('back.routes.show', array('plan' => $query->id))
            ->with('message_success', 'Cambios guardados correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blogs $route)
    {
        parent::_destroy($route);
        return redirect()
            ->route('back.routes.index')
            ->with('message_success', 'Cambios guardados correctamente');
    }

    public function translate(Request $request, Blogs $blog){
        //preparamos el array de idiomas
        $array = array();
        foreach(config('translatable.locales') as $keylocale => $locale){
            $routeLang = $blog->getTranslation('name', $keylocale);

            $array['name'][$keylocale] = $routeLang->locale == $keylocale ? $routeLang->name : '';
            $array['locale'][$keylocale] = $keylocale;
            $array['slug'][$keylocale] = '';
            $array['description'][$keylocale] = $routeLang->locale == $keylocale ? $routeLang->description : '';
            $array['seo_description'][$keylocale] = $routeLang->locale == $keylocale ? $routeLang->seo_description : '';
            $array['seo_keywords'][$keylocale] = $routeLang->locale == $keylocale ? $routeLang->seo_keywords : '';
        }

        parent::_insert_langs($array, $blog);
        $this->data['query'] = parent::_show($blog);
        return view('_back.routes.crud_langs', $this->data);
    }
}
