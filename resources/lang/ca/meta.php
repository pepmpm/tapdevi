<?php

$idioma = array();

$idioma['author'] = 'Pep Manel Palmer';
$idioma['title_default'] = 'Compra vi mallorquí • Enoturisme • Productes gourmet ✔️ Compra vi de les Illes Balears';

//index
$idioma['title_index'] = "Enoturisme a Mallorca ✔️ Tours de vi ✔️ Gaudeix d'experiències amb vi de Mallorca";
$idioma['desc_index'] = "Visites a cellers, activitats relacionades amb el vi mallorquí, esdeveniments, tallers, cursos... Coneix i gaudeix del vi de Mallorca. Més de 100 cellers per conèixer.";
$idioma['keys_index'] = 'enoturisme mallorca, experiències vi mallorca, visites cellers mallorca, tast vi mallorca, tast vi santa maria, tast vi binissalem';

//plan.index
$idioma['title_index_plan'] = "Enoturisme a Mallorca ✔️ Tours de vi a Mallorca ✔️ Gaudeix d'experiències amb vi de Mallorca";
$idioma['desc_index_plan'] = "No te perdis els tours d'enoturisme que te duràn per bodegues familiars i pels restaurants on anam els residents de Mallorca. Gaudiràs dels nostres excel.lents vins a llocs naturals que t'enamoraràn";
$idioma['keys_index_plan'] = "vi Mallorca, enoturisme Mallorca, experiències vi Mallorca, turisme vi Mallorca, tours vi Mallorca";

//plan.date.today
$idioma['title_index_plan_date_day'] = "Plans d'enoturisme per día :day ✔️ Enoturisme a Mallorca ✔️ Tours de vi a Mallorca ✔️ Gaudeix d'experiències amb vi de Mallorca";
$idioma['desc_index_plan_date_day'] = "Si no tens plans per día :day i t'agrada es vi entra i tria es plan que més t'agradi per aprofitar el día. No te perdis els tours d'enoturisme que t'enduràn per bodegues familiars i pels restaurants on anam els residents de Mallorca";
$idioma['keys_index_plan_date_day'] = "vi Mallorca, plans per :day, enoturisme Mallorca, experiències vi Mallorca :day, turisme vi Mallorca, tours vi Mallorca :day";

//plan.date.weekend
$idioma['title_index_plan_date_weekend'] = "Plans d'enoturisme per es cap de setmana ✔️ Enoturisme a Mallorca ✔️ Tours de vi a Mallorca ✔️ Gaudeix d'experiències amb vi de Mallorca";
$idioma['desc_index_plan_date_weekend'] = "Plans per es cap de setmana des de 12€ a cellers, restaurants, fires, barcos, a fora vila, a finques rústiques... es vi no enten de llocs, sino de moments. Si no tens plans per es cap de setmana noltros te donam idees.";
$idioma['keys_index_plan_date_weekend'] = "vi Mallorca cap de setmana, plans per cap de setmana, enoturisme Mallorca, experiències vi Mallorca cap de setmana, turisme vi Mallorca, tours vi Mallorca cap de setmana";


$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';

return $idioma;