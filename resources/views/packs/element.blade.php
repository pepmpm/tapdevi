@php($tmpuser = $pack->user)
<div class="col-md-{{ isset($col) ? $col : '3' }} text-center">
    <div class="news-v1-in bgcolor-light-grey">
        @if($agent->isMobile())
            <img class="img-responsive" alt="{{ $pack->name }}"
                src="{{ Imgfly::imgPublic(str_replace("upload/", "", $pack->one_photo()) . '?w=407&h=260&fit=crop-center', 
                    'upload') }}">
        @else
            <img class="img-responsive" alt="{{ $pack->name }}"
                src="{{ Imgfly::imgPublic(str_replace("upload/", "", $pack->one_photo()) . '?w=302&h=280&fit=crop-center', 
                    'upload') }}">
        @endif
        <h3 class="font-normal">
            <span>{{ $pack->name }}</span>
        </h3>
        <p>
            <small>
                {{ $tmpuser->name }}
            </small>
        </p>
        @include('plans._admin_buttons', ['plan' => $pack])
        <div class="row">
            <div class="col-md-12 text-center">
                <strong>
                    <span class="font34">{{ $pack->trueprice . "€" }}</span>
                    @if($pack->new_price < $pack->price && $pack->new_price > 0)
                        <span class="tachado font34">{{ $pack->price . "€" }}</span>
                    @endif
                </strong>
                {{ trans('plan.por_persona') }}
            </div>
        </div>
        <ul class="list-inline news-v1-info no-margin-bottom margin-top-10 border-top-tdv">
            <li>
                <i class="fa fa-map-pin color-tdv width14"></i>
                {{ $tmpuser->district->name }}
            </li>
            <li>|</li>
            <li>
                <i class="fa fa-male color-tdv width14" aria-hidden="true"></i>&nbsp;
                {{ 'Min ' . $pack->min_pax }}
                @if($pack->max_pax != null)
                    {{ ' - Max ' . $pack->max_pax }}
                @endif
            </li>
        </ul>
    </div>
</div>

