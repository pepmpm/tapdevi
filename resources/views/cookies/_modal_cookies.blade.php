<div class="modal fade" id="modal_cookies" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row margin-bottom-25">
                    <div class="col-md-12">
                        {{ trans('inicio.cookies_message') }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 text-center">
                        <a href="{{ route('politicas_cookies') }}">
                            {{ trans('inicio.politica_cookies') }}
                        </a>
                    </div>
                    <div class="col-md-6 text-center">
                        <a href="{{ route('politicas_privacidad') }}">
                            {{ trans('inicio.politica_privacidad') }}
                        </a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-u" id="aceptar_cookies" data-dismiss="modal">{{ trans('base.aceptar') }}</button>
                <button type="button" class="btn btn-default" id="denegar_cookies" data-dismiss="modal">{{ trans('base.denegar') }}</button>
            </div>
        </div>
    </div>
</div>