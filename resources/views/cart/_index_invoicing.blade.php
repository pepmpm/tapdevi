<div class="panel bgcolor-blanco">
    <div class="panel-body">
        <h2>{{ trans('order.facturacion') }}</h2>
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <input type="text" placeholder="{{ trans('order.numero_de_cuenta') }}" name="card_number" class="form-control margin-bottom-15"
                    value="{{ old('card_number') }}" maxlength="19">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 col-xs-4">
                {!! Form::select('month', $months, old('month'), 
                    array('class' => 'form-control margin-bottom-15', 'placeholder' => trans('order.mes'))) !!}
            </div>
            <div class="col-sm-4 col-xs-4">
                {!! Form::select('year', $years, old('year'), 
                    array('class' => 'form-control margin-bottom-15', 'placeholder' => trans('order.ano'))) !!}
            </div>
            <div class="col-sm-4 col-xs-4">
                <input type="tel" placeholder="CVC" name="cvc" class="form-control margin-bottom-15"
                    value="{{ old('cvc') }}">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="note">
                    <strong>{{ trans('order.note') }}:</strong>
                    {{ trans('order.mensaje_tarjetas_bbdd') }}
                </div>
            </div>
        </div>
    </div>
</div>