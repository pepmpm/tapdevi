<?php

$idioma = array();

$idioma['validation_stock_required'] = 'El stock es obligatori';
$idioma['validation_stock_numeric'] = 'El stock ha de ser numèric';
$idioma['validation_cost_numeric'] = 'Es preu de cost del producte ha de ser numèric';
$idioma['validation_price_required'] = 'El preu de venta es obligatori';
$idioma['validation_price_numeric'] = 'El preu ha de ser numèric';
$idioma['validation_new_price_numeric'] = 'El preu amb descompte ha de ser numèric';
$idioma['validation_labels_id_required'] = "S'ha de seleccionar una categoría";
$idioma['validation_labels_id_numeric'] = 'Sa categoría seleccionada es incorrecta';
$idioma['validation_users_id_required'] = "S'ha de seleccionar una bodega";
$idioma['validation_users_id_numeric'] = 'Sa bodega seleccionada es incorrecta';
$idioma['validation_name_required'] = "S'ha d'emplenar el nom del event";
$idioma['validation_name_max'] = 'Es nom del event no pot ser tan llarg';
$idioma['validation_description_required'] = "S'ha d'emplenar la descripció del event";
$idioma['validation_description_max'] = 'Sa descripció del event es massa llarga';
$idioma['validation_new_price_lt'] = 'El preu amb descompte ha de ser menor que el preu habitual';

$idioma['custom_name'] = "Caixa personalitzada de :user";
$idioma['custom_description'] = "Si vols tastar un parell de vins de la bodega :user pots seleccionar els vins que vulguis fins completar una caixa. El preu de sa caixa s'anirà actualitzant cada vegada que n'afegeixis o eliminis un vi.";
$idioma['custom_seo_description'] = "Vols vi de :user pero una caixa sencera es molt? Amb ". config('constants.NAMEWEB') ." pots triar els vins que vols que dugui sa teva caixa. Dos blancs, un rosat i tres negres? Tu tries.";
$idioma['custom_seo_keywords'] = 'caixa personalitzada :user, caixa custom :user, caixa vi variada :user, caixa variada :user, triar vins caixa :user';

$idioma['cada_lote'] = 'cada lot';
$idioma['mensaje_elegir_plan_regalo'] = 'Tens :planes plà amb vi per triar|Tens :planes plans amb vi per triar';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';

return $idioma;