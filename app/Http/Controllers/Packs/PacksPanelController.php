<?php

namespace App\Http\Controllers\Packs;

use App\Items;
use App\ItemsPacks;
use Illuminate\Http\Request;

class PacksPanelController extends PacksController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['query'] = parent::_index([]);
        if(\Auth::user()->isAdmin()){
            $this->data['users_lov'] = \App\User::whereHas('plans', function ($query) {
                    $query->where('status', '=', 'active');
                })->pluck('name', 'id')->all();
        }
        $this->data['labels_lov'] = \App\Labels::whereHas('plans', function ($query) {
                $query->where('status', '=', 'active');
            })->listsTranslations('title')->pluck('title', 'id')->all();
        $this->data['status_lov'] = Items::getPossibleEnumValues('status');
        return view('_back.packs.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['yesno_lov'] = Items::getPossibleEnumValues('is_has_parking');
        $this->data['status_lov'] = Items::getPossibleEnumValues('status');
        $this->data['numbers_lov'] = self::_get_numbers();
        return view('_back.packs.crud', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query = parent::_store($request->all());

        if(!($query instanceof Items)){
            return $query;
        }

        return redirect()->route('back.packs.show', array('item' => $query->id))
            ->with('message_success', 'Cambios guardados correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function show(Items $pack)
    {
        $this->data['status_lov'] = Items::getPossibleEnumValues('status');
        $this->data['yesno_lov'] = Items::getPossibleEnumValues('is_has_parking');
        $this->data['numbers_lov'] = self::_get_numbers();
        $this->data['query'] = parent::_show($pack);
        return view('_back.packs.crud', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Items $pack)
    {
        $query = parent::_update($request->all(), $pack);

        if(!($query instanceof Items)){
            return $query;
        }

        return redirect()->route('back.packs.show', array('pack' => $query->id))
            ->with('message_success', 'Cambios guardados correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Items $pack)
    {
        parent::_destroy($pack);
        return redirect()
            ->route('back.packs.index')
            ->with('message_success', 'Cambios guardados correctamente');
    }

    public function additems(Request $request, Items $items_id){
        if ($request->isMethod('post')){
            parent::_insert_related_items($request->all(), $items_id->id);
            return redirect()
                ->route('back.plans.additems', ['items_id' => $items_id->id])
                ->with('message_success', 'Cambios guardados correctamente');
        }

        $items_selected = ItemsPacks::where('items_id', '=', $items_id->id)
            ->pluck('items_id_related')->all();
        $this->data['items'] = Items::where('status', '=', 'active')
                ->where('type', '=', 'plan')
                ->whereNotIn('id', $items_selected)
                ->orderBy('order', 'asc')->get();
        $this->data['items_selected'] = Items::where('status', '=', 'active')
                ->where('type', '=', 'plan')
                ->whereIn('id', $items_selected)
                ->orderBy('order', 'asc')->get();
        $this->data['query'] = $items_id;
        return view('_back.packs.crud_additems', $this->data);
    }

    public function translate(Request $request, Items $item){
        //preparamos el array de idiomas
        $array = array();
        foreach(config('translatable.locales') as $keylocale => $locale){
            $itemLang = $item->getTranslation('name', $keylocale);

            $array['name'] = $itemLang->name;
            $array['locale'][$keylocale] = $keylocale;
            $array['slug'][$keylocale] = '';
            $array['description'][$keylocale] = $itemLang->locale == $keylocale ? $itemLang->description : '';
            $array['seo_description'][$keylocale] = $itemLang->locale == $keylocale ? $itemLang->seo_description : '';
            $array['seo_keywords'][$keylocale] = $itemLang->locale == $keylocale ? $itemLang->seo_keywords : '';
        }

        parent::_insert_langs($array, $item);
        $this->data['query'] = parent::_show($item);
        return view('_back.packs.crud_langs', $this->data);
    }

    private function _get_numbers(){
        $array = array();
        $array[''] = 'Sin información';
        $array['0'] = 'El mismo día';
        for($x = 1; $x < 11; $x ++){
            $array[$x] = $x;
        }

        return $array;
    }
}
