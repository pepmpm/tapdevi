<?php

$idioma = array();

$idioma['inicio'] = 'Inici';
$idioma['login'] = 'Login';
$idioma['registrarse'] = "Registra't";
$idioma['registrarse_en'] = 'Registre a ' . config('constants.NAMEWEB');
$idioma['nombre_empresa'] = "Nom de s'empresa";
$idioma['email'] = 'Email';
$idioma['password'] = 'Contrasenya';
$idioma['confirm_password'] = 'Confirma contrasenya';
$idioma['olvidaste_password'] = 'Has oblidat sa teva contraseña?';
$idioma['recordarme'] = 'Recordar-me';
$idioma['acceder'] = 'Accedir';
$idioma['acceder_a_tu_cuenta'] = 'Accedeix al teu compte';

$idioma['bienvenido_a'] = 'Benvingut a ' . config('constants.NAMEWEB');

$idioma['titulo_login_1'] = 'Control de stock';
$idioma['mensaje_login_1'] = 'Amb ' . config('constants.NAMEWEB') . ' podràs mantenir es control des teu stock i de ses comandes que vagis rebent.';
$idioma['titulo_login_2'] = 'Millora sa visibilitat';
$idioma['mensaje_login_2'] = 'Diversifica sa teva oferta i reb comandes des de la nostra web.';
$idioma['titulo_login_3'] = 'Producte mallorquí de km0';
$idioma['mensaje_login_3'] = 'Estàm compromesos amb es comerç local i reivindicam la qualitat dels productes de Mallorca.';

$idioma['enoturismo'] = 'Enoturisme';
$idioma['turismo_de_vino'] = 'Turisme del vi';

$idioma['enotourism-mallorca'] = 'Enoturisme a Mallorca';

$idioma['enotourism-mallorca-slug'] = 'enoturisme-a-mallorca';
$idioma['vinos_de_mallorca'] = 'Vins de Mallorca';
$idioma['msg_h5_inicio'] = 'Moltes opcions per regalar a cellers, hotels i restaurants';
$idioma['msg_h1_inicio'] = 'Descobreix el vi mallorquí a través de tasts, esdeveniments i maridatges';

$idioma['ver_todos'] = 'Veure tots';

$idioma['regalos_y_lotes'] = 'Regals i lots';

$idioma['destacados'] = 'Destacats';
$idioma['destacado'] = 'Destacat';
$idioma['tipo'] = 'Tipus';
$idioma['precio'] = 'Preu';
$idioma['bodegas'] = 'Bodegues';
$idioma['packs'] = 'Packs';
$idioma['planes'] = 'plans';
$idioma['recomendaciones'] = 'Recomanacions';
$idioma['mas_vendidos'] = 'Més venuts';
$idioma['regala_cata_de_vino'] = 'Regala tasts de vi';
$idioma['por_tipo'] = 'Per tipus';
$idioma['por_precio'] = 'Per preu';

$idioma['packs_slug'] = 'packs';
$idioma['recomendaciones_slug'] = 'recomanacions';
$idioma['mas_vendidos_slug'] = 'mes-venuts';

$idioma['carrito'] = 'Cistella';
$idioma['carrito_numero'] = 'Sa teva cistella te :num articles';
$idioma['revisa_tu_pedido'] = 'Revisa i modifica sa teva comanda';
$idioma['informacion_de_facturacion'] = 'Facturació';
$idioma['informacion_envio'] = 'Informació de direcció i enviament';
$idioma['pago'] = 'Pagament';
$idioma['informacion_pago'] = 'Selecciona métode de pagament';

$idioma['articulo'] = 'Article';
$idioma['cuando'] = 'Quan';
$idioma['cantidad'] = 'Quantitat';
$idioma['total'] = 'Total';
$idioma['subtotal'] = 'Subtotal';
$idioma['envio'] = 'Enviament';
$idioma['anterior'] = 'Anterior';
$idioma['siguiente'] = 'Seguent';
$idioma['codigo_descuento'] = 'Codi de descompte';
$idioma['introduce_codigo_descuento'] = 'Introdueix el teu cupó de descompte';
$idioma['aplicar_cupon'] = 'Aplicar cupó';
$idioma['calcular_envio'] = 'Calcular enviament';
$idioma['introduce_codigo_postal'] = 'Inserta es teu codi postal';
$idioma['actualizar_totales'] = 'Actualitzar totals';
$idioma['actualizar_carrito'] = 'Actualitzar';

$idioma['lotes_de_vinos_y_productos_mallorquines'] = 'Lots de vins i productes mallorquins';
$idioma['msg_vi_mallorqui'] = 'Vols tastar el vi mallorquí a casa?';
$idioma['msg_vi_mallorqui2'] = 'Selecció de vins mallorquins de cellers familiars i amb produccions limitades directament a ca teva amb enviament gratuit si vius a ses Illes Baleares';

$idioma['cookies_message'] = "Aquest lloc web utilitza galetes pròpies i de tercers, per a recopilar informació amb la finalitat de millorar els nostres serveis i mostrar publicitat relacionada amb les seves preferències, en base a un perfil elaborat a partir dels seus hàbits de navegació.";
$idioma['aceptar'] = 'Acceptar';
$idioma['llamar'] = 'Telefonar';

$idioma['contenido_carrito'] = 'Contingut de la cistella';

$idioma['plans_for_day'] = 'Plans per el día :day'; 
$idioma['plans_for_weekend'] = 'Plans para es cap de setmana';

$idioma['politica_cookies'] = 'Política de cookies';
$idioma['politica_privacidad'] = 'Polítiques de privacitat';

$idioma['msg_search'] = 'Cerca plans, cellers, llocs...';
$idioma['comprar_vino_mallorquin'] = 'Comprar vi mallorquí';
$idioma['tienda_de_vinos'] = 'Tenda de vins';
$idioma['vinos_de_mallorca-slug'] = 'vins-de-mallorca';

$idioma['msg_publica_evento'] = 'Publica gratis es teu event relacionat amb el vi a Mallorca';
$idioma['msg_publica_evento_btn'] = 'Publicar event';

$idioma['proximos_eventos'] = 'Pròxims events';
$idioma['eventos_con_vino_en_mallorca'] = 'Events amb vi a Mallorca';
$idioma['routes_mensaje_guias_vino'] = 'Informació i guíes del vi a Mallorca';
$idioma['routes_mensaje_titulo'] = 'Guíes i rutes de vi';
return $idioma;