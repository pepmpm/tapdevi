@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section ('body')

<!--=== Breadcrumbs ===-->
<div class="breadcrumbs">
    <div class="container">
        @if(isset($query))
            <h1 class="pull-left">Pack: {{$query->name}}</h1>
        @else
            <h1 class="pull-left">Nuevo pack</h1>
        @endif
    </div><!--/container-->
</div><!--/breadcrumbs-->
<div class="container content">
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('back.packs.index') }}" class="btn-u" role="button">
                Volver a la lista
            </a>
            <a href="{{ route('back.packs.create') }}" class="btn-u" role="button">
                Crear otro pack
            </a>
            @if(isset($query))
                <a href="{{ route('back.plans.additems', ['items_id' => $query->id]) }}" 
                   class="btn-u btn-u-green" role="button">
                    Empaquetar
                </a>
            @endif
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <form class="margin-bottom-20" id="form_item" role="form" method="post" enctype="multipart/form-data" 
                action="{{ isset($query) ? route('back.packs.update', ['event' => $query->id]) : route('back.packs.store') }}">
                @if(isset($query))
                    @method('PUT')
                @endif
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        @include('_layouts.messages', array('errors' => $errors))
                        @include('_back.packs.crud_info', array('query' => isset($query) ? $query : null))
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9" id="div_crud_langs">
                        @include('_back.packs.crud_langs', array('query' => isset($query) ? $query : null))
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">Acciones</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn-u btn-block margin-bottom-25">
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Status</label>
                                            {!! Form::select('status', $status_lov, 
                                                old('status', isset($query) ? $query->status: ''), 
                                                array('class' => 'form-control', 'id' => 'status')) !!}
                                        </div>
                                    </div>
                                </div>
                                <div id="div_crud_photos"> 
                                    @include('_back.packs.crud_photos', array('query' => isset($query) ? $query : null))
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section ('assets_end')
    <script src="{{ asset('assets/plugins/redactor/redactor.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/plugins/redactor/redactor.css') }}">
    <script src="{{ asset('assets/plugins/redactor/lang/es.js') }}"></script>
    <script src="{{ asset('assets/plugins/redactor/plugins/read-more-plugin.js') }}"></script>

    <script src="{{ asset('assets/plugins/fancybox/source/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/plugins/fancybox/source/jquery.fancybox.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/plugins/jquery-upload-file/uploadfile.css') }}">
    <script src="{{ asset('assets/plugins/jquery-upload-file/jquery.uploadfile.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            @if(isset($query))
                $('#btn_translate').on('click', function(event) {
                    $.get(ARNY.baseUrl + '/back/packs/translate/{{ $query->id }}',
                        function(data){
                            $('#div_crud_langs').html(data);
                            if($('.redactor').length){
                                $('.redactor').redactor({
                                    minHeight: 400,
                                    lang: 'es',
                                    plugins: ['leermas']
                                });
                            }
                        }
                    );
                    event.preventDefault();
                });
            @endif
            $("#fileuploader").uploadFile({
                url:"{{ route('back.packs.photos.store') }}",
                fileName:"p_file",
                onSuccess:function(files,data,xhr,pd){
                    $('#form_item').append('<input type="hidden" name="p_foto[]" value="'+data+'" />');
                }
            });
            if($('.redactor').length){
                $('.redactor').redactor({
                    minHeight: 400,
                    lang: 'es',
                    plugins: ['leermas']
                });
            }
        });
    </script>

    <script type="text/javascript">
        $(function(){
            if($(".fancybox").length){
                jQuery(".fancybox").fancybox({
                    groupAttr: 'data-rel',
                    prevEffect: 'fade',
                    nextEffect: 'fade',
                    openEffect  : 'elastic',
                    closeEffect  : 'fade',
                    closeBtn: true,
                    helpers: {
                        title: {
                            type: 'float'
                        }
                    }
                });
            }
        });
    </script>
    <style type="text/css">
        .btn-u-sm, a.btn-u-sm {
            padding: 5px 12px;
        }
    </style>
@stop
