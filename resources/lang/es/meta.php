<?php

$idioma = array();

$idioma['author'] = 'Pep Manel Palmer';
$idioma['title_default'] = 'Enoturismo en Mallorca • Compra vino mallorquín • Productos gourmet';

//index
$idioma['title_index'] = 'Enoturismo en Mallorca ✔️ Tours de vino ✔️ Disfruta de experiencias con vino mallorquín';
$idioma['desc_index'] = 'Visitas a bodegas, actividades relacionadas con el vino mallorquín, eventos, talleres, cursos... Conoce y disfruta del vino de Mallorca. Más de 100 bodegas por conocer.';
$idioma['keys_index'] = 'enoturismo mallorca, experiencias vino mallorca, visitas bodegas mallorca, cata vino mallorca, cata vino santa maria, cata vino binissalem';

//event.index
$idioma['title_index_event'] = 'Enoturismo en Mallorca ✔️ Eventos con vino en Mallorca ✔️ Fires, maridajes, catas y actividades con vino';
$idioma['desc_index_event'] = 'El enoturismo es un sector creciente en Mallorca con cada vez más bodegas, más ferias del vino y más actividades en bodegas o restaurantes. En Tapdevi te ofrecemos una agenda de actividades completa para que no te pierdas ningún plan.';
$idioma['keys_index_event'] = 'vino Mallorca, enoturismo Mallorca, experiencias vino Mallorca, turismo vino Mallorca, eventos vino Mallorca, fires vino mallorca, maridajes vino mallorca';

//event.map
$idioma['title_map_event'] = 'Enoturismo en Mallorca ✔️ Eventos con vino en Mallorca ✔️ Mapa de eventos con vino en Mallorca';
$idioma['desc_map_event'] = 'Echa un vistazo al mapa de actividades y eventos relacionados con vino en Mallorca. Rutas del vino en Mallorca. De esta forma no te perderás ninguna fira, ningún maridaje, ni ninguna actividad que te pueda interesar.';
$idioma['keys_map_event'] = 'mapa vino Mallorca, mapa enoturismo Mallorca, mapa experiencias vino Mallorca, mapa turismo vino Mallorca, eventos vino Mallorca, fires vino mallorca, maridajes vino mallorca';

//plan.index
$idioma['title_index_plan'] = 'Enoturismo en Mallorca ✔️ Tours de vino en Mallorca ✔️ Disfruta de experiencias con vino de Mallorca';
$idioma['desc_index_plan'] = 'No te pierdas los tours de enoturismo que te llevarán por bodegas familiares y por los restaurantes donde vamos los residentes en Mallorca. Disfrutarás de nuestros excelentes vinos en parajes naturales que te enamorarán';
$idioma['keys_index_plan'] = 'vino Mallorca, enoturismo Mallorca, experiencias vino Mallorca, turismo vino Mallorca, tours vino Mallorca';

//plan.date.today
$idioma['title_index_plan_date_day'] = 'Planes de enoturismo para el día :day ✔️ Enoturismo en Mallorca ✔️ Tours de vino en Mallorca ✔️ Disfruta de experiencias con vino de Mallorca';
$idioma['desc_index_plan_date_day'] = 'Si no tienes planes para el día :day y te gusta el vino entra y elige el plan que más te guste para aprovechar el día. No te pierdas los tours de enoturismo que te llevarán por bodegas familiares y por los restaurantes donde vamos los residentes en Mallorca';
$idioma['keys_index_plan_date_day'] = 'vino Mallorca, planes para :day, enoturismo Mallorca, experiencias vino Mallorca :day, turismo vino Mallorca, tours vino Mallorca :day';

//plan.date.weekend
$idioma['title_index_plan_date_weekend'] = 'Planes de enoturismo para el fin de semana ✔️ Enoturismo en Mallorca ✔️ Tours de vino en Mallorca ✔️ Disfruta de experiencias con vino de Mallorca';
$idioma['desc_index_plan_date_weekend'] = 'Planes para el fin de semana desde 12€ en bodegas, restaurantes, ferias, barcos, en el campo, en fincas rústicas... el vino no entiende de lugares, sino de momentos. Si no tienes planes para el fin de semana nosotros de damos ideas.';
$idioma['keys_index_plan_date_weekend'] = 'vino Mallorca fin de semana, planes para fin de semana, enoturismo Mallorca, experiencias vino Mallorca fin de semana, turismo vino Mallorca, tours vino Mallorca fin de semana';

$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';

return $idioma;