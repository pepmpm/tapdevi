<?php

namespace App\Http\Controllers\PlansStocks;

use App\Items;
use App\ItemsStocks;
use Illuminate\Http\Request;

class PlansStocksPanelController extends PlansStocksController
{

    /**
     * Display the specified resource.
     *
     * @param  \App\PlansStocks  $plan
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Items $items_id)
    {
        $this->data['query'] = $items_id;
        return view('_back.plans.crud_stocks', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query = parent::_store($request->all());

        if(!($query instanceof Items)){
            return $query;
        }

        return redirect()->route('back.plans.stocks.show', array('item' => $query->id))
            ->with('message_success', 'Cambios guardados correctamente');
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Plans  $plans
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemsStocks $stock)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Plans  $plans
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemsStocks $stock)
    {
        $items_id = $stock->items_id;
        parent::_destroy($stock);
        return redirect()->route('back.plans.stocks.show', array('item' => $query->id))
            ->with('message_success', 'Cambios guardados correctamente');
    }
}
