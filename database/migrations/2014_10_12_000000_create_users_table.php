<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('slug', 255);
            $table->string('email', 255)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password', 100);
            $table->enum('role', ['admin', 'moderator', 'company', 'user'])->default('user');

            $table->string('company_name', 250)->nullable();

            $table->unsignedInteger('locations_id')->nullable();
            $table->foreign('locations_id')->references('id')->on('locations');

            $table->unsignedInteger('districts_id')->nullable();
            $table->foreign('districts_id')->references('id')->on('districts');

            $table->string('address', 200)->nullable();
            $table->string('postal_code', 15)->nullable();
            $table->string('phone', 20)->nullable();

            $table->string('locale', 2)->default('es');


            $table->string('name_billing', 255)->nullable();

            $table->unsignedInteger('locations_id_billing')->nullable();
            $table->foreign('locations_id_billing')->references('id')->on('locations');

            $table->unsignedInteger('districts_id_billing')->nullable();
            $table->foreign('districts_id_billing')->references('id')->on('districts');

            $table->string('address_billing', 200)->nullable();
            $table->string('postal_code_billing', 15)->nullable();
            $table->string('phone_billing', 20)->nullable();


            $table->string('longitude', 30)->nullable();
            $table->string('latitude', 30)->nullable();
            $table->string('website', 100)->nullable();
            $table->string('logo', 255)->nullable();

            $table->float('fee', 5, 2)->nullable();

            $table->index('locations_id');
            $table->index('slug');
            $table->index(['longitude', 'latitude']);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
