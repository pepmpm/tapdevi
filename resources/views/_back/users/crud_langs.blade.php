<div class="panel panel-default">
    <div class="panel-heading">Textos</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                @php($looplangs = isset($query) ? $query->langs : config('translatable.locales'))
                <div class="tab-v2">
                    <ul class="nav nav-tabs">
                        @foreach($looplangs as $keylocale => $lang)
                            @php($keylang = isset($query) ? $lang->locale : $keylocale)
                            @php($valuelang = isset($query) ? config('translatable.locales')[$lang->locale] : $lang)
                            <li class="{{ Request::get('tab') == 'langs' && Request::get('idioma') == $keylang ? 'active' : (!Request::has('idioma') && $loop->first ? 'active' : '')}}">
                                <a href="#{{ $keylang }}" data-toggle="tab">{{ $valuelang }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content bg-color-white">
                        @foreach($looplangs as $keylocale => $lang)
                            @php($keylang = isset($query) ? $lang->locale : $keylocale)
                            @php($valuelang = isset($query) ? config('translatable.locales')[$lang->locale] : $lang)
                            <div class="tab-pane fade in {{ Request::get('tab') == 'langs' && Request::get('idioma') == $keylang ? 'active' : (!Request::has('idioma') && $loop->first ? 'active' : '') }}"
                                id="{{ $keylang }}">
                                <input type="hidden" name="id[{{ $keylang }}]" value="{{ isset($query) ? $lang->id : '' }}"/>
                                <input type="hidden" name="locale[{{ $keylang }}]" value="{{ $keylang }}"/>
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Title</label>
                                                    <input type="text" class="form-control {{ $errors->has('title.' . $keylang) ? 'border-rojo' : "" }}"
                                                        name="title[{{ $keylang }}]" placeholder="Title"
                                                        value="{{ old('title.' . $keylang, (isset($query) ? $lang->title : '')) }}"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Descripción</label>
                                                    <textarea class="redactor {{ $errors->has('description.' . $keylang) ? 'border-rojo' : "" }}" 
                                                        name="description[{{ $keylang }}]">{{ old('description.' . $keylang, (isset($query) ? $lang->description : '')) }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Seo tags</label>
                                                    <textarea class="form-control {{ $errors->has('seo_keywords.'.$keylang) ? 'border-rojo' : "" }}" 
                                                        name="seo_keywords[{{ $keylang }}]" 
                                                        rows="5">{{ old('seo_keywords.'.$keylang, (isset($query) ? $lang->seo_keywords : '')) }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Seo descripción</label>
                                                    <textarea class="form-control {{ $errors->has('seo_description.'.$keylang) ? 'border-rojo' : "" }}" 
                                                        name="seo_description[{{ $keylang }}]" 
                                                        rows="8">{{ old('seo_description.'.$keylang, (isset($query) ? $lang->seo_description : '')) }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>