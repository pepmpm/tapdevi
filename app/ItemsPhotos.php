<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemsPhotos extends _BaseModel{

	protected $fillable = array('order', 'url', 'origin', 'items_id');

    public function __construct(array $attributes = array()) {
        parent::__construct($attributes);
    }

    public function item(){
        return $this->belongsTo('\App\Items', 'items_id');
    }

}
