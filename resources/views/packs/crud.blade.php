@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section('title', $query->name)
@section ('meta')
    <meta name="description" content="{{ $query->seo_description }}"/>
    <meta name="keywords" content="{{ $query->seo_keywords }}" />
    <meta name="Author" content="{{ trans('meta.author') }}" />
    <meta property="og:title" content="{{ $query->name }}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:image" content="{{ url('/') . '/' . $query->one_photo('upload') }}"/>
    <meta property="og:description" content="{{ $query->seo_description }}"/>
    @parent
@stop

@section ('body')
@php($tmpdistrict = $query->user->district)
<div class="shop-product">
    @if($agent->isMobile())
        <img class="img-responsive" alt="{{ $query->name }}"
            src="{{ Imgfly::imgPublic(str_replace("upload/", "", $photo->url) . 
                '?w=344&h=350&fit=crop-center', 'upload') }}">
    @else
        <div class="breadcrumbs-v1 text-center" 
            style="background: url('{{ url('/') . '/' . $query->one_photo('upload') }}') no-repeat scroll center center / cover">
    @endif
        <div class="container">
            <span>Blog Item Page</span>
            <h1>Basic Item Page</h1>
        </div>
    </div>

    <div class="container margin-top-30">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <h2>{{ $query->name }}</h2>
                    </div>
                </div>
                @if($query->type == 'plan')
                    <ul class="list-inline add-to-wishlist add-to-wishlist-brd">
                        <li class="compare-in">
                            <i class="fa fa-glass"></i>
                            <a href="{{ route('enotourism-mallorca.user', ['district' => $tmpdistrict->slug, 'user' => $query->user->slug]) }}">
                                {{ $query->user->name }}
                            </a>
                        </li>
                        <li class="compare-in">
                            <i class="fa fa-map-pin"></i>
                            <a href="{{ route('enotourism-mallorca.dis', ['district' => $tmpdistrict->slug]) }}">
                                {{ $tmpdistrict->name }}
                            </a>
                        </li>
                    </ul>
                @endif

                <p><strong>{{ trans('plan.detallado') }}</strong></p>
                {!! $query->description !!}


                @if($query->timetable != null)
                    <hr/>
                    <p><strong>{{ trans('plan.informacion_del_horario') }}</strong></p>
                    {!! $query->timetable !!}
                @endif

                @if($query->advance_days != null)
                    @php($has_advance_days = true)
                    <hr/>
                    <p>
                        <strong><i class="fa fa-calendar-check-o"></i>{{ " " . trans('plan.reserva_con_antelacion') . ": " }}</strong>{{ $query->advance_days . " " . trans('plan.dias') }}
                    </p>
                @endif

                @if($query->cancellation_days != null)
                    @if(isset($has_advance_days) && $has_advance_days)
                        <hr/>
                    @endif
                    <p>
                        <strong><i class="fa fa-thumbs-o-up"></i>{{ " " . trans('plan.cancela_con_antelacion') . ": " }}</strong>{{ $query->cancellation_days . " " . trans('plan.dias') }}
                    </p>
                @endif

                @if($query->duration != null)
                    <hr/>
                    <p>
                        <strong><i class="fa fa-clock-o"></i>{{ " " . trans('plan.duracion') . ": " }}</strong>{{ $query->duration . " h" }}
                    </p>
                @endif

                @if($query->is_apto_pets == 'S')
                    <hr/>
                    <p>
                        <strong><i class="fa fa-paw"></i>{{ " " . trans('plan.apto_mascotas') }}</strong>
                    </p>
                @endif

                @if($query->is_apto_children == 'S')
                    <hr/>
                    <p>
                        <strong><i class="fa fa-child"></i>{{ " " . trans('plan.apto_ninos') }}</strong>
                    </p>
                @endif

                @if($query->is_apto_disabled == 'S')
                    <hr/>
                    <p>
                        <strong><i class="fa fa-wheelchair"></i>{{ " " . trans('plan.accesible_silla_ruedas') }}</strong>
                    </p>
                @endif

                @if($query->is_has_parking == 'S')
                    <hr/>
                    <p>
                        <strong><i class="fa fa-car"></i>{{ " " . trans('plan.parking_propio') }}</strong>
                    </p>
                @endif
                @include('plans._admin_buttons', ['plan' => $query])
                @if($query->confirmation != 'none')
                    @include('plans._book_panel', ['query' => $query])
                @endif
            </div>
            <div class="col-md-6">
                
                @php($count_packs = $query->packs->count())
                @if($count_packs > 0)
                    <h3 class="margin-top-30">{{ trans_choice('item.mensaje_elegir_plan_regalo', $count_packs, ['planes' => $count_packs]) }}</h3>
                    <div class="row news-v1 margin-bottom-20">
                        @foreach($query->packsitems as $pack)
                            @include('packs.element', ['pack' => $pack, 'col' => '6'])
                            @if($loop->iteration % 2 == 0)
                                </div>
                                <div class="row news-v1 margin-bottom-20"> 
                            @endif
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@stop

@section ('assets_end')
    <link rel="stylesheet" href="{{ asset('assets/plugins/owl-carousel2/assets/owl.carousel.css') }}">
    <script src="{{ asset('assets/plugins/owl-carousel2/owl.carousel.min.js') }}"></script>
    
    <!--link rel="stylesheet" href="{{ asset('assets/plugins/romedatepicker/rome.css') }}">
    <script src="{{ asset('assets/plugins/romedatepicker/rome.js') }}"></script-->

    <script type="text/javascript" src="{{ asset('js/plans.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            /*rome(
                date_event, 
                { 
                    time: false, 
                    weekStart: 1, 
                    inputFormat: 'DD/MM/YYYY',
                    dateValidator: rome.val.only([
                        @foreach(array_keys($query->stocks_array()) as $dateValidator)
                            '{{ $dateValidator }}',
                        @endforeach
                    ])
                }
            ).on('data', function (value) {
                $.post(ARNY.baseUrl + '/plans/stocks/get_time', 
                    {date: value, items_id: '{{ $query->id }}' },
                    function(data){
                        $('#div_time').html(data);
                    }
                );
            });*/
        });
    </script>
    @include('google_events', ['query' => $query])
@stop