<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Helpers;

class Coupons extends Model
{

	use Helpers;

    public function orders(){
        return $this->hasMany('\App\Orders', 'coupons_id')->orderBy('id', 'asc');
    }
}
