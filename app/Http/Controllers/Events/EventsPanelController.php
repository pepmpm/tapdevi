<?php

namespace App\Http\Controllers\Events;

use App\Items;
use Image;
use Illuminate\Http\Request;

class EventsPanelController extends EventsController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['query'] = parent::_index([]);
        if(\Auth::user()->isAdmin()){
            $this->data['users_lov'] = \App\User::whereHas('plans', function ($query) {
                    $query->where('status', '=', 'active');
                })->pluck('name', 'id')->all();
        }
        $this->data['labels_lov'] = \App\Labels::whereHas('plans', function ($query) {
                $query->where('status', '=', 'active');
            })->listsTranslations('title')->pluck('title', 'id')->all();
        $this->data['status_lov'] = Items::getPossibleEnumValues('status');
        return view('_back.events.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['users_lov'] = \App\User::where('role', '=', 'company')->pluck('name', 'id')->all();
        $this->data['districts'] = \App\Districts::pluck('name', 'id')->all();
        $this->data['labels_lov'] = \App\Labels::listsTranslations('title')->where('category', '=', 'event')
            ->pluck('title', 'id')->all();
        $this->data['yesno_lov'] = Items::getPossibleEnumValues('is_has_parking');
        $this->data['status_lov'] = Items::getPossibleEnumValues('status');
        return view('_back.events.crud', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query = parent::_store($request->all());

        if(!($query instanceof Items)){
            return $query;
        }

        return redirect()->route('back.events.show', array('item' => $query->id))
            ->with('message_success', 'Cambios guardados correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function show(Items $event)
    {
        /*$this->data['stocks'] = \App\ItemsStocks::where('items_id', '=', $plan->id)
            ->whereIn('year', [date('Y'), date('Y') + 1])
            ->orderBy('year', 'asc')->orderBy('month', 'asc')->get();*/

        $this->data['users_lov'] = \App\User::where('role', '=', 'company')->pluck('name', 'id')->all();
        $this->data['districts'] = \App\Districts::pluck('name', 'id')->all();
        $this->data['labels_lov'] = \App\Labels::listsTranslations('title')->pluck('title', 'id')->all();
        $this->data['status_lov'] = Items::getPossibleEnumValues('status');
        $this->data['yesno_lov'] = Items::getPossibleEnumValues('is_has_parking');
        $this->data['query'] = parent::_show($event);
        return view('_back.events.crud', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Items $event)
    {
        $query = parent::_update($request->all(), $event);

        if(!($query instanceof Items)){
            return $query;
        }

        return redirect()->route('back.events.show', array('plan' => $query->id))
            ->with('message_success', 'Cambios guardados correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Items $event)
    {
        parent::_destroy($event);
        return redirect()
            ->route('back.events.index')
            ->with('message_success', 'Cambios guardados correctamente');
    }

    public function translate(Request $request, Items $item){
        //preparamos el array de idiomas
        $array = array();
        foreach(config('translatable.locales') as $keylocale => $locale){
            $itemLang = $item->getTranslation('name', $keylocale);

            $array['name'] = $itemLang->name;
            $array['locale'][$keylocale] = $keylocale;
            $array['slug'][$keylocale] = '';
            $array['description'][$keylocale] = $itemLang->locale == $keylocale ? $itemLang->description : '';
            $array['seo_description'][$keylocale] = $itemLang->locale == $keylocale ? $itemLang->seo_description : '';
            $array['seo_keywords'][$keylocale] = $itemLang->locale == $keylocale ? $itemLang->seo_keywords : '';
        }

        parent::_insert_langs($array, $item);
        $this->data['query'] = parent::_show($item);
        return view('_back.events.crud_langs', $this->data);
    }

    public function instagram_get(Request $request, Items $item){
        $urlhere = $item->instagram_url;

        //remove white space
        $urlhere = trim($urlhere);
        $urlhere = rtrim($urlhere, '/');
        $urlhere = htmlspecialchars($urlhere);
        ///remove white space

        $images = array();
        if ($this->get_domain($urlhere) == "instagram.com") {
            //intento insertar desde el json
            $json = $this->file_get_contents_curl($urlhere . '/?__a=1');
            $content = json_decode($json, true);
            if(isset($content['graphql']['shortcode_media']['display_resources'])){
                foreach($content['graphql']['shortcode_media']['display_resources'] as $element){
                    self::_store_instagram_image($item, $element['src']);
                }
            }else{
                //si instagram ya me ha baneado me descargo la primera imagen
                self::_store_instagram_image($item, $urlhere . '/media/?size=l');
            }
        }
        $this->data['query'] = parent::_show($item);
        return view('_back.events.crud_photos', $this->data);
    }

    private function _store_instagram_image($item, $image){
        $ruta = 'upload/plans/'.$item->id.'/';
        \File::makeDirectory($ruta, 0775, true, true);

        $tmp = new \App\ItemsPhotos;
        $tmp->order = 100;
        $tmp->items_id = $item->id;
        $tmp->save();

        $tmp->url = $ruta . $item->slug . "-" . $tmp->id . ".webp";
        $tmp->save();

        $img = Image::make($image)->encode('webp', 90);
        $img->resize(1024, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($tmp->url);
        $this->set_72dpi_image($tmp->url);
    }

    private function file_get_contents_curl($url){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    } 

    private function get_domain($url){
        $pieces = parse_url($url);
        $domain = isset($pieces['host']) ? $pieces['host'] : $pieces['path'];
        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
            return $regs['domain'];
        }
        return false;
    }
}
