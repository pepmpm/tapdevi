@php($tmpuser = $plan->user)
<div class="col-md-{{ isset($col) ? $col : '3' }} text-center">
    <div class="news-v1-in bgcolor-light-grey">
        <a href="{{ route('enotourism-mallorca.item', 
            ['district' => $tmpuser->district->slug, 'user' => $tmpuser->slug, 'item' => $plan->slug]) }}">
            @if($agent->isMobile())
                <img class="img-responsive" alt="{{ $plan->name }}"
                    src="{{ Imgfly::imgPublic(str_replace("upload/", "", $plan->one_photo()) . '?w=407&h=260&fit=crop-center', 
                        'upload') }}">
            @else
                <img class="img-responsive" alt="{{ $plan->name }}"
                    src="{{ Imgfly::imgPublic(str_replace("upload/", "", $plan->one_photo()) . '?w=302&h=280&fit=crop-center', 
                        'upload') }}">
            @endif
            @if($plan->is_promoted == 'S')
                <p class="tdv-badge font12">
                    <strong>
                        <span><i class="fa fa-star"></i>&nbsp;{{ trans('inicio.destacado') }}</span>
                    </strong>
                </p>
            @endif
        </a>
        <h3 class="font-normal">
            <a href="{{ route('enotourism-mallorca.item', 
                ['district' => $tmpuser->district->slug, 'user' => $tmpuser->slug, 'item' => $plan->slug]) }}">
                <span>{{ $plan->name }}</span>
            </a>
        </h3>
        <p>
            <small>
                <a href="{{ route('enotourism-mallorca.user', 
                    ['district' => $tmpuser->district->slug, 'user' => $tmpuser->slug]) }}">
                    {{ $tmpuser->name }}
                </a>
            </small>
        </p>
        @include('plans._admin_buttons', ['plan' => $plan])
        <div class="row">
            <div class="col-md-12 text-center">
                <strong>
                    <span class="font34">{{ $plan->trueprice . "€" }}</span>
                    @if($plan->new_price < $plan->price && $plan->new_price > 0)
                        <span class="tachado font34">{{ $plan->price . "€" }}</span>
                    @endif
                </strong>
                {{ trans('plan.por_persona') }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="{{ route('enotourism-mallorca.item', 
                    ['district' => $tmpuser->district->slug, 'user' => $tmpuser->slug, 'item' => $plan->slug]) }}" 
                   class="btn-u btn-u-lg" role="button">
                    + Info
                </a>
            </div>
        </div>
        <ul class="list-inline news-v1-info no-margin-bottom margin-top-10 border-top-tdv">
            <li>
                <i class="fa fa-map-pin color-tdv width14"></i>
                <a href="{{ route('enotourism-mallorca.dis', 
                    ['district' => $tmpuser->district->slug]) }}">
                    {{ $tmpuser->district->name }}
                </a>
            </li>
            <li>|</li>
            <li>
                <i class="fa fa-male color-tdv width14" aria-hidden="true"></i>&nbsp;
                {{ 'Min ' . $plan->min_pax }}
                @if($plan->max_pax != null)
                    {{ ' - Max ' . $plan->max_pax }}
                @endif
            </li>
        </ul>
    </div>
</div>

