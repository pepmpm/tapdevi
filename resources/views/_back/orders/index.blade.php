@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section ('body')

<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left">Pedidos</h1>
    </div>
</div>
<div class="container content profile">
    <div class="row">
        <div class="col-md-12">
            <a href="{{route('back.orders.create')}}" 
               class="btn-u btn-u-lg" role="button">
                Crear pedido
            </a>
        </div>     
    </div>
    <hr/>
    <h2>Reservas pendientes de pagar</h2>
    <div class="row">
        <div class="col-md-12">
            <table class="table display" id="tabla">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Código</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Lugar</th>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Precio</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($query as $order)
                        @if($order->date_visited != null && $order->date_paid == null)
                        <tr>
                            <td>{{ $order->id }}</td>
                            <td>
                                <a href="{{ route('back.orders.show', array('id' => $order->id)) }}">
                                    {{ $order->code }}
                                </a>
                            </td>
                            <td>{{ $order->name }}</td>
                            <td>{{ $order->email }}</td>
                            <td>{{ $order->status }}</td>
                            <td>{{ $order->item != null && $order->item->user != null ? $order->item->user->name : '' }}</td>
                            <td>{{ $order->item->name }}</td>
                            <td>{{ $order->quantity }}</td>
                            <td>{{ $order->price . " €" }}</td>
                            <td>
                                <a href="{{ route('back.orders.show', array('id' => $order->id)) }}" 
                                   class="btn btn-primary btn-sm pull-left" role="button">
                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                </a>
                                <form action="{{ route('back.orders.destroy', [$order->id]) }}" method="POST" 
                                    onsubmit="if(!confirm('¿Estás seguro de querer borrar el pedido?')) return false"
                                    class="col-md-4">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-danger btn-sm" type="submit">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <h2>Todas las reservas</h2>
    <div class="row">
        <div class="col-md-12">
            <table class="table display" id="tabla2">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Código</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Lugar</th>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Precio</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($query as $order)
                        <tr>
                            <td>{{ $order->id }}</td>
                            <td>
                                <a href="{{ route('back.orders.show', array('id' => $order->id)) }}">
                                    {{ $order->code }}
                                </a>
                            </td>
                            <td>{{ $order->name }}</td>
                            <td>{{ $order->email }}</td>
                            <td>{{ $order->status }}</td>
                            <td>{{ $order->item != null && $order->item->user != null ? $order->item->user->name : '' }}</td>
                            <td>{{ $order->item->name }}</td>
                            <td>{{ $order->quantity }}</td>
                            <td>{{ $order->price . " €" }}</td>
                            <td>
                                <a href="{{ route('back.orders.show', array('id' => $order->id)) }}" 
                                   class="btn btn-primary btn-sm pull-left" role="button">
                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                </a>
                                <form action="{{ route('back.orders.destroy', [$order->id]) }}" method="POST" 
                                    onsubmit="if(!confirm('¿Estás seguro de querer borrar el pedido?')) return false"
                                    class="col-md-4">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-danger btn-sm" type="submit">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@stop

@section ('assets_end')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/datatables.min.css') }}">
    <script src="{{ asset('assets/plugins/datatables/datatables.min.js') }}"></script>
    
    <script type="text/javascript">
        function format ( d ) {
            $('.table'+d[1]).show();
            return $('.last-td'+d[1]).html();
        }
         
        $(document).ready(function() {
            var table = $('#tabla').DataTable({
                "language": {
                    "url": ARNY.baseUrl + "/assets/plugins/datatables/spanish.lang"
                },
                stateSave: true,
                "order": [[ 1, "desc" ]]
            });
            var table2 = $('#tabla2').DataTable({
                "language": {
                    "url": ARNY.baseUrl + "/assets/plugins/datatables/spanish.lang"
                },
                stateSave: true,
                "order": [[ 1, "desc" ]]
            });
        } );
    </script>
    <style type="text/css">
        td.details-control {
            background: url('{{ asset('assets/img/icons/details_open.png') }}') no-repeat center center;
            cursor: pointer;
        }
        tr.shown td.details-control {
            background: url('{{ asset('assets/img/icons/details_close.png') }}') no-repeat center center;
        }  
    </style>

@stop
