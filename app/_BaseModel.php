<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class _BaseModel extends Model {

    public static function boot() {
        parent::boot();

        static::saving(function($model) {
            foreach ($model->toArray() as $name => $value) {
                if (empty($value)) {
                    $model->{$name} = null;
                }
            }
            return true;
        });
    }

}