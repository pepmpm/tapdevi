@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section ('body')

<!--=== Breadcrumbs ===-->
<div class="breadcrumbs">
    <div class="container">
        @if(isset($query))
            <h1 class="pull-left">Usuario: {{ $query->name }}</h1>
        @else
            <h1 class="pull-left">Nuevo usuario</h1>
        @endif
    </div><!--/container-->
</div><!--/breadcrumbs-->
<div class="container content">
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('back.users.index') }}" 
               class="btn-u btn-u-lg" role="button">
                Volver a la lista
            </a>
            <a href="{{ route('back.users.create') }}" 
               class="btn-u btn-u-lg" role="button">
                Crear otro usuario
            </a>
        </div>     
    </div>
    <hr/>
    <form class="margin-bottom-20" id="form_item" role="form" method="post" enctype="multipart/form-data" autocomplete="off" 
        action="{{ isset($query) ? route('back.users.update', $query->id) : route('back.users.store') }}">
        @if(isset($query))
            @method('PUT')
        @endif
        @csrf
        <div class="row">
            <div class="col-md-8">
                @include('_layouts.messages', array('errors' => $errors))
                <div class="panel panel-default">
                    <div class="panel-heading">Información</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" class="form-control" name="{{ isset($query) ? 'name' : 'users_id' }}" 
                                        placeholder="Nombre" id="name" 
                                        value="{{ old(isset($query) ? 'name' : 'users_id', isset($query) ? $query->name : '') }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Provincia</label>
                                    {!! Form::select('locations_id', array('' => '') + $locations, 
                                        old('locations_id', isset($query) ? $query->locations_id : ''), 
                                        array('class' => 'form-control locations_id', 'id' => 'locations_id')) !!}
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Población</label>
                                    {!! Form::select('districts_id', array('' => '') + (isset($districts) ? $districts : []), 
                                        old('districts_id', isset($query) ? $query->districts_id : ''), 
                                        array('class' => 'form-control districts_id', 'id' => 'districts_id' )) !!}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Rol</label>
                                    {!! Form::select('role', $roles, old('role', isset($query) ? $query->role : ''), 
                                        array('class' => 'form-control role', 'id' => 'role')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Dirección</label>
                                    <input type="text" class="form-control" name="address" id="address" placeholder="Dirección"
                                        value="{{ old('address', isset($query) ? $query->address : '') }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>C. postal</label>
                                    <input type="text" class="form-control" name="postal_code" id="postal_code" placeholder="C.P."
                                        value="{{ old('postal_code', isset($query) ? $query->postal_code : '') }}"/>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Teléfono</label>
                                    <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone"
                                        value="{{ old('phone', isset($query) ? $query->phone : '') }}"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" name="email" id="email" placeholder="Email"
                                        value="{{ old('email', isset($query) ? $query->email : '') }}"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Web</label>
                                    <input type="text" class="form-control" name="website" id="website" placeholder="Web"
                                        value="{{ old('website', isset($query) ? $query->website : '') }}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Acciones</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn-u btn-block margin-bottom-25">
                                    Guardar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                @if(isset($query))
                    <div class="panel panel-default">
                        <div class="panel-heading">Geoposicionamiento</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Latitud</label>
                                        <input type="text" class="form-control" name="latitude" id="latitud" placeholder="Latitud"
                                                value="{{ old('latitude', isset($query) ? $query->latitude: '') }}"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Longitud</label>
                                        <input type="text" class="form-control" name="longitude" id="longitud" placeholder="Longitud"
                                                value="{{ old('longitude', isset($query) ? $query->longitude: '') }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Localización</label>
                                    <button type="button" id="btn_refresh_map" class="btn btn-u margin-bottom-5">
                                        <i class="fa fa-refresh"></i>
                                        Actualizar mapa
                                    </button>
                                    <div id="map" class="height350"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </form>
</div>
@stop

@section ('assets_end')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBw_spE5_xC8pjOBJVWs5LHLBKn0EC-cfQ"></script>
    <script src="{{ asset('assets/plugins/gmap/gmap.js') }}"></script>

    <script src="{{ asset('assets/plugins/select2/dist/js/select2.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/dist/css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/dist/css/select2-bootstrap.css') }}">

    <script type="text/javascript">
        var map;
        var marker;
        $(document).ready(function() {
            if($('#map').length){
                map = new GMaps({
                    div: '#map',
                    scrollwheel: false,             
                    lat: 40.416775,
                    lng: -3.703790,
                    zoom: 5
                });
                if($("#latitud").val() && $("#longitud").val()){
                    postGeocoords(map, $("#longitud").val(), $("#latitud").val());
                }

                $("#btn_refresh_map").click(function () {
                    postGeocode(map);
                });
            }
        });
        $(function(){
            $(".locations_id").select2({theme: "bootstrap", selectOnClose: true});
            $(".districts_id").select2({tags: true, theme: "bootstrap", selectOnClose: true});
            $(document).on('focus', '.select2', function() {
                $(this).siblings('select').select2('open');
            });

            $( ".locations_id" ).change(function(event){
                $.post(ARNY.baseUrl + '/back/locations/get_districts', 
                    {locations_id: $(this).val()},
                    function(data){
                        var options = $('.districts_id');
                        options.empty();
                        $.each(JSON.parse(data), function(i, it) {
                            options.append('<option value="' + it.id + '">'+ it.text +'<option/>');
                        });

                    }
                );
            });

            $(".locations_id_billing").select2({theme: "bootstrap", selectOnClose: true});
            $(".districts_id_billing").select2({tags: true, theme: "bootstrap", selectOnClose: true});

            $( ".locations_id_billing" ).change(function(event){
                $.post(ARNY.baseUrl + '/back/locations/get_districts', 
                    {locations_id: $(this).val()},
                    function(data){
                        var options = $('.districts_id_billing');
                        options.empty();
                        $.each(JSON.parse(data), function(i, it) {
                            options.append('<option value="' + it.id + '">'+ it.text +'<option/>');
                        });

                    }
                );
            });

        });
        function postGeocoords(mapa, longitud, latitud){
            mapa.setCenter(latitud, longitud);
            mapa.setZoom(15);
            if(marker == null){
                marker = mapa.addMarker({
                    lat: latitud,
                    lng: longitud,
                    draggable: true
                });
            }else{
                marker.setPosition( new google.maps.LatLng( latitud, longitud ) );
                mapa.panTo( new google.maps.LatLng( latitud, longitud ) );
            }

            $("#latitud").val(latitud);
            $("#longitud").val(longitud);

            google.maps.event.addListener(
                marker,
                'drag',
                function() {
                    $("#latitud").val(marker.position.lat());
                    $("#longitud").val(marker.position.lng());
                }
            );
        }

        function postGeocode(mapa){
            var direccion = $("#name").val() + " " + $("#address").val();
            direccion += " "  + $("#postal_code").val() + " "  + $("#locations_id option:selected").text();
            direccion += " "  + $("#districts_id option:selected").text();
            GMaps.geocode({
                address: direccion,
                callback: function(results, status) {
                    if (status == 'OK') {
                        var latlng = results[0].geometry.location;
                        mapa.setCenter(latlng.lat(), latlng.lng());
                        mapa.setZoom(15);
                        if(marker == null){
                            marker = mapa.addMarker({
                                lat: latlng.lat(),
                                lng: latlng.lng(),
                                draggable: true
                            });
                        }else{
                            marker.setPosition( new google.maps.LatLng( latlng.lat(), latlng.lng() ) );
                            mapa.panTo( new google.maps.LatLng( latlng.lat(), latlng.lng() ) );
                        }

                        $("#latitud").val(latlng.lat());
                        $("#longitud").val(latlng.lng());

                        google.maps.event.addListener(
                            marker,
                            'drag',
                            function() {
                                $("#latitud").val(marker.position.lat());
                                $("#longitud").val(marker.position.lng());
                            }
                        );
                    }
                }
            });
        }
    </script>
@stop
