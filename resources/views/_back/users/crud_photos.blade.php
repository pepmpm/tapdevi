<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label>Fotos</label>
            <div id="fileuploader">Upload</div>
        </div>
    </div>
</div>
@if(old('p_foto', null) != null)
    <div class="row margin-bottom-20">
        @foreach(old('p_foto') as $tmpphoto)
            <div class="col-md-6">
                <input type="hidden" name="p_foto[]" value="{{ $tmpphoto }}">
                <a class="fancybox" data-rel="galeria_fotos"
                    href="{{ url('/') . '/' . $tmpphoto }}">
                    <img src="{{ url('/') . '/' . $tmpphoto }}" class="img-responsive">
                </a>
                
            </div>
            @if($loop->iteration % 2 == 0)
                </div><div class="row"> 
            @endif
        @endforeach
    </div>
@endif
@if(isset($query))
    <div class="row margin-bottom-20 team-v3">
        @foreach($query->photos as $tmpphoto)
            <div class="col-md-6">
                <div class="team-img">
                    <img src="{{ url('/') . '/' . $tmpphoto->url }}" class="img-responsive">
                    <div class="team-hover margin-top--40">
                        <ul class="list-inline team-social-v3">
                            <li>
                                <a class="fancybox" data-rel="galeria_fotos"
                                    title="{{ $query->name }}"
                                    href="{{ url('/') . '/' . $tmpphoto->url }}">
                                    <i class="rounded-x fa fa-search"></i>
                                </a>
                            </li>
                            <li>
                                <form action="{{ route('back.users.photos.destroy', [$tmpphoto->id]) }}" method="POST" 
                                    onsubmit="if(!confirm('¿Estás seguro de querer borrar la foto?')) return false"
                                    class="col-md-4">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-link" type="submit">
                                        <i class="rounded-x fa fa-trash"></i>
                                    </button>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            @if($loop->iteration % 2 == 0)
                </div><div class="row margin-bottom-20 team-v3"> 
            @endif
        @endforeach
    </div>
@endif