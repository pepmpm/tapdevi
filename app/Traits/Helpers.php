<?php
namespace App\Traits;

use Image;
use DB;

trait Helpers {
 
    public function getSlug($str, $field, $model) {
        $slug = str_slug($str);
        $slugCount = count($model->whereRaw("{$field} REGEXP '^{$slug}(-[0-9]*)?$'")->get());

        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    }

    public function set_72dpi_image($ruta_imagen) {
        Image::make($ruta_imagen, array(
            'width' => 1280,
            'height' => 960
        ))->encode('webp', 90)->save($ruta_imagen);
    }

    public static function fec_str_to_mysql($fecha_nom = null, $fechadefault = true) {
        if ($fecha_nom == null) {
            if($fechadefault){
                return date("Y-m-d");
            }
            return null;
        } else {
            $array_fecha = explode("/", $fecha_nom);
            $fecha_nom = $array_fecha[2] . "-" . $array_fecha[1] . "-" . $array_fecha[0];
        }
        return $fecha_nom;
    }
    
    public static function fec_mysql_to_str($fecha_nom = null) {
        if ($fecha_nom == null) {
            $fecha_nom = date("d/m/Y");
        } else {
            $array_fecha = explode("/", $fecha_nom);
            if (isset($array_fecha[1])) {
                $fecha_nom = $array_fecha[2] . "/" . $array_fecha[1] . "/" . $array_fecha[0];
            } else {
                $array_fecha = explode("-", $fecha_nom);
                $fecha_nom = $array_fecha[2] . "/" . $array_fecha[1] . "/" . $array_fecha[0];
            }
        }
        return $fecha_nom;
    }

    public static function fec_mysql_to_mdy($fecha_nom = null) {
        if ($fecha_nom == null) {
            $fecha_nom = date("d/m/Y");
        } else {
            $array_fecha = explode("/", $fecha_nom);
            if (isset($array_fecha[1])) {
                $fecha_nom = $array_fecha[1] . "/" . $array_fecha[2] . "/" . $array_fecha[0];
            } else {
                $array_fecha = explode("-", $fecha_nom);
                $fecha_nom = $array_fecha[1] . "/" . $array_fecha[2] . "/" . $array_fecha[0];
            }
        }
        return $fecha_nom;
    }

    public static function fec_mysql_to_dm($fecha_nom = null) {
        if ($fecha_nom == null) {
            $fecha_nom = date("d/m");
        } else {
            $array_fecha = explode("/", $fecha_nom);
            if (isset($array_fecha[1])) {
                $fecha_nom = $array_fecha[2] . "/" . $array_fecha[1];
            } else {
                $array_fecha = explode("-", $fecha_nom);
                $fecha_nom = $array_fecha[2] . "/" . $array_fecha[1];
            }
        }
        return $fecha_nom;
    }

    public static function fec_mysql_to_dmonth($fecha_nom = null) {
        if ($fecha_nom == null) {
            $fecha_nom = date("d/m");
        } else {
            $array_fecha = explode("/", $fecha_nom);
            if (isset($array_fecha[1])) {
                $fecha_nom = $array_fecha[2] . "/" . strtoupper(trans('base.mes' . $array_fecha[1]));
            } else {
                $array_fecha = explode("-", $fecha_nom);
                $fecha_nom = $array_fecha[2] . "/" . strtoupper(trans('base.mes' . $array_fecha[1]));
            }
        }
        return $fecha_nom;
    }

    public static function fec_str_to_dm($fecha_nom = null) {
        if ($fecha_nom == null) {
            $fecha_nom = date("d/m");
        } else {
            $array_fecha = explode("/", $fecha_nom);
            if (isset($array_fecha[1])) {
                $fecha_nom = $array_fecha[0] . "/" . $array_fecha[1];
            } else {
                $array_fecha = explode("-", $fecha_nom);
                $fecha_nom = $array_fecha[0] . "/" . $array_fecha[1];
            }
        }
        return $fecha_nom;
    }

    public static function fechora_str_to_mysql($fecha = null) {
        if ($fecha != null) {
            $fecha = date_create_from_format('d/m/Y H:i:s', $fecha);
            return date_format($fecha, 'Y-m-d H:i:s');
        } else {
            return date('Y-m-d H:i:s');
        }
    }

    public static function fechora_mysql_to_str($fecha = null, $format = 'd/m/Y') {
        if ($fecha != null) {
            $fecha = date_create_from_format('Y-m-d H:i:s', $fecha);
            return date_format($fecha, $format);
        } else {
            return date('d/m/Y H:i:s');
        }
    }

    public static function fec_str_to_ymd($fecha = null) {
        if ($fecha == null) {
            return date("Ymd");
        }
        $array_fecha = explode("/", $fecha);
        if (count($array_fecha) == 1) {
            $array_fecha = explode("-", $fecha);
        }
        $fecha = $array_fecha[2] . $array_fecha[1] . $array_fecha[0];
        return $fecha;
    }
    
    public static function fec_str_to_y_m_d($fecha = null) {
        if ($fecha == null) {
            return date("d/m/Y");
        }
        $array_fecha = explode("/", $fecha);
        if (count($array_fecha) == 1) {
            $array_fecha = explode("-", $fecha);
        }
        $fecha = $array_fecha[2] . '-' . $array_fecha[1] . '-' . $array_fecha[0];
        return $fecha;
    }
    
    public static function fec_ymd_to_str($fecha = null) {
        if ($fecha == null) {
            return null;
        }
        return substr($fecha, 6, 2) . "/" . substr($fecha, 4, 2) . "/" . substr($fecha, 0, 4);
    }

    public static function fec_ymd_to_dm($fecha = null) {
        if ($fecha == null) {
            return null;
        }
        return substr($fecha, 6, 2) . "/" . substr($fecha, 4, 2);
    }

    public static function fec_y_m_d_to_dm($fecha = null) {
        $array_fecha = explode("-", $fecha);
        $fecha = $array_fecha[2] . "/" . $array_fecha[1];
        return $fecha;
    }

    public static function fec_y_m_d_to_str($fecha = null) {
        if($fecha == null){
            return null;
        }
        $array_fecha = explode("-", $fecha);
        $fecha = $array_fecha[2] . "/" . $array_fecha[1] . "/" . $array_fecha[0];
        return $fecha;
    }

    public static function fec_y_m_d_to_mysql($fecha = null) {
        if($fecha == null){
            return null;
        }
        $array_fecha = explode("-", $fecha);
        $fecha = $array_fecha[0] . "/" . $array_fecha[1] . "/" . $array_fecha[2];
        return $fecha;
    }

    public static function fec_ymd_to_mysql($fecha = null) {
        if ($fecha == null) {
            return null;
        }
        return substr($fecha, 0, 4) . "/" . substr($fecha, 4, 2) . "/" . substr($fecha, 6, 2);
    }
    
    public static function fec_ymd_to_y_m_d($fecha = null) {
        if ($fecha == null) {
            return null;
        }
        return substr($fecha, 0, 4) . "-" . substr($fecha, 4, 2) . "-" . substr($fecha, 6, 2);
    }
    
    public static function sumardias($fecha, $dias, $formato = 'Y-m-d') {
        if($fecha == null || $dias == null || $dias == 0){
            return null;
        }
        $myDateTime = \DateTime::createFromFormat($formato, date($formato, strtotime($fecha)));
        $fechaformateada = $myDateTime->format('d/m/Y');

        list($day, $mon, $year) = explode('/', $fechaformateada);
        return date($formato, mktime(0, 0, 0, (int)$mon, (int)($day + $dias), (int)$year));
    }
    
    public static function diferenciadias($fechadesde, $fechahasta){
        $fechainicio = date('Y-m-d', strtotime(str_replace('/', '-', $fechadesde)));
        $fechafin = date('Y-m-d', strtotime(str_replace('/', '-', $fechahasta)));
        $dias = (strtotime($fechainicio) - strtotime($fechafin))/86400;
        $dias = abs($dias);
        $dias = floor($dias);       
        return $dias;
    }

    public static function getPossibleEnumValues($name, $filter = null){
        $instance = new static; // create an instance of the model to be able to get the table name
        $type = DB::select( DB::raw('SHOW COLUMNS FROM '.$instance->getTable().' WHERE Field = "'.$name.'"') )[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = array();
        foreach(explode(',', $matches[1]) as $value){
            //si enviamos un filtro de columnas y está dentro del array lo pintamos, sino no
            $v = trim( $value, "'" );
            if($filter == null){
                $enum[$v] = $v;
            }else if(in_array($v, $filter)){
                $enum[$v] = $v;
            }
        }
        return $enum;
    }

    public static function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 2) {
        // Cálculo de la distancia en grados
        $degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));
     
        // Conversión de la distancia en grados a la unidad escogida (kilómetros, millas o millas naúticas)
        switch($unit) {
            case 'km':
                // 1 grado = 111.13384 km, basándose en el diametro promedio de la Tierra (12.735 km)
                $distance = $degrees * 111.13384; 
                break;
            case 'mi':
                // 1 grado = 69.05482 millas, basándose en el diametro promedio de la Tierra (7.913,1 millas)
                $distance = $degrees * 69.05482; 
                break;
            case 'nmi':
                // 1 grado = 59.97662 millas naúticas, basándose en el diametro promedio de la Tierra (6,876.3 millas naúticas)
                $distance =  $degrees * 59.97662; 
        }
        return round($distance, $decimals);
    }

    public function sendTelegram($text, $chatid = '-455735690'){
        $token = "1174746680:AAETrSw1n0Z92AP1q6qw-8X1ywz0s1Wcg2o";

        /*$data = [
            'text' => $text,
            'chat_id' => $chatid,
            'parse_mode' => 'html'
        ];*/

        file_get_contents("https://api.telegram.org/bot". $token ."/sendMessage?chat_id=" . $chatid . "&parse_mode=HTML&text=" . urlencode($text));
    }

    public function getFinalStripeKey($type = 'key'){
        if(config('app.env') != 'produccion'){
            return config('services.stripe_test.' . $type);
        }
        if(\Auth::check() && \Auth::user()->isAdmin()){
            return config('services.stripe_test.' . $type);
        }

        return config('services.stripe.' . $type);
    }

    public function rtrans($file, $value){
        $langarray = \Lang::get($file);
        $flipped = array_flip($langarray);
        return $flipped[$value];
    }

    public function get_discount($base, $dto){
        $ahorro = ( $base * $dto) / 100;
        return number_format($base - $ahorro, 2);
    }

    public function esBisiesto($year = null) {
        $year = ($year == null) ? date('Y') : $year;
        return ( ($year%4 == 0 && $year%100 != 0) || $year%400 == 0 );
    }

    public function show_menu(){
        return !(session()->has('cart') && isset(session('cart')['show_menu']) && !session('cart')['show_menu']);
    }

}