@php($tmpimage = Imgfly::imgPublic(str_replace("upload/", "", $query->photos->first()->url) . 
    '?w=141&h=120&fit=crop-center', 'upload'))
@php($tmpdate = $query->next_stock()->count() > 0 ? $query->next_stock()->first()->date : null)

contentString =
    '<div id="content" class="width550">' +
    '<div id="row"><div class="col-md-12">' +
    '<div class="funny-boxes funny-boxes-top-tdv">' +
        '<div class="row">' +
            '<div class="col-md-4 funny-boxes-img">' +
                '<img class="img-responsive" src="{{ $tmpimage }}" alt="{{ $query->name }}">' +
            '</div>' +
            '<div class="col-md-8">' +
                '<h2 class="font15">' +
                '<strong><a href="{{ route('enotourism-mallorca.events.show', ['item' => $query->slug]) }}">{{ $query->name }}</a></strong></h2>' +
                '<div class="row"><div class="col-md-12">' +
                '<ul class="list-unstyled">' +
                    @if($tmpdate != null)
                        '<li><span class="font15 color-tdv"><i class="rounded-x fa fa-calendar fa-2x"></i> {{ $helpers->fec_mysql_to_str($tmpdate) }}</span></li>' +
                    @endif
                    @if($query->districts_id != null)
                        '<li><span class="font15 color-tdv"><i class="rounded-x fa fa-map-marker fa-2x"></i> {{ $query->district->name }}</span></li>' +
                    @endif
                    '<li><span class="font15 color-tdv"><i class="rounded-x fa fa fa-eur fa-2x"></i> {{ $query->eventprice }}</span></li>' +
                '</ul>' +
                '</div></div>' +
            '</div>' +
        '</div>' +
    '</div>' +
    "</div></div>";

marker = map.addMarker({
    lat: '{{ $query->latitude }}',
    lng: '{{ $query->longitude }}',
    icon: icon,
    title: "{{ $query->name }}",
    infoWindow: {
        content: contentString
    },
    id: {{ $query->id }}
});