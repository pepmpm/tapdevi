<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('labels', function (Blueprint $table) {
            if (!Schema::hasColumn('labels', 'status')) {
                $table->enum('status', ['active', 'hidden', 'disabled'])->default('active')->after('labels_id');
            }
        });

        Schema::table('users', function (Blueprint $table) {
            if (!Schema::hasColumn('users', 'order')) {
                $table->unsignedInteger('order')->default('1000')->after('fee');
            }
            if (!Schema::hasColumn('users', 'facebook_url')) {
                $table->string('facebook_url', 200)->nullable()->after('website');
            }
            if (!Schema::hasColumn('users', 'town')) {
                $table->string('town', 250)->nullable()->after('districts_id');
            }
            if (!Schema::hasColumn('users', 'instagram_url')) {
                $table->string('instagram_url', 200)->nullable()->after('website');
            }
            if (Schema::hasColumn('users', 'is_attach_wine_order_list')) {
                $table->dropColumn('is_attach_wine_order_list');
            }
            if (!Schema::hasColumn('users', 'is_promoted')) {
                $table->enum('is_promoted', ['S', 'N'])->default('N');
            }
            if (!Schema::hasColumn('users', 'active')) {
                $table->enum('active', ['S', 'N'])->default('S');
            }
            if (!Schema::hasColumn('users', 'is_notificated_booking')) {
                $table->enum('is_notificated_booking', ['S', 'N'])->default('N');
            }
            if (Schema::hasColumn('users', 'locations_id_billing')) {
                $table->dropColumn('name_billing');
                $table->dropColumn('last_name_billing');
                $table->dropForeign(['locations_id_billing']);
                $table->dropColumn('locations_id_billing');
                $table->dropForeign(['districts_id_billing']);
                $table->dropColumn('districts_id_billing');
                $table->dropColumn('address_billing');
                $table->dropColumn('postal_code_billing');
                $table->dropColumn('phone_billing');
                $table->dropColumn('is_attach_wine_order_list');
                $table->dropColumn('fee');
            }
        });

        Schema::table('districts', function (Blueprint $table) {
            if (!Schema::hasColumn('districts', 'order')) {
                $table->unsignedInteger('order')->default('1000');
            }
        });

        Schema::table('items', function (Blueprint $table) {
            if (!Schema::hasColumn('items', 'languages')) {
                $table->string('languages', 50)->default('ES')->after('confirmation');
            }
            if (!Schema::hasColumn('items', 'is_75_cls')) {
                $table->enum('is_75_cls', ['S', 'N'])->default('S')->after('new_price');
            }
            if (!Schema::hasColumn('items', 'is_apto_pets')) {
                $table->enum('is_apto_pets', ['S', 'N'])->default('N')->after('languages');
                \DB::statement("ALTER TABLE items MODIFY confirmation enum('none', 'email', 'direct', 'fee', 'groupon')");
            }
            if (!Schema::hasColumn('items', 'is_apto_children')) {
                $table->enum('is_apto_children', ['S', 'N'])->default('N')->after('languages');
            }
            if (!Schema::hasColumn('items', 'is_apto_disabled')) {
                $table->enum('is_apto_disabled', ['S', 'N'])->default('N')->after('languages');
            }
            if (!Schema::hasColumn('items', 'is_has_parking')) {
                $table->enum('is_has_parking', ['S', 'N'])->default('N')->after('languages');
            }
            if (!Schema::hasColumn('items', 'is_promoted')) {
                $table->enum('is_promoted', ['S', 'N'])->default('N')->after('languages');
                $table->dropColumn('is_75_cls');

                Schema::dropIfExists('grapes');
                Schema::dropIfExists('grapes_items');
                Schema::dropIfExists('items_details');
                Schema::dropIfExists('items_lots');
            }
            if (!Schema::hasColumn('items', 'advance_days')) {
                $table->unsignedInteger('advance_days')->nullable();
            }
            if (!Schema::hasColumn('items', 'cancellation_days')) {
                $table->unsignedInteger('cancellation_days')->nullable();
            }
            if (!Schema::hasColumn('items', 'duration')) {
                $table->string('duration')->nullable();
            }
            if (!Schema::hasColumn('items', 'is_giftable')) {
                $table->enum('is_giftable', ['S', 'N'])->default('S')->after('is_75_cls');
            }
            if (Schema::hasColumn('items', 'need_confirm')) {
                $table->dropColumn('need_confirm');
            }
            if (!Schema::hasColumn('items', 'advisor_name')) {
                $table->text('advisor_name')->after('duration')->nullable();
            }
            if (!Schema::hasColumn('items', 'advisor_email')) {
                $table->text('advisor_email')->after('advisor_name')->nullable();
            }
            if (!Schema::hasColumn('items', 'instagram_url')) {
                $table->text('instagram_url')->after('advisor_email')->nullable();
            }
            if (!Schema::hasColumn('items', 'is_promoted')) {
                $table->enum('is_promoted', ['S', 'N'])->default('N')->after('languages');
            }
            if (!Schema::hasColumn('items', 'districts_id')) {
                $table->unsignedInteger('districts_id')->after('advisor_email')->nullable();
                $table->foreign('districts_id')->references('id')->on('districts');

                $table->string('address', 200)->after('districts_id')->nullable();
                $table->string('postal_code', 15)->after('address')->nullable();
                $table->string('longitude', 30)->after('postal_code')->nullable();
                $table->string('latitude', 30)->after('longitude')->nullable();
            }

        });

        Schema::table('items_langs', function (Blueprint $table) {
            if (!Schema::hasColumn('items_langs', 'short_description')) {
                \DB::statement("ALTER TABLE items MODIFY type enum('item', 'plan', 'wine', 'lot')");
                \DB::statement("ALTER TABLE labels MODIFY category enum('enotourism-mallorca', 'wines', 'gourmet', 'liqueur', 'lot')");
                $table->text('short_description')->after('seo_keywords')->nullable();
            }
            if (!Schema::hasColumn('items_langs', 'timetable')) {
                $table->text('timetable')->after('description')->nullable();
            }
        });

        Schema::table('items_stocks', function (Blueprint $table) {
            if (!Schema::hasColumn('items_stocks', 'times')) {
                $table->text('times')->after('date')->nullable();
            }
        });

        Schema::table('orders', function (Blueprint $table) {
            if (!Schema::hasColumn('orders', 'time')) {
                $table->text('time')->after('date_event')->nullable();
            }
            if (!Schema::hasColumn('orders', 'is_gift')) {
                $table->enum('is_gift', ['S', 'N'])->default('N')->after('postal_code');
            }
            if (!Schema::hasColumn('orders', 'gift_remarks')) {
                $table->text('gift_remarks')->after('remarks')->nullable();
                $table->string('gift_recipient', 255)->after('gift_remarks')->nullable();
                $table->string('gift_name', 255)->after('gift_remarks')->nullable();
            }
            if (Schema::hasColumn('orders', 'date_prepared')) {
                $table->dropColumn('date_prepared');
                $table->dropColumn('date_plan_confirmed');
                $table->dropColumn('date_sent');
                $table->dropColumn('shipping');
                $table->dropColumn('tracking_code');
                $table->dropColumn('postal_code');
            }
            if (!Schema::hasColumn('orders', 'date_visited')) {
                $table->timestamp('date_visited')->after('status')->nullable();
            }
            if (!Schema::hasColumn('orders', 'partner_code')) {
                $table->timestamp('partner_code')->after('date_mail_sent')->nullable();
            }
            if (!Schema::hasColumn('orders', 'date_mail_sent')) {
                $table->timestamp('date_mail_sent')->after('date_visited')->nullable();
                // \DB::statement("UPDATE orders set date_mail_sent = now()");
            }
            if (!Schema::hasColumn('orders', 'email')) {
                $table->string('email', 255)->after('status')->nullable();
            }
            if (!Schema::hasColumn('orders', 'name')) {
                $table->string('name', 255)->after('email')->nullable();
            }
            if (!Schema::hasColumn('orders', 'phone')) {
                $table->string('phone', 14)->after('name')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
