<?php

use Illuminate\Database\Seeder;
use \App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        \DB::table('users')->truncate();

        User::create(['name' => 'Pep', 'slug' => 'pep', 'email' => 'pepmpm@gmail.com', 'locations_id' => 8, 'districts_id' => 4, 'password' => '$2y$10$qv.CSf6W1OQq01f3osBe0.IDkSXYH8ZZxYpDOuqQIyHAUrctu.G2G', 'role' => 'admin']);
    }
}
