<?php

namespace App\Http\Controllers\Routes;

use App\Blogs;
use App\BlogsLangs;
use App\BlogsPhotos;
use Illuminate\Http\Request;
use App\Traits\Helpers;
use App\Traits\Plans;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use Validator;
use Cache;

class RoutesController extends \App\Http\Controllers\Controller
{
    private $default_keywords = 'eventos vino mallorca, catas vino mallorca, ferias vino mallorca, agenda vinos mallorca, que hacer mallorca, bodegas mallorca';
    use Helpers;
    use Plans;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function _index($params)
    {
        $blog = Blogs::where('category', '=', 'route');

        if(isset($params['status'])){
            $blog = $blog->where('status', '=', $params['status']);
        }
        if(isset($params['paginate'])){
            return $blog->paginate($params['paginate']);
        }
        return $blog->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function _store($request)
    {
        $validation = Validator::make($request, self::_get_rules($request), self::_get_messages());

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }

        $tmp = new Blogs;
        $tmp->order = 100;
        $tmp->category = 'route';
        $tmp->tags = isset($request['tags']) ? implode('#', $request['tags']) : null;
        $tmp->status = $request['status'];
        $tmp->save();

        self::_insert_langs($request, $tmp);

        if(isset($request['p_foto'])){
            self::_insert_images($request, $tmp);
        }
        self::_clean();
        return $tmp;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _show(Blogs $blog)
    {
        return $blog;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _update($request, Blogs $tmp)
    {
        $validation = Validator::make($request, self::_get_rules($request), self::_get_messages());

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }

        $tmp->order = 100;
        $tmp->tags = isset($request['tags']) ? implode('#', $request['tags']) : null;
        $tmp->status = $request['status'];
        $tmp->save();

        self::_edit_langs($request, $tmp);

        if(isset($request['p_foto'])){
            self::_insert_images($request, $tmp);
        }
        self::_clean();
        return $tmp;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _destroy(Blogs $blog)
    {
        $ruta = 'upload/blogs/'.$blog->id.'/';
        if(is_dir($ruta)){
            $success = \File::deleteDirectory($ruta, true);
            rmdir($ruta);
            if(!$success){
                throw new \Exception('El directorio de imagenes de '.$blog->id.' no existe');
            }
        }

        $blog->delete();
        self::_clean();
        return true;
    }

    private function _insert_images($request, $blog){
        $ruta = 'upload/blogs/'.$blog->id.'/';
        \File::makeDirectory($ruta, 0775, true, true);

        foreach($request['p_foto'] as $foto){
            $tmp = new BlogsPhotos;
            $tmp->order = 100;
            $tmp->blogs_id = $blog->id;
            $tmp->save();

            $tmp->url = $ruta . $blog->slug . "-" . $tmp->id . ".webp";
            $tmp->save();

            $img = Image::make($foto)->encode('webp', 90);
            $img->resize(1024, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($tmp->url);
            unlink($foto);
            $this->set_72dpi_image($tmp->url);
        }
    }

    public function _insert_langs($request, $tmp){
        $spanish = [
            'name' => (isset($request['name']['es']) ? $request['name']['es'] : $request['name']),
            'description' => $request['description']['es'],
            'seo_keywords' => $this->default_keywords
        ];

        foreach(config('translatable.locales') as $keylocale => $locale){
            if(isset($request['name']) && isset($request['description'][$keylocale])){
                $lang = BlogsLangs::firstOrNew(['locale' => $keylocale, 'blogs_id' => $tmp->id]);
                
                $lang->locale = $keylocale;
                $lang->blogs_id = $tmp->id;
                $lang->name = parent::_translate_langs(
                    $spanish['name'], 
                    isset($request['name'][$keylocale]) ? $request['name'][$keylocale] : $request['name'],
                    $keylocale,
                    255);
                $lang->slug = $this->getSlug($lang->name, 'slug', BlogsLangs::getModel());

                $lang->description = parent::_translate_langs(
                    $spanish['description'], $request['description'][$keylocale], $keylocale, null);

                $lang->seo_description = str_limit($lang->description, 250);

                $lang->seo_keywords = parent::_translate_langs(
                    $spanish['seo_keywords'] . " " . $lang->name, null, $keylocale, 255
                );

                $lang->save();
            }
        }
    }

    private function _edit_langs($request, $tmp){
        $spanish = [
            'name' => $request['name']['es'],
            'description' => $request['description']['es'],
            'seo_keywords' => $this->default_keywords
        ];

        foreach($tmp->langs as $lang){
            $tmpname = parent::_translate_langs(
                $spanish['name'], $request['name'][$lang->locale], $lang->locale, 255);
            if($tmpname != $lang->name){
                $lang->name = $tmpname;
                $lang->slug = $this->getSlug($tmpname, 'slug', BlogsLangs::getModel());
            }

            $lang->description = parent::_translate_langs(
                $spanish['description'], $request['description'][$lang->locale], $lang->locale, null);
            $lang->seo_description = str_limit($lang->description, 250);

            $lang->seo_keywords = parent::_translate_langs(
                $spanish['seo_keywords'] . " " . $lang->name, null, $lang->locale, 255
            );

            $lang->save();
        }
    }

    private function _get_rules($request){
        $rules = array();

        //$rules['name.es'] = 'required|max:250';
        $rules['description.es'] = 'required|max:2000';

        return $rules;
    }
    
    private function _get_messages(){
        $messages = array();
        $messages['name.es.required'] = trans('item.validation_name_required');
        $messages['name.es.max'] = trans('item.validation_name_max');
        $messages['description.es.required'] = __('item.validation_description_required', [], 'es');
        $messages['description.es.max'] = __('item.validation_description_max', [], 'es');
        return $messages;
    }

    private function _clean(){
        \Cache::flush();
    }

    public function get_tags(){
        $blogs = Blogs::where('status', '=', 'active')->get();
        $result = [];

        foreach($blogs as $blog){
            foreach(explode('#', $blog->tags) as $tag){
                if(!empty($tag)){
                    $result[$tag] = $tag;
                }
            }
        }

        return $result;
    }

}
