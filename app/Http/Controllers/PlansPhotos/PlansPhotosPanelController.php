<?php

namespace App\Http\Controllers\PlansPhotos;

use App\Items;
use App\ItemsPhotos;
use Illuminate\Http\Request;

class PlansPhotosPanelController extends PlansPhotosController
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $return = parent::_store($request->all());
		echo $return;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Plans  $plans
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemsPhotos $item_photo)
    {
        $this->data['query'] = parent::_update($request->all(), $item_photo);
        return redirect()
            ->route('back.plans.show', array('items' => $request->get('id')))
            ->with('message_success', 'Cambios realizados correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Plans  $plans
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemsPhotos $item_photo)
    {
        $items_id = parent::_destroy($item_photo);
        return redirect()
            ->route('back.plans.show', array('item' => $items_id))
            ->with('message_success', 'Cambios realizados correctamente');
    }
}
