<?php

namespace App\Http\Controllers\EventsPhotos;

use App\Items;
use App\ItemsPhotos;
use Illuminate\Http\Request;

class EventsPhotosPanelController extends EventsPhotosController
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $return = parent::_store($request->all());
		echo $return;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Plans  $plans
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemsPhotos $photo)
    {
        $this->data['query'] = parent::_update($request->all(), $photo);
        return redirect()
            ->route('back.events.show', array('item' => $request->get('id')))
            ->with('message_success', 'Cambios realizados correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Plans  $plans
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemsPhotos $photo)
    {
        $items_id = parent::_destroy($photo);
        return redirect()->back()->with('message_success', 'Cambios realizados correctamente');
    }
}
