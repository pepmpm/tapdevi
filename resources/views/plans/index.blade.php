@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@include('plans._meta')

@section ('body')
@include('plans._breadcrumbs')

<div class="content container">
    <div class="row">
        <div class="col-md-3 filter-by-block md-margin-bottom-60">
            @include('plans._sidebar', ['search' => true])
        </div>
        <div class="col-md-9">
            <div class="row margin-bottom-5">
                <div class="col-sm-8 result-category">
                    <h2>{{ trans('inicio.enotourism-mallorca') }}</h2>
                    <small class="shop-bg-red badge-results">{{ $query->count() }} Results</small>
                </div>
            </div>
            <div class="row">
                <!--div class="col-sm-4">
                    <ul class="list-inline clear-both pull-right">
                        <li class="sort-list-btn">
                            <h3>Sort By :</h3>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    Popularity <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">All</a></li>
                                    <li><a href="#">Best Sales</a></li>
                                    <li><a href="#">Top Last Week Sales</a></li>
                                    <li><a href="#">New Arrived</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div-->
            </div>
            <div class="row news-v1 margin-bottom-20">
                @foreach($query as $plan)
                    @include('plans.element', ['plan' => $plan, 'col' => '4'])
                    @if($loop->iteration % 3 == 0)
                        </div>
                        <div class="row news-v1 margin-bottom-20"> 
                    @endif
                @endforeach
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    {{ $query->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

<!--div class="shop-subscribe">
    <div class="container">
        <div class="row">
            <div class="col-md-8 md-margin-bottom-20">
                <h2>subscribe to our weekly <strong>newsletter</strong></h2>
            </div>
            <div class="col-md-4">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Email your email...">
                    <span class="input-group-btn">
                        <button class="btn" type="button"><i class="fa fa-envelope-o"></i></button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div-->
@stop

@section ('assets_end')
    <link rel="stylesheet" href="{{ asset('assets/css/pages/timeline2.css') }}">
    
    <script type="text/javascript" src="{{ asset('js/plans.js') }}"></script>
@stop
