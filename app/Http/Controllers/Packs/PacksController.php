<?php

namespace App\Http\Controllers\Packs;

use App\Items;
use App\ItemsStocks;
use App\ItemsLangs;
use App\ItemsPhotos;
use App\ItemsPacks;
use Illuminate\Http\Request;
use Image;
use App\Traits\Helpers;
use Validator;
use Cache;

class PacksController extends \App\Http\Controllers\Controller
{
    use Helpers;

    public function __construct(array $attributes = array()) {
        parent::__construct($attributes);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function _index($params)
    {
        $items = Items::where('type', '=', 'pack')->orderBy('id', 'desc');
        return $items->orderBy('id', 'desc')->get();
    }

    public function _get_plans($params){
        return $this->getPlans($params);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function _all()
    {
        return Items::where('type', '=', 'pack')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function _store($request)
    {
        $validation = Validator::make($request, self::_get_rules($request), self::_get_messages());

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }

        $tmp = new Items;
        $tmp->order = 100;
        $tmp->users_id = 1; //siempre es Tapdevi
        $tmp->price = isset($request['price']) ? $request['price'] : 999;
        $tmp->confirmation = 'groupon';
        $tmp->type = 'pack';
        $tmp->is_giftable = 'S';
        $tmp->price = $request['price'];
        $tmp->new_price = $request['new_price'];
        $tmp->min_pax = $request['min_pax'];
        $tmp->max_pax = $request['max_pax'];
        $tmp->advance_days = $request['advance_days'];
        $tmp->cancellation_days = $request['cancellation_days'];
        $tmp->duration = $request['duration'];
        $tmp->status = isset($request['status']) ? $request['status'] : 'outofstock';
        $tmp->save();

        self::_insert_langs($request, $tmp);

        if(isset($request['p_foto'])){
            self::_insert_images($request, $tmp);
        }

        self::_clean();

        return $tmp;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _show(Items $item)
    {
        return $item;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _update($request, Items $tmp)
    {
        $validation = Validator::make($request, self::_get_rules($request), self::_get_messages());

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }

        $tmp->order = 100;
        $tmp->users_id = 1; //siempre es Tapdevi
        $tmp->price = isset($request['price']) ? $request['price'] : 0;
        $tmp->confirmation = 'groupon';
        $tmp->type = 'pack';
        $tmp->price = $request['price'];
        $tmp->new_price = $request['new_price'];
        $tmp->min_pax = $request['min_pax'];
        $tmp->max_pax = $request['max_pax'];
        $tmp->advance_days = $request['advance_days'];
        $tmp->cancellation_days = $request['cancellation_days'];
        $tmp->duration = $request['duration'];
        $tmp->status = isset($request['status']) ? $request['status'] : 'outofstock';
        $tmp->save();

        self::_edit_langs($request, $tmp);

        if(isset($request['p_foto'])){
            self::_insert_images($request, $tmp);
        }

        self::_clean();

        return $tmp;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _destroy(Items $item)
    {
        $ruta = 'upload/packs/'.$item->id.'/';
        if(is_dir($ruta)){
            $success = \File::deleteDirectory($ruta, true);
            rmdir($ruta);
            if(!$success){
                throw new \Exception('El directorio de imagenes de '.$item->id.' no existe');
            }
        }

        $item->delete();

        self::_clean();

        return true;
    }

    private function _insert_images($request, $item){
        $ruta = 'upload/packs/'.$item->id.'/';
        \File::makeDirectory($ruta, 0775, true, true);

        foreach($request['p_foto'] as $foto){
            $tmp = new ItemsPhotos;
            $tmp->order = 100;
            $tmp->items_id = $item->id;
            $tmp->save();

            $tmp->url = $ruta . $item->slug . "-" . $tmp->id . ".webp";
            $tmp->save();

            $img = Image::make($foto)->encode('webp', 90);
            $img->resize(1800, 300, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($tmp->url);
            unlink($foto);
            $this->set_72dpi_image($tmp->url);
        }
    }

    public function _insert_langs($request, $tmp){
        $spanish = [
            'name' => $request['name']['es'],
            'description' => $request['description']['es'],
            'seo_keywords' => $request['seo_keywords']['es'], 
            'seo_description' => $request['seo_description']['es']
        ];
        foreach(config('translatable.locales') as $keylocale => $locale){
            $lang = ItemsLangs::firstOrNew(['locale' => $keylocale, 'items_id' => $tmp->id]);
            $lang->locale = $keylocale;
            $lang->items_id = $tmp->id;
            $lang->name = parent::_translate_langs($spanish['name'], isset($request['name'][$keylocale]) ? $request['name'][$keylocale] : null, $keylocale, 255);
            $lang->slug = $this->getSlug($lang->name, 'slug', ItemsLangs::getModel());


            $lang->seo_description = parent::_translate_langs(
                $spanish['seo_description'], isset($request['seo_description'][$keylocale]) ? $request['seo_description'][$keylocale] : null, $keylocale, 255);
            $lang->seo_keywords = parent::_translate_langs(
                $spanish['seo_keywords'], isset($request['seo_keywords'][$keylocale]) ? $request['seo_keywords'][$keylocale] : null, $keylocale, 255);
            $lang->description = parent::_translate_langs(
                $spanish['description'], isset($request['description'][$keylocale]) ? $request['description'][$keylocale] : null, $keylocale, null);

            $lang->save();
        }
    }

    public function _insert_related_items($request, $item){
        if(isset($request['item_selected'])){
            foreach($request['item_selected'] as $tmpitem){
                $tmp = ItemsPacks::firstOrNew(['items_id' => $item, 'items_id_related' => $tmpitem]);
                $tmp->items_id = $item;
                $tmp->items_id_related = $tmpitem;
                $tmp->save();
            }
        }

        if(isset($request['item_unselected'])){
            foreach($request['item_unselected'] as $tmpitem){
                ItemsPacks::where('items_id', '=', $item)
                    ->where('items_id_related', '=', $tmpitem)->delete();
            }
        }
    }

    private function _edit_langs($request, $tmp){
        $spanish = [
            'name' => $request['name']['es'],
            'description' => $request['description']['es'],
            'seo_keywords' => $request['seo_keywords']['es'], 
            'seo_description' => $request['seo_description']['es']
        ];

        foreach($tmp->langs as $lang){
            $tmpname = parent::_translate_langs(
                $spanish['name'], $request['name'][$lang->locale], $lang->locale, 255);
            if($tmpname != $lang->name){
                $lang->name = $tmpname;
                $lang->slug = $this->getSlug($tmpname, 'slug', ItemsLangs::getModel());
            }

            $lang->seo_description = parent::_translate_langs(
                $spanish['seo_description'], $request['seo_description'][$lang->locale], $lang->locale, 255);
            $lang->seo_keywords = parent::_translate_langs(
                $spanish['seo_keywords'], $request['seo_keywords'][$lang->locale], $lang->locale, 255);
            $lang->description = parent::_translate_langs(
                $spanish['description'], $request['description'][$lang->locale], $lang->locale, null);

            $lang->save();
        }
    }

    private function _get_rules($request){
        $rules = array();

        $rules['name'] = 'required|max:250';
        $rules['description.es'] = 'required|max:2000';

        return $rules;
    }
    
    private function _get_messages(){
        $messages = array();
        $messages['name.required'] = trans('item.validation_name_required');
        $messages['name.max'] = trans('item.validation_name_max');
        $messages['description.es.required'] = __('item.validation_description_required', [], 'es');
        $messages['description.es.max'] = __('item.validation_description_max', [], 'es');
        return $messages;
    }

    private function _clean(){
        \Cache::flush();
    }
}
