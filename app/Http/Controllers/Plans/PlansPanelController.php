<?php

namespace App\Http\Controllers\Plans;

use App\Items;
use Illuminate\Http\Request;

class PlansPanelController extends PlansController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['query'] = parent::_index();
        if(\Auth::user()->isAdmin()){
            $this->data['users_lov'] = \App\User::whereHas('plans', function ($query) {
                    $query->where('status', '=', 'active');
                })->pluck('name', 'id')->all();
        }
        $this->data['labels_lov'] = \App\Labels::whereHas('plans', function ($query) {
                $query->where('status', '=', 'active');
            })->listsTranslations('title')->pluck('title', 'id')->all();
        $this->data['status_lov'] = Items::getPossibleEnumValues('status');
        return view('_back.plans.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['users_lov'] = \App\User::where('role', '=', 'company')->pluck('name', 'id')->all();
        $this->data['locations'] = \App\Locations::pluck('name', 'id')->all();
        $this->data['labels_lov'] = \App\Labels::listsTranslations('title')->where('category', '=', 'enotourism-mallorca')
            ->pluck('title', 'id')->all();
        $this->data['yesno_lov'] = Items::getPossibleEnumValues('is_has_parking');
        $this->data['status_lov'] = Items::getPossibleEnumValues('status');
        $this->data['confirmation_lov'] = Items::getPossibleEnumValues('confirmation');
        $this->data['languages_lov'] = self::_get_languages();
        $this->data['numbers_lov'] = self::_get_numbers();
        return view('_back.plans.crud', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query = parent::_store($request->all());

        if(!($query instanceof Items)){
            return $query;
        }

        return redirect()->route('back.plans.show', array('item' => $query->id))
            ->with('message_success', 'Cambios guardados correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function show(Items $plan)
    {
        /*$this->data['stocks'] = \App\ItemsStocks::where('items_id', '=', $plan->id)
            ->whereIn('year', [date('Y'), date('Y') + 1])
            ->orderBy('year', 'asc')->orderBy('month', 'asc')->get();*/

        $this->data['query'] = parent::_show($plan);
        if($plan->type == 'pack'){
            return redirect()->route('back.packs.show', ['id' => $plan->id]);
        }
        $this->data['users_lov'] = \App\User::where('role', '=', 'company')->pluck('name', 'id')->all();
        $this->data['locations'] = \App\Locations::pluck('name', 'id')->all();
        $this->data['labels_lov'] = \App\Labels::listsTranslations('title')->where('category', '=', 'enotourism-mallorca')
            ->pluck('title', 'id')->all();
        $this->data['status_lov'] = Items::getPossibleEnumValues('status');
        $this->data['yesno_lov'] = Items::getPossibleEnumValues('is_has_parking');
        $this->data['confirmation_lov'] = Items::getPossibleEnumValues('confirmation');
        $this->data['languages_lov'] = self::_get_languages();
        $this->data['numbers_lov'] = self::_get_numbers();
        return view('_back.plans.crud', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Items $plan)
    {
        $query = parent::_update($request->all(), $plan);

        if(!($query instanceof Items)){
            return $query;
        }

        return redirect()->route('back.plans.show', array('plan' => $query->id))
            ->with('message_success', 'Cambios guardados correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Items $plan)
    {
        parent::_destroy($plan);
        return redirect()
            ->route('back.plans.index')
            ->with('message_success', 'Cambios guardados correctamente');
    }

    public function toggle_promoted(Request $request, Items $item){
        $item->is_promoted = $item->is_promoted == 'S' ? 'N' : 'S';
        $item->save();

        return redirect()->back();
    }

    private function _get_languages(){
        $array = array();
        $array['ES'] = 'Español';
        $array['CA'] = 'Catalán';
        $array['EN'] = 'Inglés';
        $array['DE'] = 'Alemán';
        $array['FR'] = 'Francés';
        $array['IT'] = 'Italiano';
        $array['RU'] = 'Ruso';
        $array['SU'] = 'Sueco';
        $array['PO'] = 'Portugués';
        $array['GR'] = 'Griego';

        return $array;
    }

    private function _get_numbers(){
        $array = array();
        $array[''] = 'Sin información';
        $array['0'] = 'El mismo día';
        for($x = 1; $x < 11; $x ++){
            $array[$x] = $x;
        }

        return $array;
    }
}
