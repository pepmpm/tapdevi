<?php

namespace App\Http\Controllers\UsersPhotos;

use App\User;
use App\UsersPhotos;
use Illuminate\Http\Request;
use Image;
use App\Traits\Helpers;

class UsersPhotosController extends \App\Http\Controllers\Controller
{
    use Helpers;

    public function __construct(array $attributes = array()) {
        parent::__construct($attributes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function _store($request)
    {
        $ruta = 'upload/users/pending/';
        \File::makeDirectory($ruta, 0775, true, true);

        $file = $ruta . time() . str_random(6) . '.webp';

        $img = Image::make(file_get_contents($request['p_file']))->encode('webp', 90);
        $img->resize(1024, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($file);

        $this->set_72dpi_image($file);

		return $file;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Plans  $plans
     * @return \Illuminate\Http\Response
     */
    protected function _update($request, UsersPhotos $users)
    {
        //pasamos todas las imagenes a orden 100
        UsersPhotos::where('users_id', '=', $request['id'])
                ->update(array('order' => 100));

        //pasamos la imagen seleccionada a orden 1
        $users->order = 1;
        $users->save();

        return $users;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Plans  $plans
     * @return \Illuminate\Http\Response
     */
    protected function _destroy(UsersPhotos $user)
    {
        $users_id = $user->users_id;

        if(\File::exists($user->url)) {
            \File::delete($user->url);
        }

        $user->delete();

        return $users_id;
    }
}
