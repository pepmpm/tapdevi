@extends('_layouts/inicio')

@section('title', trans('meta.title_index'))
@section ('meta')
    <meta name="description" content="{{ trans('meta.description_index') }}"/>
    <meta name="keywords" content="{{ trans('meta.keywords_index') }}" />
    <meta name="Author" content="{{ trans('meta.author') }}" />
    <meta property="og:title" content="{{ trans('meta.title_index') }}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:image" content="{{ url('assets/img/logo3.png') }}"/>
    <meta property="og:description" content="{{ trans('meta.description_index') }}" />
    @parent
@stop

@section ('body')
<div class="log-reg-v3 content-md">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-4 col-md-4">
                <form id="sky-form1" method="POST" action="{{ route('login') }}" class="log-reg-block sky-form">
                    @csrf
                    <h2>{{ trans('inicio.acceder_a_tu_cuenta') }}</h2>

                    <section>
                        <label class="input login-input">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="email" placeholder="{{ trans('inicio.email') }}" name="email" 
                                    class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}">
                            </div>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </label>
                    </section>
                    <section>
                        <label class="input login-input no-border-top">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input type="password" placeholder="{{ trans('inicio.password') }}" name="password"
                                    class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required>
                            </div>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </label>
                    </section>
                    <div class="row margin-bottom-5">
                        <div class="col-xs-6">
                            <label class="checkbox">
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}/>
                                <i></i>
                                {{ trans('inicio.recordarme') }}
                            </label>
                        </div>
                        <div class="col-xs-6 text-right">
                            @if (Route::has('password.request'))
                                <a href="{{ route('password.request') }}">
                                    {{ trans('inicio.olvidaste_password') }}
                                </a>
                            @endif
                        </div>
                    </div>
                    <button class="btn-u btn-u-sea-shop btn-block margin-bottom-20" type="submit">{{ trans('inicio.acceder') }}</button>

                </form>

                <div class="margin-bottom-20"></div>
                <p class="text-center"><a href="{{ route('register') }}">{{ trans('inicio.registrarse') }}</a></p>
            </div>
        </div>
    </div>
</div>
@stop

@section ('assets_end')

    <link rel="stylesheet" href="{{ asset('assets/css/pages/log-reg-v3.css') }}">

@stop
