<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_stocks', function (Blueprint $table) {
            $table->increments('id');

            $table->date('date')->nullable();

            $table->unsignedInteger('items_id');
            $table->foreign('items_id')->references('id')->on('items')->onDelete('cascade');

            $table->timestamps();
            
            $table->unique(['date', 'items_id']);
            $table->index('date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items_stocks');
    }
}
