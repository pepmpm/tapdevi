<?php

namespace App\Http\Controllers\Orders;

use App\Orders;
use App\OrdersLangs;
use Illuminate\Http\Request;
use App\Traits\Helpers;
use Intervention\Image\ImageManagerStatic as Image;
use Validator;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use App\Mail\Voucher;

class OrdersController extends \App\Http\Controllers\Controller
{
    use Helpers;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function _index($params = null)
    {
        $orders = Orders::getModel();
        if(isset($params['users_id'])){
            $orders = $orders->where('users_id', '=', $params['users_id']);
        }
        return $orders->where('status', '!=', 'cart')->orderBy('id', 'desc')->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function _all()
    {
        return Orders::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function _store($request)
    {
        $validation = Validator::make($request, self::_get_rules_create($request), self::_get_messages());

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }
        //cuando se crea el carrito encriptado se tiene que eliminar el menú y hacer una versión lite

        $order = new Orders;
        $order->status = 'pending';
        $order->is_gift = $request['is_gift'];
        $order->name = $request['name'];
        $order->email = $request['email'];
        $order->phone = $request['phone'];
        $order->price = $request['price'];
        $order->discount = isset($request['discount']) ? $request['discount'] : 0;
        $order->total = $this->get_discount($order->price, $order->discount);
        $order->subtotal = $request['price'];
        $order->pay_now = isset($request['pay_now']) ? $request['pay_now'] : 0;
        $order->pay_the_day = isset($request['pay_the_day']) ? $request['pay_the_day'] : 0;
        $order->gift_remarks = isset($request['gift_remarks']) ? $request['gift_remarks'] : null;
        $order->gift_recipient = isset($request['gift_recipient']) ? $request['gift_recipient'] : null;
        $order->gift_name = isset($request['gift_name']) ? $request['gift_name'] : null;
        $order->coupons_id = isset($request['coupon']) ? $request['coupon']->id : null;
        $order->remarks = isset($request['remarks']) ? $request['remarks'] : null;

        $order->date_event = isset($request['date_event']) && $request['date_event'] != null 
            ? $this->fec_str_to_mysql($request['date_event']) : null;
        $order->time = isset($request['time']) && $request['time'] != null ? $request['time'] : null;
        $order->quantity = $request['quantity'];
        $order->items_id = $request['item']->id;

        $order->save();

        $order->code = 'TDV-' . date('y') . '-' . $order->id;
        $order->save();

        return $order;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _show(Orders $order)
    {
        return $order;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _update($request, Orders $order)
    {
        $order->status = $request['status'];
        $order->is_gift = isset($request['is_gift']) ? $request['is_gift'] : $order->is_gift;
        if(isset($request['name'])){
            $order->name = $request['name'];
        }
        if(isset($request['email'])){
            $order->email = $request['email'];
        }
        if(isset($request['phone'])){
            $order->phone = $request['phone'];
        }
        if(isset($request['quantity'])){
            $order->quantity = $request['quantity'];
        }
        if(isset($request['items_id'])){
            $order->items_id = $request['items_id'];
        }
        if(isset($request['price'])){
            $order->price = $request['price'];
        }
        if(isset($request['time'])){
            $order->time = $request['time'] != null ? $request['time'] : null;
        }
        
        if($request['status'] == 'confirmed'){
            $order->date_confirmed = date('Y-m-d H:i:s');
        }
        // self::_send_telegram($order);
        
        $order->save();

        return $order;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _destroy(Orders $order)
    {
        $order->delete();
        return true;
    }

    protected function _send_telegram($order){
        $item = $order->item;
        $cadena = "- " . $order->quantity . " x " . $item->name . " \n";
        $cadena .= "Email: " . $order->email . " \n";
        if($order->items_id != null){
            $cadena .= "Empresa: " . $order->item->user->name . " \n";
        }
        if($order->is_gift == 'S'){
            $cadena .= "Es para regalo" . " \n";
        }

        $cadena .= "\n" . '<a href="' . 
            route('orders.confirm_visit', [
                'order_encrypted' => base64_encode($order->id)
            ]) . '">Enlace para el proveedor</a>' . " \n\n";

        $cadena .= '<a href="' . 
            route('orders.confirm_payment', [
                'order_encrypted' => base64_encode(json_encode(['user' => 1, 'order' => $order->id]))
            ]) . '">Para confirmar pago a proveedor</a>';
        $this->sendTelegram("<strong>Nova comanda CONFIRMADA i PAGADA a sa web: ". $order->code ."</strong> \n" . $cadena);
    }

    protected function _get_rules_create($params){
        $rules = array();
        $rules['phone'] = 'nullable|numeric|digits_between:6,15';
        $rules['email'] = 'required|email|max:250';

        return $rules;
    }
    
    protected function _get_messages(){
        $messages = array();
        $messages['phone.numeric'] = trans('order.validation_phone_numeric');
        $messages['phone.max'] = trans('order.validation_phone_max');
        $messages['email.required'] = trans('order.validation_email_required');
        $messages['email.email'] = trans('order.validation_email_email');
        $messages['email.max'] = trans('order.validation_email_max');

        return $messages;
    }

    /*protected function _refresh_cart($order){
        $order['discount'] = isset($order['discount']) ? $order['discount'] : 0;
        $order['price'] = $order['price_unit'] * $order['quantity'];
        $order['pay_now'] = 0;
        $order['pay_the_day'] = 0;

        // si se ha puesto para regalo lo pagarán todo hoy
        if($order['submit'] == 'gift'){
            $order['pay_now'] += $order['quantity'] * self::get_discount($order['price_unit'], $order['discount']);
            $order['pay_the_day'] = 0;
        }else{
            if($order['item']->confirmation == 'direct'){
                $order['pay_now'] = 0;
                $order['pay_the_day'] += $order['quantity'] * self::get_discount($order['price_unit'], $order['discount']);
            }
            if($order['item']->confirmation == 'fee'){
                $order['pay_now'] += $order['quantity'] * 
                    self::get_discount(($order['price_unit'] - $order['item']->cost), $order['discount']);
                $order['pay_the_day'] += $order['quantity'] * self::get_discount($order['item']->cost, $order['discount']);
            }
        }

        $order['subtotal'] = self::get_discount($order['price'], $order['discount']);
        $order['total'] = $order['subtotal'];
        $order['total_sin_iva'] = number_format($order['total'] / 1.21, 2);
        $order['iva'] = $order['total'] - $order['total_sin_iva'];

        return $order;
    }*/
}
