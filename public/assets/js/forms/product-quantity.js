$(function(){
    $( ".add_button" ).click(function(event){
        var index = parseInt($(this).attr('index'));
        var value = parseInt($('#qty' + index).val());
        if(index != ''){
            $('#qty' + index).val(value + 1);
        }
        event.preventDefault();
    });
    $( ".sub_button" ).click(function(event){
        var index = parseInt($(this).attr('index'));
        var value = parseInt($('#qty' + index).val());
        if(index != ''){
            if(value - 1 >= 0){
                $('#qty' + index).val(value - 1);
            }
        }
        event.preventDefault();
    });
});
