@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section('title', trans('meta.title_index_event'))
@section ('meta')
    <meta name="description" content="{{ trans('meta.desc_index_event') }}"/>
    <meta name="keywords" content="{{ trans('meta.keys_index_event') }}" />
    <meta name="Author" content="{{ trans('meta.author') }}" />
    <meta property="og:title" content="{{ trans('meta.title_index_event') }}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:image" content="{{ url('assets/img/home/enoturismo-en-mallorca.webp') }}"/>
    <meta property="og:description" content="{{ trans('meta.desc_index_event') }}"/>
    @parent
@stop

@section ('body')
<div class="breadcrumbs-v4 breadcrumbs-day">
    <div class="container">
        <span class="page-name">{{ trans('plan.eventos') }}</span>
        <h1>{{ trans('inicio.routes_mensaje_guias_vino') }}</h1>
        <ul class="breadcrumb-v4-in">
            <li><a href="{{ route('/') }}">{{ trans('inicio.inicio') }}</a></li>
            <li class="active">{{ trans('inicio.routes_mensaje_guias_vino') }}</li>
        </ul>
    </div>
</div>
<div class="content container">
    <div class="row">
        <div class="col-md-12">
            <div class="row margin-bottom-5">
                <div class="col-sm-8 result-category">
                    <h2>{{ trans('inicio.routes_mensaje_guias_vino') }}</h2>
                </div>
            </div>
            <div class="row news-v1 margin-bottom-20">
                @foreach($query as $blog)
                    @include('blogs.element', ['blog' => $blog])
                    @if($loop->iteration % 3 == 0)
                        </div>
                        <div class="row news-v1 margin-bottom-20"> 
                    @endif
                @endforeach
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    {{ $query->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section ('assets_end')

@stop
