<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
			$table->string('code', 30)->nullable();
            $table->enum('status', ['pending', 'confirmed', 'prepared', 'sent', 'deleted'])->default('pending');
            $table->timestamp('date_paid')->nullable();
			$table->timestamp('date_confirmed')->nullable();
			$table->timestamp('date_prepared')->nullable();
            $table->timestamp('date_plan_confirmed')->nullable();
			$table->timestamp('date_sent')->nullable();

            $table->date('date_event')->nullable();

            $table->unsignedInteger('quantity')->nullable();

            $table->float('price', 5, 2)->default(0);
			$table->float('shipping', 5, 2)->default(0);
            $table->unsignedInteger('discount')->default(0);

            $table->float('subtotal')->default(0);
            $table->float('total')->default(0);
            $table->float('pay_now')->default(0);
            $table->float('pay_the_day')->default(0);

            $table->string('language')->nullable();

            $table->unsignedInteger('items_id');
            $table->foreign('items_id')->references('id')->on('items');

            $table->unsignedInteger('users_id')->nullable();
            $table->foreign('users_id')->references('id')->on('users');

            $table->unsignedInteger('coupons_id')->nullable();
            $table->foreign('coupons_id')->references('id')->on('coupons');

            $table->text('remarks')->nullable();

			$table->string('tracking_code', 255)->nullable();//el código de seguimiento de Correos
            $table->string('postal_code', 15)->nullable();
            $table->timestamps();
			
            $table->index('code');
            $table->index('price');
            $table->index('date_event');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
