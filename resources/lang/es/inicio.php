<?php

$idioma = array();

$idioma['inicio'] = 'Inicio';
$idioma['login'] = 'Login';
$idioma['registrarse'] = 'Registrarse';
$idioma['registrarse_en'] = 'Registro en ' . config('constants.NAMEWEB');
$idioma['nombre_empresa'] = 'Nombre de la empresa';
$idioma['email'] = 'Email';
$idioma['password'] = 'Contraseña';
$idioma['confirm_password'] = 'Confirma contraseña';
$idioma['olvidaste_password'] = '¿Olvidaste tu contraseña?';
$idioma['recordarme'] = 'Recordarme';
$idioma['acceder'] = 'Acceder';
$idioma['acceder_a_tu_cuenta'] = 'Acceder a tu cuenta';

$idioma['bienvenido_a'] = 'Bienvenido a ' . config('constants.NAMEWEB');

$idioma['titulo_login_1'] = 'Control de stock';
$idioma['mensaje_login_1'] = 'Con ' . config('constants.NAMEWEB') . ' podrás mantener un control de tu stock y de los pedidos que vayas recibiendo.';
$idioma['titulo_login_2'] = 'Mejora la visibilidad';
$idioma['mensaje_login_2'] = 'Diversifica tu oferta y recibe pedidos desde nuestra web.';
$idioma['titulo_login_3'] = 'Producto mallorquín de km0';
$idioma['mensaje_login_3'] = 'Estamos comprometidos con el comercio local y reivindicamos la calidad de los productos de Mallorca';

$idioma['enoturismo'] = 'Enoturismo';
$idioma['turismo_de_vino'] = 'Turismo del vino';

$idioma['enotourism-mallorca'] = 'Enoturismo en Mallorca';
$idioma['msg_h5_inicio'] = 'Muchas opciones para regalar en bodegas, hoteles y restaurantes';
$idioma['msg_h1_inicio'] = 'Descubre el vino mallorquín a través de catas, eventos y maridajes';

$idioma['enotourism-mallorca-slug'] = 'enoturismo-en-mallorca';
$idioma['vinos_de_mallorca'] = 'Vinos de Mallorca';

$idioma['ver_todos'] = 'Ver todos';

$idioma['regalos_y_lotes'] = 'Regalos y lotes';

$idioma['destacados'] = 'Destacados';
$idioma['destacado'] = 'Destacado';
$idioma['tipo'] = 'Tipo';
$idioma['precio'] = 'Precio';
$idioma['bodegas'] = 'Bodegas';
$idioma['packs'] = 'Packs';
$idioma['planes'] = 'planes';
$idioma['recomendaciones'] = 'Recomendaciones';
$idioma['mas_vendidos'] = 'Más vendidos';
$idioma['regala_cata_de_vino'] = 'Regala catas de vino';
$idioma['por_tipo'] = 'Por tipo';
$idioma['por_precio'] = 'Por precio';

$idioma['packs_slug'] = 'packs';
$idioma['recomendaciones_slug'] = 'recomendaciones';
$idioma['mas_vendidos_slug'] = 'mas-vendidos';

$idioma['carrito'] = 'Carrito';
$idioma['carrito_numero'] = 'Tu carrito tiene :num artículos';
$idioma['revisa_tu_pedido'] = 'Revisa y modifica tu pedido';
$idioma['informacion_de_facturacion'] = 'Facturación';
$idioma['informacion_envio'] = 'Información de dirección y envío';
$idioma['pago'] = 'Pago';
$idioma['informacion_pago'] = 'Selecciona método de pago';

$idioma['articulo'] = 'Artículo';
$idioma['cuando'] = 'Cuando';
$idioma['cantidad'] = 'Cantidad';
$idioma['total'] = 'Total';
$idioma['subtotal'] = 'Subtotal';
$idioma['envio'] = 'Envío';
$idioma['anterior'] = 'Anterior';
$idioma['siguiente'] = 'Siguiente';
$idioma['codigo_descuento'] = 'Código de descuento';
$idioma['introduce_codigo_descuento'] = 'Introduce tu cupón de descuento';
$idioma['aplicar_cupon'] = 'Aplicar cupón';
$idioma['calcular_envio'] = 'Calcular envío';
$idioma['introduce_codigo_postal'] = 'Introduce tu código postal';
$idioma['actualizar_totales'] = 'Actualizar totales';
$idioma['actualizar_carrito'] = 'Actualizar';

$idioma['lotes_de_vinos_y_productos_mallorquines'] = 'Lotes de vinos y productos mallorquines';
$idioma['msg_vi_mallorqui'] = '¿Quieres probar el vino mallorquín en casa?';
$idioma['msg_vi_mallorqui2'] = 'Selección de vinos mallorquines de bodegas familiares y con producciones limitadas directamente en tu casa con envío gratuito si vives en Islas Baleares';

$idioma['cookies_message'] = 'Este sitio web utiliza Cookies propias y de terceros, para recopilar información con la finalidad de mejorar nuestros servicios y mostrarle publicidad relacionada con sus preferencias, en base a un perfil elaborado a partir de sus hábitos de navegación.';

$idioma['aceptar'] = 'Aceptar';
$idioma['llamar'] = 'Llamar';

$idioma['plans_for_day'] = 'Planes para el día :day'; 
$idioma['plans_for_weekend'] = 'Planes para el fin de semana';

$idioma['contenido_carrito'] = 'Contenido del carrito';

$idioma['politica_cookies'] = 'Política de cookies';
$idioma['politica_privacidad'] = 'Políticas de privacidad';

$idioma['msg_search'] = 'Buscar experiencias, planes, bodegas, lugares...';


$idioma['comprar_vino_mallorquin'] = 'Comprar vino mallorquín';
$idioma['tienda_de_vinos'] = 'Tienda de vinos';
$idioma['vinos_de_mallorca-slug'] = 'vinos-de-mallorca';

$idioma['msg_publica_evento'] = 'Publica gratis tu evento relacionado con el vino en Mallorca';
$idioma['msg_publica_evento_btn'] = 'Publicar evento';

$idioma['proximos_eventos'] = 'Próximos eventos';
$idioma['eventos_con_vino_en_mallorca'] = 'Eventos con vino en Mallorca';
$idioma['routes_mensaje_guias_vino'] = 'Información y guías del vino en Mallorca';
$idioma['routes_mensaje_titulo'] = 'Guías y rutas de vino';
return $idioma;