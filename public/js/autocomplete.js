$(document).ready(function() {
	const autoCompleteJS = new autoComplete({
	  name: "food & drinks",
	  data: {
	    src: async function () {
	      // Loading placeholder text
	      document.querySelector("#autoComplete").setAttribute("placeholder", "Loading...");
	      const lang = document.querySelector("#autoComplete").getAttribute("language");
	      // Fetch External Data Source
	      const source = await fetch("../../../search/home_search_"+lang+".json");
	      const data = await source.json();
	      // Post Loading placeholder text
	      document.querySelector("#autoComplete").setAttribute("placeholder", autoCompleteJS.placeHolder);
	      // Returns Fetched data
	      return data;
	    },
	    key: ["name"],
	    results: (list) => {
	      // Filter duplicates
	      const filteredResults = Array.from(new Set(list.map((value) => value.match))).map((food) => {
	        return list.find((value) => value.match === food);
	      });

	      return filteredResults;
	    },
	  },
	  trigger: {
	    event: ["input", "focus"],
	  },
	  searchEngine: "strict",
	  highlight: true,
	  maxResults: 5,
	  resultItem: {
	    content: (data, element) => {
	      // Modify Results Item Style
	      element.style = "display: flex; justify-content: space-between;";
	      // Modify Results Item Content
	      var cadena ="";
	      cadena = `<span style="text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">
	            ${data.match}
	        </span>`;
	      const show_company = document.querySelector("#autoComplete").getAttribute("show_company");
	      if(show_company == 'S'){
	        cadena += `<span style="display: flex; align-items: center; font-size: 13px; font-weight: 100; text-transform: uppercase; color: rgba(0,0,0,.2);">
	            ${data.value.user}
	        </span>`;
	      }
	      element.innerHTML = cadena;
	    },
	  },
	  noResults: (dataFeedback, generateList) => {
	    // Generate autoComplete List
	    generateList(autoCompleteJS, dataFeedback, dataFeedback.results);
	    // No Results List Item
	    const result = document.createElement("li");
	    result.setAttribute("class", "no_result");
	    result.setAttribute("tabindex", "1");
	    result.innerHTML = `<span style="display: flex; align-items: center; font-weight: 100; color: rgba(0,0,0,.2);">Found No Results for "${dataFeedback.query}"</span>`;
	    document.querySelector(`#${autoCompleteJS.resultsList.idName}`).appendChild(result);
	  },
	  onSelection: (feedback) => {
	    document.querySelector("#autoComplete").blur();
	    // Prepare User's Selected Value
	    const selection = feedback.selection.value[feedback.selection.key];
	    // Replace Input value with the selected value
	    document.querySelector("#autoComplete").value = selection;
	    document.querySelector("#home_search").value = feedback.selection.value.id;
	  },
	});
});