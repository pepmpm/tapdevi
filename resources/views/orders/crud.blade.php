@inject('helpers', 'App\Http\Controllers\AuxController')
@php($locale = 'ES')
@extends('_layouts/inicio')

@section ('body')

<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left">{{ trans('order.pedido_finalizado') }}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="index.html">{{ trans('inicio.inicio') }}</a></li>
            <li class="active">{{ trans('order.pedido_finalizado') }}</li>
        </ul>
    </div>
</div>

<div class="container content">
    <div class="row invoice-header">
        <div class="col-xs-6">
            <h2 class="pull-left">{{ trans('order.factura') }}</h2>
        </div>
    </div>

    <div class="row invoice-info">
        <div class="col-xs-6">
            <div class="tag-box tag-box-v3">
                <h2>{{ trans('order.tu_informacion') }}:</h2>
                <ul class="list-unstyled">
                    <li><strong>{{ trans('order.nombre') }}:</strong> {{ $order->name }}</li>
                    <li><strong>{{ trans('order.email') }}:</strong> {{ $order->email }}</li>
                    <li><strong>{{ trans('order.telefono') }}:</strong> {{ $order->phone }}</li>
                </ul>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="tag-box tag-box-v3">
                <h2>{{ trans('order.datos_del_pedido') }}:</h2>
                <ul class="list-unstyled">
                    <li><strong>{{ trans('order.pedido') }}:</strong> {{ $order->code }}</li>
                    <li><strong>{{ trans('order.estado') }}:</strong> {{ $order->status }}</li>
                    <li><strong>{{ trans('order.fecha') }}:</strong> {{ $helpers->fechora_mysql_to_str($order->date_confirmed) }}</li>
                </ul>
            </div>
        </div>
    </div>
    
    @php($item = $order->item)
    
    <div class="panel panel-default margin-bottom-40">
        <div class="panel-heading">
            <h3 class="panel-title">{{ trans('order.pedido') }}</h3>
        </div>

        @if($item->type == 'plan' && $order->is_gift == 'N')
            <div class="panel-body">
                <p>{{ trans('order.mensaje_factura_' . $item->confirmation) }}</p>
            </div>
        @endif
        <table class="table table-striped invoice-table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{ trans('mail.nombre') }}</th>
                    <th>
                        @if($item->type == 'plan')
                            {{ trans('mail.fecha') }}
                        @endif
                    </th>
                    <th>{{ trans('mail.cantidad') }}</th>
                    <th>{{ trans('mail.precio') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>{{ $item->name }}</td>
                    <td width="15%">
                        @if($item->type == 'plan' && $order->date_event != null)
                            <div class="row">
                                <div class="col-md-12">
                                    <span id="span_date_temp_{{ $item->id }}">
                                        {{ 
                                            $helpers->fec_mysql_to_str($order->date_event) . 
                                            ($order->time != null ? ' (' . $order->time . 'h)' : '')
                                        }}
                                    </span>
                                </div>
                            </div>
                        @else
                            {{ trans('plan.contacta_establecimiento_reservar') }}
                        @endif
                    </td>
                    <td>
                        {{ $order->quantity . " " . $item->get_unit_name() }}
                    </td>
                    <td>
                        {{ $order->price . " €" }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <!--End Invoice Table-->

    <!--Invoice Footer-->
    <div class="row">
        <div class="col-xs-6">
        </div>

        <div class="col-xs-6 text-right">
            <ul class="list-unstyled invoice-total-info">
                @if($order->coupons_id != null)
                    <li><strong>{{ trans('order.codigo_descuento_aplicado', ['code' => $order->coupon->code]) }}</strong></li>
                @endif
                <li><strong>{{ trans('inicio.subtotal') }}:</strong> {{ $order->subtotal . " €" }}</li>
                @if($order->coupons_id != null)
                    <li><strong>{{ trans('inicio.codigo_descuento') }}:</strong> {{ "- " . $order->coupon->discount . "%" }}</li>
                @endif
                <li><strong>{{ trans('mail.precio_total') }}:</strong> {{ $order->total . " €" }}</li>
            </ul>

            <ul class="list-unstyled invoice-total-info">
                @if($order->pay_now > 0)
                    <li><strong>{{ trans('mail.ya_pagado') }}:</strong> {{ $order->pay_now . " €" }}</li>
                    <li><strong>{{ trans('mail.a_pagar_el_dia') }}:</strong> {{ $order->pay_the_day . " €" }}</li>
                @endif
            </ul>
        </div>
    </div>
</div>
@include('orders._modal_contact_info')
@stop

@section ('assets_end')

@stop
