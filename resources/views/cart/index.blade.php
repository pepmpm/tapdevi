@inject('helpers', 'App\Http\Controllers\AuxController')

@extends('_layouts/inicio')

@section ('body')

<div class="breadcrumbs-v4 breadcrumbs-carrito">
    <div class="container">
        <span class="page-name">{{ trans('inicio.carrito_numero', ['num' => session()->has('cart') ? session('cart')['quantity'] : 0]) }}</span>
        <h1>{{ trans('inicio.carrito') }}</h1>
        <ul class="breadcrumb-v4-in">
            <li><a href="{{ $helpers->show_menu() ? route('/') : route('orders.index') }}">{{ trans('inicio.inicio') }}</a></li>
            <li class="active">{{ trans('inicio.carrito') }}</li>
        </ul>
    </div>
</div>

<div class="container content bgcolor-light-grey3">
    <div class="shopping-cart">
        <div class="div_index_content">
            @include('cart._index_content')
        </div>
    </div>
</div>

@stop

@section ('assets_end')
    <script type="text/javascript" src="{{ asset('assets/js/forms/product-quantity.js') }}"></script>
    <script src="{{ asset('js/orders.js') }}"></script>
    
    <script type="text/javascript">
        $(function(){
            $( "#sel_quantity" ).live("change", function(event) {
                $('.div_total').html('<img src="{{ asset('assets/img/spinner-200px.gif') }}">');
                $.post(ARNY.baseUrl + '/cart/update', 
                    { quantity: $(this).val() },
                    function(data){
                        $( ".div_index_content" ).html( data );
                    }
                );
            });

            $( "#txt_price" ).live("change", function(event) {
                $('.div_total').html('<img src="{{ asset('assets/img/spinner-200px.gif') }}">');
                $.post(ARNY.baseUrl + '/cart/update', 
                    { price: $(this).val() },
                    function(data){
                        $( ".div_index_content" ).html( data );
                    }
                );
            });

            $( "#btn_coupon" ).live("click", function(event) {
                var coupon = $('#coupons_id').val();
                $('.div_total').html('<img src="{{ asset('assets/img/spinner-200px.gif') }}">');
                $.get( ARNY.baseUrl + '/cart/coupon/' + coupon, function( data ) {
                    $( ".div_index_content" ).html( data );
                });
            });
        });
    </script>
@stop
