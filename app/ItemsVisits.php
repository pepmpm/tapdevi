<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemsVisits extends Model
{
    protected $fillable = ['items_id', 'year', 'month'];

    public function item(){
        return $this->belongsTo('\App\Items', 'items_id');
    }
}
