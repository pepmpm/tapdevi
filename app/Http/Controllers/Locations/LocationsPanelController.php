<?php

namespace App\Http\Controllers\Locations;

use App\Locations;
use Illuminate\Http\Request;

class LocationsPanelController extends LocationsController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['query'] = parent::_index();
        return view('_back.locations.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('_back.locations.crud', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->data['query'] = parent::_store($request->all());

        if($this->data['query'] == null){
            return redirect()->back()->with('message_error', 'Debes informar el nombre');
        }

        return redirect()
            ->route('back.admin.locations.show', array('location' => $this->data['query']->id))
            ->with('message_success', 'Cambios guardados correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function show(Locations $location)
    {
        $this->data['query'] = parent::_show($location);
        return view('_back.locations.crud', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Locations $location)
    {
        $this->data['query'] = parent::_update($request->all(), $location);

        if($this->data['query'] == null){
            return redirect()->back()->with('message_error', 'Debes informar el nombre y subir una foto');
        }

        return redirect()
            ->route('back.admin.locations.show', array('location' => $this->data['query']->id))
            ->with('message_success', 'Cambios guardados correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Locations $location)
    {
        parent::_destroy($location);
        return redirect()
            ->route('back.admin.locations.index')
            ->with('message_success', 'Cambios guardados correctamente');
    }

    public function get_districts(Request $request){
        $districts = \App\Districts::where('locations_id', '=', $request->get('locations_id'))
            ->orderBy('name', 'asc')->get();

        $json = array();
        foreach ($districts as $district) {
            $json[] = array('id' => $district->id, 'text' => $district->name);
        }
        return json_encode($json, JSON_UNESCAPED_UNICODE);
    }
}
