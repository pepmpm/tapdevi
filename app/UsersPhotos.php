<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersPhotos extends _BaseModel{

	protected $fillable = array('order', 'url', 'origin', 'users_id');

    public function __construct(array $attributes = array()) {
        parent::__construct($attributes);
    }

    public function user(){
        return $this->belongsTo('\App\User', 'users_id');
    }

}
