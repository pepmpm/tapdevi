<h2>Fotos del municipio</h2>
<div class="row margin-bottom-40">
    @include('_layouts.messages', array('errors' => $errors))
    <div class="alert alert-danger fade in" id="alert-subir-foto" style="display: none">
        <button class="close" type="button" data-dismiss="alert">×</button>
        Ahora debes pulsar el botón de Subir Foto para guardarla en la base de datos.
    </div>
    <form method="post" enctype="multipart/form-data" 
          action="{{ route('back.admin.locations.photos.store') }}">
        <input type="hidden" name="locations_id" value="{{ $query->id }}"/>
        @csrf
        <div class="col-md-7">
            <input id="p_foto" type="hidden" name="p_foto">
        </div>
        <div class="col-md-2">
            <button class="btn btn-primary" type="submit">
                <i class="fa fa-upload"></i>&nbsp;Subir foto
            </button>
        </div>
    </form>
</div>
@if (count($query->photos) > 0)
<div class="row margin-bottom-30">
    <div class="col-md-12 cube-portfolio container">
        <div id="grid-container" class="cbp-l-grid-agency">
            @foreach($query->photos as $key => $photo)
            <div class="cbp-item bgcolorgris" style="height: 250px">
                <div class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img src="{{ url('/') . '/' . $photo->url }}" class="img-responsive"/> 
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignCenter">
                            <div class="cbp-l-caption-body">
                                <ul class="link-captions">
                                    <li>
                                        <form action="{{ route('back.admin.locations.photos.destroy', $photo->id) }}" method="POST">
                                            <input type="hidden" name="locations_id" value="{{ $query->id }}">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-link padding0"
                                                 onclick="if(!confirm('¿Estás seguro de querer borrar la foto?')) return false">
                                                <i class="rounded-x fa fa-trash bgcolor-rojo"></i>
                                            </button>
                                        </form>
                                    </li>
                                    <li>
                                        <a href="{{ url('/').'/'.$photo->url }}" class="cbp-lightbox" data-title="{{ $query->slug }}">
                                            <i class="rounded-x fa fa-search"></i>
                                        </a>
                                    </li>
                                    <li>
                                        @php($url_facebook = url('/') . '/' . $photo->url)
                                        <a href="javascript: void(0);" onclick="window.open('http://www.facebook.com/sharer.php?u={{ $url_facebook }}', 'Facebook', 'toolbar=0, status=0, width=550, height=350');">
                                            <i class="rounded-x fa fa-facebook color-blanco" style="background-color: #425F9C !important"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <form action="{{ route('back.admin.locations.photos.update', $photo->id) }}" method="POST">
                                            <input type="hidden" name="locations_id" value="{{ $query->id }}">
                                            @method('PUT')
                                            @csrf
                                            <button type="submit" class="btn btn-link padding0">
                                                <i class="rounded-x fa fa-star"></i>
                                            </button>
                                        </form>
                                    </li>
                                </ul>
                                <div class="cbp-l-grid-agency-desc">{{ $query->slug }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endif