<?php

$idioma = array();

$idioma['gracias_por_comprar_en_'] = 'Gracies per comprar a ' . config('constants.NAMEWEB');
$idioma['codigo_del_pedido'] = 'Codi de sa comanda';
$idioma['tus_datos'] = 'Ses teves dades';
$idioma['nombre'] = 'Nom';
$idioma['email'] = 'Email';
$idioma['telefono'] = 'Telèfon';
$idioma['poblacion'] = 'Població';
$idioma['direccion'] = 'Direcció';

$idioma['para_ver_la_ubicacion_en_google_maps'] = "Per veure s'ubicació a Google maps pitja";
$idioma['aqui'] = 'aquí';
$idioma['comentarios'] = 'Comentaris';
$idioma['pedido'] = 'Comanda';

$idioma['cantidad'] = 'Quantity';
$idioma['precio'] = 'Preu';
$idioma['precio_total'] = 'Preu total';

$idioma['de'] = 'de';
$idioma['fecha'] = 'Data';
$idioma['informacion'] = 'Informació';
$idioma['detalle'] = 'Detall';
$idioma['a_pagar_ahora'] = 'A pagar ara';
$idioma['ya_pagado'] = 'Ja pagat';
$idioma['a_pagar_el_dia'] = "A pagar es dia";
$idioma['es_un_regalo'] = "Aquesta activitat ja està pagada. Enhorabona!";
$idioma['precio_envio'] = "Preu d'enviament";
$idioma['lugar'] = 'Bodega';

$idioma['reservado_el_dia'] = 'Reservat el día';
$idioma['fecha_propuesta'] = 'Data proposada';
$idioma['valido_hasta_el_'] = 'Vàlid fins el :day';

$idioma['mensaje_qr'] = "Has d'imprimir o dur en el mòbil aquest bono. <br/>Codi QR a escanejar el dia de la visita per marcar-la com a realitzada";
$idioma['mensaje_pdf_pack'] = "Per veure els plans inclosos a n'aquest pack pitja al seguent enllaç: ";

return $idioma;