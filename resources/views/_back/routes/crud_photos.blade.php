<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label>Subir fotos</label>
            <div id="fileuploader">Upload</div>
        </div>
    </div>
</div>
@if(old('p_foto', null) != null)
    <div class="row margin-bottom-20">
        @foreach(old('p_foto') as $tmpphoto)
            <div class="col-md-6">
                <input type="hidden" name="p_foto[]" value="{{ $tmpphoto }}">
                <a class="fancybox" data-rel="galeria_fotos"
                    href="{{ url('/') . '/' . $tmpphoto }}">
                    <img src="{{ url('/') . '/' . $tmpphoto }}" class="img-responsive">
                </a>
                
            </div>
            @if($loop->iteration % 2 == 0)
                </div><div class="row"> 
            @endif
        @endforeach
    </div>
@endif
@if(isset($query))
    @foreach($query->photos as $tmpphoto)
        @php($found = true)
        @foreach($query->langs as $lang)
            @if(strpos($lang->description, 'TDV_IMAGE_' . $tmpphoto->id) === false)
                @php($found = false)
            @endif
        @endforeach
        <div class="row margin-bottom-20 team-v3">
            <div class="col-md-12">
                <div class="team-img">
                    <img src="{{ url('/') . '/' . $tmpphoto->url }}" class="img-responsive {{ $found ? '' : 'border-red' }}">
                    <div class="team-hover margin-top--40">
                        <ul class="list-inline team-social-v3">
                            <li>
                                <a class="fancybox" data-rel="galeria_fotos"
                                    title="{{ $query->name }}"
                                    href="{{ url('/') . '/' . $tmpphoto->url }}">
                                    <i class="rounded-x fa fa-search"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('back.blogs.photos.destroy', [$tmpphoto->id]) }}">
                                    <i class="rounded-x fa fa-trash"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="btn_insert_text" image="TDV_IMAGE_{{ $tmpphoto->id }}">
                                    <i class="rounded-x fa fa-hand-o-left" aria-hidden="true"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif