@inject('helpers', 'App\Http\Controllers\AuxController')
@php($locale = 'ES')
@extends('_layouts/inicio')

@section ('body')

<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left">Confirmar visita</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{{ route('/') }}">{{ trans('inicio.inicio') }}</a></li>
            <li class="active">Confirmar visita</li>
        </ul>
    </div>
</div>

<div class="container content">
    <div class="row invoice-info">
        <div class="col-xs-6">
            <div class="tag-box tag-box-v3">
                <h2>Información del cliente:</h2>
                <ul class="list-unstyled">
                    <li><strong>{{ trans('order.nombre') }}:</strong> {{ $order->name }}</li>
                    <li><strong>{{ trans('order.email') }}:</strong> {{ $order->email }}</li>
                    <li><strong>{{ trans('order.telefono') }}:</strong> {{ $order->phone }}</li>
                </ul>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="tag-box tag-box-v3">
                <h2>Información de la reserva:</h2>
                <ul class="list-unstyled">
                    <li><strong>{{ trans('order.pedido') }}:</strong> {{ $order->code }}</li>
                    <li><strong>{{ trans('order.estado') }}:</strong> {{ $order->status }}</li>
                    <li><strong>Creada el día:</strong> {{ $helpers->fechora_mysql_to_str($order->created_at, 'd/m/Y H:i') }}</li>
                </ul>
            </div>
        </div>
    </div>
    
    @php($item = $order->item)
    
    <div class="panel panel-default margin-bottom-40">
        <div class="panel-heading">
            <h3 class="panel-title">{{ trans('order.pedido') }}</h3>
        </div>

        <table class="table table-striped invoice-table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{ trans('mail.nombre') }}</th>
                    <th>{{ trans('mail.cantidad') }}</th>
                    <th>{{ trans('mail.precio') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>{{ $item->name }}</td>
                    <td>
                        {{ $order->quantity . " " . $item->get_unit_name() }}
                    </td>
                    <td>
                        {{ $order->price . " €" }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <!--End Invoice Table-->

    <!--Invoice Footer-->
    <div class="row">
        <div class="col-xs-6">
        </div>

        <div class="col-xs-6 text-right">
            <ul class="list-unstyled invoice-total-info">
                @if($order->coupons_id != null)
                    <li><strong>{{ trans('order.codigo_descuento_aplicado', ['code' => $order->coupon->code]) }}</strong></li>
                @endif
                <li><strong>{{ trans('inicio.subtotal') }}:</strong> {{ $order->subtotal . " €" }}</li>
                @if($order->coupons_id != null)
                    <li><strong>{{ trans('inicio.codigo_descuento') }}:</strong> {{ "- " . $order->coupon->discount . "%" }}</li>
                @endif
                <li><strong>{{ trans('mail.precio_total') }}:</strong> {{ $order->total . " €" }}</li>
            </ul>

            <ul class="list-unstyled invoice-total-info">
                <li><strong>El cliente ya ha pagado:</strong> {{ $order->pay_now . " €" }}</li>
                <li><strong>El cliente debe pagar el día de la actividad:</strong> {{ $order->pay_the_day . " €" }}</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="list-unstyled invoice-total-info">
                <li>
                    @if($order->date_visited != null)
                        @if($helpers->diferenciadias($order->date_visited, date('Y-m-d H:i')) <= 1)
                            <span class="label label-success font16 width100_">
                                La actividad se realizó el día {{ $helpers->fechora_mysql_to_str($order->date_visited, 'd/m/Y \a \l\a\s H:i') }}
                            </span>
                        @else
                            <a href="{{ route('orders.confirm_visit', ['order_encrypted' => $order_encrypted, 'set' => 'X']) }}"
                                class="btn btn-success width100_">
                                La actividad se realizó el día {{ $helpers->fechora_mysql_to_str($order->date_visited, 'd/m/Y \a \l\a\s H:i') }}
                            </a>
                        @endif
                    @else
                        <a href="{{ route('orders.confirm_visit', ['order_encrypted' => $order_encrypted, 'set' => 'S']) }}"
                            class="btn btn-u width100_" onclick="if(!confirm('¿Seguro? Esta operación no se puede revertir')) return false">
                            Pulsa aquí cuando se haya <br/>completado la visita
                        </a>
                    @endif
                </li>
            </ul>
        </div>
    </div>
</div>
@stop

@section ('assets_end')

@stop
