@if(isset($params['date']) && $params['date'] != null)
    @if($params['date'] == 'weekend')
        <div class="breadcrumbs-v4 breadcrumbs-weekend">
            <div class="container">
                <span class="page-name">{{ trans('inicio.turismo_de_vino') }}</span>
                <h1>
                    {{ trans('inicio.plans_for_weekend') }}
                </h1>
                <ul class="breadcrumb-v4-in">
                    <li><a href="{{ route('/') }}">Home</a></li>
                    <li><a href="{{ route('enotourism-mallorca.cat') }}">{{ trans('inicio.enotourism-mallorca') }}</a></li>
                    <li class="active">{{ trans('inicio.plans_for_weekend') }}</li>
                </ul>
            </div>
        </div>
    @else
        <div class="breadcrumbs-v4 breadcrumbs-day">
            <div class="container">
                <span class="page-name">{{ trans('inicio.turismo_de_vino') }}</span>
                <h1>
                    {{ trans('inicio.plans_for_day', ['day' => $params['date']]) }}
                </h1>
                <ul class="breadcrumb-v4-in">
                    <li><a href="{{ route('/') }}">Home</a></li>
                    <li><a href="{{ route('enotourism-mallorca.cat') }}">{{ trans('inicio.enotourism-mallorca') }}</a></li>
                    <li class="active">{{ trans('inicio.plans_for_day', ['day' => $params['date']]) }}</li>
                </ul>
            </div>
        </div>
    @endif
@elseif($params['district'] != null)
    <div class="breadcrumbs-v4" 
        style="{{ 'background-image: url(' . asset('assets/img/breadcrumbs/enotourism-mallorca.png') . ')' }}">
        <div class="container">
            <span class="page-name">{{ trans('inicio.turismo_de_vino') }}</span>
            <h1>
                {{ trans('inicio.enoturismo') . " " . trans('base.en') }}
                <span class="shop-green">
                    {{ $main->name }}
                </span>
            </h1>
            <ul class="breadcrumb-v4-in">
                <li><a href="{{ route('/') }}">Home</a></li>
                <li><a href="{{ route('enotourism-mallorca.cat') }}">{{ trans('inicio.enotourism-mallorca') }}</a></li>
                <li class="active">{{ $district->name }}</li>
            </ul>
        </div>
    </div>
@elseif($params['cat'] != null)
    <div class="breadcrumbs-v4" 
        style="{{ 'background-image: url(' . url('/') . '/' . $category->url . ')' }}">
        <div class="container">
            <span class="page-name">{{ trans('inicio.turismo_de_vino') }}</span>
            <h1>
                {{ $main->title }}&nbsp;
            </h1>
            <ul class="breadcrumb-v4-in">
                <li><a href="{{ route('/') }}">Home</a></li>
                <li><a href="{{ route('enotourism-mallorca.cat') }}">{{ trans('inicio.enotourism-mallorca') }}</a></li>
                <li class="active">{{ $main->title }}</li>
            </ul>
        </div>
    </div>
@else
    <div class="breadcrumbs-v4" 
        style="{{ 'background-image: url(' . asset('assets/img/breadcrumbs/enotourism-mallorca.png') . ')' }}">
        <div class="container">
            <span class="page-name">{{ trans('inicio.turismo_de_vino') }}</span>
            <h1>
                {{ trans('inicio.enotourism-mallorca') }}&nbsp;
            </h1>
            <ul class="breadcrumb-v4-in">
                <li><a href="{{ route('/') }}">Home</a></li>
                <li class="active">{{ trans('inicio.enotourism-mallorca') }}</li>
            </ul>        
        </div>
    </div>
@endif