@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section ('body')
<div class="breadcrumbs-v4 breadcrumbs-crearevento">
    <div class="container">
        <span class="page-name">{{ trans('plan.eventos') }}</span>
        <h1>{{ trans('plan.nueva_experiencia_de_enoturismo') }}</h1>
        <ul class="breadcrumb-v4-in">
            <li><a href="{{ route('/') }}">{{ trans('inicio.inicio') }}</a></li>
            <li class="active">{{ trans('plan.nueva_experiencia_de_enoturismo') }}</li>
        </ul>
    </div>
</div>
<div class="content container">
    <div class="row">
        <div class="col-md-12 text-center">
            @include('_layouts.messages', array('errors' => $errors))
            <h3>{{ trans('plan.gracias_por_enviar_el_evento') }}</h3>
            <p class="font18">{{ trans('plan.enseguida_que_lo_revisemos') }}</p>
            <img src="{{ asset('assets/img/wine-loading-180.gif') }}"/>
        </div>
    </div>
</div>

@stop

@section ('assets_end')
    
@stop
