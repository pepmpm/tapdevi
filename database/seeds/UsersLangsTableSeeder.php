<?php

use Illuminate\Database\Seeder;
use \App\User;
use \App\UsersLangs;

class UsersLangsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        \DB::table('users_langs')->truncate();

        UsersLangs::create(['title' => null, 'description' => null, 'seo_keywords' => null, 'seo_description' => null, 'users_id' => 1, 'locale' => 'es']);
        UsersLangs::create(['title' => null, 'description' => null, 'seo_keywords' => null, 'seo_description' => null, 'users_id' => 1, 'locale' => 'ca']);
        UsersLangs::create(['title' => null, 'description' => null, 'seo_keywords' => null, 'seo_description' => null, 'users_id' => 1, 'locale' => 'en']);

	}
}
