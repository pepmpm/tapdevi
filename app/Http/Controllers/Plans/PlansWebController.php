<?php

namespace App\Http\Controllers\Plans;

use App\Items;
use Illuminate\Http\Request;
use Cache;
use App\Traits\Helpers;
use App\Traits\Plans;

class PlansWebController extends PlansController
{

    use Helpers;
    use Plans;

    public function search(Request $request){
        if($request->has('cat')){
            return redirect()->route('enotourism-mallorca.cat', ['cat' => $request->get('cat')]);
        }
        if($request->has('dis')){
            return redirect()->route('enotourism-mallorca.dis', ['district' => $request->get('dis')]);
        }
        return redirect()->back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_dis(Request $request, $district = null)
    {
        $this->data['params'] = [
            'district' => $district,
            'cat' => null,
            'locale' => \App::getLocale(),
            'user' => null,
            'limit' => 15,
        ];

        self::_load_commons($this->data['params']);

        $this->data['query'] = parent::_search($this->data['params']);

        $this->data['main'] = null;
        if($this->data['params']['cat'] != null){
            $this->data['main'] = $this->data['category'];
        }
        if($this->data['params']['district'] != null){
            $this->data['main'] = $this->data['district'];
        }

        return view('plans.index', $this->data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_cat(Request $request, $cat = null)
    {
        $this->data['params'] = [
            'district' => null,
            'cat' => $cat,
            'locale' => \App::getLocale(),
            'user' => null,
            'limit' => 15,
        ];

        self::_load_commons($this->data['params']);

        $this->data['query'] = parent::_search($this->data['params']);

        $this->data['main'] = null;
        if($this->data['params']['cat'] != null){
            $this->data['main'] = $this->data['category'];
        }
        if($this->data['params']['district'] != null){
            $this->data['main'] = $this->data['district'];
        }

        return view('plans.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $params = $this->_get_params($request);
        $this->data['params'] = $params;
        self::_load_commons($this->data['params']);

        $plan = Items::with('langs', 'photos')->whereHas('langs', function ($query) use ($request) {
            $query->where('slug', '=', $request->route('item'));
            $query->where('locale', '=', \App::getLocale());
        })->whereIn('type', ['plan', 'pack'])->where('status', '=', 'active')->first();

        if($plan){
            $this->data['cross_selling'] = self::_get_cross_selling($plan);
            $this->data['query'] = parent::_show($plan);
            $this->data['languages'] = $plan->languages != null ? self::_get_languages(explode("#", $plan->languages)) : array();

            self::_save_visits($request, $plan->id);
            return view($plan->type . 's.crud', $this->data);
        }

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function edit(Bikes $bikes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bikes $bikes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bikes $bikes)
    {
        //
    }

    private function _get_category($params){
        if($params['cat'] != null){
            return Cache::remember('cat_' . $params['cat'] . "_" . $params['locale'], 1440, 
                function () use ($params) {
                    return \App\Labels::whereHas('langs', function ($query) use ($params) {
                            $query->where('slug', '=', $params['cat']);
                            $query->where('locale', '=', $params['locale']);
                        })->first();
                }
            );
        }

        return null;
    }

    private function _get_district($params){
        if($params['district'] != null){
            return Cache::remember('district_' . $params['district'], 1440, 
                function () use ($params) {
                    return \App\Districts::where('slug', '=', $params['district'])->first();
                }
            );
        }

        return null;
    }

    private function _get_cross_selling($plan){
        return Items::with('langs', 'user', 'photos')->where('type', '=', 'plan')->where('status', '=', 'active')
            ->where('id', '!=', $plan->id)->take(8)->get();
    }

    private function _save_visits($request, $plan){
        $cookie = config('constants.NAMELITE') . '.plan.' . $plan;
        if(!isset($_COOKIE[str_replace('.', '_', $cookie)])){
            $tmp = \App\ItemsVisits::firstOrNew(['items_id' => $plan, 'year' => date('Y'), 'month' => date('m')]);
            $tmp->items_id = $plan;
            $tmp->year = date('Y');
            $tmp->month = date('m');
            $tmp->{'day_' . date('j')} = $tmp->{'day_' . date('j')} + 1;
            $tmp->save();

            setcookie(config('constants.NAMELITE') . '.plan.' . $plan, 'X', time() + 86400);
        }
    }

    private function _load_commons($params){
        /*$this->data['day_stocks'] = \App\ItemsStocks::select('date')->distinct()->where('date', '>=', date('Y-m-d'))->pluck('date')->all();*/

        $this->data['category'] = self::_get_category($params);
        $this->data['district'] = self::_get_district($params);

        $this->data['districts_sidebar_plans'] = $this->_get_districts();
        $this->data['labels_sidebar_plans'] = $this->_get_labels();
        $this->data['users_sidebar_plans'] = $this->_get_users();

        //$this->data['index_events'] = $this->getEvents(['limit' => '15']);
    }

    private function _get_languages($values){
        $array = array();
        if(in_array("ES", $values)){
            $array['ES'] = 'Español';
        }
        if(in_array("CA", $values)){
            $array['CA'] = 'Catalán';
        }
        if(in_array("EN", $values)){
            $array['EN'] = 'Inglés';
        }
        if(in_array("DE", $values)){
            $array['DE'] = 'Alemán';
        }
        if(in_array("FR", $values)){
            $array['FR'] = 'Francés';
        }
        if(in_array("IT", $values)){
            $array['IT'] = 'Italiano';
        }
        if(in_array("RU", $values)){
            $array['RU'] = 'Ruso';
        }
        if(in_array("SU", $values)){
            $array['SU'] = 'Sueco';
        }
        if(in_array("PO", $values)){
            $array['PO'] = 'Portugués';
        }
        if(in_array("GR", $values)){
            $array['GR'] = 'Griego';
        }

        return $array;
    }
}
