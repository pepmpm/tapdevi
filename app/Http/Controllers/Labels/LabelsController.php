<?php

namespace App\Http\Controllers\Labels;

use App\Labels;
use App\LabelsLangs;
use Illuminate\Http\Request;
use App\Traits\Helpers;
use Intervention\Image\ImageManagerStatic as Image;

class LabelsController extends \App\Http\Controllers\Controller
{
    use Helpers;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function _index()
    {
        return Labels::whereNull('labels_id')->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function _all()
    {
        return Labels::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function _store($request)
    {
        $tmp = new Labels;
        $tmp->labels_id = $request['labels_id'];
        $tmp->category = $request['category'];
        $tmp->status = $request['status'];
        $tmp->icon = isset($request['icon']) ? $request['icon'] : null;
        $tmp->save();

        if($request['p_foto'] != null){
            $tmp = self::__upload_image($request, $tmp);
        }
        if($request['p_menu'] != null){
            $tmp = self::__upload_image_small($request, $tmp);
        }

        foreach(config('translatable.locales') as $keylocale => $locale){
            $lang = new LabelsLangs;
            $lang->locale = $keylocale;
            $lang->labels_id = $tmp->id;
            $lang->save();
        }

        return $tmp;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _show(Labels $location)
    {
        return $location;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _update($request, Labels $tmp)
    {
        $tmp->labels_id = $request['labels_id'];
        $tmp->category = $request['category'];
        $tmp->status = $request['status'];
        $tmp->icon = isset($request['icon']) ? $request['icon'] : null;
        $tmp->save();

        if(isset($request['p_foto']) && $request['p_foto'] != null){
            logger('entro1');
            $tmp = self::__upload_image($request, $tmp);
        }
        if(isset($request['p_menu']) && $request['p_menu'] != null){
            logger('entro2');
            $tmp = self::__upload_image_small($request, $tmp);
        }

        return $tmp;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _destroy(Labels $location)
    {
        $ruta = 'upload/labels/'.$location->id.'/';
        if(is_dir($ruta)){
            $success = \File::deleteDirectory($ruta, true);
            rmdir($ruta);
            if(!$success){
                throw new \Exception('El directorio de imagenes de '.$location->id.' no existe');
            }
        }

        $location->delete();

        return true;
    }

    private function __upload_image($request, $tmp){
        $ruta = 'upload/labels/'.$tmp->id.'/';
        \File::makeDirectory($ruta, 0775, true, true);

        // read image from temporary file
        $img = Image::make(base64_decode(substr($request['p_foto'], strpos($request['p_foto'], ",")+1)))->encode('webp', 90);

        $tmp->url = $ruta . $tmp->id . ".webp";
        $tmp->save();

        /*****************REDIMENSIONAMOS IMAGEN************************/
        $img->resize(1600, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->brightness(-10);
        $img->save($tmp->url);
        if(env("APP_ENV") != 'local'){
            $this->set_72dpi_image($tmp->url);
        }

        return $tmp;
    }

    private function __upload_image_small($request, $tmp){
        $ruta = 'upload/labels/'.$tmp->id.'/';
        \File::makeDirectory($ruta, 0775, true, true);

        // read image from temporary file
        $img = Image::make(base64_decode(substr($request['p_menu'], strpos($request['p_menu'], ",")+1)))->encode('webp', 90);

        $tmp->icon = $ruta . $tmp->category . "_" .  $tmp->id . ".webp";
        $tmp->save();

        /*****************REDIMENSIONAMOS IMAGEN************************/
        $img->resize(250, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->brightness(-20);
        $img->save($tmp->icon);
        if(env("APP_ENV") != 'local'){
            $this->set_72dpi_image($tmp->icon);
        }

        return $tmp;
    }
}
