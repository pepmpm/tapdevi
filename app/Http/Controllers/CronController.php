<?php 

namespace App\Http\Controllers;

use Cache;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\Voucher;
use Illuminate\Notifications\Notification;

class CronController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function reorder(Request $request) {

        /******************************/
        /***********ITEMS**************/
        /******************************/
        $tmpvisits = \App\ItemsVisits::where('year', '=', date('Y'))->where('month', '=', date('m'))->get();

        $items = array();
        //recorremos las visitas para este mes y año
        foreach($tmpvisits as $tmpvisit){
            //si está promocionado le sumamos 1000 para asegurar que salgan los primeros
            $items[$tmpvisit->items_id] = $tmpvisit->{'day_' . date('d')};
        }
        //ordenamos los items por visitas de forma descendente
        arsort($items);
        
        //seteamos todos los orders de los items
        \DB::table('items')->update(['order' => 999999]);

        //recorremos los items que tienen visitas para ordenarlos
        $index = 1;
        foreach($items as $keyitem => $tmpitem){
            $tmp = \App\Items::find($keyitem);
            $tmp->order = $index;
            $tmp->save();

            $index++;
        }

        /******************************/
        /***********USERS**************/
        /******************************/
        $tmpvisits = \App\UsersVisits::where('year', '=', date('Y'))->where('month', '=', date('m'))->get();

        $users = array();
        //recorremos las visitas para este mes y año
        foreach($tmpvisits as $tmpvisit){
            if($tmpvisit->item->is_promoted == 'S'){
                $items[$tmpvisit->items_id] = $tmpvisit->{'day_' . date('d')} + 1000;
            }else{
                $items[$tmpvisit->items_id] = $tmpvisit->{'day_' . date('d')};
            }
        }
        //ordenamos los users por visitas de forma descendente
        arsort($users);
        
        //seteamos todos los orders de los items
        \DB::table('users')->update(['order' => 999999]);

        //recorremos los items que tienen visitas para ordenarlos
        $index = 1;
        foreach($users as $keyuser => $tmpuser){
            $tmp = \App\User::find($keyuser);
            $tmp->order = $index;
            $tmp->save();

            $index++;
        }
    }

    public function send_emails(Request $request){
        echo "***********INIT***********" . "<br/>";
        $orders = \App\Orders::whereNull('date_mail_sent')->get();
        $orders = \App\Orders::where('id', '=', '69')->get();
        
        foreach($orders as $order){
            echo "email sent for voucher " . $order->code . "<br/>" . $order->email . "<br/>";
            Mail::send(new Voucher($order));
            $order->date_mail_sent = date('Y-m-d H:i:s');
            $order->save();

            if($order->date_event != null && $order->user->is_notificated_booking == 'S'){
                echo "sending notification for partner " . 
                    $order->item->user->name . "<br/>" . $order->item->user->email . "<br/>";
                $order->item->user->notify(new \App\Notifications\NewBooking($order));
            }
        }
        echo "***********END***********";
    }
}