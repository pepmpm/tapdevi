<?php

namespace App\Http\Controllers\PlansPhotos;

use App\Items;
use App\ItemsPhotos;
use Illuminate\Http\Request;
use Image;
use App\Traits\Helpers;

class PlansPhotosController extends \App\Http\Controllers\Controller
{
    use Helpers;

    public function __construct(array $attributes = array()) {
        parent::__construct($attributes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function _store($request)
    {
        $ruta = 'upload/plans/pending/';
        \File::makeDirectory($ruta, 0775, true, true);

        $file = $ruta . time() . str_random(6) . '.webp';

        $img = Image::make(file_get_contents($request['p_file']))->encode('webp', 90);
        $img->resize(1024, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($file);

        $this->set_72dpi_image($file);

		return $file;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Plans  $plans
     * @return \Illuminate\Http\Response
     */
    protected function _update($request, ItemsPhotos $items)
    {
        //pasamos todas las imagenes a orden 100
        ItemsPhotos::where('items_id', '=', $request['id'])
                ->update(array('order' => 100));

        //pasamos la imagen seleccionada a orden 1
        $items->order = 1;
        $items->save();

        return $items;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Plans  $plans
     * @return \Illuminate\Http\Response
     */
    protected function _destroy(ItemsPhotos $item)
    {
        $items_id = $item->items_id;

        $ruta = 'upload/plans/' . $item->items_id . '/';
        if(is_dir($ruta)){
            $success = \File::deleteDirectory($ruta, true);
            rmdir($ruta);
            if(!$success){
                throw new \Exception('El directorio de imagenes de ' . $item->name . ' no existe');
            }
        }

        $item->delete();

        return $items_id;
    }
}
