<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewBooking extends Notification
{
    use Queueable;
    protected $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(\App\Orders $order)
    {
         $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Hola!!')
                    ->line('Has recibido una nueva reserva para el día ' . $this->order->date_event
                        . ' a las ' . $this->order->time)
                    ->line('Para ' . $this->order->quantity . ' personas')
                    ->line('Pendiente de pago ' . $this->order->pay_the_day . ' euros')
                    ->line('Datos de contacto del cliente: ' 
                        . $this->order->name . ' | ' . $this->order->email)
                    ->from(config('constants.MAILWEB'), config('constants.NAMEWEB'))
                    ->subject("Nueva reserva: " . $this->order->code)
                    ->salutation('Un saludo');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
