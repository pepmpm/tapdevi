<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class Locations extends Model
{
    use Translatable;
    public $translatedAttributes = ['description', 'seo_description', 'seo_keywords'];

    public function langs(){
        return $this->hasMany('\App\LocationsLangs', 'locations_id');
    }

    public function routes(){
        return $this->belongsToMany('\App\Routes', 'routes_locations_pivot', 'locations_id', 'routes_id');
    }

    public function photos(){
        return $this->hasMany('\App\LocationsPhotos', 'locations_id')->orderBy('order', 'asc');
    }
}
