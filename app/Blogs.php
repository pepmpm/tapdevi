<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use App\Traits\Helpers;
use App\Blogs;

class Blogs extends Model
{
	use Helpers;
    use Translatable;
    public $translatedAttributes = ['name', 'title', 'slug', 'short_description', 'description', 'seo_description', 'seo_keywords', 'timetable'];
    protected $with = ['langs'];
    
    public function langs(){
        return $this->hasMany('\App\BlogsLangs', 'blogs_id')->orderBy('id', 'asc');
    }

    public function visits(){
        return $this->hasMany('\App\BlogsVisits', 'blogs_id');
    }

    public function photos(){
        return $this->hasMany('\App\BlogsPhotos', 'blogs_id')->orderBy('order', 'asc');
    }

    public function one_photo($default = ''){
        $photos = $this->hasMany('\App\BlogsPhotos', 'blogs_id')->orderBy('order', 'asc')->orderBy('id', 'asc');
        if($photos->count() > 0){
            return $photos->first()->url;
        }
        return $default . '/' . 'default.png';
    }
}
