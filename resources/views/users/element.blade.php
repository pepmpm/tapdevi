@php($tmpuser = $plan->user)
<div class="row">
    <div class="col-md-12 margin-bottom-20">
        <div class="funny-boxes funny-boxes-top-tdv">
            <div class="row">
                <div class="col-md-4 funny-boxes-img">
                    <a href="{{ route('enotourism-mallorca.item', 
                        ['district' => $tmpuser->district->slug, 'user' => $tmpuser->slug, 'item' => $plan->slug]) }}">
                        @if($agent->isMobile())
                            <img class="img-responsive" alt="{{ $plan->name }}"
                                src="{{ Imgfly::imgPublic(str_replace("upload/", "", $plan->one_photo()) . 
                                    '?w=' . (isset($img_width_mobile) ? $img_width_mobile : '302') . 
                                    '&h=' . (isset($img_height_mobile) ? $img_height_mobile : '250') . '&fit=crop-center', 'upload') }}">
                        @else
                            <img class="img-responsive" alt="{{ $plan->name }}"
                                src="{{ Imgfly::imgPublic(str_replace("upload/", "", $plan->one_photo()) . 
                                    '?w=' . (isset($img_width) ? $img_width : '383') . 
                                    '&h=' . (isset($img_height) ? $img_width : '250') . '&fit=crop-center', 'upload') }}">
                        @endif
                        @if($plan->is_promoted == 'S')
                            <p class="tdv-badge font12">
                                <strong>
                                    <span><i class="fa fa-star"></i>&nbsp;{{ trans('inicio.destacado') }}</span>
                                </strong>
                            </p>
                        @endif
                    </a>
                    <ul class="list-unstyled">
                        <li>
                            <i class="fa fa-map-pin color-tdv width14"></i>
                            <a href="{{ route('enotourism-mallorca.dis', 
                                ['district' => $tmpuser->district->slug]) }}">
                                {{ $tmpuser->district->name }}
                            </a>
                        </li>
                        <li>
                            <i class="fa fa-glass color-tdv width14"></i>
                            <a href="{{ route('enotourism-mallorca.user', 
                                    ['district' => $tmpuser->district->slug, 'user' => $tmpuser->slug]) }}">
                                {{ $tmpuser->name }}
                            </a>
                        </li>
                        <li>
                            <i class="fa fa-male fa-2x color-tdv width14" aria-hidden="true"></i>&nbsp;
                            {{ 'Min ' . $plan->min_pax }}
                            @if($plan->max_pax != null)
                                {{ ' - Max ' . $plan->max_pax }}
                            @endif
                        </li>
                        @if($plan->duration != null)
                            <li>
                                <i class="fa fa-clock-o fa-2x color-tdv width14" aria-hidden="true"></i>
                                {{ trans('plan.duracion') . ": " . $plan->duration . " h" }}
                            </li>
                        @endif
                    </ul>
                </div>
                <a href="{{ route('enotourism-mallorca.item', 
                    ['district' => $tmpuser->district->slug, 'user' => $tmpuser->slug, 'item' => $plan->slug]) }}">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="font18">
                                    {{ $plan->name }}
                                </h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                {!! $plan->short_description != null ? $plan->short_description : $plan->description !!}
                            </div>
                        </div>
                    </div>
                </a>
                <div class="col-md-2"> 
                    @include('plans._admin_buttons', ['plan' => $plan])
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <strong>
                                <span class="font34">{{ $plan->trueprice . "€" }}</span>
                                @if($plan->new_price < $plan->price && $plan->new_price > 0)
                                    <span class="tachado font34">{{ $plan->price . "€" }}</span>
                                @endif
                            </strong>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <p>{{ trans('plan.por_persona') }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <a href="{{ route('enotourism-mallorca.item', 
                                ['district' => $tmpuser->district->slug, 'user' => $tmpuser->slug, 'item' => $plan->slug]) }}" 
                               class="btn-u btn-u-lg" role="button">
                                + Info
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>