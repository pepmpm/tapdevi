<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url', 250)->nullable();
            $table->unsignedInteger('order')->default(0);

            $table->unsignedInteger('blogs_id');
            $table->foreign('blogs_id')->references('id')->on('blogs')->onDelete('cascade');
            $table->timestamps();

            $table->index('blogs_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs_photos');
    }
}
