<?php

namespace App\Http\Controllers\PlansStocks;

use App\Items;
use App\ItemsStocks;
use App\ItemsStocksHours;
use Illuminate\Http\Request;
use Image;
use App\Traits\Helpers;

class PlansStocksController extends \App\Http\Controllers\Controller
{
    use Helpers;

    private $days_to_insert = 366;

    public function __construct(array $attributes = array()) {
        parent::__construct($attributes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function _store($request)
    {
        for($x = 0; $x < count($request['date']); $x++){
            $tmpdate = date_create_from_format('Y-m-d', $request['date'][$x]);

            $tmp = ItemsStocks::where('items_id', '=', $request['items_id'])
                ->whereMonth('date', $tmpdate->format('m'))
                ->whereDay('date', $tmpdate->format('d'))->first();

            if(!$tmp){
                $tmp = new ItemsStocks;
            }

            $tmp->items_id = $request['items_id'];
            $tmp->date = $request['date'][$x];
            $tmp->times = $request['times'][$tmp->date];
            $tmp->updated_at = date('Y-m-d H:i:s');
            $tmp->save();
        }

        //borramos todos los que no hemos informado
        ItemsStocks::where('items_id', '=', $request['items_id'])->whereNotIn('date', $request['date'])->delete();

    	return Items::find($request['items_id']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Plans  $plans
     * @return \Illuminate\Http\Response
     */
    protected function _update($request, ItemsStocks $stock)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Plans  $plans
     * @return \Illuminate\Http\Response
     */
    protected function _destroy(ItemsStocks $stock)
    {
    	ItemsStocks::where('items_id', '=', $stock->items_id)->delete();

        return true;
    }
}
