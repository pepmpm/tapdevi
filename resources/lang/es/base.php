<?php

$idioma = array();

$idioma['hola'] = 'Hola';
$idioma['cargando'] = 'Cargando';
$idioma['en'] = 'en';
$idioma['guardar'] = 'Guardar';
$idioma['cerrar'] = 'Cerrar';
$idioma['anyadir'] = 'Añadir';
$idioma['buscar'] = 'Buscar';
$idioma['gratuito'] = 'Gratuito';
$idioma['pendiente'] = 'Pendiente';

$idioma['aceptar'] = 'Aceptar';
$idioma['denegar'] = 'Denegar';

$idioma['ver_carrito'] = 'Ver carrito';
$idioma['realizar_compra'] = 'Finalizar compra';
$idioma['calendario'] = 'Calendario';

$idioma['minimo'] = 'Mínimo';
$idioma['maximo'] = 'Máximo';

$idioma['January'] = 'Enero';
$idioma['February'] = 'Febrero';
$idioma['March'] = 'Marzo';
$idioma['April'] = 'Abril';
$idioma['May'] = 'Mayo';
$idioma['June'] = 'Junio';
$idioma['July'] = 'Julio';
$idioma['August'] = 'Agosto';
$idioma['September'] = 'Septiembre';
$idioma['October'] = 'Octubre';
$idioma['November'] = 'Noviembre';
$idioma['December'] = 'Diciembre';

$idioma['num_enero'] = '01';
$idioma['num_febrero'] = '02';
$idioma['num_marzo'] = '03';
$idioma['num_abril'] = '04';
$idioma['num_mayo'] = '05';
$idioma['num_junio'] = '06';
$idioma['num_julio'] = '07';
$idioma['num_agosto'] = '08';
$idioma['num_septiembre'] = '09';
$idioma['num_octubre'] = '10';
$idioma['num_noviembre'] = '11';
$idioma['num_diciembre'] = '12';

$idioma['mes1'] = 'Ene';
$idioma['mes2'] = 'Feb';
$idioma['mes3'] = 'Mar';
$idioma['mes4'] = 'Abr';
$idioma['mes5'] = 'May';
$idioma['mes6'] = 'Jun';
$idioma['mes7'] = 'Jul';
$idioma['mes8'] = 'Ago';
$idioma['mes9'] = 'Sep';
$idioma['mes10'] = 'Oct';
$idioma['mes11'] = 'Nov';
$idioma['mes12'] = 'Dic';

$idioma['mes01'] = 'Ene';
$idioma['mes02'] = 'Feb';
$idioma['mes03'] = 'Mar';
$idioma['mes04'] = 'Abr';
$idioma['mes05'] = 'May';
$idioma['mes06'] = 'Jun';
$idioma['mes07'] = 'Jul';
$idioma['mes08'] = 'Ago';
$idioma['mes09'] = 'Sep';
$idioma['mes10'] = 'Oct';
$idioma['mes11'] = 'Nov';
$idioma['mes12'] = 'Dic';

$idioma['day1'] = 'L';
$idioma['day2'] = 'M';
$idioma['day3'] = 'X';
$idioma['day4'] = 'J';
$idioma['day5'] = 'V';
$idioma['day6'] = 'S';
$idioma['day7'] = 'D';

$idioma['day_1'] = 'Lun';
$idioma['day_2'] = 'Mar';
$idioma['day_3'] = 'Mie';
$idioma['day_4'] = 'Jue';
$idioma['day_5'] = 'Vie';
$idioma['day_6'] = 'Sab';
$idioma['day_7'] = 'Dom';

$idioma['day_1_'] = 'Lunes';
$idioma['day_2_'] = 'Martes';
$idioma['day_3_'] = 'Miércoles';
$idioma['day_4_'] = 'Jueves';
$idioma['day_5_'] = 'Viernes';
$idioma['day_6_'] = 'Sábado';
$idioma['day_7_'] = 'Domingo';

return $idioma;