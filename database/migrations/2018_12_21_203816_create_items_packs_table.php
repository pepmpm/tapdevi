<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsPacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_packs', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('items_id');
            $table->foreign('items_id')->references('id')->on('items')->onDelete('cascade');

            $table->unsignedInteger('items_id_related');
            $table->foreign('items_id_related')->references('id')->on('items')->onDelete('cascade');

            $table->timestamps();

            $table->unique('items_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items_packs');
    }
}
