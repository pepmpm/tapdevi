<?php

$idioma = array();

$idioma['por_zona'] = 'Cerca per zona';
$idioma['por_tipo'] = 'Cerca per tipus';
$idioma['por_experiencia'] = 'Cerca per experiència';
$idioma['por_bodega'] = 'Cerca per lloc';

$idioma['anadir_al_carrito'] = 'Afegir a la cistella';
$idioma['categorias'] = 'Categoríes';
$idioma['comentarios'] = 'Comentaris';
$idioma['comentar'] = 'Afegir comentari';
$idioma['cantidad'] = 'Quantitat';
$idioma['seleccionar_fecha'] = 'Seleccionar data';
$idioma['mensaje_confirmacion_email'] = "+Info per Whatsapp o Email";
$idioma['mensaje_whatsapp_plan'] = 'Estic interessat en: :plan';
$idioma['idiomas_disponibles'] = 'Idiomes';

$idioma['horario'] = 'Horari';
$idioma['llamar'] = 'Telefonar';
$idioma['ajustar_busqueda'] = 'Ajustar cerca';
$idioma['remarks_placeholder'] = 'Comentaris, al·lèrgens, detalls...';
$idioma['gift_remarks_placeholder'] = 'Escriu la teva dedicatòria. Apareixerà al bono';
$idioma['tu_mensaje'] = 'Es teu missatge';
$idioma['tu_nombre'] = 'Es teu nom';
$idioma['quien_lo_regala'] = 'Qui ho regala?';
$idioma['nombre_destinatario'] = 'Nom del destinatari';
$idioma['anyade_mensaje_de_regalo'] = 'Afegeix un missatge regal';
$idioma['reserva_con_antelacion'] = 'Reserva amb antelació';
$idioma['cancela_con_antelacion'] = 'Cancela amb antelació';
$idioma['duracion'] = 'Duració';
$idioma['confirmacion_direct'] = "Pagament el día de la l'activitat<br/>(excepte en cas de regal)";
$idioma['comprar_como_regalo'] = 'Comprar com a regal';
$idioma['comprar'] = 'Comprar';
$idioma['guia'] = 'Guía';
$idioma['dias'] = 'díes';
$idioma['accesible_silla_ruedas'] = 'Accesible cadira de rodes';
$idioma['apto_mascotas'] = 'Apto per mascotes';
$idioma['apto_ninos'] = 'Apto per nins';
$idioma['parking_propio'] = 'Parking propi';
$idioma['la_experiencia'] = "L'experiència";
$idioma['detallado'] = 'Detallat';
$idioma['precio_por_persona'] = 'Preu per persona';
$idioma['poblacion'] = 'Població';
$idioma['direccion'] = 'Direcció';
$idioma['codigo_postal'] = 'Codi postal';
$idioma['note_precio'] = 'Si no ho saps no ho emplenis i si es gratuït posa un 0';
$idioma['informacion_del_horario'] = 'Informació del horari';
$idioma['por_persona'] = 'per persona';
$idioma['reservar'] = 'Reservar';
$idioma['mensaje_panel_reservar'] = 'Selecciona nombre de participants, data i idioma';
$idioma['mensaje_note_boton_gift'] = 'O per si no saps la data';
$idioma['mensaje_panel_reservar_email'] = 'Per més info contácta-mos per whatsapp o telefonada';
$idioma['contacta_establecimiento_reservar'] = 'Pendent de reservar data';

$idioma['nueva_experiencia_de_enoturismo'] = "Nova experiència d'enoturisme a Mallorca";
$idioma['eventos'] = 'Events';
$idioma['tus_datos'] = 'Ses teves dades';
$idioma['tu_nombre'] = 'Es teu nom';
$idioma['tu_email'] = 'Es teu email';
$idioma['datos_del_evento'] = 'Dades del event';
$idioma['nombre_del_evento'] = 'Nom del event';
$idioma['precio_por_persona'] = 'Preu per persona';
$idioma['poblacion'] = 'Població';
$idioma['direccion'] = 'Direcció';
$idioma['horarios'] = 'Horaris';
$idioma['codigo_postal'] = 'Codi postal';
$idioma['note_precio'] = 'Si no ho saps no ho emplenis i si es gratuït posa un 0';
$idioma['por_ejemplo'] = 'Per exemple: Sushi maridat amb un tast de vins mallorquins';
$idioma['cuando_empieza'] = 'Quan comença i quan acaba?';
$idioma['detalles_del_evento'] = 'Detalls del event';
$idioma['imagen_del_evento'] = 'Imatge del event';
$idioma['enlace_foto_instagram'] = 'Enllaç a la foto a Instagram';
$idioma['subir'] = 'Pujar';
$idioma['enviar_evento'] = 'Enviar event';
$idioma['tu_propuesta_se_mostrara'] = "Sa teva proposta es mostrarà al public després d'haver-la revisat";
$idioma['more_info'] = '+Info';

$idioma['gracias_por_enviar_el_evento'] = "Gracies per enviar-nos l'información";
$idioma['enseguida_que_lo_revisemos'] = "De seguida que revisem i completem l'informació del event el publicarem";
$idioma['selecciona_una_categoria'] = 'Selecciona una categoría';
$idioma['mensaje_comentario'] = 'A ' . config('constants.NAMEWEB') . " mos interessa sa teva opinió sobre sa visita que vares fer. I de pas servirà a altres aficionats al vi a coneixer millor l'experiència.";

return $idioma;