<?php

$idioma = array();

$idioma['validation_users_id_required'] = 'Client name is mandatory';
$idioma['validation_users_id_max'] = 'Client name is too long';

$idioma['validation_name_required'] = 'Name is mandatory';
$idioma['validation_name_max'] = 'Name is too long';
$idioma['validation_phone_numeric'] = "Phone must be a number";
$idioma['validation_phone_max'] = 'Phone number is too long';
$idioma['validation_email_required'] = "Email is required";
$idioma['validation_email_email'] = 'Email is wrong';
$idioma['validation_email_max'] = 'Email is too long';
$idioma['validation_items_id_required'] = 'You must select a product';
$idioma['validation_items_id_numeric'] = 'There is an error with a product';
$idioma['validation_quantity_required'] = 'Quantity is required';
$idioma['validation_quantity_numeric'] = 'There is an error with the quantity';
$idioma['validation_discount_numeric'] = 'Discount must be a number';
$idioma['validation_credit_card_card_invalid'] = 'Card number is not correct';
$idioma['validation_credit_card_card_cvc_invalid'] = 'CVC code is not correct';
$idioma['validation_credit_card_card_card_expiration_month_invalid'] = 'Month filled is not correct';
$idioma['validation_credit_card_card_expiration_year_invalid'] = 'Year filled is not correct';
$idioma['validation_credit_card_required'] = 'Missing credit card information';
$idioma['botellas'] = 'bottles';
$idioma['unidades'] = 'units';
$idioma['personas'] = 'persons';

$idioma['carrito'] = 'Cart';
$idioma['cantidad'] = 'Quantity';
$idioma['informacion_envio'] = 'Booking information';
$idioma['facturacion'] = "Billing";
$idioma['nombre'] = 'Name';
$idioma['apellidos'] = 'Last name';
$idioma['email'] = 'Email';
$idioma['telefono'] = 'Phone';
$idioma['direccion'] = 'Address';
$idioma['provincia'] = 'Location';
$idioma['poblacion'] = 'City';
$idioma['codigo_postal'] = 'Postal Code / Zip';
$idioma['numero_de_cuenta'] = 'Card number';
$idioma['mes'] = 'Month';
$idioma['ano'] = 'Year';
$idioma['cvv'] = 'CVV';
$idioma['note'] = 'Note';
$idioma['mensaje_tarjetas_bbdd'] = config('constants.NAMEWEB') . ' will not save any billing data.';
$idioma['enviar_direccion_diferente'] = 'I need an invoice for tax purposes (usually required by companies for tax purposes)';

$idioma['codigo_descuento_aplicado'] = 'Discount code :code applied';
$idioma['descuento_aplicado'] = 'Applied discount';
$idioma['descuento_en_el_envio'] = 'Shipping discount';

$idioma['caja'] = 'Box|Boxes';
$idioma['persona'] = 'Person|Persons';
$idioma['articulo'] = 'Item|Items';
$idioma['lote'] = 'Lot|Lots';

$idioma['gratuito_islas_baleares'] = 'Free shipping to Balearic Islands';
$idioma['_gratuito_islas_baleares'] = 'free to Balearic Islands';

$idioma['elige_un_metodo_de_pago'] = 'Choose a payment method';
$idioma['tarjeta_de_credito_o_debito'] = 'Credit or debit card';
$idioma['pago_por_transferencia_bancaria'] = 'Bank transfer payment';

$idioma['finalizar_pedido'] = 'Finish order';
$idioma['pedido'] = 'Order';

$idioma['mensaje_cabecera_login'] = 'Insert your email and the password that you received in the email of your first order with us';


$idioma['mensaje_factura_direct'] = 'Payment will be made on the day of the experience';
$idioma['mensaje_factura_fee'] = 'The rest of the payment will be made on the day of the experience';
$idioma['mensaje_factura_groupon'] = 'The payment for the experience has already been made, now you must contact the company to arrange the day';

$idioma['usar_este_usuario'] = 'Use this email';
$idioma['producto_anyadido_al_carrito'] = 'Item added to the cart';

$idioma['_DE_QUIEN_'] = 'FROM WHO?';
$idioma['_PARA_QUIEN_'] = 'FOR?';
$idioma['FUTUROS_EVENTOS'] = 'FUTURE EVENTS';
$idioma['_ACTIVIDAD_'] = 'ACTIVITY';
$idioma['_VALE_POR_'] = 'WORTH FOR';
$idioma['_CUANDO_'] = 'WHEN?';
$idioma['_DONDE_'] = 'WHERE?';
$idioma['_CODIGO_'] = 'BOOKING CODE';
$idioma['_CUANTO_'] = 'HOW MUCH?';

$idioma['proponer_fecha'] = 'Propose date';
$idioma['contactar'] = 'Contact us';

return $idioma;