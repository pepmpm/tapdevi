<div class="modal fade" id="modal_contact_info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row margin-bottom-25">
                    <div class="col-md-12">
                        {{ trans('order.contactar') }}
                    </div>
                </div>
                <div class="row margin-bottom-15">
                    <div class="col-md-12 text-center">
                        <a href="https://api.whatsapp.com/send?phone={{ config('constants.PHONEWEB') }}" target="_blank" 
                            class="btn btn-success btn-block color-blanco">
                            <i class="fa fa-whatsapp color-blanco" aria-hidden="true"></i>&nbsp;Whatsapp
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="mailto:{{ config('constants.MAILWEB') }}" target="_blank" class="btn btn-u btn-block color-blanco">
                            <i class="fa fa-envelope-o color-blanco" aria-hidden="true"></i>&nbsp;Email
                        </a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="denegar_cookies" data-dismiss="modal">{{ trans('base.cerrar') }}</button>
            </div>
        </div>
    </div>
</div>