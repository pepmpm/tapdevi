<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->text('text')->nullable();
            $table->timestamps();


            $table->unsignedInteger('items_id');
            $table->foreign('items_id')->references('id')->on('items')->onDelete('cascade');

            $table->unique('items_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items_comments');
    }
}
