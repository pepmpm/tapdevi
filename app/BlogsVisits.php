<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogsVisits extends Model
{
    protected $fillable = ['blogs_id', 'year', 'month'];

    public function blog(){
        return $this->belongsTo('\App\Blogs', 'blogs_id');
    }
}
