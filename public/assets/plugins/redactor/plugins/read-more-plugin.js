if (!RedactorPlugins) var RedactorPlugins = {};
     
RedactorPlugins.leermas = {

    init: function(obj) {

        btnCallback = function(obj, event, key) {
            this.insertHtml('<!--leermas-->');
        }

        this.buttonAdd('leermas', 'Agregar Leer más', btnCallback);
        this.buttonAwesome('leermas', 'fa-tasks');
    }

}