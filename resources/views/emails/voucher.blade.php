@inject('helpers', 'App\Http\Controllers\AuxController')
<!doctype html>
@php($locale = 'ES')
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Responsive Email Template</title>
        
        <style type="text/css">
            .ReadMsgBody {width: 100%; background-color: #ffffff;}
            .ExternalClass {width: 100%; background-color: #ffffff;}
            body     {width: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;font-family: Arial, Helvetica, sans-serif}
            table {border-collapse: collapse;}

            @media only screen and (max-width: 640px)  {
                body[yahoo] .deviceWidth {width:440px!important; padding:0;}    
                body[yahoo] .center {text-align: center!important;}  
            }

            @media only screen and (max-width: 479px) {
                body[yahoo] .deviceWidth {width:280px!important; padding:0;}    
                body[yahoo] .center {text-align: center!important;}  
            }
        </style>
    </head>
    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix" style="font-family: Arial, Helvetica, sans-serif">

        <!-- Wrapper -->
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
            <tr>
                <td width="100%" valign="top" bgcolor="#ffffff" style="padding-top:20px">

                    <!--Start Header-->
                    <table width="700" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                        <tr>
                            <td width="100%" >
                                <!--Start logo-->
                                <table width="700" border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth">
                                    <tr>
                                        <td class="center" style="padding: 10px 0px 10px 0px">
                                            <img src="{{ $message->embed(url('assets/img/logo_170.png')) }}" 
                                                alt="Logo" style="width: 170px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">
                                            <span style="font-size: 11px">Powered by <a href="{{route('/')}}" target="_blank">{{config('constants.NAMEWEB')}}</a></span>
                                        </td>
                                    </tr>
                                </table><!--End logo-->
                            </td>
                        </tr>
                    </table>
                    <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                        <tr>
                            <td width="100%" bgcolor="#a31158" class="center" style="font-size: 16px; color: white; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 10px 20px;">
                                <h2>{{ trans('mail.gracias_por_comprar_en_', [], null, $locale) }}</h2>
                                <strong>{{ trans('mail.codigo_del_pedido', [], null, $locale) . ": " }}</strong>{{ $order->code }}
                            </td>
                        </tr>
                    </table>
                    <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                        <tr>
                            <td width="100%" bgcolor="#ffffff">
                                <table width="99%"  border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth">
                                    <tr>
                                        <td class="center">
                                            <table  border="0" cellpadding="0" cellspacing="0" align="center"> 
                                                <tr>
                                                    <td valign="top" style="padding: 20px 0px " class="center">
                                                        <img src="{{ $message->embed(public_path().'/assets/img/email/usuario.png') }}" 
                                                            alt="Tus datos"/>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table  border="0" cellpadding="0" cellspacing="0" align="center"> 
                                                <tr>
                                                    <td class="center" style="font-size: 16px; color: #303030; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; " >
                                                        {{ trans('mail.tus_datos', array('usuario' => $order->name), null, $locale) }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 12px; color: #687074; font-family: Arial, Helvetica, sans-serif; padding: 10px 10px; padding-top: 20px;">
                                                        <strong>{{ trans('mail.nombre', [], null, $locale) . ": " }}</strong>{{ $order->name }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 12px; color: #687074; font-family: Arial, Helvetica, sans-serif; padding: 10px 10px;">
                                                        <strong>{{ trans('mail.email', [], null, $locale) . ": " }}</strong>{{ $order->email }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 12px; color: #687074; font-family: Arial, Helvetica, sans-serif; padding: 10px 10px;">
                                                        <strong>{{ trans('mail.telefono', [], null, $locale) . ": " }}</strong>{{ $order->phone }}
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <div style="height:15px">&nbsp;</div>

                    <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                        <tr>
                            <td width="100%" bgcolor="#a31158" class="center" style="font-size: 16px; color: white; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 10px 20px;">
                                @if($order->remarks != null)
                                    <h2>{{ trans('mail.comentarios', [], null, $locale) }}</h2>
                                    {!! $order->remarks !!}
                                @endif
                            </td>
                        </tr>
                    </table>
                    <table width="700" bgcolor="#f7f7f7" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                        <tr>
                            <td class="center" style="font-size: 16px; color: #303030; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 10px; ">
                                {{ trans('mail.pedido', [], null, $locale) }}
                            </td>
                        </tr>
                        @php($tmpitem = $order->item)
                        <tr>
                            <td>
                                <!--Start Two Pictures-->
                                <table width="25%" border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth">
                                    <tr>
                                        <td width="100%" bgcolor="#f7f7f7">
                                            <!--Left box-->
                                            <table border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth">
                                                <tr>
                                                    <td valign="top" style="padding: 20px 20px " class="center">
                                                        <a href="#">
                                                            <img src="{{ $message->embed(Imgfly::imgPublic(str_replace("upload/", "", $tmpitem->one_photo()) . '?w=130', 'upload')) }}" 
                                                                alt="{{ $tmpitem->name }}"
                                                                style="width: 130px"/>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <table width="74%" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" style="margin-top: 25px">
                                    <tr>
                                        <td bgcolor="#f7f7f7">
                                            <table border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                                                <tr>
                                                    <td class="center" style="font-size: 16px; color: #687074; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px 10px;">
                                                        {{ "# 1 " . $tmpitem->name }}
                                                    </td>
                                                </tr>
                                                @if($tmpitem->users_id != null)
                                                    <tr>
                                                        <td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px 10px; " >
                                                            {{ $tmpitem->user->name }}
                                                        </td>
                                                    </tr>
                                                @endif
                                                <tr>
                                                    <td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px 10px; " >
                                                        {{ trans('mail.cantidad', [], null, $locale) . ": " . $order->quantity . " " . $tmpitem->get_unit_name() }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="center" style="font-size: 12px; color: #a31158; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px 10px; " >
                                                        {{ trans('mail.precio', [], null, $locale) . ": " . ($order->price) . " €" }}
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table width="700" bgcolor="#f7f7f7" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                        <tr>
                            <td bgcolor="#dbdbdb" style="width: 50%">
                                
                            </td>
                            <td bgcolor="#dbdbdb" style="width: 50%">
                                <table border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" style="margin-top: 25px; margin-bottom: 25px; margin-right: 25px;">
                                    @if($order->coupons_id != null)
                                        @php($coupon = $order->coupon)
                                        <tr>
                                            <td>
                                                <strong>{{ trans('order.descuento_aplicado', [], null, $locale) . ": " . "- " . $coupon->discount . "%" }}</strong>
                                            </td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <td>
                                            <strong>{{ trans('mail.precio_total', [], null, $locale) . ": " . $order->total . " €" }}</strong>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <table width="700" bgcolor="#a31158" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                        <tr>
                            <td style=" padding: 20px 0 20px 0;">
                                <table align="center">
                                    <tbody>
                                        <tr>
                                            <td style="padding: 7px 15px; text-align: center; background-color: #3cad3c;" class="center" valign="top">
                                                <a style="color: #fff; font-size: 12px; font-weight: bold; text-decoration: none; font-family: Arial, sans-serif; text-alight: center;"
                                                    href="{{ route('back.orders.download_voucher', ['orders_id' => $order->id, 'email' => $order->email]) }}" target="_blank">
                                                    {{ trans('mail.descargar_bono', [], null, $locale) }}
                                                </a>
                                            </td>
                                         </tr>
                                    </tbody>
                                </table>
                            </td>
                            @if($tmpitem->type != 'plan')
                                <td style=" padding: 20px 0 20px 0;">
                                    <table align="center">
                                        <tbody>
                                            <tr>
                                                <td style="padding: 7px 15px; text-align: center; background-color: #3cad3c;" class="center" valign="top">
                                                    <a style="color: #fff; font-size: 12px; font-weight: bold; text-decoration: none; font-family: Arial, sans-serif; text-alight: center;"
                                                        href="{{ route('enotourism-mallorca.gift', ['item' => $tmpitem->slug]) }}" target="_blank">
                                                        {{ trans('mail.ver_planes_incluidos', [], null, $locale) }}
                                                    </a>
                                                </td>
                                             </tr>
                                        </tbody>
                                    </table>
                                </td>
                            @endif
                        </tr>
                    </table>

                    <table width="700" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                        <tr>
                            <td>
                                <table width="700"  border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                                    <tr>
                                        <td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 20px; vertical-align: middle; padding: 10px 50px 30px; " >
                                            Copyright © {{config('constants.NAMEWEB') . " " . date('Y')}}                           
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <div style="height:15px">&nbsp;</div>
                </td>
            </tr>
        </table>
    </body>
</html>