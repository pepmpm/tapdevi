<?php

$idioma = array();

$idioma['validation_stock_required'] = 'Debes rellenar el stock que tendrá el producto';
$idioma['validation_stock_numeric'] = 'El stock debe ser un número';
$idioma['validation_cost_numeric'] = 'El precio de coste del producto debe ser un número';
$idioma['validation_price_required'] = 'Debes rellenar el precio de venta del producto';
$idioma['validation_price_numeric'] = 'El precio debe ser un número';
$idioma['validation_new_price_numeric'] = 'El precio con descuento debe ser numérico';
$idioma['validation_shipping_numeric'] = 'El precio a añadir a los portes base debe ser numérico';
$idioma['validation_labels_id_required'] = 'Se debe seleccionar una categoría';
$idioma['validation_labels_id_numeric'] = 'La categoría seleccionada es incorrecta';
$idioma['validation_name_required'] = 'Se debe rellenar el nombre del producto';
$idioma['validation_name_max'] = 'El nombre del producto no puede ser tan largo';
$idioma['validation_description_required'] = 'Se debe rellenar una descripción del producto';
$idioma['validation_description_max'] = 'La descripción del producto es demasiado larga';
$idioma['validation_new_price_lt'] = 'El precio con descuendo debe ser menor que el precio habitual';

$idioma['otros_vinos'] = 'Otros vinos que debes probar';
$idioma['mensaje_cross_selling'] = 'A continuación te presentamos otros vinos seleccionados para adentrarte en la cultura vitivinícola de Mallorca.';
$idioma['selecciona_cantidad'] = 'Selecciona la cantidad';
$idioma['crear_una_caja_personalizada'] = 'Crear tu caja personalizada';
$idioma['wines'] = 'Vinos';
$idioma['experiences'] = 'Experiencias';
$idioma['comprar_caja_personalizada_con'] = 'Comprar una caja de vino personalizada de :user';
$idioma['tu_caja_personalizada'] = 'Tu caja personalizada';
$idioma['elige_vinos_para_anyadirlos_a_tu_caja'] = 'Elige vinos para añadirlos a tu caja';

$idioma['vino'] = 'Vino';
$idioma['precio'] = 'Precio';
$idioma['bodega'] = 'Bodega';

$idioma['message_custom_box'] = 'Pulsa sobre un vino del listado para añadirlo a tu caja personalizada. Cuando hayas elegido las 6 botellas te aparecerá el botón de añadir al carrito.';

$idioma['msg_transferencia_bancaria'] = 'Recuerda escribir el código de pedido :code en el concepto de la transferencia';
$idioma['por_botella'] = 'por botella';
$idioma['por_variedad'] = 'Por variedad';

$idioma['elige_una_bodega_para_vino_personalizado'] = 'Elige una bodega para crear tu caja de vino variada';

$idioma['mensaje_botellas_envio_gratuito'] = 'Envío de vino por cajas. Debes añadir :num botellas más al carrito';
$idioma['envio_gratuito'] = '¡Enhorabuena! Ya podemos proceder a finalizar el pedido';

$idioma['novedad'] = 'Novedad';


return $idioma;