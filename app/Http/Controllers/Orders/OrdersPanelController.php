<?php

namespace App\Http\Controllers\Orders;

use App\Orders;
use Illuminate\Http\Request;
use App\Traits\Helpers;

class OrdersPanelController extends OrdersController
{
    use Helpers;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $params = array();
        if(\Auth::check() && \Auth::user()->role == 'user'){
            $params['users_id'] = \Auth::id();
            $this->data['query'] = parent::_index($params);
            return view('_back.orders.index_user', $this->data);
        }

        $this->data['query'] = parent::_index($params);
        return view('_back.orders.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        self::_get_lovs();
        return view('_back.orders.crud', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = \App\Items::find($request->get('items_id'));
        $query = parent::_store($request->all() + ['item' => $item]);

        if(!($query instanceof Orders)){
            return $query;
        }

        return redirect()->route('back.orders.show', array('item' => $query->id))
            ->with('message_success', 'Cambios guardados correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function show(Orders $order)
    {
        $this->data['query'] = $order;
        self::_get_lovs($order);
        return view('_back.orders.crud', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Orders $order)
    {
        $query = parent::_update($request->all(), $order);

        if(!($query instanceof Orders)){
            return $query;
        }

        return redirect()->route('back.orders.show', array('item' => $query->id))
            ->with('message_success', 'Cambios guardados correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Orders $order)
    {
        parent::_destroy($order);
        return redirect()
            ->route('back.orders.index')
            ->with('message_success', 'Cambios guardados correctamente');
    }

    private function _get_lovs($order = null){
        $this->data['users'] = \App\User::whereIn('role', ['user', 'admin'])->get();
        $this->data['items'] = \App\Items::where('status', '=', 'active')
            ->whereIn('type', ['pack', 'plan'])->get();
        $this->data['locations'] = \App\Locations::orderBy('name', 'asc')->pluck('name', 'id')->all();
        $this->data['districts'] = \App\Districts::orderBy('name', 'asc')->pluck('name', 'id')->all();
        $this->data['status_lov'] = Orders::getPossibleEnumValues('status');
        $this->data['yesno_lov'] = Orders::getPossibleEnumValues('is_gift');
        $this->data['time_lov'] = $order != null ? 
            self::_get_time($order->item->id, $this->fec_mysql_to_str($order->date_event), $order->time) : '';
    }

    public function get_time(Request $request){
        return self::_get_time($request->get('items_id'), $request->get('date'));
    }

    private function _get_time($items_id, $date, $time = null){
        $stock = \App\ItemsStocks::where('items_id', '=', $items_id)
            ->where('date', '=', $this->fec_str_to_mysql($date))->first();

        if($stock && $stock->times != null){
            $times = array_filter(explode("#", $stock->times));
            return \Form::select('time', $times, $time, array('class' => 'form-control input-sm'));
        }

        return null;
    }

    public function download_voucher($orders_id, $email){
        $params = array();
        $params['order'] = \App\Orders::where('id', '=', $orders_id)->where('email', '=', $email)->first();
        $params['logo'] = \Imgfly::imgPublic('img/logo.png' . '?w=100', 'assets');

        if($params['order'] != null && $params['order']->id != null){
            $pdf = \PDF::loadView('emails.pdf.pdf_voucher', $params);
            return $pdf->download(trans('order.pedido') . '_' . $params['order']->code . '.pdf');
        }
        return redirect('/');
    }

    public function mark_booking(Request $request, Orders $order, $action){
        $order->{'date_' . $action} = date('Y-m-d H:i:s');
        $order->save();

        return redirect()->route('back.orders.show', array('item' => $order->id))
            ->with('message_success', 'Cambios guardados correctamente');
    }

}
