@extends('_layouts/inicio')

@section ('body')

<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left">Blogs</h1>
    </div>
</div>
<div class="container content profile">
    <div class="row">
        <div class="col-md-12">
            <a href="{{route('back.routes.create')}}" 
               class="btn-u btn-u-lg" role="button">
                Nuevo blog
            </a>
        </div>     
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Búsqueda</div>
                <div class="panel-body">
                    <form role="form" method="get" enctype="multipart/form-data" action="{{ route('back.routes.index') }}">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Categoría</label>

                            </div>
                            @if(\Auth::user()->isAdmin())
                                <div class="col-md-3">
                                    <label>Proveedor</label>

                                </div>
                            @endif
                            <div class="col-md-3">
                                <label>Status</label>

                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn-u margin-top-25">
                                    Buscar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Items</div>
                <div class="panel-body">
                    @include('_layouts.messages', array('errors' => $errors))
                    <table class="table" id="tabla">
                        <thead>
                            <th>Nombre</th>
                            <th>Categoría</th>
                            <th>Tags</th>
                            <th>Status</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                            @foreach($query as $blog)
                                <tr>
                                    <td>{{ $blog->name }}</td>
                                    <td>{{ $blog->category }}</td>
                                    <td>{{ $blog->tags }}</td>
                                    <td>{{ $blog->status }}</td>
                                    <td>
                                        <a href="{{ route('back.routes.show', array('id' => $blog->id)) }}" 
                                           class="btn btn-primary btn-sm pull-left" role="button">
                                            <i class="fa fa-edit" aria-hidden="true"></i>
                                        </a>
                                        <form action="{{ route('back.routes.destroy', [$blog->id]) }}" method="POST" 
                                            onsubmit="if(!confirm('¿Estás seguro de querer borrar el artículo?')) return false"
                                            class="col-md-4">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-danger btn-sm" type="submit">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>     
    </div>
</div><!--/container-->    
<!--=== End Profile ===-->

@stop

@section ('assets_end')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/datatables.min.css') }}">
    <script src="{{ asset('assets/plugins/datatables/datatables.min.js') }}"></script>
    
    <script type="text/javascript">
        $(document).ready(function() {
            $('#tabla').DataTable({
                "language": {
                    "url": ARNY.baseUrl + "/assets/plugins/datatables/spanish.lang"
                },
                stateSave: true
            });
        });
    </script>
@stop
