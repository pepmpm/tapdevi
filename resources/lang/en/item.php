<?php

$idioma = array();

$idioma['validation_stock_required'] = 'Stock is mandatory';
$idioma['validation_stock_numeric'] = 'Stock must be a number';
$idioma['validation_cost_numeric'] = 'Cost price must be a number';
$idioma['validation_price_required'] = 'Product price is mandatory';
$idioma['validation_price_numeric'] = 'Product price must be a number';
$idioma['validation_new_price_numeric'] = 'Discount price must be a number';
$idioma['validation_shipping_numeric'] = 'Shipping cost added must be a number';
$idioma['validation_labels_id_required'] = 'You must select a category';
$idioma['validation_labels_id_numeric'] = 'The category selected is wrong';
$idioma['validation_users_id_required'] = 'You must select a winery';
$idioma['validation_users_id_numeric'] = 'The supplier selected is wrong';
$idioma['validation_name_required'] = 'Event name is mandatory';
$idioma['validation_name_max'] = 'Event name is too long';
$idioma['validation_description_required'] = 'Event description is mandatory';
$idioma['validation_description_max'] = 'Event description is too long';
$idioma['validation_new_price_lt'] = 'Discount price must be lower than current price';

$idioma['custom_name'] = ':user custom wine box';
$idioma['custom_description'] = 'If you want to try several wines from :user you can select the wines you want until completing a box. The price of the box will be updated every time you add or remove a wine.';
$idioma['custom_seo_description'] = 'Do you want wine from :user but a whole box is too much? With '. config('constants.NAMEWEB') .' you can choose the wines you want in your box. Two white wines, one rosé and three reds? You choose.';
$idioma['custom_seo_keywords'] = 'custom box :user, custom wine box :user, varied wine box :user, varied box :user, choose wines box :user';

$idioma['cada_lote'] = 'each lot';
$idioma['mensaje_elegir_plan_regalo'] = 'You got :planes wine plans to choose|You got :planes wine planes to choose';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';
$idioma[''] = '';

return $idioma;