$(document).ready(function() {
    $('#btn_down').click(function(){
        $('html, body').animate({ scrollTop: $('#go_inicio').offset().top}, 500, 'linear');
    });
    if($(".owl2-carousel-v1").length){
        $('.owl2-carousel-v1').owlCarousel({
            items: 3,
            margin: 30,
            loop: true,
            autoplay: true,
            autoplayTimeout: 10000,
            // autoplayHoverPause: true,
            responsive: {
                0:{
                    items: 1
                },
                768:{
                    items: 2
                },
                992:{
                    items: 2
                },
                1200:{
                    items: 3
                }
            },
        });
    }
    
    if($("#owl-slider").length){
        $("#owl-slider").owlCarousel({
            items: [4],
            itemsDesktop : [1000,3], //3 items between 1000px and 901px
            itemsDesktopSmall : [979,2], //2 items between 901px
            itemsTablet: [600,1], //1 items between 600 and 0;
            slideSpeed: 1000
        });

        // Custom Navigation Events
        $(".events-next").click(function(){
            $("#owl-slider").trigger('owl.next');
        });
        $(".events-prev").click(function(){
            $("#owl-slider").trigger('owl.prev');
        });
    }
});