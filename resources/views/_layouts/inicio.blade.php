<? ob_start("ob_gzhandler"); ?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="es" class="ie9" xmlns:og="http://opengraphprotocol.org/schema/"> <![endif]-->
<!--[if !IE]><!--> <html lang="{{ $idiomaweb }}" xmlns:og="http://opengraphprotocol.org/schema/"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <title>@yield('title', trans('meta.title_default')) | {{config('constants.NAMEWEB')}}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="_token" content="{{ csrf_token() }}" />

        <link rel="alternate" href="{{ Request::url() }}" hreflang="x-default" />
        <link rel="alternate" href="{{ LaravelLocalization::getLocalizedURL('en', Request::url()) }}" hreflang="en-gb" />

        @section('meta')
            <meta http-equiv="Content-Language" content="es-ES"/>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <meta name="Robots" content="all" />
            <meta name="geo.region" content="ES-PM" />
            <meta name="geo.placename" content="Pina" />
            <meta name="geo.position" content="39.599188;2.925614" />
            <meta name="ICBM" content="39.599188, 2.925614" />
            
            <meta property="og:url" content="{{Request::url()}}"/>
            <meta property="og:site_name" content="{{config('constants.NAMEWEB')}}"/>
            <meta property="fb:admins" content="1417278898515342"/>
            <meta property='fb:app_id' content='1417278898515342'/>
        @show

        <!-- Required -->
        <link rel="shortcut icon" href="{!! asset('favicon.ico') !!}" type="image/x-icon">

        <script type="text/javascript">
            var ARNY = (function(){
                return{
                    "baseUrl": "{{ URL::to('/') }}"
                }
            })();
        </script>
        
        <!-- extra js-->
        @yield('assets_ini')

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-174246186-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-174246186-1');
        </script>

    </head>
    <body id="body" data-spy="scroll">
        <main class="wrapper">
            @section('header')
                @include('_layouts.' . (isset($header) ? $header : 'header'))
            @show
            @if(Auth::check())
                @include('_layouts.nav_panel')
            @endif
            @yield('body')
            
            @section('footer')
                @include('_layouts.footer')
                @if($agent->isMobile() && isset($contact_bar) && $contact_bar != 'none')
                    <div class="footer-fixed text-center height50 bg-opacity-white">
                        <div class="container-fluid">
                            <div class="row">
                                @if($contact_bar == 'whatsapp' || $contact_bar == 'full')
                                    <div class="{{ $contact_bar == 'full' ? 'col-md-6 col-xs-6' : 'col-md-12 col-xs-12' }}">
                                        <a href="https://api.whatsapp.com/send?phone={{ config('constants.PHONEWEB') }}" target="_blank" 
                                            class="btn btn-success btn-block color-blanco">
                                            <i class="fa fa-whatsapp color-blanco" aria-hidden="true"></i>&nbsp;Whatsapp
                                        </a>
                                    </div>
                                @endif
                                @if($contact_bar == 'phone' || $contact_bar == 'full')
                                    <div class="{{ $contact_bar == 'full' ? 'col-md-6 col-xs-6' : 'col-md-12 col-xs-12' }}">
                                        <a class="btn-u btn-block" href="tel:+{{ config('constants.PHONEWEB') }}">
                                            <i class="fa fa-phone color-blanco" aria-hidden="true"></i>&nbsp;{{ trans('inicio.llamar') }}
                                        </a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                @endif
            @show
        </main>
        @include('cookies._modal_cookies')
    </body>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}" rel="preload">
    <link rel="stylesheet" href="{{ asset('assets/css/blocks.min.css') }}" rel="preload">
    <link rel="stylesheet" href="{{ asset('assets/css/shop.style.css') }}">

    <!-- CSS Header and Footer -->
    @if(isset($header) && $header == 'header_home')
        <link rel="stylesheet" href="{{ asset('assets/css/headers/header-v6.css') }}">
    @else
        <link rel="stylesheet" href="{{ asset('assets/css/headers/header-v5.css') }}">
    @endif
    <link rel="stylesheet" href="{{ asset('assets/css/footers/footer-v4.css') }}">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/line-icons/line-icons.css') }}">
    <link rel="stylesheet" rel="preload" href="{{ asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}">

    <!-- CSS Theme -->
    <link rel="stylesheet" href="{{ asset('assets/css/theme-colors/default.css') }}" id="style_color">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">

    <!-- JS Global Compulsory -->
    <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery/jquery-migrate.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- JS Implementing Plugins -->
    <script src="{{ asset('assets/plugins/back-to-top.js') }}"></script>
    <script src="{{ asset('assets/plugins/smoothScroll.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery.parallax.js') }}"></script>
    <!-- JS Customization -->
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    <!-- JS Page Level -->
    <script src="{{ asset('assets/js/shop.app.js') }}"></script>

    <script src="{{ asset('assets/plugins/cookie/jquery.cookie.js') }}"></script>

    <script>
        jQuery(document).ready(function() {
            App.init();
            App.initParallaxBg();
        });
    </script>

    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name="_token"]').attr('content')
                }
            });
            $('#aceptar_cookies').on('click', function(e) {
                $.cookie('tdv_cookies', 'S', { expires: 365, path: '/' });
            });
            $('#denegar_cookies').on('click', function(e) {
                $.cookie('tdv_cookies', 'N', { expires: 365, path: '/' });
            });
        });

        $(window).on('load', function() {
            @if(!\Request::is('politicas_privacidad') && !\Request::is('politicas_cookies'))
                if($.cookie('tdv_cookies') == undefined){
                    $('#modal_cookies').modal('show');
                }
            @endif
        });
    </script>

    @yield('assets_end')
</html>