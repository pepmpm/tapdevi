<?php

namespace App\Http\Controllers\Orders;

use App\Items;
use App\Orders;
use App\OrdersDetails;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Traits\Helpers;

use Validator;

use LVR\CreditCard\CardCvc;
use LVR\CreditCard\CardNumber;
use LVR\CreditCard\CardExpirationYear;
use LVR\CreditCard\CardExpirationMonth;

use Illuminate\Support\Facades\Crypt;

class OrdersWebController extends OrdersController
{
    use Helpers;

    const BASE_URL = 'https://api.stripe.com';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = array();
        if(!$request->has('id')){
            $params = is_array(session('cart')) ? session('cart') : [];
        }else{
            $params['item'] = \App\Items::find($request->get('id'));
            $params['is_gift'] = $request->get('submit') == 'gift' ? 'S' : 'N';
            $params['quantity'] = $request->has('quantity') ? $request->get('quantity') : $params['item']->min_pax;
            $params['price_unit'] = floatval($params['item']->trueprice);
            $params['price'] = floatval($params['quantity']) * floatval($params['item']->trueprice);

            session(['cart' => self::_refresh_cart($params)]);
        }

        self::_get_data_index_content();
        foreach($params as $key => $param){
            $this->data[$key] = $param;
        }
        return view('cart.index', $this->data);
    }

    public function add_to_cart(Request $request){
        $carrito = json_decode(base64_decode($request->get('q')), true);
        $carrito['show_menu'] = false;
        $carrito['item'] = \App\Items::find($carrito['items_id']);
        $carrito['price_unit'] = floatval($carrito['item']->trueprice);
        unset($carrito['items_id']);
        session(['cart' => self::_refresh_cart($carrito)]);
        return redirect()->route('orders.index');
    }

    private function get_encripted_link($params){
        if(!$params || count($params) == 0){
            return "";
        }
        $array = array();
        $array['items_id'] = $params['item']->id;
        $array['is_gift'] = $params['is_gift'];
        $array['quantity'] = $params['quantity'];
        $array['price'] = $params['price'];
        return base64_encode(json_encode($array));
    }

    public function update(Request $request){
        self::_get_data_index_content();
        if(session()->has('cart')){
            $order = session('cart');
            $order['quantity'] = is_numeric($request->get('quantity')) ? $request->get('quantity') : $order['quantity'];
            $order['price_unit'] = is_numeric($request->get('price')) ? 
                floatval($request->get('price')) : $order['price_unit'];
            $order['price'] = floatval($order['quantity']) * floatval($order['price_unit']);

            session(['cart' => self::_refresh_cart($order)]);

            return view('cart._index_content', $this->data);
        }
        $request->session()->flash('message_error', 'Tu carrito se ha vaciado automaticamente a las 2 horas');
        return view('cart._index_content', $this->data);
    }

    public function coupon(Request $request, $coupons_id = null){
        self::_get_data_index_content();
        if(!empty($coupons_id)){
            $cupon = \App\Coupons::where('code', '=', mb_strtoupper($coupons_id, 'UTF-8'))
                ->where('active', '=', 'S')->first();
            if($cupon){
                $order = session('cart');
                $order['coupon'] = $cupon;
                $order['discount'] = $cupon->discount;

                session(['cart' => self::_refresh_cart($order)]);

                return view('cart._index_content', $this->data);
            }
        }
        //session(['cart' => self::_refresh_cart(session('cart'))]);
        $request->session()->flash('message_error', 'El código del cupón no es correcto');
        return view('cart._index_content', $this->data);
    }

    private function _get_data_index_content(){
        $this->data['encryptedCart'] = self::get_encripted_link(session('cart'));
        $this->data['months'] = ['01' => '01', '02' => '02', '03' => '03', '04' => '04', '05' => '05', '06' => '06', '07' => '07', '08' => '08', '09' => '09', '10' => '10', '11' => '11', '12' => '12'];

        $years = array();
        for($x = date('Y'); $x <= date('Y')+8; $x++){
            $years[$x] = $x;
        }
        $this->data['years'] = $years;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function show($code, $email)
    {
        $this->data['order'] = Orders::where('code', '=', $code)->where('email', '=', $email)->first();
        if($this->data['order'] == null){
            return redirect()->route('/');
        }
        return view('orders.crud', $this->data);
    }

    public function delete($id){
        session()->forget('cart');
        return redirect()->back()->with('message_success', 'Producto eliminado del carrito');
    }

    public function save(Request $request){
        if(!session()->has('cart')){
            return redirect()->back()->with('message_error', 'Tu carrito se ha vaciado automaticamente a las 2 horas');
        }
        $validation = Validator::make($request->all(), self::_get_rules($request), self::_get_messages());

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }

        $tmp = session('cart');

        $tmp['gift_remarks'] = $request->has('gift_remarks') ? $request->get('gift_remarks') : null;
        $tmp['gift_recipient'] = $request->has('gift_recipient') ? $request->get('gift_recipient') : null;
        $tmp['gift_name'] = $request->has('gift_name') ? $request->get('gift_name') : null;
        $tmp['name'] = $request->get('name');
        $tmp['email'] = $request->get('email');
        $tmp['phone'] = $request->has('phone') ? $request->get('phone') : null;

        $order = parent::_store($tmp);

        if(!($order instanceof Orders)){
            // si el objeto no es de tipo Orders es que es un error
            return $order;
        }

        $tmp['transaction_id'] = $order->id;
        if($tmp['item']->confirmation == 'direct'){
            return self::_redirect(Orders::find($tmp['transaction_id']));
        }

        return self::payment(array_merge($tmp, $request->all()));
    }

    protected function _refresh_cart($order){
        $order['discount'] = isset($order['discount']) ? $order['discount'] : 0;
        $order['price'] = floatval($order['quantity']) * floatval($order['price_unit']);
        $order['pay_now'] = 0;
        $order['pay_the_day'] = 0;

        // si se ha puesto para regalo lo pagarán todo hoy
        if($order['is_gift'] == 'S'){
            $order['pay_now'] += self::get_discount($order['price'], $order['discount']);
            $order['pay_the_day'] = 0;
        }else{
            if($order['item']->confirmation == 'direct'){
                $order['pay_now'] = 0;
                $order['pay_the_day'] += self::get_discount($order['price'], $order['discount']);
            }
            if($order['item']->confirmation == 'fee'){
                $totalCost = floatval($order['quantity']) * floatval($order['item']->cost);
                $order['pay_now'] += self::get_discount(($order['price'] - $totalCost), $order['discount']);
                $order['pay_the_day'] += self::get_discount($totalCost, $order['discount']);
            }
        }

        $order['total'] = self::get_discount($order['price'], $order['discount']);

        return $order;
    }

    private function _redirect($order){
        parent::_update(['status' => 'confirmed'], $order);
        session()->forget('cart');

        return redirect()->route('orders.show', ['code' => $order->code, 'email' => $order->email]);
    }

    /**
     * submit payment page
     *
     * @return void
     */
    private function payment($request)
    {
        $payment_url = self::BASE_URL.'/v1/payment_methods';

        $payment_data = [
            'type' => 'card',
            'card[number]' => $request['card_number'],
            'card[exp_month]' => $request['month'],
            'card[exp_year]' => $request['year'],
            'card[cvc]' => $request['cvc'],
            'billing_details[email]' => $request['email'],
            'billing_details[name]' => $request['name'],
            'billing_details[phone]' => $request['phone']
        ];

        $payment_payload = http_build_query($payment_data);

        $payment_headers = [
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: Bearer ' . $this->getFinalStripeKey('secret')
        ];

        // sending curl request
        // see last function for code
        $payment_body = $this->curlPost($payment_url, $payment_payload, $payment_headers);

        $payment_response = json_decode($payment_body, true);

        // create payment intent request if payment method response contains id
        // see below documentation for options
        // https://stripe.com/docs/api/payment_intents/create
        if (isset($payment_response['id']) && $payment_response['id'] != null) {

            $request_url = self::BASE_URL.'/v1/payment_intents';

            $request_data = [
                'amount' => $request['pay_now'] * 100, // multiply amount with 100
                'currency' => "EUR",
                'payment_method_types[]' => 'card',
                'payment_method' => $payment_response['id'],
                'confirm' => 'true',
                'capture_method' => 'automatic',
                'return_url' => route('orders.callback', ['id' => $request['transaction_id']]),
                'payment_method_options[card][request_three_d_secure]' => 'automatic',
            ];

            $request_payload = http_build_query($request_data);

            $request_headers = [
                'Content-Type: application/x-www-form-urlencoded',
                'Authorization: Bearer ' . $this->getFinalStripeKey('secret')
            ];

            // another curl request
            $response_body = $this->curlPost($request_url, $request_payload, $request_headers);

            $response_data = json_decode($response_body, true);

            // transaction required 3d secure redirect
            if (isset($response_data['next_action']['redirect_to_url']['url']) && 
                $response_data['next_action']['redirect_to_url']['url'] != null) {

                return redirect()->away($response_data['next_action']['redirect_to_url']['url']);
            
            // transaction success without 3d secure redirect
            } elseif (isset($response_data['status']) && $response_data['status'] == 'succeeded') {

                return self::_redirect(Orders::find($request['transaction_id']));

            // transaction declined because of error
            } elseif (isset($response_data['error']['message']) && $response_data['error']['message'] != null) {
                return redirect()->back()->with('message_error', $response_data['error']['message'])->withInput();

            } else {

                return redirect()->back()->with('message_error', 'Something went wrong, please try again.');
            }

        // error in creating payment method
        } elseif (isset($payment_response['error']['message']) && $payment_response['error']['message'] != null) {

            return redirect()->back()->with('message_error', $payment_response['error']['message'])->withInput();

        }
    }

    /**
     * response from 3ds page
     *
     * @return Stripe response
     */
    public function callback(Request $request, $transaction_id)
    {
        $request_data = $request->all();

        // if only stripe response contains payment_intent
        if (isset($request_data['payment_intent']) && $request_data['payment_intent'] != null) {

            // here we will check status of the transaction with payment_intents from stripe server
            $get_url = self::BASE_URL . '/v1/payment_intents/' . $request_data['payment_intent'];

            $get_headers = [
                'Authorization: Bearer ' . $this->getFinalStripeKey('secret')
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $get_url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $get_headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $get_response = curl_exec($ch);

            curl_close ($ch);

            $get_data = json_decode($get_response, 1);

            // get record of transaction from database
            // so we can verify with response and update the transaction status
            // $input = \DB::table('transactions')
            //     ->where('transaction_id', $transaction_id)
            //     ->first();

            // here you can check amount, currency etc with $get_data
            // which you can check with your database record
            // for example amount value check
            /*if ($input['amount'] * 100 == $get_data['amount']) {
                // nothing to do
            } else {
                // something wrong has done with amount
            }*/

            // succeeded means transaction success
            if (isset($get_data['status']) && $get_data['status'] == 'succeeded') {

                return self::_redirect(Orders::find($transaction_id));

                // update here transaction for record something like this
                // $input = \DB::table('transactions')
                //     ->where('transaction_id', $transaction_id)
                //     ->update(['status' => 'success']);

            } elseif (isset($get_data['error']['message']) && $get_data['error']['message'] != null) {
                
                return route('orders.index')->with('error', $get_data['error']['message']);

            } else {

                return route('orders.index')->with('error', 'Payment request failed.');
            }
        } else {

            return route('orders.index')->with('error', 'Payment request failed.');

        }
    }

    protected function _get_rules($params){
        $rules = array();
        
        $rules['name'] = 'required|max:250';
        $rules['phone'] = 'nullable|numeric|digits_between:6,15';
        $rules['email'] = 'required|email|max:250';
        $rules['quantity'] = 'required|numeric';
        $tmp = session('cart');
        if($tmp['item']->confirmation != 'direct'){
            $rules['card_number'] = ['required', new CardNumber];
            $rules['month'] = 
                ['required', new CardExpirationMonth(isset($params['year']) ? $params['year'] : null)];
            $rules['year'] = 
                ['required', new CardExpirationYear(isset($params['month']) ? $params['month'] : null)];
            $rules['cvc'] = 
                ['required', new CardCvc(isset($params['card_number']) ? $params['card_number'] : null)];
        }

        return $rules;
    }
    
    protected function _get_messages(){
        $messages = array();
        $messages['name.required'] = trans('order.validation_name_required');
        $messages['name.max'] = trans('order.validation_name_max');
        $messages['phone.numeric'] = trans('order.validation_phone_numeric');
        $messages['phone.max'] = trans('order.validation_phone_max');
        $messages['email.required'] = trans('order.validation_email_required');
        $messages['email.email'] = trans('order.validation_email_email');
        $messages['email.max'] = trans('order.validation_email_max');

        $messages['quantity.required'] = trans('order.validation_quantity_required');
        $messages['quantity.numeric'] = trans('order.validation_quantity_numeric');

        $messages['card_number.validation.credit_card.card_invalid'] = trans('order.validation_credit_card_card_invalid');
        $messages['card_number.validation.credit_card.card_pattern_invalid'] = trans('order.validation_credit_card_card_invalid');
        $messages['card_number.validation.credit_card.card_length_invalid'] = trans('order.validation_credit_card_card_invalid');
        $messages['card_number.validation.credit_card.card_checksum_invalid'] = trans('order.validation_credit_card_card_invalid');

        $messages['cvc.validation.credit_card.card_cvc_invalid'] = trans('order.validation_credit_card_card_cvc_invalid');
        $messages['month.validation.credit_card.card_expiration_month_invalid'] = 
            trans('order.validation_credit_card_card_card_expiration_month_invalid');
        $messages['year.validation.credit_card.card_expiration_year_invalid'] = 
            trans('order.validation_credit_card_card_expiration_year_invalid');

        $messages['card_number.required'] = trans('order.validation_credit_card_required') . " (:attribute)";
        $messages['cvc.required'] = trans('order.validation_credit_card_required') . " (:attribute)";
        $messages['month.required'] = trans('order.validation_credit_card_required') . " (:attribute)";
        $messages['year.required'] = trans('order.validation_credit_card_required') . " (:attribute)";

        return $messages;
    }

    /**
     * create curl request
     * we have created seperate method for curl request
     * instead of put code at every request
     *
     * @return Stripe response
     */
    private function curlPost($url, $data, $headers)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        curl_close ($ch);

        return $response;
    }

    public function confirm_visit(Request $request, $order_encrypted, $set = 'N'){
        $tmporder = base64_decode($order_encrypted);
        $this->data['order'] = \App\Orders::find($tmporder);

        if($set == 'S' || $set == 'X'){
            $this->data['order']->date_visited = $set == 'S' ? date('Y-m-d H:i:s') : null;
            $this->data['order']->save();
            return redirect()->route('orders.confirm_visit', ['order_encrypted' => $order_encrypted]);
        }

        $this->data['order_encrypted'] = $order_encrypted;
        return view('orders.confirm_visit', $this->data);
    }

    public function confirm_payment(Request $request, $crypt, $set = 'N'){
        $params = json_decode(base64_decode($crypt), true);

        if(\Auth::loginUsingId($params['user'])){
            $this->data['order'] = \App\Orders::find($params['order']);

            if($set == 'S' || $set == 'X'){
                $this->data['order']->date_paid = $set == 'S' ? date('Y-m-d H:i:s') : null;
                $this->data['order']->save();
                return redirect()->route('orders.confirm_payment', ['crypt' => $crypt]);
            }

            $this->data['crypt'] = $crypt;
            return view('orders.confirm_payment', $this->data);
        }
        return redirect()->route('/');
    }
}
