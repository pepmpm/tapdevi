<div class="row">
    <div class="col-md-12">
        @include('_layouts.messages', array('errors' => $errors))
    </div>
</div>
@if(session()->has('cart'))
    @php($plan = session('cart')['item'])
    <form role="form" method="post" enctype="multipart/form-data" autocomplete="off" action="{{ route('orders.save') }}">
        @csrf
        <div class="row">
            <div class="col-md-8">
                <div class="panel bgcolor-blanco">
                    <div class="panel-body">
                        <h2>{{ trans('inicio.articulo') }}</h2>
                        <div class="row">
                            <div class="col-md-11">
                                <h3>{{ $plan->name }}</h3>
                            </div>
                            <div class="col-md-1">
                                @if(!(isset($readonly) && $readonly))
                                    <a class="close" href="{{ route('orders.delete', ['id' => $plan->id]) }}">
                                        <span>&times;</span><span class="sr-only">Close</span>
                                    </a>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <img class="img-responsive" alt="{{ $plan->name }}"
                                    src="{{ Imgfly::imgPublic(str_replace("upload/", "", $plan->one_photo()) . '?w=407&h=260&fit=crop-center', 'upload') }}">
                                @if($plan->type != 'pack')
                                    <ul class="list-unstyled">
                                        <li>{{ $plan->user->name }}</li>
                                        <li>{{ $plan->user->district->name }}</li>
                                    </ul>
                                @endif
                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-8 text-right col-xs-8">
                                        <strong>{{ trans('inicio.precio') }}</strong>
                                    </div>
                                    <div class="col-md-4 col-xs-4">
                                        <div class="input-group">
                                            <input type="text" id="txt_price" class="form-control input-sm" {{ \Auth::check() && \Auth::user()->isAdmin() ? '' : 'readonly="readonly"' }} 
                                                value="{{ session('cart')['price_unit'] }}" />
                                            <div class="input-group-addon">
                                                <span class="input-group-text">€</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8 text-right col-xs-8">
                                        <strong>{{ trans('inicio.cantidad') }}</strong>
                                    </div>
                                    <div class="col-md-4 col-xs-4">
                                        @if(isset($readonly) && $readonly)
                                            {{ session('cart')['quantity'] }}
                                        @else
                                            <select name="quantity" id="sel_quantity" class="form-control input-sm">
                                                @for($x = $plan->min_pax; $x <= ($plan->max_pax != null ? $plan->max_pax : 40); $x++)
                                                    <option value="{{ $x }}" {{ session('cart')['quantity'] == $x ? 'selected=selected' : '' }}>{{ $x }}</option>
                                                @endfor
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <hr class="border-top-bbb margin-top-bottom-15" />
                                <div class="row margin-bottom-35">
                                    <div class="col-md-8 col-xs-8 text-right">
                                        <strong>{{ trans('inicio.total') }}</strong>
                                    </div>
                                    <div class="col-md-4 col-xs-4">
                                        <input type="text" class="form-control input-sm" readonly="readonly" 
                                            value="{{ (session('cart')['price']) . "€" }}" /> 
                                    </div>
                                </div>
                                @if(!isset(session('cart')['coupon']))
                                    @if(session()->has('cart'))
                                        <div class="row">
                                            <div class="col-md-5">
                                                <strong>{{ trans('inicio.codigo_descuento') }}</strong><br/>
                                                {{ trans('inicio.introduce_codigo_descuento') }}
                                            </div>
                                            <div class="col-md-3 col-xs-6">
                                                <input class="form-control margin-bottom-10" type="text" id="coupons_id" name="coupons_id">
                                            </div>
                                            <div class="col-md-4 col-xs-6 pull-right">
                                                <button type="button" id="btn_coupon" class="btn-u btn-brd btn-brd-hover btn-block">
                                                    {{ trans('inicio.aplicar_cupon') }}
                                                </button>
                                            </div>
                                        </div>
                                    @endif
                                @else
                                    <div class="pull-right">
                                        <div class="alert alert-info fade in">
                                            @php($coupon = session('cart')['coupon'])
                                            <p><strong>{{ trans('order.codigo_descuento_aplicado', ['code' => $coupon['code']]) }}</strong></p>
                                            @if($coupon->discount > 0)
                                                <p>{{ trans('inicio.codigo_descuento') . ": -" . $coupon['discount'] . '%' }}</p>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @if(session('cart')['is_gift'] == 'S')
                    <div class="row">
                        <div class="col-md-12">
                            <section>
                                <div class="panel bgcolor-blanco">
                                    <div class="panel-body">
                                        <h2>{{ trans('plan.anyade_mensaje_de_regalo') }}</h2>
                                        <div class="row margin-bottom-25">
                                            <div class="col-md-12">
                                                <textarea name="gift_remarks" placeholder="{{ trans('plan.gift_remarks_placeholder') }}" 
                                                    class="form-control">{{ old('gift_remarks') }}</textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input class="form-control margin-bottom-10" type="text" name="gift_name" 
                                                    placeholder="{{ trans('plan.quien_lo_regala') }}" value="{{ old('gift_name') }}">
                                            </div>
                                            <div class="col-md-6">
                                                <input class="form-control margin-bottom-10" type="text" name="gift_recipient" 
                                                    placeholder="{{ trans('plan.nombre_destinatario') }}" value="{{ old('gift_recipient') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                @endif
                <div class="panel bgcolor-blanco">
                    <div class="panel-body">
                        <h2>{{ trans('order.informacion_envio') }}</h2>
                        @if(\Auth::check())
                            @php($tmpuser = \Auth::user())
                        @endif
                        <div class="row">
                            <div class="col-sm-5 col-xs-12">
                                <input type="text" placeholder="{{ trans('order.nombre') }}" name="name" class="form-control margin-bottom-15"
                                    value="{{ old('name', isset($tmpuser) ? $tmpuser->name : '') }}">
                            </div>
                            <div class="col-sm-4 col-xs-6">
                                <input type="email" placeholder="{{ trans('order.email') }}" name="email" class="form-control margin-bottom-15"
                                    value="{{ old('email', isset($tmpuser) ? $tmpuser->email : '') }}">
                            </div>
                            <div class="col-sm-3 col-xs-6">
                                <input type="tel" placeholder="{{ trans('order.telefono') }}" name="phone" class="form-control margin-bottom-15"
                                    value="{{ old('phone', isset($tmpuser) ? $tmpuser->phone : '') }}">
                            </div>
                        </div>
                        <div class="row margin-bottom-25">
                            <div class="col-md-12">
                                <textarea name="remarks" placeholder="{{ trans('plan.remarks_placeholder') }}" 
                                    class="form-control">{{ old('remarks') }}</textarea>
                            </div>
                        </div>

                    </div>
                </div>
                @if($plan->confirmation == 'fee' || $plan->confirmation == 'groupon' || session('cart')['is_gift'] == 'S')
                    @include('cart._index_invoicing')
                @endif
            </div>
            <div class="col-md-4 div_total">
                <div class="panel bgcolor-blanco">
                    <div class="panel-body">
                        <ul class="list-inline total-result">
                            @if (\Auth::check() && \Auth::user()->isAdmin())
                                <li>
                                    <h4 class="margin-top-0">Encrypted Cart:</h4>
                                    <div class="total-result-in">
                                        <input type="text" value="{{ route('orders.add_to_cart', ['q' => $encryptedCart]) }}">
                                    </div>
                                </li>
                            @endif
                            <li>
                                <h4 class="margin-top-0">{{ trans('inicio.subtotal') }}:</h4>
                                <div class="total-result-in">
                                    <span>{{ session('cart')['price'] . "€" }}</span>
                                </div>
                            </li>
                            @if(isset(session('cart')['coupon']))
                                @php($coupon = session('cart')['coupon'])
                                <li>
                                    <h4 class="margin-top-0">{{ trans('inicio.codigo_descuento') }}:</h4>
                                    <div class="total-result-in">
                                        <span class="text-right">{{ "- " . $coupon['discount'] . "%" }}</span>
                                    </div>
                                </li>
                            @endif
                            <li class="divider"></li>
                            @if(session('cart')['pay_the_day'] > 0)
                                <li class="margin-bottom-15">
                                    <h4 class="margin-top-0">{{ trans('mail.a_pagar_ahora') }}:</h4>
                                    <div class="total-result-in">
                                        <span>{{ session('cart')['pay_now'] . "€" }}</span>
                                    </div>
                                </li>
                                <li class="margin-bottom-15">
                                    <h4 class="margin-top-0">{{ trans('mail.a_pagar_el_dia') }}:</h4>
                                    <div class="total-result-in">
                                        <span>{{ session('cart')['pay_the_day'] . "€" }}</span>
                                    </div>
                                </li>
                            @endif
                            <li class="total-price margin-bottom-15">
                                <h4 class="margin-top-0">{{ trans('inicio.total') }}:</h4>
                                <div class="total-result-in">
                                    <span>{{ session('cart')['total'] . "€" }}</span>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-md-6 col-sm-offset-6">
                                        <button type="submit" name="submit" class="btn btn-block btn-u btn-u-lg">
                                            {{ trans('inicio.siguiente') }}
                                        </button>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endif