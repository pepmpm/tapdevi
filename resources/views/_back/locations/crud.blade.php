@extends('_layouts/inicio')

@section ('body')

<!--=== Breadcrumbs ===-->
<div class="breadcrumbs">
    <div class="container">
        @if(isset($query))
            <h1 class="pull-left">Localización: {{$query->name}}</h1>
        @else
            <h1 class="pull-left">Nueva localización</h1>
        @endif
    </div><!--/container-->
</div><!--/breadcrumbs-->
<div class="container content">
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('back.admin.locations.index') }}" 
               class="btn-u btn-u-lg" role="button">
                Volver a la lista
            </a>
            <a href="{{ route('back.admin.locations.create') }}" 
               class="btn-u btn-u-lg" role="button">
                Crear otra localización
            </a>
        </div>     
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            @include('_layouts.messages', array('errors' => $errors))
            <div class="panel panel-u margin-bottom-40 bg-grey">
                <div class="panel-body">
                    <div class="row">
                        <form class="margin-bottom-20" role="form" method="post" enctype="multipart/form-data" 
                              action="{{ isset($query) ? route('back.admin.locations.update', $query->id) : route('back.admin.locations.store') }}">
                            @if(isset($query))
                                @method('PUT')
                            @endif
                            @csrf
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Nombre</label>
                                            <input type="text" class="form-control" name="name" id="nombre" placeholder="Nombre"
                                                    value="{{ old('name', isset($query) ? $query->name: '') }}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success btn-block" style="margin-top: 25px">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Latitud</label>
                                            <input type="text" class="form-control" name="latitude" id="latitud" placeholder="Latitud"
                                                    value="{{ old('latitude', isset($query) ? $query->latitude: '') }}"/>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Longitud</label>
                                            <input type="text" class="form-control" name="longitude" id="longitud" placeholder="Longitud"
                                                    value="{{ old('longitude', isset($query) ? $query->longitude: '') }}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Localización</label>
                                        <button type="button" id="btn_refresh_map" class="btn btn-u margin-bottom-5">
                                            <i class="fa fa-refresh"></i>
                                            Actualizar mapa
                                        </button>
                                        <div id="map" class="height350"></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    @if(isset($query))
                        <div class="tab-v2">
                            <ul class="nav nav-tabs">
                                <li class="{{!Request::has('tab') || Request::get('tab') == 'photos' ? 'active' : ''}}"><a href="#photos" data-toggle="tab">Fotos</a></li>
                                <li class="{{Request::get('tab') == 'langs' ? 'active' : ''}}"><a href="#descripciones" data-toggle="tab">Descripciones</a></li>
                            </ul>
                            <div class="tab-content bg-color-white">
                                <div class="tab-pane fade in {{!Request::has('tab') || Request::get('tab') == 'photos' ? 'active' : ''}}" id="photos">
                                    @include('_back.locations.crud_photos', array('query' => $query))
                                </div>
                                <div class="tab-pane fade in {{Request::get('tab') == 'langs' ? 'active' : ''}}" id="descripciones">
                                    @include('_back.locations.crud_langs', array('query' => $query))
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section ('assets_end')
    <link rel="stylesheet" href="{{ asset('assets/plugins/crop-image/components/imgareaselect/css/imgareaselect-default.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/crop-image/css/jquery.awesome-cropper.css') }}">
    <script src="{{ asset('assets/plugins/crop-image/components/imgareaselect/scripts/jquery.imgareaselect.js') }}"></script>
    <script src="{{ asset('assets/plugins/crop-image/build/jquery.awesome-cropper.js') }}"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBw_spE5_xC8pjOBJVWs5LHLBKn0EC-cfQ"></script>
    <script src="{{ asset('assets/plugins/gmap/gmap.js') }}"></script>

    <script src="{{ asset('assets/plugins/redactor/redactor.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/plugins/redactor/redactor.css') }}">
    <script src="{{ asset('assets/plugins/redactor/lang/es.js') }}"></script>
    <script src="{{ asset('assets/plugins/redactor/plugins/read-more-plugin.js') }}"></script>

    <link rel="stylesheet" href="{{ asset('assets/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css') }}">

    <script src="{{ asset('assets/plugins/cube-portfolio/cubeportfolio/js/jquery.cubeportfolio.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/cube-portfolio/cube-portfolio-4.js') }}"></script>

    <script src="{{ asset('assets/plugins/fancybox/source/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/plugins/fancybox/source/jquery.fancybox.css') }}">

    <script type="text/javascript">
        $(document).ready(function() {
            if($('.redactor').length){
                $('.redactor').redactor({
                    minHeight: 400,
                    lang: 'es',
                    plugins: ['leermas']
                });
            }
        });
    </script>

    <script type="text/javascript">
        var map;
        var marker;
        $(document).ready(function() {
            if($('#map').length){
                map = new GMaps({
                    div: '#map',
                    scrollwheel: false,             
                    lat: 40.416775,
                    lng: -3.703790,
                    zoom: 5
                });
                if($("#latitud").val() && $("#longitud").val()){
                    postGeocoords(map, $("#longitud").val(), $("#latitud").val());
                }

                $("#btn_refresh_map").click(function () {
                    postGeocode(map);
                });
            }
            $('#p_foto').awesomeCropper({
                width: 1024,
                height: 768
            });
        });
        $(function(){
            if($(".fancybox").length){
                jQuery(".fancybox").fancybox({
                    groupAttr: 'data-rel',
                    prevEffect: 'fade',
                    nextEffect: 'fade',
                    openEffect  : 'elastic',
                    closeEffect  : 'fade',
                    closeBtn: true,
                    helpers: {
                        title: {
                            type: 'float'
                        }
                    }
                });
            }
        });
        function postGeocoords(mapa, longitud, latitud){
            mapa.setCenter(latitud, longitud);
            mapa.setZoom(15);
            if(marker == null){
                marker = mapa.addMarker({
                    lat: latitud,
                    lng: longitud,
                    draggable: true
                });
            }else{
                marker.setPosition( new google.maps.LatLng( latitud, longitud ) );
                mapa.panTo( new google.maps.LatLng( latitud, longitud ) );
            }

            $("#latitud").val(latitud);
            $("#longitud").val(longitud);

            google.maps.event.addListener(
                marker,
                'drag',
                function() {
                    $("#latitud").val(marker.position.lat());
                    $("#longitud").val(marker.position.lng());
                }
            );
        }

        function postGeocode(mapa){
            var nombre = $("#nombre").val() + " Mallorca";
            GMaps.geocode({
                address: nombre,
                callback: function(results, status) {
                    if (status == 'OK') {
                        var latlng = results[0].geometry.location;
                        mapa.setCenter(latlng.lat(), latlng.lng());
                        mapa.setZoom(15);
                        if(marker == null){
                            marker = mapa.addMarker({
                                lat: latlng.lat(),
                                lng: latlng.lng(),
                                draggable: true
                            });
                        }else{
                            marker.setPosition( new google.maps.LatLng( latlng.lat(), latlng.lng() ) );
                            mapa.panTo( new google.maps.LatLng( latlng.lat(), latlng.lng() ) );
                        }

                        $("#latitud").val(latlng.lat());
                        $("#longitud").val(latlng.lng());

                        google.maps.event.addListener(
                            marker,
                            'drag',
                            function() {
                                $("#latitud").val(marker.position.lat());
                                $("#longitud").val(marker.position.lng());
                            }
                        );
                    }
                }
            });
        }
    </script>
@stop
