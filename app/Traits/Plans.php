<?php
namespace App\Traits;

use Cache;
use App\Items;
use App\ItemsLangs;
use App\ItemsPhotos;
use Illuminate\Http\Request;

trait Plans {

    /** $params
    $params['cat'] = label
    $params['user'] = usuario (empresa)
    $params['district'] = distrito (pueblo)

    $params['order'] = ordenación de los planes
    $params['date'] = fechas (today, tomorrow, weekend, all)
    $params['take'] = cuantos planes coger
    $params['limit'] = planes en cada página
    **/
    public function getPlans($params) {
        $labels = \App\Labels::where('category', '=', 'enotourism-mallorca');

        //buscamos categorias
        if(isset($params['cat']) && $params['cat'] != null){
            $labels = $labels->whereHas('langs', function ($query) use ($params) {
                $query->where('slug', '=', $params['cat']);
                $query->where('locale', '=', $params['locale']);
            });
        }

        $users = \App\User::whereIn('role', ['company', 'admin']);

        if(isset($params['user']) && $params['user'] != null){
            $users = $users->where('slug', '=', $params['user']);
        }else{
            if(isset($params['district']) && $params['district'] != null){
                $users = $users->whereHas('district', function ($query) use ($params) {
                    $query->where('slug', '=', $params['district']);
                });
            }
        }

        if(isset($params['date'])){
            if($params['date'] == 'weekend'){
                $tmpdias = [
                    date("Y-m-d", strtotime('next Friday')),
                    date("Y-m-d", strtotime('next Saturday')),
                    date("Y-m-d", strtotime('next Sunday')),
                ];
            }else{
                $splitday = explode("-", $params['date']);
                if(count($splitday) == 3){
                    $tmpdias = [
                        date("Y-m-d", strtotime($splitday[2] . "-" . 
                            trans('base.num_' . $splitday[1]) . "-" . 
                            $splitday[0]))
                    ];
                }
            }

            if(isset($tmpdias) && count($tmpdias) > 0){
                $stocks = \App\ItemsStocks::whereIn('date', $tmpdias)->pluck('items_id')->all();
            }
        }

        $tmpitems = [];
        $items = Items::where('type', '=', 'plan')->whereIn('labels_id', $labels->pluck('id')->all())
            ->whereIn('users_id', $users->pluck('id')->all())
            ->where('status', '=', 'active')
            ->get();

        foreach($items as $item){
            $insert = true;

            if(isset($params['promoted']) && $item->is_promoted != $params['promoted']){
                $insert = false;
            }

            if(isset($tmpdias) && isset($stocks) && count($tmpdias) > 0){
                //recorremos los días
                foreach($tmpdias as $tmpdia){
                    //miramos si entre hoy y el día filtrado hay más días de los que la empresa exige de antelación
                    if( $this->diferenciadias( date('Y-m-d'), $tmpdia ) < $item->advance_days ){
                        //si los hay filtramos el item
                        if(in_array($item->id, $stocks)){
                            $insert = false;
                        }
                    }
                }
            }

            if($insert){
                $tmpitems[] = $item->id;
            }
        }

        $items = Items::with('photos', 'stocks', 'next_stock', 'langs')
            ->whereIn('id', $tmpitems)
            ->orderBy(isset($params['order']) ? $params['order'] : 'order', 'asc');

        if(isset($params['take'])){
            return $items->take($params['take'])->get();
        }

        return $items->{isset($params['paginate']) ? $params['paginate'] : 'paginate'}(isset($params['limit']) ? $params['limit'] : 20);
    }


    public function getEvents($params) {
        $items = [];
        $items_id = [];
        $tmpitems = \App\Items::where('type', '=', 'event')->where('status', '=', 'active')->pluck('id')->all();
        $stocks = \App\ItemsStocks::with('item', 'item.district')->whereIn('items_id', $tmpitems);

        if(isset($params['year'])){
            $stocks = $stocks->whereYear('date', $params['year']);
        }

        if(isset($params['month'])){
            $stocks = $stocks->whereMonth('date', $params['month']);
        }

        if(isset($params['day']) && $params['day'] != null){
            $stocks = $stocks->whereDay('date', $params['day']);
        }

        if(!isset($params['month']) && !isset($params['year']) && !isset($params['day'])){
            $stocks = $stocks->where('date', '>=', date('Y-m') . '-1');
        }

        foreach($stocks->orderBy('date', 'asc')->get() as $stock){
            if(in_array($stock->items_id, $items_id)){
                continue;
            }

            if(isset($params['not_in']) && $params['not_in'] == $stock->items_id){
                continue;
            }

            if(isset($params['limit']) && $params['limit'] <= count($items)){
                break;
            }

            if(isset($params['get']) && $params['get'] == 'stocks'){
                $items[$stock->id] = $stock;
            } else {
                $items[$stock->items_id] = $stock->item;
            }
            
        }

        return $items;
    }


    protected function _get_labels(){
        return \Cache::remember('labels_sidebar_plans', 1440, function () {
            return \App\Labels::with('plans')->withCount('plans')->whereHas('plans', function ($query) {
                    $query->where('status', '=', 'active');
                })->where('category', '=', 'enotourism-mallorca')->get();
        });
    }

    protected function _get_districts(){
        return Cache::remember('districts_sidebar_plans', 1440, function () {
            return \App\Districts::withCount(['plans' => function ($query) {
                    $query->where('status', '=', 'active');
                    $query->where('type', '=', 'plan');
                }])->whereHas('plans', function ($query) {
                    $query->where('status', '=', 'active');
                    $query->where('type', '=', 'plan');
                })->orderBy('name', 'asc')->get();
        });
    }

    protected function _get_users(){
        return Cache::remember('users_sidebar_plans', 1440, function () {
            return \App\User::withCount(['plans' => function ($query) {
                    $query->where('status', '=', 'active');
                    $query->where('type', '=', 'plan');
                }])->whereHas('plans', function ($query) {
                    $query->where('status', '=', 'active');
                    $query->where('type', '=', 'plan');
                })->orderBy('name', 'asc')->get();
        });
    }

    private function _get_params($request){
        $params = array();
        $params['district'] = $request->route('district');
        $params['cat'] = $request->route('cat');
        $params['user'] = $request->route('user');
        $params['locale'] = \App::getLocale();

        return $params;
    }
}