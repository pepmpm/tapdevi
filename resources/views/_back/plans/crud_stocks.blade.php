@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section ('body')

<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left">Actividad: {{ $query->name }}</h1>
    </div>
</div>
<div class="container content">
    <div class="row margin-bottom-15">
        <div class="col-md-12">
            @include('_layouts.messages', array('errors' => $errors))
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('back.plans.index') }}" 
               class="btn-u btn-u-lg" role="button">
                Volver a la lista
            </a>
            <a href="{{ route('back.plans.create') }}" 
               class="btn-u btn-u-lg" role="button">
                Crear otra actividad
            </a>
            <a href="{{ route('back.plans.show', [$query->id]) }}" 
               class="btn-u btn-u-green btn-u-lg" role="button">
                Modificar información
            </a>
        </div>
    </div>
    <hr/>

    @if($query->stocks->count() > 0)
        @php($stocks_array = $query->stocks_array())
        @php($times_array = $query->times_array())
        <div class="row margin-bottom-15">
            <div class="col-md-12">
                <form action="{{ route('back.plans.stocks.destroy', [$query->stocks->first()->id]) }}" method="POST" 
                    onsubmit="if(!confirm('¿Estás seguro de querer borrar toda la información de stocks y horarios?')) return false"
                    class="pull-right">
                    @method('DELETE')
                    @csrf
                    <button class="btn btn-danger btn-sm" type="submit">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                </form>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Por días de la semana</div>
                <div class="panel-body">
                    <div class="row margin-bottom-15">
                        <div class="col-md-12">
                            <table class="table table-bordered table-condensed" id="tabla_stock_week" style="table-layout: fixed;">
                                <thead>
                                    <tr>
                                        <th>L</th>
                                        <th>M</th>
                                        <th>X</th>
                                        <th>J</th>
                                        <th>V</th>
                                        <th>S</th>
                                        <th>D</th>
                                        <th width="30%">Horarios</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <input type="checkbox" class="check_dia_semana" value="1"/>
                                        </td>
                                        <td>
                                            <input type="checkbox" class="check_dia_semana" value="2"/>
                                        </td>
                                        <td>
                                            <input type="checkbox" class="check_dia_semana" value="3"/>
                                        </td>
                                        <td>
                                            <input type="checkbox" class="check_dia_semana" value="4"/>
                                        </td>
                                        <td>
                                            <input type="checkbox" class="check_dia_semana" value="5"/>
                                        </td>
                                        <td>
                                            <input type="checkbox" class="check_dia_semana" value="6"/>
                                        </td>
                                        <td>
                                            <input type="checkbox" class="check_dia_semana" value="7"/>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control times_dia_semana" placeholder="hora1#hora2#..."/>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form method="post" action="{{ route('back.plans.stocks.store') }}">
        @csrf
        <input type="hidden" name="items_id" value="{{ $query->id }}"/>

        <div class="row margin-bottom-15">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary btn-block btn-sm" role="button">
                    <i class="fa fa-save" aria-hidden="true"></i>&nbsp;Guardar
                </button>
            </div>
        </div>

        @for($x = 0; $x < 12; $x++)
            @php($current_month = strtotime("+" . $x . " month", strtotime(date('Y-m') . '-1')))

            <div class="row margin-bottom-15">
                <div class="col-md-12">
                    <table class="table table-bordered table-condensed" style="table-layout: fixed;">
                        <thead>
                            <tr class="active">
                                <td colspan="7" class="text-center">
                                    {{ trans('base.mes' . date("n", $current_month)) . date("y", $current_month) }}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">L</td>
                                <td class="text-center">M</td>
                                <td class="text-center">M</td>
                                <td class="text-center">J</td>
                                <td class="text-center">V</td>
                                <td class="text-center">S</td>
                                <td class="text-center">D</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                @php($first_day_week = date('N', $current_month))
                                @for($colspan_td = 1; $colspan_td < $first_day_week; $colspan_td++)
                                    <td></td>
                                @endfor

                                @for($dia = 0; $dia < date('t', $current_month); $dia ++)
                                    @php($tmpdate = date_create_from_format('Y-m-d', date("Y-m-d", strtotime("+" . $dia . " day", $current_month))))
                                    @php($tmppartialdate = date("Y-m", strtotime("+" . $x . " month")))

                                    @if( strtotime($tmpdate->format('Y-m-d')) < strtotime('now') )
                                        <td></td>
                                    @else

                                        <td class="text-center">
                                            <label><strong>{{ $tmpdate->format('d') }}</strong></label>
                                            <input type="checkbox" name="date[]" class="check_dia_mes" 
                                                value="{{ $tmpdate->format('Y-m-d') }}" 
                                                dia_semana="{{ date("N", strtotime($tmppartialdate . "-" . $tmpdate->format('j'))) }}"
                                                {{ isset($stocks_array[$tmpdate->format('Y-m-d')]) ? 'checked="checked"' : '' }} />
                                            <br/>
                                            <input type="text" name="times[{{ $tmpdate->format('Y-m-d') }}]" class="form-control times_dia_mes font11" 
                                                dia_semana="{{ date("N", strtotime($tmppartialdate . "-" . $tmpdate->format('j'))) }}" 
                                                placeholder="hora1#hora2#..." 
                                                value="{{ isset($times_array[$tmpdate->format('Y-m-d')]) ? $times_array[$tmpdate->format('Y-m-d')] : '' }}" />
                                        </td>

                                    @endif
                                    @if($tmpdate->format('N') % 7 == 0)
                                        </tr><tr>
                                    @endif
                                @endfor
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        @endfor

        <div class="row margin-bottom-15">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary btn-block btn-sm" role="button">
                    <i class="fa fa-save" aria-hidden="true"></i>&nbsp;Guardar
                </button>
            </div>
        </div>
    </form>
</div>
@stop

@section ('assets_end')
    <script type="text/javascript">
        $(function(){
            $(".check_dia_semana").change(function() {
                var checkeado = this.checked;
                var dia_checkeado = $(this).val();
                $(".check_dia_mes").each(function() {
                    if($(this).attr('dia_semana') == dia_checkeado){
                        this.checked = checkeado;
                        if(!checkeado){
                            $(this).closest('td').find("input[type=text]").val("");
                        }
                    }
                });
            });
            $(".times_dia_semana").change(function() {
                var valor_times_semana = $(this).val();

                $(".times_dia_mes").each(function() {
                    var times_dia_mes = $(this);
                    var dia_semana = $(this).attr('dia_semana');
                    $(".check_dia_semana").each(function() {
                        if($(this).is(':checked') && dia_semana == $(this).val()){
                            times_dia_mes.val(valor_times_semana);
                        }
                    });
                });
            });
        });
    </script>
@stop