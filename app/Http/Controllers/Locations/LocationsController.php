<?php

namespace App\Http\Controllers\Locations;

use App\Locations;
use App\LocationsLangs;
use Illuminate\Http\Request;
use App\Traits\Helpers;
use Intervention\Image\ImageManagerStatic as Image;

class LocationsController extends \App\Http\Controllers\Controller
{
    use Helpers;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function _index()
    {
        return Locations::all();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function _all()
    {
        return Locations::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function _store($request)
    {
        if(!isset($request['name']) || $request['name'] == ''){
            return null;
        }

        //insertamos
        $tmp = new Locations;
        $tmp->name = $request['name'];
        $tmp->slug = $this->getSlug($request['name'], 'slug', Locations::getModel());
        $tmp->latitude = $request['latitude'];
        $tmp->longitude = $request['longitude'];
        $tmp->save();

        foreach(config('translatable.locales') as $keylocale => $locale){
            $lang = new LocationsLangs;
            $lang->locale = $keylocale;
            $lang->locations_id = $tmp->id;
            $lang->save();
        }

        return $tmp;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _show(Locations $location)
    {
        return $location;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _update($request, Locations $location)
    {
        if(!isset($request['name']) || $request['name'] == ''){
            return null;
        }

        //insertamos
        $location->name = $request['name'];
        $location->slug = $this->getSlug($request['name'], 'slug', Locations::getModel());
        $location->save();

        if(isset($request['p_foto']) && $request['p_foto'] != null){
            $location = self::__upload_image($request, $location);
        }

        return $location;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bikes  $bikes
     * @return \Illuminate\Http\Response
     */
    public function _destroy(Locations $location)
    {
        $ruta = 'upload/locations/'.$location->id.'/';
        if(is_dir($ruta)){
            $success = \File::deleteDirectory($ruta, true);
            rmdir($ruta);
            if(!$success){
                throw new \Exception('El directorio de imagenes de '.$location->id.' no existe');
            }
        }

        $location->delete();

        return true;
    }

    private function __upload_image($request, $tmp){
        $ruta = 'upload/locations/'.$tmp->id.'/';
        \File::makeDirectory($ruta, 0775, true, true);

        // read image from temporary file
        $img = Image::make(base64_decode(substr($request['p_foto'], strpos($request['p_foto'], ",")+1)))->encode('webp', 90);

        $tmp->logo = $ruta . $tmp->slug . "-" . $tmp->id . ".webp";
        $tmp->save();

        /*****************REDIMENSIONAMOS IMAGEN************************/
        $img->resize(1024, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($tmp->logo);
        if(env("APP_ENV") != 'local'){
            $this->set_72dpi_image($tmp->logo);
        }

        return $tmp;
    }
}
