$(document).ready(function() { 
    $(document).on("click", ".next_month", function (event) {
        var index = parseInt($(this).attr('index'));
        if(index < 2){
            $('.table_month').hide();
            $('.table_month_' + (index + 1)).removeClass( "hide" ).css("display", "block");
        }
        event.preventDefault();
    });
    $(document).on("click", ".previous_month", function (event) {
        var index = parseInt($(this).attr('index'));
        if(index > 0){
            $('.table_month').hide();
            $('.table_month_' + (index - 1)).removeClass( "hide" ).css("display", "block");
        }
        event.preventDefault();
    });
});