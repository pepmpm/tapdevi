<?php

use Illuminate\Database\Seeder;
use \App\Districts;
use \App\DistrictsLangs;

class DistrictsLangsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        \DB::table('districts_langs')->truncate();
		
		$dis = \App\Districts::all();
		
		foreach($dis as $tmp){
			foreach(config('translatable.locales') as $keylocale => $locale){
				$lang = new DistrictsLangs;
				$lang->locale = $keylocale;
				$lang->districts_id = $tmp->id;

				if($keylocale == 'es'){
					$lang->title = 'Enoturismo en '. $tmp->name .' ✔️ Tours de vino en '. $tmp->name .' ✔️ Disfruta de experiencias con vino de '. $tmp->name;
					$lang->description = "No te pierdas los tours de enoturismo que te llevarán por bodegas familiares y por los restaurantes donde vamos los residentes en ". $tmp->name .". Disfrutarás de nuestros excelentes vinos en parajes naturales que te enamorarán.";
					$lang->seo_keywords = "vino ". $tmp->name .", enoturismo ". $tmp->name .", experiencias vino ". $tmp->name .", turismo vino ". $tmp->name .", tours vino ". $tmp->name;
					$lang->seo_description = "No te pierdas los tours de enoturismo que te llevarán por bodegas familiares y por los restaurantes donde vamos los residentes en ". $tmp->name .". Disfrutarás de nuestros excelentes vinos en parajes naturales que te enamorarán.";
				}
				
				if($keylocale == 'ca'){
					$lang->title = "Enoturisme a ". $tmp->name ." ✔️ Tours de vi a ". $tmp->name ." ✔️ Gaudeix d'experiències amb vi de ". $tmp->name;
					$lang->description = "No te perdis els tours d'enoturisme que te duràn per bodegues familiars i pels restaurants on anam els residents de ". $tmp->name .". Gaudiràs dels nostres excel.lents vins a llocs naturals que t'enamoraràn";
					$lang->seo_keywords = "vi ". $tmp->name .", enoturisme ". $tmp->name .", experiencies vi ". $tmp->name .", turisme vi ". $tmp->name .", tours vi ". $tmp->name;
					$lang->seo_description = "No te perdis els tours d'enoturisme que te duràn per bodegues familiars i pels restaurants on anam els residents de ". $tmp->name .". Gaudiràs dels nostres excel.lents vins a llocs naturals que t'enamoraràn";

				}

				if($keylocale == 'en'){
					$lang->title = "Enotourism in ". $tmp->name ." ✔️ Wine Tours in ". $tmp->name ." ✔️ Enjoy experiences with wine from ". $tmp->name ."";
					$lang->description = "Do not miss the wine tourism tours that will take you to family wineries and to the restaurants where residents of ". $tmp->name ." go. You will enjoy our excellent wines in natural settings that will make you fall in love";
					$lang->seo_keywords = "". $tmp->name ." wine, enotourism ". $tmp->name .", wine experiences ". $tmp->name .", wine tourism ". $tmp->name .", ". $tmp->name ." wine tours";
					$lang->seo_description = "Do not miss the wine tourism tours that will take you to family wineries and to the restaurants where residents of ". $tmp->name ." go. You will enjoy our excellent wines in natural settings that will make you fall in love";
				}

				$lang->save();
			}
		}
    }
}
