@inject('helpers', 'App\Http\Controllers\AuxController')
@extends('_layouts/inicio')

@section('title', trans('meta.title_map_event'))
@section ('meta')
    <meta name="description" content="{{ trans('meta.desc_map_event') }}"/>
    <meta name="keywords" content="{{ trans('meta.keys_map_event') }}" />
    <meta name="Author" content="{{ trans('meta.author') }}" />
    <meta property="og:title" content="{{ trans('meta.title_map_event') }}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:image" content="{{ url('assets/img/home/enoturismo-en-mallorca.webp') }}"/>
    <meta property="og:description" content="{{ trans('meta.desc_map_event') }}"/>
    @parent
@stop

@section ('body')
<div class="shop-product">
    <div class="container">
        <ul class="breadcrumb-v5">
            <li><a href="{{ route('/') }}"><i class="fa fa-home"></i></a></li>
            <li class="active">
                {{ trans('events.rutas_de_vino_mallorca') }}
            </li>
        </ul>
    </div>
    <div class="container content profile">
        <div class="row">
            <div class="col-md-9">
                <div class="profile-body">
                    <div class="profile-bio">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="map" class="min-height-100vh"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if($index_events->count() > 0)
                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-heading overflow-h">
                            <h2 class="panel-title heading-sm pull-left">
                                <i class="fa fa-calendar-plus-o"></i> {{ trans('inicio.proximos_eventos') }}
                            </h2>
                        </div>
                        <div class="panel-body">
                            @include('events._timeline', ['fullheight' => true, 'show_link_map' => false])
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
@stop

@section ('assets_end')
    <link rel="stylesheet" href="{{ asset('assets/css/pages/profile.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pages/timeline2.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/plugins/scrollbar/css/jquery.mCustomScrollbar.css') }}">
    <script type="text/javascript" src="{{ asset('assets/plugins/scrollbar/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/timeline.js') }}"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBw_spE5_xC8pjOBJVWs5LHLBKn0EC-cfQ"></script>
    <script src="{{ asset('assets/plugins/gmap/gmap.js') }}"></script>

    <script type="text/javascript">
        var map;
        var marker;
        var icon = '{{ url('assets/img/icons/wine-glass-icon-48.png') }}';
        var icon_hover = '{{ url('assets/img/icons/wine-glass-icon-48-hover.png') }}';

        $(document).ready(function() {
            if($('#map').length){
                var contentString = null;
                map = new GMaps({
                    div: '#map',
                    scrollwheel: false,
                    lat: 39.621134,
                    lng: 2.959170,
                    zoom: 10
                });
                @foreach($index_events as $event)
                    @include('events._marker_map', ['query' => $event])
                @endforeach
                @foreach($index_plans as $plan)
                    @include('events._marker_map', ['query' => $plan])
                @endforeach
            }
        });
        $(document).on('click', '.pan-to-marker', function(e) {
            e.preventDefault();

            var lat, lng;

            var $index = $(this).data('marker-index');
            var $lat = $(this).data('marker-lat');
            var $lng = $(this).data('marker-lng');

            if ($index != undefined) {
                // using indices
                var position = map.markers[$index].getPosition();
                for(var x = 0; x < map.markers.length; x++){
                    map.markers[x].setIcon(icon);
                }
                map.markers[$index].setIcon(icon_hover);
                lat = position.lat();
                lng = position.lng();
            } else {
                // using coordinates
                lat = $lat;
                lng = $lng;
            }

            map.setCenter(lat, lng);
        });

    </script>
@stop