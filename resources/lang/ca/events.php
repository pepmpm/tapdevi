<?php

$idioma = array();

$idioma['ver_en_mapa'] = 'Veure en es mapa';
$idioma['agenda_de_eventos'] = "Agenda d'events";
$idioma['rutas_de_vino_mallorca'] = 'Rutes del vi';
$idioma['mensaje_evento_pasado'] = 'Aquest esdeveniment ja es va celebrar. Si busques activitats relacionades amb vi et recomanem els plans següents: ';

return $idioma;